/* Applies some changes to a FIST file generated from CASA, so that it can go into cuwarp. Will have to chenge if dada2ms changes
  because that's where the original data comes from.
  Does not use CFITSIO.
*/
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
  

int main(int argc, char *argv[]) {
  struct stat statbuf;
  char *file_contents, *aips_an;
  int f, i, new_size, start, end, j;


  if ( argc != 3 ) {
    fprintf(stderr,"Expecting input and output file names\n");
    return 1;
  }

  stat(argv[1], &statbuf);
  file_contents = (char*) malloc(statbuf.st_size);
  if ( (f=open(argv[1],O_RDONLY)) == -1 ) {
    fprintf(stderr,"Failed to open %s\n",argv[1]);
    return 1;
  }
  read(f, file_contents, statbuf.st_size);
  close(f);

  // Separate AIPS AN
  for (i=0; i<statbuf.st_size; ++i) { 
    if ( file_contents[i] == 'X' && file_contents[i+1] == 'T' && strncmp(file_contents+i,"XTENSION",8) == 0 ) {
      start = i; 
      // find the end by finding the next table or EOF
      end = statbuf.st_size;
      for (j=start+1; j<statbuf.st_size; ++j) {
        if ( file_contents[j] == 'X' && file_contents[j+1] == 'T' && strncmp(file_contents+j,"XTENSION",8) == 0 ) 
          end = j;
        }
      
      aips_an = (char*)malloc(end-start); 

      // Save the table
      bcopy(file_contents+start, aips_an, end-start);     // don't use strcpy because of binary chars

      if ( strstr(aips_an,"EXTNAME = 'AIPS AN '") != NULL ) break;
      
    }
  }

  // Get rid of the XTENSION tables but keep AIPS AN, so tack it on the end of the first table
  for (i=0; i<statbuf.st_size; ++i)
    if ( file_contents[i] == 'X' && file_contents[i+1] == 'T' && strncmp(file_contents+i,"XTENSION",8) == 0 ) {
      bcopy(aips_an,file_contents+i,end-start);
      new_size = i+end-start;
      break;
    }

  i = 0; 
  while ( strncmp(file_contents+i,"HISTORY",7) != 0 ) i += 80;
  i += 80;
  strncpy(file_contents+i,"HISTORY Modified by H.G for cuwarp",34);

  // Add RA/DEC
  i = 0; 
  while ( strncmp(file_contents+i,"ORIGIN",6) != 0 ) i += 80;
  i += 80;
  strncpy(file_contents+i,"OBSRA   =         289.58508411",30);                                                  
  strncpy(file_contents+i+80,"OBSDEC  =             37.23978",30);     
  strncpy(file_contents+i+2*80,"END",3);     
    
    
  if ( (f=open(argv[2],O_WRONLY|O_TRUNC|O_CREAT)) == -1 ) {
    fprintf(stderr,"Failed to open %s\n",argv[2]);
    return 1;
  }
  write(f, file_contents, new_size);
  close(f);	
	
  return 0;
}


