% data reader for LEDA prototype, capturing F-engine data

function [data_perm_rs] = f_eng_v5(filenames,numch,numcheck)

numf = 64;
ant_per_f = 8;
timePerChunk=2;

data_cat=[];
for i=1:length(filenames)
    f=filenames{i};
    f_dir=dir(f);
    
    fid = fopen(f,'r');
    header = fread(fid,4096,'char');
    header_txt = char(header);
    header_string = sprintf(header_txt);
    data_all_asDouble=fread(fid,(f_dir.bytes),'double');
    data_cat=[data_cat;data_all_asDouble];
    fclose(fid);
end
nFrames=numel(data_cat)/(numch*512*2/16);

if ~exist('numcheck','var')
    numcheck=floor(nFrames);
else
    numcheck=floor(min(numcheck,nFrames));
end
numcheck=timePerChunk*floor(numcheck/timePerChunk);
data_cat=data_cat(1:(numch*512/8*numcheck));
if(numel(data_cat)>=71992320)
    numelRun=floor(numel(data_cat)/4);
    assert(floor(numel(data_cat)/4)==(numel(data_cat)/4));
    i1=numelRun*0+1;ii=(i1:(i1+numelRun-1));d1=dada_to_matlab(data_cat(ii));
    i1=numelRun*1+1;ii=(i1:(i1+numelRun-1));d2=dada_to_matlab(data_cat(ii));
    i1=numelRun*2+1;ii=(i1:(i1+numelRun-1));d3=dada_to_matlab(data_cat(ii));
    i1=numelRun*3+1;ii=(i1:(i1+numelRun-1));d4=dada_to_matlab(data_cat(ii));
    
    data_all_c=[d1 d2 d3 d4];
%     data_all_c=[dada_to_matlab(data_cat(1:numelRun));dada_to_matlab(data_cat(numelRun+1:end))];
else
    data_all_c=dada_to_matlab(data_cat);
end

%dimension order (before):
%real/imag
%feng-antenna
%channel
%time (even/odd)
%feng
%time (full)

%dimension order (after):
%feng-antenna
%feng
%channel
%time (even/odd)
%time (full)
%real/imag

data_rs=reshape(data_all_c,2,ant_per_f,numch,timePerChunk,numf,numcheck/timePerChunk);
data_perm=permute(data_rs,[1,2,5,3,4,6]);

data_perm_rs=reshape(data_perm,2,numf*ant_per_f,numch,numcheck);



% This is C code, being the dada_to_matlab routine:

//////////////////////////////////////////////////////////////////////////
// efficiently convert double-packed MATLAB data into unpacked bit4 values
// this is a memory hog!  Will multiply amount of data by 16....
//////////////////////////////////////////////////////////////////////////

#include "mex.h"
#include "matrix.h"
//#include "opencvgpumex.hpp"
//#include "gpu/mxGPUArray.h"

// On some platforms, the following include is needed for "placement new".
// For more information see: http://en.wikipedia.org/wiki/Placement_syntax
#include <memory>
inline char doubleSliceTo4(double in,char index);
inline char fourBitU_toS(char in);


//////////////////////////////////////////////////////////////////////////////
// The main MEX function entry point
//////////////////////////////////////////////////////////////////////////////
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  //check inputs
  char *pointer;
  double *data;
  
  mexPrintf("check1\n ");
  if (nrhs !=1)
    mexErrMsgTxt("Incorrect input count. accepts only a single array");
  if (mxGetClassID(prhs[0])!=mxDOUBLE_CLASS)
		   mexErrMsgTxt("Incorrect input type. accepts only an array of doubles");
  mexPrintf("check2\n ");

mwSize nels=mxGetNumberOfElements(prhs[0]);


 plhs[0] = mxCreateNumericMatrix(16*nels,1, mxINT8_CLASS, mxREAL);
 pointer = (char *) mxGetData(plhs[0]);
 data = mxGetPr(prhs[0]);
 mexPrintf("check3\n ");

    /* Copy data into the mxArray */
    for ( int index = 0; index < nels; index++ )
      for (int i=0;i<16;i++)
	pointer[index*16+i] = fourBitU_toS(doubleSliceTo4(data[index],i));
	//pointer[index*16+i] = (doubleSliceTo4(data[index],i));
    

}

inline char doubleSliceTo4(double in,char index) {
  union {
        double doubleValue;
        char   asChars[sizeof(double)];
    };
  doubleValue=in;

  char byteTemp=asChars[index/2];
  
  if (!(index%2)) return byteTemp & 15;
  return (byteTemp & 240)>> 4;
  

}
inline char fourBitU_toS(char in) {
  if (in<=7) return in;
  return in-16;
}

