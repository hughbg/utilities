/* A program to run a command line in the background, without connection to its parent processes, I/O, or terminal.
   Lighter weight than screen. I use this to start a long batch job on a machine, then logout. 
   I usually send output to a log file. */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>


#define TRUE 1
#define FALSE 0

extern void exit(int);
extern char *getenv(const char *name);

void usage() {
  fprintf(stderr,"Usage: asdaemon [ -test ] command\n");
  exit(1);
}

char *name(char *s) {

  if ( index(s,'/') == NULL ) return s;

  return rindex(s,'/')+1;
}

int main(int argc, char *argv[]) {
  int i, devnull, testing=FALSE, dsize;
  char **newargv=NULL;
 

  if ( getenv("SHELL") == NULL ) {
    fprintf(stderr,"Unable to determine shell.\n");
    exit(0);
  }

  if ( argc == 2 && strcmp(argv[1],"-test") == 0 ) usage();
    

  if ( argc > 2 && strcmp(argv[1],"-test") == 0 ) {
    testing = TRUE;
    newargv = (char**)malloc(sizeof(char*)*(argc-1));
    for (i=2; i<argc; ++i) newargv[i-2] = argv[i];
    newargv[i-2] = NULL;
  }

  if ( newargv == NULL ) { 
    newargv = (char**)malloc(sizeof(char*)*(argc));
    for (i=1; i<argc; ++i) newargv[i-1] = argv[i];
    newargv[i-1] = NULL;
  }


  if ( !testing ) {
    if ( fork() == 0 ) setsid();
    else exit(0);    /* parent exits */

    /* Reset descriptors in preparation for exec. All i/o goes to dev/null */

    if ( (devnull = open("/dev/null",O_RDWR|O_CREAT|O_TRUNC,644)) == -1 ) { perror("open"); exit(1); }
    if ( dup2(devnull,0) == -1 ) exit(1);  
    if ( dup2(devnull,1) == -1 ) exit(1);
    if ( dup2(devnull,2) == -1 ) exit(1);

    /* Close any other descriptors */
    dsize = getdtablesize();
    for (i=3; i<dsize; i++) close(i);
  }
  

  if ( execvp(newargv[0],newargv) == -1 ) { perror("exec"); exit(1); } 

  return 0;
}

