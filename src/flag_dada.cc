/*
 * Test file for the aoflagger library. Runs flagging on a DADA file and can print the number of flags or list the data.
 */

#include <iostream>
#include <cstdlib>
#include "aoflagger.h"
#include "globals.h"
#include "dada_reader.h"

using namespace aoflagger;

static const int NUM_TIME_SCANS = 10;	// for test file
static const int NUM_CHANNELS = 109;
static const int NUM_POLARIZATIONS = 4;    
 
// Put a visibility value in the aoflagger image buffers. Two buffers, one for real/imag
void set_aoflagger_val(ImageSet& image_set, int scan, int chan, int pol, complex<float>& val) {
  // http://aoflagger.sourceforge.net/doc/api/classaoflagger_1_1ImageSet.html#aebc60bed59a75644cb07e4f70925385a
  *(image_set.ImageBuffer(2*pol) +   scan + chan * image_set.HorizontalStride()) = val.real();
  *(image_set.ImageBuffer(2*pol+1) + scan + chan * image_set.HorizontalStride()) = val.imag();
}

// Get a value from the aoflagger image buffers
complex<float> get_aoflagger_val(ImageSet& image_set, int scan, int chan, int pol) {
  complex<float> val;
  // http://aoflagger.sourceforge.net/doc/api/classaoflagger_1_1ImageSet.html#aebc60bed59a75644cb07e4f70925385a
  val.real() = *(image_set.ImageBuffer(2*pol) +   scan + chan * image_set.HorizontalStride());
  val.imag() = *(image_set.ImageBuffer(2*pol+1) + scan + chan * image_set.HorizontalStride());
  return val;
}

// Run flagging on a baseline. The only inputs are the visibility data arrayed by time, channel, polarization.
int flag_dada_baseline(int bl_index) {
  complex<float> val;

  AOFlagger flagger;
  ImageSet dada_data = flagger.MakeImageSet(NUM_TIME_SCANS, NUM_CHANNELS, NUM_POLARIZATIONS*2);
  DadaReader dadareader("2015-02-03-18:17:02_0000000000000000.000000.dada");
	
  // Add data to the aoflagger buffers. Get the data from DADA file.
  for (int scan=0; scan<NUM_TIME_SCANS; ++scan) {	// correlator dump index in the file 0..9
    dadareader.next_time_scan();
    for (int chan=0; chan<NUM_CHANNELS; ++chan) {
      for (int pol=0; pol<NUM_POLARIZATIONS; ++pol) {
        val = dadareader.get_value(chan, bl_index, pol/2, pol%2);
        set_aoflagger_val(dada_data, scan, chan, pol, val);
      }
    }
  }
    
  // Define strategy and run flagging
  Strategy strategy = flagger.MakeStrategy();
  FlagMask flagged = flagger.Run(strategy, dada_data);
  
  float values[NUM_TIME_SCANS*NUM_POLARIZATIONS][NUM_CHANNELS];   // for re-ordering
  
  // Find out the result in the flags array
  int num_flagged = 0;
  for (int scan=0; scan<NUM_TIME_SCANS; ++scan) {
    for (int chan=0; chan<NUM_CHANNELS; ++chan) {
      if ( *(flagged.Buffer() + scan + chan * flagged.HorizontalStride()) ) ++num_flagged;
      
      for (int pol=0; pol<NUM_POLARIZATIONS; ++pol) 
	if ( *(flagged.Buffer() + scan + chan * flagged.HorizontalStride()) ) values[scan*NUM_POLARIZATIONS+pol][chan] = 0; 
	else values[scan*NUM_POLARIZATIONS+pol][chan] = abs(get_aoflagger_val(dada_data, scan, chan, pol));
    }

  }
      
  // Display the data. 0 values were flagged..  
  //for (int i=0; i<NUM_TIME_SCANS*NUM_POLARIZATIONS; ++i)
  //  for (int j=0; j<NUM_CHANNELS; ++j) cout << i << " " << j << " " << values[i][j] << endl;
    
  return num_flagged;


}

int main() {
  /*for (int i=0; i<NUM_STATIONS; ++i)
    for (int j=0; j<i+1; ++j)
      cout << i << " " << j << " " << i*(i+1)/2+j << " " << flag_dada_baseline(i*(i+1)/2+j) << endl;*/
  cout << "Flagged " << flag_dada_baseline(539) << endl;
}