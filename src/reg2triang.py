import numpy as np
import sys

# Code taken from leda_dbtransient program
class Mod(object):        

	def __init__(self, nstation, tile_nrow=2, tile_ncol=2):
		self.npol = 2
		self.nstation = 256
		self.nfrequency = 109
                out_nbaseline  = nstation*(nstation+1)/2
                self._tri2rtt = [ 0 for i in range(out_nbaseline) ]
                nstation_tiles = (nstation-1)/tile_ncol+1
                ntile          = nstation_tiles*(nstation_tiles+1)/2
                self._in_nbaseline      = tile_nrow*tile_ncol*ntile
                for i in range(nstation):
                        for j in range(i+1):
                                out_b    = self.ants2baseline(i,j)
                                row_tile = i / tile_nrow
                                col_tile = j / tile_ncol
                                b_tile   = col_tile + row_tile*(row_tile+1)/2
                                tile_row = i % tile_nrow
                                tile_col = j % tile_ncol
                                in_b     = b_tile + ntile*(tile_col + tile_ncol*tile_row)
                                self._tri2rtt[out_b] = in_b
                        
                
        
        def unpack_and_add(self, ntime, nchan, src, dst):
		if len(src) != 115187712/4:		# floats
	      	  print "REG_TILE chunk is of wrong size"
	          sys.exit(0)
		
                out_nbaseline = len(self._tri2rtt)
                for t in range(ntime):
                        for c in range(nchan):
                                for out_b in range(out_nbaseline):
                                        in_b = self._tri2rtt[out_b]
					#print 0+8*(out_b+out_nbaseline*(c+nchan*t)), in_b+self._in_nbaseline*(c+nchan*(0+2*t))
                                        dst[0+8*(out_b+out_nbaseline*(c+nchan*t))] += src[0+4*(in_b+self._in_nbaseline*(c+nchan*(0+2*t)))]
                                        dst[1+8*(out_b+out_nbaseline*(c+nchan*t))] += src[0+4*(in_b+self._in_nbaseline*(c+nchan*(1+2*t)))]
                                        dst[2+8*(out_b+out_nbaseline*(c+nchan*t))] += src[1+4*(in_b+self._in_nbaseline*(c+nchan*(0+2*t)))]
                                        dst[3+8*(out_b+out_nbaseline*(c+nchan*t))] += src[1+4*(in_b+self._in_nbaseline*(c+nchan*(1+2*t)))]
                                        dst[4+8*(out_b+out_nbaseline*(c+nchan*t))] += src[2+4*(in_b+self._in_nbaseline*(c+nchan*(0+2*t)))]
                                        dst[5+8*(out_b+out_nbaseline*(c+nchan*t))] += src[2+4*(in_b+self._in_nbaseline*(c+nchan*(1+2*t)))]
                                        dst[6+8*(out_b+out_nbaseline*(c+nchan*t))] += src[3+4*(in_b+self._in_nbaseline*(c+nchan*(0+2*t)))]
                                        dst[7+8*(out_b+out_nbaseline*(c+nchan*t))] += src[3+4*(in_b+self._in_nbaseline*(c+nchan*(1+2*t)))]

        

        def ants2baseline(self, i, j):
                return i*(i+1)/2 + j

def fix_data(data):
  for st1 in range(256):
    for st2 in range(st1+1):
      bl_index = mod.ants2baseline(st1, st2)
      for chan in range(109):
        for p1 in range(2):
          for p2 in range(2):
            if st2%2 == 1 and (st2-st1+1)%2 == 0:
             # print st1, st2, "->", st1+1, st2-1
              tmp_vis = data[chan, bl_index, p1, p2].copy()
              other_bl_index = mod.ants2baseline(st1+1, st2-1)
              data[chan, bl_index, p1, p2] = data[chan, other_bl_index, p1, p2].copy()
              data[chan, other_bl_index, p1, p2] = tmp_vis.copy()

REG_TILE_FILE_SIZE = 115191808	# 1 dump plus header,in bytes
HEADER_SIZE = 4096
SIZEOF_FLOAT = 4

mod = Mod(256)

# Load REG_TILE data
#f = open("/home/leda/astm-dada-file-example/2016-03-19-01:44:01_0000000000000000.000000.dada", "rb")
f = open("x.dada", "rb")
reg_tile = np.fromfile(f, dtype=np.float32, count=REG_TILE_FILE_SIZE/SIZEOF_FLOAT)
f.close()

triangular = np.zeros(32896*109*2*2*2) 

# Run the transformation
mod.unpack_and_add(1, 109, reg_tile[HEADER_SIZE/SIZEOF_FLOAT:], triangular)

triangular = triangular.reshape((109,32896,2,2,2))
print "Fixing ..."
#fix_data(triangular)
print "Done"

# Check if baselines have zeros
"""
for st1 in range(256):
  for st2 in range(st1+1):
    bl_index = st1*(st1+1)//2+st2
    all_zero = True
    for j in range(109):
      for k in range(2):
        for l in range(2):
	  for m in range(2):
            if abs(triangular[j, bl_index, k, l, m]) > 0: all_zero = False
    if all_zero: print st1, st2

"""

"""st1 = 112
st2 = 110
bl_index = st1*(st1+1)//2+st2
print bl_index
chan = 50
for k in range(2):
  for l in range(2):
    print k, l, triangular[chan, bl_index, k, l, 0], triangular[chan, bl_index, k, l, 1]
"""

def close(x, y):
  return abs(x-y) < 0.1

def find1(v1, hint):
  j = hint[0]
  bl_index = mod.ants2baseline(hint[1], hint[2])
  k = hint[3]
  l = hint[4]
  if close(triangular[j, bl_index, k, l, 0], v1):
    return ( hint[1], hint[2], k, l, j, triangular[j, bl_index, k, l, 0], triangular[j, bl_index, k, l, 1] )
  

  num_matches = 0
  result = None
  for st1 in range(256):
    for st2 in range(st1+1):
      bl_index = mod.ants2baseline(st1, st2)
      for j in range(109):
        for k in range(2):
          for l in range(2):
            if close(triangular[j, bl_index, k, l, 0], v1) or close(triangular[j, bl_index, k, l, 1], v1):
	      result = ( st1, st2, k, l, j, triangular[j, bl_index, k, l, 0], triangular[j, bl_index, k, l, 1] ) 
  	      num_matches += 1
  if num_matches > 1: print "Many matches!!"
  return result

def find2(v1, v2, hint):
  j = hint[0]
  bl_index = mod.ants2baseline(hint[1], hint[2])
  k = hint[3]
  l = hint[4]
  if close(triangular[j, bl_index, k, l, 0], v1) and close(triangular[j, bl_index, k, l, 1], v2):
    return ( hint[1] , hint[2], k, l, j, triangular[j, bl_index, k, l, 0], triangular[j, bl_index, k, l, 1] )

  num_matches = 0
  result = None
  for st1 in range(256):
    for st2 in range(st1+1):
      bl_index = mod.ants2baseline(st1, st2)
      for j in range(109):
        for k in range(2):
          for l in range(2):
            if close(triangular[j, bl_index, k, l, 0], v1) and close(triangular[j, bl_index, k, l, 1], v2):
              result = ( st1, st2, k, l, j, triangular[j, bl_index, k, l, 0], triangular[j, bl_index, k, l, 1] ) 
              num_matches += 1
  if num_matches > 1: print "Many matches!!"
  return result

print "Channel Stand2 Channel Pol1 Pol2 -> Channel Stand1 Stand2 Pol1 Pol2"
print "---"
values = np.loadtxt("x.dat")
for i in range(len(values)):
  # Channel Stand1 Stand2 Pol1 Pol2
  if values[i, 1] == values[i, 2]: print "Autocorrelation"
  print int(values[i, 0]), int(values[i, 1]), int(values[i, 2]), int(values[i, 3]), int(values[i, 4]), " ->",
  if values[i, 1] == values[i, 2]: 
    result = find1(values[i, 5], ( int(values[i, 0]), int(values[i, 2]), int(values[i, 1]), int(values[i, 4]), int(values[i, 3]) ))
    if result: 
      print result[4], result[0], result[1], result[2], result[3]
      print "LOC1 REG_TILE", complex(values[i, 4], values[i, 5]), "TRIANGULAR", complex(triangular[50, mod.ants2baseline(values[i, 1], values[i, 2]), 0, 0, 0], triangular[50, mod.ants2baseline(values[i, 1], values[i, 2]), 0, 0, 1])
      print "LOC2 REG_TILE ? TRIANGULAR", complex(triangular[result[4],  mod.ants2baseline(result[1], result[0]), 0, 0, 0], triangular[result[4],  mod.ants2baseline(result[1], result[0]), 0, 0, 0])
    else: 
      print "Not found"
      print "What is at that location in TRIANGULAR:", complex(triangular[50, mod.ants2baseline(values[i, 1], values[i, 2]), 0, 0, 0], triangular[50, mod.ants2baseline(values[i, 1], values[i, 2]), 0, 0, 1]),
      print "Searching for", complex(values[i, 4], values[i, 5])
  else:
    result = find2(values[i, 5], values[i, 6], ( int(values[i, 0]), int(values[i, 2]), int(values[i, 1]), int(values[i, 4]), int(values[i, 3]) ))
    if result: print result[4], result[0], result[1], result[2], result[3],
    else: 
      print "Not found"
      print "What is at that location in TRIANGULAR:", complex(triangular[50, mod.ants2baseline(values[i, 1], values[i, 2]), 0, 0, 0], triangular[50, mod.ants2baseline(values[i, 1], values[i, 2]), 0, 0, 1]),
      print "Searching for", complex(values[i, 4], values[i, 5])


  if result:	# Check for a match. The baselines and the pols have to be flipped for the test, but they are not flipped for printing
    if result[0] == values[i][2] and result[1] == values[i][1] and result[4] == values[i, 0] and result[2] == values[i, 4] and result[3] == values[i, 3]: print "MATCH"
    else: print "MISMATCH",

  print "---"
