// Simulate the gains in the quantization
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <random>

using namespace std;

const double scale_fac =  0.63025;
const int shift_fac = 27;

const int BIT_BOTTOM = 32;

extern long int two_up(int);

// ------------ ArrayFloat

class ArrayFloat {
public:
  double *data;
  int len;
  
  ArrayFloat() { len = 0; data = NULL; }
  ArrayFloat(const char *fname) { load_gains(fname); }
  ArrayFloat(int l) { data = new double [l]; len = l; for (int i=0; i<len; ++i) data[i] = 0; }
  ~ArrayFloat() { if ( data != NULL ) delete[] data; }
  void savetxt(const char *s);
  void flat_gains(int length);
  void load_gains(const char* filename);
  double& operator[](int i) { return data[i]; }
  void bp_setup(const ArrayFloat&);
  void flip();
  void flat();
  void scale(double scale);
  double mean();
};

void ArrayFloat::load_gains(const char *fname) {
   std::fstream myfile(fname, std::ios_base::in);
   if ( !myfile ) {
	   cerr << "Failed to open " << fname << endl;
	   exit(1);
   }
   float a;
   len = 0;
   while ( myfile >> a ) len += 1;
   
   myfile.close();
  
   data = new double[len];
   
   std::fstream myfile_again(fname, std::ios_base::in);

   int i = 0;
   while ( myfile_again >> data[i] ) ++i;
   
   myfile_again.close();
   
   double odata = (two_up(shift_fac)-1)*scale_fac;
   for (int i=0; i<len; ++i) data[i] *= odata*10.2;
   for (int i=0; i<len; ++i) data[i] /= 1e4;
}

void ArrayFloat::flat_gains(int length) {
  len = length;
  
  data = new double[len];
  double odata = (two_up(shift_fac)-1)*scale_fac;
  for (int i=0; i<len; ++i) data[i] = odata;
}

void ArrayFloat::savetxt(const char *fname) {
  ofstream datafile(fname);
  
  for (int i=0; i<len; ++i) datafile << data[i] << endl;
  datafile.close();
}


void ArrayFloat::bp_setup(const ArrayFloat& gains) {
  len = gains.len;
  
  data = new double[len];
  for (int i=0; i<gains.len; ++i) { 
  	data[i] = 1e9/gains.data[i];
  }
}

void ArrayFloat::flip() {
  double max=-1e39, min=1e39;
  for(int i=0; i<len; ++i) { 
	  if ( data[i] > max ) max = data[i]; 
	  if ( data[i] < min ) min = data[i];
  }
  for(int i=0; i<len; ++i) data[i] = max-data[i]+min;
} 

void ArrayFloat::flat() {
  double mean = 0;
  for(int i=0; i<len; ++i) mean += data[i];
  mean /= len;
  for(int i=0; i<len; ++i) data[i] = mean;
}
  

void ArrayFloat::scale(double scale) {
	for (int i=0; i<len; ++i) data[i] *= scale;
}

double ArrayFloat::mean() {
	double mean = 0;
	for(int i=0; i<len; ++i) mean += data[i];
	mean /= len;
	return mean;
}

// ----------------------ArrayInt64
class ArrayInt64 {
public:
  long int *data;
  int len;
  
  ArrayInt64() { len = 0; data = NULL; }
  ArrayInt64(int l) { data = new long int[l]; len = l; for (int i=0; i<len; ++i) data[i] = 0; }
  void savetxt(const char *s);
  long int& operator[](int i) { return data[i]; }
};

void ArrayInt64::savetxt(const char *fname) {
  ofstream datafile(fname);
  
  for (int i=0; i<len; ++i) datafile << data[i] << endl;
  datafile.close();
}




// --------------------- LongInt

class LongInt {
public:
  long int val;
  
  LongInt() { val = 0; }
  LongInt(double v) { val = (long int)round(v); }
  LongInt(long int v) { val = v; }
  string binary_str();
  void round_even();
  void trunc();
  LongInt operator*(const LongInt&);
  LongInt& operator=(const LongInt& li) { val = li.val; return *this; }
  void right_shift(int num) { val >>= num; }
};

string LongInt::binary_str() {
  char s[65];
 
  long int v = val;
  long int v1;
  for (int i=0; i<64; ++i) {
    v1 = v & 0b0000000000000000000000000000000000000000000000000000000000000001;
    if ( v1 == 1 ) s[64-i-1] = '1'; else s[64-i-1] = '0';
    v >>= 1;
  }
  s[64] = '\0';
  return string(s);
}

void LongInt::round_even() {
  // If the number is -ve then work on the +ve version.
  bool neg = false;
   
  if ( val < 0 ) { 
    val = -val;
    neg = true;
  }

  // For rounding select the remainder which is bit 31 down, and put it after the decimal point. 
  // This simple bit op would be messy if the number is -ve due to complements etc.
  long int remainder = val & 0b0000000000000000000000000000000011111111111111111111111111111111;     // Bottom BIT_BOTTOM bits, i.e. 31:0

  // Get the number that is in the top 32 bits of x
  long int number = val >> 32;
  
  // Doing rounding.
  if ( remainder > 0b0000000000000000000000000000000010000000000000000000000000000000 ||
	  ( remainder == 0b0000000000000000000000000000000010000000000000000000000000000000 && ((number+1)%2 == 0) ) )
      number += 1; 

  val = number << 32;
  
  if ( neg ) val = -val;

}

void LongInt::trunc() {
  // If the number is -ve then work on the +ve version.
  bool neg = false;
   
  if ( val < 0 ) { 
    val = -val;
    neg = true;
  }

  // Get the number that is in the top 32 bits of x
  long int number = val >> 32;
  
  val = number << 32;
  
  if ( neg ) val = -val;

}

LongInt LongInt::operator*(const LongInt& li) {
  return LongInt(val*li.val);
}


// ------------ Utility functions
long int two_up(int power) {
  long int x = 1;
  for (int i=0; i<power; ++i) x *= 2;
  return x;
}


bool find_option(int argc, char *argv[], const char *opt) {
  string opt_s = opt;
  for (int i=0; i<argc; ++i) {
    string argv_s = argv[i];
    if ( opt_s == argv_s ) return true;
  }
  return false;
}
  
  
int main(int argc, char *argv[]) {

  for (int i=1; i<argc; ++i) cout << argv[i] << " ";
  cout << endl;

  double user_scale = atof(argv[1]);
  int num_samples = atoi(argv[2]);
  
  ArrayFloat gains, bandpass;
  
  default_random_engine generator;
  normal_distribution<double> random(0.0, 1.0);
  
  if ( find_option(argc, argv, "gains:flat") ) {
	ArrayFloat solved("/home/hgarsden/tmp/gains_solved.dat");
    gains.flat_gains(solved.len);
    bandpass.bp_setup(solved);
    
  } else if ( find_option(argc, argv, "gains:lines") ) {
    gains.load_gains("/home/hgarsden/tmp/gains_solved_lines.dat");
    bandpass.bp_setup(gains);
    
  } else if ( find_option(argc, argv, "gains:curved") ) {
    gains.load_gains("/home/hgarsden/tmp/gains_solved.dat");
    bandpass.bp_setup(gains);
  } else {
    cerr << "No gains specification\n";
    return 1;
  }
  
  if ( find_option(argc, argv, "bandpass_flip:on") ) 
    bandpass.flip();
  if ( find_option(argc, argv, "bandpass_flat:on") ) bandpass.flat();

  
  int saturation_positive=0;
  int saturation_negative=0;
  int saturation_check=0;

  ArrayInt64 signal_equiv(bandpass.len);			// Signal multiplied by gain
  ArrayFloat signal_equivf(bandpass.len);  

  double max_raw = -1e39;
            
  // Find out what the values are like, so can scale
  for (int j=0; j<10; ++j) {
    for (int i=0; i<bandpass.len; ++i) { 
    
      double val_raw = bandpass[i]*random(generator)*gains[i];
      if ( val_raw > max_raw ) max_raw = val_raw;
    }
  }
  double scale = two_up(35)/max_raw;
  cout << "Scale " <<  scale << endl; 
  
  bandpass.scale(scale);
  gains.savetxt("gains.dat");
  bandpass.savetxt("bandpass.dat"); 
  cout << "Bandpass mean: " << bandpass.mean() << endl;
  cout << "Gains mean: " << gains.mean() << endl;
  
  max_raw = -1e39;
  double min_raw = 1e39;
  double max_sat = max_raw;
  double min_sat = min_raw;
  double flipper = 0.3;		// flip this +ve/-ve as sudo random around 0
  bool do_flip = find_option(argc, argv, "random:flip");
  bool saturation_check_on = find_option(argc, argv, "saturation_check:on");
  bool rounding_on = find_option(argc, argv, "rounding:on");

  // The loop that does it

  for (int j=0; j<num_samples; ++j) {
            
    for (int i=0; i<gains.len; ++i) {
      double val_raw;
    	 
      if ( do_flip ) {
    	val_raw = bandpass[i]*flipper*user_scale;
    	flipper *= -1;
      } else
        val_raw = bandpass[i]*random(generator)*user_scale;		// Make sure is a high valued integer for resolution
      if ( val_raw > max_raw ) max_raw = val_raw;
      if ( val_raw < min_raw ) min_raw = val_raw;
    

      // Multiply by an unsigned 32b coefficient ("digital gain") producing a signed 50b product.
      LongInt val = LongInt(val_raw)*LongInt(gains[i]);
    
           
      //val = LongInt(0b0000000000000000000000000000010111010101011101000000010001110110);

      // Select bits 35:32 (of 49:0), rounding symmetrically using the value of bit 31.
      LongInt val_round = val;
      if ( rounding_on ) val_round.round_even();
      else val_round.trunc();     
      LongInt val_result = val_round; 
      val_result.right_shift(32);			// Select the number. Down shift preserves negative. Leave top bits for saturation check.
      //cout << i << " " << val_result.val << " " << LongInt(val_raw).val << " " << LongInt(gains[i]).val << " " << LongInt(val).binary_str() << " " << setprecision(15) << val_raw*gains[i]/two_up(32) << endl;
    
       
      LongInt val_sat = val_result;         // save it
      if ( val_sat.val > max_sat ) max_sat = val_sat.val;
      if ( val_sat.val < min_sat ) min_sat = val_sat.val;
    

      // Saturation: If bits 49:35 are not all the same, force the 4b result to 0b0111 (+7) if it is positive and to 0b1001 (-7) if it is negative.
      // If the result is now 0b1000 (-8), force it to 0b1001 (-7). 
      if ( saturation_check_on ) {
        ++saturation_check;
        if  ( val_result.val < -7 || val_result.val > 7 ) {
          //cout << "Saturation ";
          if ( val_result.val > 7 ) { 
            //cout << "positive\n";
            val_result.val = 7;
            ++saturation_positive;
          } else { 
            //cout << "negative\n";
            ++saturation_negative;
            val_result.val = -7;
          }
        }
      }
                
    
      /*cout << "Orig Val (int x int)  \n" << val.binary_str() << " (" << val.val << ")" << endl;
      string shadow;
      for (int si=63; si>=0; --si) 
       if ( si >= 32 && si <= 35  ) shadow += "*";
       else shadow += " ";            
      cout << shadow << endl;
      cout << "After rounding\n" << val_round.binary_str() << endl;
      cout << "Val after shift/after saturation remove\n" << val_sat.binary_str() << "\n" <<  val_result.binary_str() << endl;
      cout << "Res " << val_result.val << endl;
      */
      
      signal_equiv[i] += val_result.val*val_result.val;

	      // Float version
      double valf = (val_raw*gains[i])/two_up(32);
      signal_equivf[i] += valf*valf;

  
    }
    
    if ( j%10000 == 0 && j > 0 ) {
      cout << "Wrote " << j << endl;
      char name[64];
      sprintf(name, "signal_equiv_%d.dat", j);
      signal_equiv.savetxt(name);
      sprintf(name, "signal_equivf_%d.dat", j);
      signal_equivf.savetxt(name);

      cout << "Saturation positive " << saturation_positive << " Saturation negative " << saturation_negative << endl;
      cout << "Bandpass range after scaling " << min_raw << " " << max_raw << endl;
      cout << "Equalized data range " << min_sat << " " << max_sat << endl;
    }
  }
  
  char name[64];
  sprintf(name, "signal_equiv.dat");
  signal_equiv.savetxt(name);
  sprintf(name, "signal_equivf.dat");
  signal_equivf.savetxt(name);
  cout << "Saturation positive " << saturation_positive << " Saturation negative " << saturation_negative << " checked " << saturation_check << endl;
  cout << "Data range " << min_raw << " " <<  max_raw << endl;

}
