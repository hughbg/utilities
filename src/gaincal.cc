#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include "levmar-2.6/levmar.h"
#include "matrix.h"

using namespace std;

const int NUM_STANDS = 256;

JonesMatrix V[NUM_STANDS][NUM_STANDS];
JonesMatrix V_perturb[NUM_STANDS][NUM_STANDS];
JonesMatrix J[NUM_STANDS];
JonesMatrix J_perturb[NUM_STANDS];
JonesMatrix J_new[NUM_STANDS];
JonesMatrix P[NUM_STANDS][NUM_STANDS];

JonesMatrix *sub_J;

vector<string> split(string line) {
   vector<string> tokens;
   string token;

   for (int i=0; i<line.size(); ++i) if ( line[i] == '\t' ) line[i] = ' ';
   stringstream l(line);

   while ( getline(l, token, ' ') )
     if ( token.size() > 0 ) tokens.push_back(token);

   return tokens;
}

class Baselines {
public:
  int baselines[32896][2];
  bool stands_present[256];
  int num_baselines;

  Baselines(const char *fname);
  bool exists(int st1, int st2);
};

Baselines::Baselines(const char *fname) {
    int index=0;
    string line;
    vector<string> words;

    for (int i=0; i<32896; ++i) { baselines[i][0] = baselines[i][1] = -1; }
    for (int i=0; i<256; ++i) stands_present[i] = false;
    
    ifstream file1(fname);
    while( getline(file1, line) ) 
      if ( line.size() > 0 && line[0] != '#' ) {
        words = split(line);
        int st1 = atoi(words[0].c_str()); 
        int st2 = atoi(words[1].c_str()); 
        baselines[index][0] = st1; baselines[index][1] = st2;
        ++index;
    }
    file1.close(); 
    
    for (int i=0; i<32896; ++i) {
    	if ( baselines[i][0] != -1 && baselines[i][1] != -1 ) {
    		stands_present[baselines[i][0]] = true; stands_present[baselines[i][1]] = true; 
    	}
    }
    //for (int i=0; i<256; ++i) 
    //	if ( stands[i] ) cout << i << endl;
    
    num_baselines = index; 
}

bool Baselines::exists(int st1, int st2) {
  for (int i=0; i<32896; ++i) 
    if( ( baselines[i][0] == st1 && baselines[i][1] == st2 ) || ( baselines[i][0] == st2 && baselines[i][1] == st1 ) ) return true;
  return false;
}
  

class StandMapping {
public:
  int mapping[NUM_STANDS];

  StandMapping() { for (int i=0; i<NUM_STANDS; ++i) mapping[i] = -1; }

  void add(int st);
  int map(int st);
  int num_mapped();
  void clear() { for (int i=0; i<NUM_STANDS; ++i) mapping[i] = -1; }
  void print() { for (int i=0; i<NUM_STANDS; ++i) if ( mapping[i] != -1 ) cout << i << " -> " << mapping[i] << endl; }
};

void StandMapping::add(int st) {
  int i;

  for (i=0; i<NUM_STANDS && mapping[i] != -1 && mapping[i] != st; ++i);    // have to be in order
  if ( i == NUM_STANDS ) return;	// reached capacity
  else if ( mapping[i] == st ) return; 	// already in
  else mapping[i] = st;
}

int StandMapping::map(int i) {
  return mapping[i];
}

int StandMapping::num_mapped() {
	int num = 0;
	for (int i=0; i<NUM_STANDS; ++i) 
		if ( mapping [i] != -1 ) ++num;
	return num;
}

StandMapping stands;

void perturb(JonesMatrix& j) {
  j[0][0] *= 1.0+((float)rand()/RAND_MAX-0.5)/2.5;
  j[0][1] *= 1.0+((float)rand()/RAND_MAX-0.5)/2.5;
  j[1][0] *= 1.0+((float)rand()/RAND_MAX-0.5)/2.5;
  j[1][1] *= 1.0+((float)rand()/RAND_MAX-0.5)/2.5;
}

float ratio(float x, float y) {
  if ( x == 0 && y == 0 ) return 0;
  else if ( x == 0 ) return fabs(y);
  else if ( y == 0 ) return fabs(x);
  else return fabs((x-y)/y);
}

float sqr(float x) { return x*x; }

complex<float> complex_rand() {
  float r = ((float)rand()/RAND_MAX-0.5)*10;
  float i = ((float)rand()/RAND_MAX-0.5)*10;

  return complex<float>(r, i);
}

double squared_norm(JonesMatrix& m) {   // Squared frobenius norm
  complex<float> a = m[0][0];
  complex<float> b = m[0][1];
  complex<float> c = m[1][0];
  complex<float> d = m[1][1];
  return abs(a*conj(a)+b*conj(b)+c*conj(c)+d*conj(d));  
}

float residual() {  // Between V and J.P.Jt
  double res = 0;
  int num_stands = stands.num_mapped(); 

  for (int j=0; j<num_stands; ++j)
    for (int k=j+1; k<num_stands; ++k) {    // Not using the whole thing
      int jj = stands.map(j);
      int kk = stands.map(k);
   	
      JonesMatrix Vresidual = V_perturb[jj][kk]-(sub_J[j]*P[jj][kk]*sub_J[k].conjugate_transpose());
      res += squared_norm(Vresidual);
    }

  return sqrt(res);
}

void save_residual(const char *fname) {
	int num_stands = stands.num_mapped(); 
	
	ofstream outputFile;
	outputFile.open(fname);
	

	for (int j=0; j<num_stands; ++j)
		for (int k=j+1; k<num_stands; ++k) {    // Not using the whole thing
			int jj = stands.map(j);
			int kk = stands.map(k);
	   	
			JonesMatrix Vresidual = V_perturb[jj][kk]-(sub_J[j]*P[jj][kk]*sub_J[k].conjugate_transpose());
			outputFile << jj << " " << kk << " " << squared_norm(Vresidual) << endl;
		}


	outputFile.close();
}

void residual_for_opt(double *r) {  // Between V and J.P.Jt
  int index=0;
  int num_stands = stands.num_mapped(); 
  for (int j=0; j<num_stands; ++j)
    for (int k=j+1; k<num_stands; ++k) {  
       int jj = stands.map(j);
       int kk = stands.map(k);
    
      if ( squared_norm(V_perturb[jj][kk]) == 0 || squared_norm(P[jj][kk]) == 0 ) r[index++] = 0;
      else {
        
        JonesMatrix Vresidual = V_perturb[jj][kk]-(sub_J[j]*P[jj][kk]*sub_J[k].conjugate_transpose());
        r[index++] = squared_norm(Vresidual);
      }
    }

}


void setup() {
  for (int j=0; j<NUM_STANDS; ++j) {
    J[j].set( complex_rand(), complex_rand(), complex_rand(), complex_rand() );
    for (int k=j+1; k<NUM_STANDS; ++k) {
      P[j][k].set( complex_rand(), complex_rand(), complex_rand(), complex_rand() );
      P[k][j] = P[j][k].conjugate_transpose();
    }
  }

  for (int j=0; j<NUM_STANDS; ++j) 
    for (int k=0; k<NUM_STANDS; ++k) {
      V[j][k] = J[j]*P[j][k]*J[k].conjugate_transpose();
    }

   
}

void method1() { 
  // Perturb J
  for (int j=0; j<NUM_STANDS; ++j) {
    J_perturb[j] = J[j];
    perturb(J_perturb[j]); 
  }

  // Calculate perturbed V
  for (int j=0; j<NUM_STANDS; ++j) 
    for (int k=0; k<NUM_STANDS; ++k) {
      V_perturb[j][k] = J_perturb[j]*P[j][k]*J_perturb[k].conjugate_transpose();
    }
  
  V[1][2].print();

  V_perturb[1][2].print();
  
  cout << "Residual " << residual() << endl;
  
  
  for (int attempts=0; attempts<500; ++attempts) {   // in range(36270):

    for (int j=0; j<NUM_STANDS; ++j) {
      JonesMatrix first_term;

      for (int k=0; k<NUM_STANDS; ++k) {	// problem with k=j+1 is that the top J is never computed. It always stays the same or 0
	if ( j != k ) {
          JonesMatrix multiplied = V_perturb[j][k]*J[k]*P[j][k].conjugate_transpose();
          first_term += multiplied;
	}
      }
       
      JonesMatrix second_term;
      for (int k=0; k<NUM_STANDS; ++k) {
	if ( j != k ) {
          JonesMatrix multiplied = P[j][k]*J[k].conjugate_transpose()*J[k]*P[j][k].conjugate_transpose();
          second_term += multiplied;
	}
      }

      J_new[j] = first_term*second_term.inverse();
      if ( !J_new[j].is_normal() ) { J_new[j] = J[j]; cerr << "Hit nan\n"; }    // nan

    }

    for (int j=0; j<NUM_STANDS; ++j) J[j] += (J_new[j]-J[j])*0.1;

  }
  
  cout << "Residual " << residual() << endl;
  
  JonesMatrix V_test = J[1]*P[1][2]*J[2].conjugate_transpose();
  V_test.print();
}

//double *p,         /* I/O: initial parameter estimates. On output contains the estimated solution */
//double *x,         /* I: measurement vector. NULL implies a zero vector */
//int m,             /* I: parameter vector dimension (i.e. #unknowns) */
//int n,             /* I: measurement vector dimension */
//void *adata)       /* I: pointer to possibly needed additional data, passed uninterpreted to func.
//                    * Set to NULL if not needed

// x is initially a zero vector, that's what we want to fit to. p is to be found. x
// passed to this function is the current fit of x to the desired x.
static void errorfunc(double *p, double *x, int m, int n, void *adata) {
  for (int j=0; j<stands.num_mapped(); ++j) {
    sub_J[j][0][0] = complex<float>(p[j*8+0], p[j*8+1]);
    sub_J[j][0][1] = complex<float>(p[j*8+2], p[j*8+3]);
    sub_J[j][1][0] = complex<float>(p[j*8+4], p[j*8+5]);
    sub_J[j][1][1] = complex<float>(p[j*8+6], p[j*8+7]);
  }
  residual_for_opt(x); //cout << residual()<<endl;
}

void method1_1() { 
  // Perturb J
  for (int j=0; j<NUM_STANDS; ++j) {
    J_perturb[j] = J[j];
    perturb(J_perturb[j]); 
  }
  
  
  // Calculate perturbed V
  for (int j=0; j<NUM_STANDS; ++j) 
    for (int k=0; k<NUM_STANDS; ++k) {
      V_perturb[j][k] = J_perturb[j]*P[j][k]*J_perturb[k].conjugate_transpose();
    }
  
  cout << "Residual " << residual() << endl;
  
  // Flatten 
  double *flat = new double[NUM_STANDS*8];
  for (int j=0; j<NUM_STANDS; ++j) {
    flat[j*8+0] = J[j][0][0].real(); flat[j*8+1] = J[j][0][0].imag();
    flat[j*8+2] = J[j][0][1].real(); flat[j*8+3] = J[j][0][1].imag();
    flat[j*8+4] = J[j][1][0].real(); flat[j*8+5] = J[j][1][0].imag();
    flat[j*8+6] = J[j][1][1].real(); flat[j*8+7] = J[j][1][1].imag();
   }
  int m = NUM_STANDS*8;
  int n = 32640;

  double *first = new double[n];
  residual_for_opt(first);

  dlevmar_dif(errorfunc, flat, NULL, m, n, 100, NULL, NULL, NULL, NULL, NULL);
  
  cout << "Residual " << residual() << endl;

  JonesMatrix V_test = J[1]*P[1][2]*J[2].conjugate_transpose();
  V_test.print();
}



void method_file(const char *fname) {
	string line;
	vector<string>  words;
	Baselines baselines(fname);

	// Load

	ifstream file(fname);
    while( getline(file, line) ) 
      if ( line.size() > 0 && line[0] != '#' ) {
        words = split(line);
        
        int st1 = atoi(words[0].c_str()); 
        int st2 = atoi(words[1].c_str());

        complex<float> meas0 = complex<float>(atof(words[2].c_str()), atof(words[3].c_str()));
        complex<float> meas1 = complex<float>(atof(words[4].c_str()), atof(words[5].c_str()));
        complex<float> meas2 = complex<float>(atof(words[6].c_str()), atof(words[7].c_str()));
        complex<float> meas3 = complex<float>(atof(words[8].c_str()), atof(words[9].c_str()));

        complex<float> model0 = complex<float>(atof(words[10].c_str()), atof(words[11].c_str()));
        complex<float> model1 = complex<float>(atof(words[12].c_str()), atof(words[13].c_str()));
        complex<float> model2 = complex<float>(atof(words[14].c_str()), atof(words[15].c_str()));
        complex<float> model3 = complex<float>(atof(words[16].c_str()), atof(words[17].c_str()));
        
        
        V_perturb[st1][st2].set(meas0, meas1, meas2, meas3);
        P[st1][st2].set(model0, model1, model2, model3);
        
        
      }

   
    cout << "Loaded\n";
    
    for (int i=0; i<NUM_STANDS; ++i) J[i].set_identity();
    
    // Flatten 
    double *flat;
    int m;
    int n;

    for (int i=0; i<NUM_STANDS; ++i)
    	if ( baselines.stands_present[i] ) stands.add(i);
    sub_J = new JonesMatrix[stands.num_mapped()];
    for (int j=0; j<stands.num_mapped(); ++j) sub_J[j] = J[stands.map(j)];

    
    cout << stands.num_mapped() << " in total\n";
    float res1;
    cout << "Residual " << (res1=residual()) << endl;
    save_residual("res1.dat");
    delete[] sub_J;
    

    for (int bl_index=0; bl_index<baselines.num_baselines; ++bl_index) {
    	const int SUB_STAND_NUM = 8;
    	
    	int st1 = baselines.baselines[bl_index][0]; 
    	int st2 = baselines.baselines[bl_index][1]; 
    	
    	StandMapping sub_stands;		// actually just using it to accumulate them
    	sub_stands.add(st1); sub_stands.add(st2);
    	
    	// See if there is other stands that has a baseline with st1 and st2
        int num = 0;
    	for (int st=st2; st<NUM_STANDS&&num<SUB_STAND_NUM-2; ++st) {
    		bool bl_exists_with_all = true;
    		for (int i=0; i<sub_stands.num_mapped()&&bl_exists_with_all; ++i)
    			bl_exists_with_all = baselines.exists(sub_stands.mapping[i], st);
    		if ( bl_exists_with_all ) { sub_stands.add(st); ++num; }
    		
    	}

    	if ( sub_stands.num_mapped() >= SUB_STAND_NUM ) {
    		stands.clear(); for (int i=0; i<sub_stands.num_mapped(); ++i) stands.add(sub_stands.mapping[i]);
    		cout << " Sub\n"; stands.print();

    		int num_mapped = stands.num_mapped();
    		
    		sub_J = new JonesMatrix[num_mapped];
    		for (int j=0; j<num_mapped; ++j) sub_J[j] = J[stands.map(j)];
    		float sub1;
    		cout << "Sub Residual before " << (sub1=residual()) << endl;  save_residual("x.dat");

    		
    	    // Flatten 
    	    flat = new double[num_mapped*8];
    	    for (int j=0; j<num_mapped; ++j) {
    	      flat[j*8+0] = J[stands.map(j)][0][0].real(); flat[j*8+1] = J[stands.map(j)][0][0].imag();
    	      flat[j*8+2] = J[stands.map(j)][0][1].real(); flat[j*8+3] = J[stands.map(j)][0][1].imag();
    	      flat[j*8+4] = J[stands.map(j)][1][0].real(); flat[j*8+5] = J[stands.map(j)][1][0].imag();
    	      flat[j*8+6] = J[stands.map(j)][1][1].real(); flat[j*8+7] = J[stands.map(j)][1][1].imag();
    	    }
    	    
    	    m = num_mapped*8;
    	    n = num_mapped*(num_mapped-1)/2;   if ( n < m ) n = m;  		
    		
    		dlevmar_dif(errorfunc, flat, NULL, m, n, 1000, NULL, NULL, NULL, NULL, NULL);
    		
    		float sub2;
    		cout << "Sub Residual after " << (sub2=residual()) << endl; save_residual("y.dat");
    		cout << "Sub Factor: " << sub1/sub2 << endl; 
    		
    		for (int i=0; i<num_mapped; ++i) { 	
    			J[stands.map(i)] = sub_J[i];
    		}
    		
    		delete[] sub_J; exit(0);
    		delete[] flat; 
    	}
    		
    }
    
    cout << "Done sub trees\n";

    

    stands.clear();
    for (int i=0; i<NUM_STANDS; ++i)
    	if ( baselines.stands_present[i] ) stands.add(i);
    sub_J = new JonesMatrix[stands.num_mapped()];
    for (int j=0; j<stands.num_mapped(); ++j) sub_J[j] = J[stands.map(j)];
    cout << "Residual " << residual() << endl;

 
		
    // Flatten 
    flat = new double[stands.num_mapped()*8];
    for (int j=0; j<stands.num_mapped(); ++j) {
    	flat[j*8+0] = J[stands.map(j)][0][0].real(); flat[j*8+1] = J[stands.map(j)][0][0].imag();
    	flat[j*8+2] = J[stands.map(j)][0][1].real(); flat[j*8+3] = J[stands.map(j)][0][1].imag();
    	flat[j*8+4] = J[stands.map(j)][1][0].real(); flat[j*8+5] = J[stands.map(j)][1][0].imag();
    	flat[j*8+6] = J[stands.map(j)][1][1].real(); flat[j*8+7] = J[stands.map(j)][1][1].imag();
    }
	    
    m = stands.num_mapped()*8;
    n = stands.num_mapped()*(stands.num_mapped()-1)/2;   if ( n < m ) n = m;  		
		
    dlevmar_dif(errorfunc, flat, NULL, m, n, 1000, NULL, NULL, NULL, NULL, NULL);
			
    float res2;
    cout << "Residual " << (res2=residual()) << endl;
    save_residual("res2.dat");
    cout << "Factor: " << res1/res2 << endl;
    
 		
    delete[] sub_J; 
    delete[] flat;
    
    


}



int main() {
  method_file("vis.dat");
}

