#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#define TRUE 1
#define FALSE 0
#define DIMENSION 512
#define FITS_FIELD_SIZE 80
#define FITS_BLOCK_SIZE 2880

static unsigned char block[FITS_FIELD_SIZE];

float invert_endian(float f) {
  float ff = 0;
  unsigned char *c1 = (unsigned char*)&ff;
  unsigned char *c2 = (unsigned char*)&f;
  c1[3] = c2[0];
  c1[2] = c2[1];
  c1[1] = c2[2];
  c1[0] = c2[3];
  return ff;
}

static int END() {
  int ok = TRUE;
  int i;

  if ( block[0] != 'E' || block[1] != 'N' || block[2] != 'D') ok = FALSE;

  for (i=3; i<FITS_FIELD_SIZE; ++i)
    if ( block[i] != ' ' ) { ok = FALSE; break; }

  return ok;
}

int  main(int argc, char *argv[]) {
  unsigned char ch;
  float f;
  int width, old_width, offsetx, offsety,naxis1, naxis2;
  int inf, outf;
  int i, j, num_chars, dimension;
  FILE *mapf;

  if ( argc != 4 && argc != 6 ) {
    fprintf(stderr,"Usage: zoom_fits width in_file out_file\nOR\nzoom_fits width offsetx offsety in_file out_file\n");
    exit(1);
  }
  width = atof(argv[1]);  
  if ( width%2 != 0 ) {
    fprintf(stderr,"Width not even\n");
    exit(1);
  }
  
  if ( argc == 6 ) {
    offsetx = atoi(argv[2]); offsety = atoi(argv[3]);
    inf = open(argv[4],O_RDONLY); 
    outf = creat(argv[5],0644);
  } else {
    offsetx = offsety = 0;
    inf = open(argv[2],O_RDONLY); 
    outf = creat(argv[3],0644);
  }    
  
  if ( inf == -1 ) {
    fprintf(stderr,"Problem opening input file\n");
    exit(1);
  }
  if ( inf == -1 ) {
    fprintf(stderr,"Problem opening output file\n");
    exit(1);
  }
  
  num_chars = 0;
  do {
    read(inf, block, FITS_FIELD_SIZE);
    if ( strncmp("NAXIS1",block,6) == 0 || strncmp("NAXIS2",block,6) == 0 ) {
      sscanf(block+10," %d",&old_width); if ( strncmp("NAXIS1",block,6) == 0 ) naxis1 = old_width; else naxis2 = old_width;
      for (i=10; i<FITS_FIELD_SIZE; ++i) block[i] = ' ';
      sprintf(block+10,"%20d",width);
      for (i=10; i<FITS_FIELD_SIZE; ++i) if ( block[i] == '\0' ) block[i] = ' ';
    }
    if ( strncmp("CRPIX1",block,6) == 0 || strncmp("CRPIX2",block,6) == 0 ) {
      sscanf(block+9,"%f",&f); 
      if ( (int)f != old_width/2 ) {
	fprintf(stderr,"Consistency error %f %d\n",f,old_width/2);
        return 1;
      }
      for (i=10; i<FITS_FIELD_SIZE; ++i) block[i] = ' ';
      if ( strncmp("CRPIX1",block,6) == 0 )   // RA
	sprintf(block+10,"%20e",(float)width/2+0-offsetx);
      else sprintf(block+10,"%20e",(float)width/2+0-offsety);
      for (i=10; i<FITS_FIELD_SIZE; ++i) if ( block[i] == '\0' ) block[i] = ' ';
    }
    write(outf, block, FITS_FIELD_SIZE); num_chars += FITS_FIELD_SIZE; 
  } while ( !END() );

  if ( naxis1 != naxis2 ) {
    fprintf(stderr,"Consistency error %d %d\n",naxis1,naxis2);
    return 1;
  }

  // pad

  while ( num_chars % FITS_BLOCK_SIZE != 0 ) {
    read(inf, block, 1);  
    write(outf, block, 1);
    ++num_chars;
  }

  i = j = 0; 		// old_width
  
  num_chars = 0;
  while ( read(inf, &f, sizeof(float)) > 0 ) {

    if ( (old_width-width)/2+offsety <= i && i < (old_width+width)/2+offsety && (old_width-width)/2+offsetx <= j && j < (old_width+width)/2+offsetx ) {
       write(outf, &f, sizeof(float)); num_chars += sizeof(float);
    }

    ++j; if (j >= old_width ) { j = 0; ++i; }
  } 
  
  // pad
  block[0] = '\0';
  while ( num_chars % FITS_BLOCK_SIZE != 0 ) {
    write(outf, block, 1);
    ++num_chars;
  }
  
  close(inf); close(outf);
  
  return 0;
}
