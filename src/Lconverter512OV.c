#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define NPOL 2
#define NSTATION 256
#define NFREQUENCY 109

typedef char ReImInput;

typedef struct ComplexInputStruct {
  ReImInput real;
  ReImInput imag;
} ComplexInput;

typedef struct ComplexStruct {
  float real;
  float imag;
} Complex;

/*
  Data ordering for input vectors is (running from slowest to fastest)
  [time][channel][station][polarization][complexity]

  Output matrix has ordering (mostly - with a few extra terms!)
  [channel][station][station][polarization][polarization][complexity]
*/

// reorder the matrix from REGISTER_TILE_TRIANGULAR_ORDER to HERMITIAN TRIANGULAR_ORDER
// This routine is unchanged except to remove the Complex type and revert to float arrays.

int close(float x, float y) { return fabsf(x-y) < 0.1; }
int GET_VIS_INDEX(int ch, int bl, int p1, int p2) { return (ch*(  32896*2*2*2) +bl*2*2*2 + p1*2*2 +p2*2); }

	static inline int ants2baseline(int i, int j) {
		return i*(i+1)/2 + j;
	}

void find1(float *matrix, float x, int *chan, int *st1, int *st2, int *p1, int *p2) {
  int f, i, j, pol1, pol2;

  *chan = *st1 = *st2 = *p1 = *p2 = -1;
  for(f=0; f<NFREQUENCY; f++)
    for(i=0; i<NSTATION; i++)
      for (j=0; j<=i; j++)
        for (pol1=0; pol1<NPOL; pol1++)
          for (pol2=0; pol2<NPOL; pol2++)
            if ( close(matrix[GET_VIS_INDEX(f, ants2baseline(i, j), pol1, pol2)], x) ) { *chan = f; *st1 = i; *st2 = j; *p1 = pol1; *p2 = pol2; return; }
}

void find2(float *matrix, float x, float y, int *chan, int *st1, int *st2, int *p1, int *p2) {
  int f, i, j, pol1, pol2;

  *chan = *st1 = *st2 = *p1 = *p2 = -1;
  for(f=0; f<NFREQUENCY; f++)
    for(i=0; i<NSTATION; i++)
      for (j=0; j<=i; j++)
        for (pol1=0; pol1<NPOL; pol1++)
          for (pol2=0; pol2<NPOL; pol2++)
            if ( close(matrix[GET_VIS_INDEX(f, ants2baseline(i, j), pol1, pol2)], x) && close(matrix[GET_VIS_INDEX(f, ants2baseline(i, j), pol1, pol2)+1], y) ) { 
	      *chan = f; *st1 = i; *st2 = j; *p1 = pol1; *p2 = pol2; return; 
            }
}

void xgpuReorderMatrix(float *matrix, int nstation, int nfrequency, int npol) {
  int matLength = 2*nfrequency*((nstation/2+1)*(nstation/4)*npol*npol*4);    	// number of floats in REG_TILE dump 
  int f, i, rx, j, ry, pol1, pol2;
  float *tmp = (float *) malloc(matLength*sizeof(float));
  memset(tmp, 0, matLength*sizeof(float));


  for(f=0; f<nfrequency; f++) {
    for(i=0; i<nstation/2; i++) {
      for (rx=0; rx<2; rx++) {
	for (j=0; j<=i; j++) {
	  for (ry=0; ry<2; ry++) {
	    int k = f*(nstation+1)*(nstation/2) + (2*i+rx)*(2*i+rx+1)/2 + 2*j+ry;
	    int l = f*4*(nstation/2+1)*(nstation/4) + (2*ry+rx)*(nstation/2+1)*(nstation/4) + i*(i+1)/2 + j;
	    for (pol1=0; pol1<npol; pol1++) {
	      for (pol2=0; pol2<npol; pol2++) {
		size_t tri_index = (k*npol+pol1)*npol+pol2;
		size_t reg_index = (l*npol+pol1)*npol+pol2;

		tmp[2*tri_index] = matrix[reg_index];
		tmp[2*tri_index+1] = matrix[reg_index+matLength/2];
	      }
	    }
	  }
	}
      }
    }
  }
   
  memcpy(matrix, tmp, matLength*sizeof(float));

  free(tmp);

  return;
}



// Extract the matrix from the packed Hermitian form.
void xgpuExtractMatrix(float *matrix, float *packed, int nstation, int nfrequency, int npol) {
  int f, i, j, pol1, pol2;

  for(f=0; f<nfrequency; f++){
    for(i=0; i<nstation; i++){
      for (j=0; j<=i; j++) {
	int k = f*(nstation+1)*(nstation/2) + i*(i+1)/2 + j;
        for (pol1=0; pol1<npol; pol1++) {
	  for (pol2=0; pol2<npol; pol2++) {
	    int index = (k*npol+pol1)*npol+pol2;
	
            // Generate TRIANGULAR format data. Can be indexed as [ chan, baseline, pol, pol ].
	    matrix[GET_VIS_INDEX(f, ants2baseline(i, j), pol1, pol2)] += packed[2*index];
	    if ( i == j && pol1 == pol2 ) matrix[GET_VIS_INDEX(f, ants2baseline(i, j), pol1, pol2)+1] = 0;     // auto correlation
            else matrix[GET_VIS_INDEX(f, ants2baseline(i, j), pol1, pol2)+1] += packed[2*index+1];
	    //printf("%d %d %d %d %d %d %d\n",f,i,j,k,pol1,pol2,index);
	  }
	}
      }
    }
  }


}


#define TRUE 1
#define FALSE 0
int main() {
  float *buff = (float*)malloc(115187712);
  float *buff_out = (float*)malloc(109*32896*2*2*2*sizeof(float));

  int f = open("x.dada", O_RDONLY);
  int num;

  memset(buff_out, 0, 109*32896*2*2*2);
  read(f, buff, 4096);

  num = read(f, buff, 115187712);

  if ( num != 115187712 ) {
    fprintf(stderr,"Failed to read\n");
    exit(1);
  }

  int i, j, pol1, pol2;
  xgpuReorderMatrix(buff, NSTATION, NFREQUENCY, NPOL); xgpuExtractMatrix(buff_out, buff, NSTATION, NFREQUENCY, NPOL);
  for(i=0; i<NSTATION; i++)
    for (j=0; j<=i; j++) {
      int all_zero = TRUE;
      for(f=0; f<NFREQUENCY; f++)
        for (pol1=0; pol1<NPOL; pol1++) 
	  for (pol2=0; pol2<NPOL; pol2++) 
	    if ( fabsf(buff_out[GET_VIS_INDEX(f, ants2baseline(i, j), pol1, pol2)]) > 0 || fabsf(buff_out[GET_VIS_INDEX(f, ants2baseline(i, j), pol1, pol2)+1]) > 0 ) all_zero = FALSE;
      if ( all_zero ) puts("yes");
    }

  
  
  float re, im;
  int chan, st1, st2, p1, p2;
  FILE *vals_file = fopen("x.dat", "r");
  while ( fscanf(vals_file, "%d %d %d %d %d %f %f\n", &f, &i, &j, &p1, &p2, &re, &im) == 7 ) {
      printf("%d %d %d %d %d -> ", f, i, j, p1, p2);
      if ( buff_out[GET_VIS_INDEX(f, ants2baseline(j, i), p2, p1)] == re && buff_out[GET_VIS_INDEX(f, ants2baseline(j, i), p2, p1)+1] == im ) puts("MATCH");
      else puts("MISMATCH"); 
  }
}
