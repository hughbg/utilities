#include <complex.h>
#include <fftw3.h>

void non_shifted_psf(double *p) {
  int i, j;
  for (i=0; i<256; ++i) for (j=0; j<256; ++j) {
    if ( 122 <= i && i <= 134 && 122 <= j && j <= 134  ) p[i*256+j] = 1;
    else p[i*256+j] = 0;
  }
}

void shifted_psf(double *p) {
  int i, j, ii, jj;
  for (i=0; i<256; ++i) for (j=0; j<256; ++j) {
    ii = i-128; jj = j-128;

    if ( ( ii <= -122 && jj <= -122 ) ||
         ( ii <= -122 && jj >= 122 ) ||
         ( ii >= 122 && jj <= -122 ) ||
         ( ii >= 122 && jj >= 122 ) ) p[i*256+j] = 1;
    else p[i*256+j] = 0;
  }
}

void do_fft(double *image, int nx, int ny, fftw_complex *output) {
  fftw_plan plan_forward;
 
  plan_forward = fftw_plan_dft_r2c_2d(nx, ny, image, output, FFTW_ESTIMATE );

  fftw_execute(plan_forward);
  fftw_free(plan_forward);
}


void do_ifft(fftw_complex *fft_in, int nx, int ny, double *output) {
  fftw_plan plan_backward;  

  plan_backward = fftw_plan_dft_c2r_2d (nx, ny, fft_in, output, FFTW_ESTIMATE );


  fftw_execute (plan_backward);
  fftw_free(plan_backward);
}

main() {
/*
  cols_fft = (cols/2)+1;

  out_fft = new fftw_complex[rows*cols_fft];

  do_fft(image, rows, cols, out_fft);

*/
  double image[256*256];
  fftw_complex image_fft[256*129];
  double psf[256*256];
  fftw_complex psf_fft[256*129];
  int i, j;

  for (i=0; i<256; ++i) for (j=0; j<256; ++j) image[i*256+j] = 0;
  image[128*256+128] = 1;
  non_shifted_psf(psf);
  do_fft(image, 256, 256, image_fft); 
  do_fft(psf, 256, 256, psf_fft);
  for (i=0; i<256*129; ++i) image_fft[i] *= psf_fft[i];
  do_ifft(image_fft, 256, 256, image);


  printf("256\n");
  for (i=0; i<256; ++i) for (j=0; j<256; ++j) printf("%f\n", image[i*256+j]);
}
