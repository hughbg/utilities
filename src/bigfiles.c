#include <dirent.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#define TRUE 1

static char pathname[4096];
static char big_file[4096];
static unsigned max_file_size;
static char separator = '/';
static int any_bigger;

void push_path(const char *p) {
	char sep[2];
   sep[0] = separator; sep[1] = '\0';
	strcat(pathname,sep);
   strcat(pathname,p);
}

void pop_path(void) {
  char *s;

  s = pathname+strlen(pathname)-1;

  while ( s > pathname && *s != separator ) --s;

  if ( *s == separator ) *s = '\0';
}



static void action(char *fname, unsigned size) {
	//printf("%s %u\n",fname,size);

   if ( any_bigger ) {
   	if ( size >= max_file_size ) printf("%s: %u\n",fname,size);
   } else {
	   if ( size > max_file_size ) {
 		  	max_file_size = size;
      	strcpy(big_file,fname);
	   }
   }
}

void check_dir(void) {
   DIR *dir;
   struct dirent *ent;
	char full_path[4096];
   char sep[2];
	struct stat statbuf;   // of last file accessed

   sep[0] = separator; sep[1] = '\0';

   if ((dir = opendir(pathname)) == NULL) { fputs("opendir failed\n",stderr); perror(pathname); return; }
   while ((ent = readdir(dir)) != NULL) {
      if ( strcmp(ent->d_name,".")==0 || strcmp(ent->d_name,"..")==0 ) continue;

      // make file name
	  	strcpy(full_path,pathname);
      strcat(full_path,sep);
   	strcat(full_path,ent->d_name);

		if ( stat(full_path,&statbuf) == 0 ) {
   	   if ( statbuf.st_mode & S_IFDIR ) {
     	 		push_path(ent->d_name);
     	    	check_dir();
     	    	pop_path();
         } else action(full_path,statbuf.st_size);
      } else  {
      	fputs("stat failed\n",stderr);
      	perror(full_path);
      }

	}

   closedir(dir);

}

int get_number(const char *s) {
	const char *p = s;

   while ( *p != '\0' && isdigit(*p++) );

   if ( *p != '\0' ) return 0;
   else return atoi(s);
}


void usage(void) {
	fprintf(stderr,"Find the biggest file.\n\nUsage: bigfiles [ size ]\n\n");
   fprintf(stderr,"size: Find any files bigger than this many bytes\n");

   exit(1);
}


void main(int argc, char *argv[]) {
	if ( argc == 2 )
   	if ( (max_file_size=get_number(argv[1])) == 0 ) usage();
      else any_bigger = TRUE;

   if ( argc > 2 ) usage();

   pathname[0] = '.';

   check_dir();

   if ( argc == 1 && big_file[0] != '\0' ) printf("%s: %u\n",big_file,max_file_size);

}
