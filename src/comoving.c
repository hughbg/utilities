// gcc comoving.c -o comoving  -lm -lgsl -lblas
#include <math.h>
#include <stdio.h>
#include <gsl/gsl_integration.h>

#define SQR(x) ((x)*(x))
#define CUBE(x) ((x)*(x)*(x))
const double aipy_const_arcmin = 0.0002908882086657216;
const double f21cm = 1.42040575177e9;


// Cosmological parameters
const double Omega_M = 0.26;
const double Omega_k = 0;
const double Omega_Lambda = 0.74;

const int c = 299792458;	// Units m/s
const double h = 0.74;

// GSL integration routines ----------------------------------
const int workspace_length = 5000;
double func(double z, void *params) {

  return 1/sqrt(Omega_M*CUBE(1+z)+Omega_k*SQR(1+z)+Omega_Lambda);
}

double gsl_integrate_E(gsl_integration_workspace *w, double z) {
  double result, error;

  gsl_function F;
  F.function = &func;
  F.params = NULL;

  gsl_integration_qag(&F, 0, z, 0, 1e-7, GSL_INTEG_GAUSS61, workspace_length, w, &result, &error); 

  //printf ("result          = % .18f\n", result);
  //printf ("estimated error = % .18f\n", error);
  //printf ("intervals       = %zu\n", w->size);
  return result;

}

// End GSL ---------------------------------------------------

// Convert frequency (Hz) to redshift for 21cm line.
double f2z(double fq) {
    return (f21cm / fq - 1);
}


// These taken from the conversion routines used by Paper and Pober.
// Multiply by this to convert an angle on the sky to a transverse distance in Mpc/h at redshift z
double dL_dth(double z) {
    // """[h^-1 Mpc]/radian, from Furlanetto et al. (2006)"""
    return 1.9 * (1./aipy_const_arcmin) * pow((1+z)/10.0, 0.2);
}

//Multiply by this to convert a bandwidth in GHz to a line of sight distance in Mpc/h at redshift z
double dL_df(double z, double omega_m) {
    //"""[h^-1 Mpc]/GHz, from Furlanetto et al. (2006)"""		// Eqn 4
    return (1.7 / 0.1) * pow((1+z) / 10., .5) * pow(omega_m/0.15, -0.5) * 1e3;
}

// The next two routines are the "correct" ones. E(z) has to be integrated numerically.


// These routines convert an angle on the sky to distance in Mpc
double Integral_transverse_distance(gsl_integration_workspace *w, double angle, double z) {
  double H0;       // Units h km/s/Mpc
  double D_H;      // Units Mpc/h

  H0 = 100*h*1000;      // Convert units to meters, hence the 1000 factor
  D_H = c/H0;           // Units Mpc/h

  double D_C = D_H*gsl_integrate_E(w, z);    // E is dimensionless so units Mpc/h
  double D_M = D_C;       		// for flat universe Omega_k = 0
  return D_M*angle;
}

double Furlanetto_transverse_distance(double angle, double z) {
  return angle*dL_dth(z)/h;
}

// These routines convert an angle on the sky to distance in Mpc
double Integral_los_distance(gsl_integration_workspace *w, double chan_width /*Hz*/, double z, double f /*Hz*/) {
  double new_z;
  double H0;       // Units h km/s/Mpc
  double D_H;      // Units Mpc/h

  H0 = 100*h*1000;      // Convert units to meters, hence the 1000 factor
  D_H = c/H0;           // Units Mpc/h

  // Top frequency 
  double D_C_top = D_H*gsl_integrate_E(w, z);	// E is dimensionless so units Mpc/h
  // Bottom frequency 

  new_z = z+f21cm/(f-chan_width)-f21cm/f;
  double D_C_bottom = D_H*gsl_integrate_E(w, new_z); 

  return D_C_bottom-D_C_top;
}

double Furlanetto_los_distance(double chan_width /*Hz*/, double z, double f) {

  return chan_width/1e9*dL_df(z, Omega_M)/h;
}

void transverse_distance() {
  const double angle = 0.1;
  double z = 5.0;
  int i;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(workspace_length);
 
  
  for (i=0; i<400; ++i) {
    printf("%f %f %f\n", z, Furlanetto_transverse_distance(aipy_const_arcmin, z), Integral_transverse_distance(w, aipy_const_arcmin, z));
    z += 0.1;
  }
  gsl_integration_workspace_free(w);

}

void los_distance() {
  double start_f = 100e6;	// All in Hz
  double end_f = 30e6;
  double chan_width = 24e3;
  double f = start_f;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(workspace_length);

  while ( f >= end_f ) {
    double z = f2z(f);
    printf("%f %f %f\n", z, Furlanetto_los_distance(chan_width, z, f), Integral_los_distance(w, chan_width, z, f));
    f -= chan_width*10;
  }
  gsl_integration_workspace_free(w);
}

int main() {
  los_distance();
}


