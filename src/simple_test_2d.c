/*
 * Copyright (c) 2002, 2017 Jens Keiner, Stefan Kunis, Daniel Potts
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

// gcc -E -DHAVE_CONFIG_H -I. -I../../include  -I../../include   -O3 -fomit-frame-pointer -malign-double -fstrict-aliasing -ffast-math -march=native  -MT simple_test.o -MD -MP -MF .deps/simple_test.Tpo -c -o simple_test.o simple_test.c

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <complex.h>

#define NFFT_PRECISION_DOUBLE

#include "nfft3mp.h"

#define FALSE 0
#define TRUE 1




typedef struct {
  int chan;
  double uv;
  double x, y;	// channel, uv_length, normalized to -0.5 to 0.5
  int use;
  fftw_complex val;
} Point;

typedef struct {
  Point *points;
  double min_chan, max_chan, min_uv, max_uv;	// for nomalizing to [-0.5, 0.5]. The max should be above the max value, Python style
  int N, N1, N2;				// N is total points, N1, N2 for FFT
} Points;

typedef struct {
  fftw_complex **fft;
  fftw_complex *solution;
} Solution;

int compare (const void * a, const void * b) {
  if ( ((Point*)a)->y - ((Point*)b)->y > 0 ) return 1;
  else if ( ((Point*)a)->y - ((Point*)b)->y < 0 ) return -1;
  else return 0;
}

void thin_points(Points data, double uv_min, double uv_max, double sep_factor) {
  double min_sep; 
  int i, j, k, ch, num_channels;

  // Find out range
  num_channels = (int)data.max_chan;  

  for (i=0; i<data.N; ++i) {
    data.points[i].use = TRUE;
  }

  // Sort
  qsort(data.points, data.N, sizeof(Point), compare); 

  min_sep = (data.max_uv-data.min_uv)/data.N2;		// data.N2 is optimal, i.e. don't make smaller than that
  min_sep /= sep_factor;
  printf("Min sep %.15lf\n", min_sep);


  for (ch=0; ch<num_channels; ++ch) {
    j = 0;
    while ( j < data.N-1 )  {
      while ( j < data.N-1 && (int)data.points[j].chan != ch ) ++j;
      k = j; 
      while ( j < data.N-1 && data.points[j+1].uv-data.points[k].uv < min_sep ) {
        if ( (int)data.points[j+1].chan == ch ) data.points[j+1].use = FALSE;
        ++j;
      }
      if ( j == k ) ++j;
    }

  }

  // Restrict by uv
  for (i=0; i<data.N; ++i) {
    if ( ! (uv_min <= data.points[i].uv && data.points[i].uv < uv_max ) ) data.points[i].use = FALSE;
  }

  

}
// http://www.fftw.org/fftw3_doc/Complex-One_002dDimensional-DFTs.html
static  void ifft(fftw_complex *in, fftw_complex *out, int N) {
    fftw_plan p;
    p = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p); 
    fftw_destroy_plan(p);
}


static Solution nfft_solver(Points data, int niter, fftw_complex *guess) {
  int DIM=2;
  Solution solution;
  int k, l, i, j, sum, new_i, new_j; /**< index for nodes, freqencies,iter*/
  NFFT(plan) p; /**< plan for the nfft               */
  SOLVER(plan_complex) ip; /**< plan for the inverse nfft       */
  const char *error_str;
  fftw_complex **output_fft;

  sum = 0;
  for (i=0; i<data.N; ++i) if ( data.points[i].use ) ++sum;
  printf("Use %d points out of %d\n", sum, data.N);

  /** initialise a two dimensional plan */
  NFFT(init_2d)(&p, data.N1, data.N2, sum);			// N -> number of nodes i.e. image values

  /** init FFT grid nodes */
  j = 0;
  for (i=0; i<data.N; ++i) {
    if ( data.points[i].use ) {
      p.x[DIM*j] = data.points[i].x;  
      p.x[DIM*j+1] = data.points[i].y;
      ++j;
    } 
  }

  /** precompute psi, the entries of the matrix B */
  if (p.flags & PRE_ONE_PSI)
    NFFT(precompute_one_psi)(&p);

  /** initialise inverse plan */
  SOLVER(init_complex)(&ip, (NFFT(mv_plan_complex)*) (&p));

  /** init FFT values in grid */
  j = 0;
  for (i=0; i<data.N; ++i) {
      if ( data.points[i].use ) {
	ip.y[j] = data.points[i].val; 	// complex
	++j;
      }
  }

  //NFFT(vpr_complex)(ip.y, p.M_total, "Given data, vector y");


  /** initialise some guess f_hat_0 and solve */
  if ( guess == NULL ) 
    for (k = 0; k < p.N_total; k++)
      ip.f_hat_iter[k] = NFFT_K(0.0);
  else
    for (k = 0; k < p.N_total; k++)
      ip.f_hat_iter[k] = guess[k];


  //NFFT(vpr_complex)(ip.f_hat_iter, p.N_total,
  //    "Initial guess, vector f_hat_iter");

  /** check for valid parameters before calling any trafo/adjoint method */
  error_str = NFFT(check)(&p);
  if ( error_str != 0 ) {
    printf("Error in nfft module: %s\n", error_str);
    solution.fft = NULL; solution.solution = NULL;
    return solution;
  }

  NFFT_CSWAP(ip.f_hat_iter, p.f_hat);
  NFFT(trafo)(&p);
  //NFFT(vpr_complex)(p.f, p.M_total, "Data fit, vector f");
  NFFT_CSWAP(ip.f_hat_iter, p.f_hat);

  SOLVER(before_loop_complex)(&ip);
  //printf("\n Residual r=%" NFFT__FES__ "\n", ip.dot_r_iter);

  for (l = 0; l < niter; l++)
  {
    //printf("\n********** Iteration l=%d **********\n", l);
    SOLVER(loop_one_step_complex)(&ip);
    //NFFT(vpr_complex)(ip.f_hat_iter, p.N_total,
    //    "Approximate solution, vector f_hat_iter");

    NFFT_CSWAP(ip.f_hat_iter, p.f_hat);
    NFFT(trafo)(&p);
    //NFFT(vpr_complex)(p.f, p.M_total, "Data fit, vector f");
    NFFT_CSWAP(ip.f_hat_iter, p.f_hat);

    printf("\n Residual r=%"  NFFT__FES__ "\n", ip.dot_r_iter);
  }


  // Dump in the format used by fftw3 and numpy.fft. Means doing an fftshift, and
  // also the sign on the values is all over the place and needs to be fixed.
  output_fft = malloc(data.N1*sizeof(fftw_complex*));
  for (i=0; i<data.N1; ++i) output_fft[i] = malloc(data.N2*sizeof(fftw_complex));
  new_i = new_j = 0;

  for (i=data.N1/2; i < data.N1; ++i) { 
    for (j=data.N2/2; j < data.N2; ++j) 
      if ( (i%2 ==0 && j%2 == 1) || (i%2==1 && j%2 == 0 ) ) {
        output_fft[new_i][new_j] = -creal(ip.f_hat_iter[i*data.N2+j])+I*cimag(ip.f_hat_iter[i*data.N2+j]);
        ++new_j; if ( new_j == data.N2 ) { ++new_i; new_j = 0; }
      } else { 
        output_fft[new_i][new_j] = creal(ip.f_hat_iter[i*data.N2+j])-I*cimag(ip.f_hat_iter[i*data.N2+j]);
        ++new_j; if ( new_j == data.N2 ) { ++new_i; new_j = 0; }
      }
    for (j=0; j < data.N2/2; ++j)
      if ( (i%2 ==0 && j%2 == 1) || (i%2==1 && j%2 == 0 ) ) {
        output_fft[new_i][new_j] = -creal(ip.f_hat_iter[i*data.N2+j])+I*cimag(ip.f_hat_iter[i*data.N2+j]);
        ++new_j; if ( new_j == data.N2 ) { ++new_i; new_j = 0; }
      } else { 
        output_fft[new_i][new_j] = creal(ip.f_hat_iter[i*data.N2+j])-I*cimag(ip.f_hat_iter[i*data.N2+j]);
        ++new_j; if ( new_j == data.N2 ) { ++new_i; new_j = 0; }
      }
  }
  for (i=0; i < data.N1/2; ++i) { 
    for (j=data.N2/2; j < data.N2; ++j)
      if ( (i%2 ==0 && j%2 == 1) || (i%2==1 && j%2 == 0 ) ) {
        output_fft[new_i][new_j] = -creal(ip.f_hat_iter[i*data.N2+j])+I*cimag(ip.f_hat_iter[i*data.N2+j]);
	++new_j; if ( new_j == data.N2 ) { ++new_i; new_j = 0; }
      } else {
	output_fft[new_i][new_j] = creal(ip.f_hat_iter[i*data.N2+j])-I*cimag(ip.f_hat_iter[i*data.N2+j]);
	++new_j; if ( new_j == data.N2 ) { ++new_i; new_j = 0; }
      }
    for (j=0; j < data.N2/2; ++j) 
      if ( (i%2 ==0 && j%2 == 1) || (i%2==1 && j%2 == 0 ) ) {
        output_fft[new_i][new_j] = -creal(ip.f_hat_iter[i*data.N2+j])+I*cimag(ip.f_hat_iter[i*data.N2+j]);
	++new_j; if ( new_j == data.N2 ) { ++new_i; new_j = 0; }
      } else {
	output_fft[new_i][new_j] = creal(ip.f_hat_iter[i*data.N2+j])-I*cimag(ip.f_hat_iter[i*data.N2+j]);
	++new_j; if ( new_j == data.N2 ) { ++new_i; new_j = 0; }
      }
  }



  solution.fft = output_fft;
  solution.solution = (fftw_complex*)malloc(p.N_total*sizeof(fftw_complex));
  for (i=0; i<p.N_total; ++i) solution.solution[i] = ip.f_hat_iter[i];

  SOLVER(finalize_complex)(&ip);
  NFFT(finalize)(&p);

  return solution;

}

void invert_one_axis(fftw_complex **fft, int N1, int N2) {
  fftw_complex *tmp_buf;
  int i;

  // Inverse FFT along one axis, the uv
  tmp_buf = malloc(N2*sizeof(fftw_complex));
  for (i=0; i<N1; ++i) {
    ifft(fft[i], tmp_buf, N2);    
    memcpy(fft[i], tmp_buf, N2*sizeof(fftw_complex));

  }

  free(tmp_buf);
}

void shift_one_axis(fftw_complex **fft, int N1, int N2) {
  fftw_complex *tmp_buf;
  int i, j;

  // Do fft shift along one axis, the channels
  tmp_buf = malloc(N1*sizeof(fftw_complex));
  for (i=0; i<N2; ++i) {
    for (j=0; j<N1; ++j) tmp_buf[j] = fft[j][i];    
    for (j=0; j<N1/2; ++j) fft[j+N1/2][i] = tmp_buf[j];
    for (j=N1/2; j<N1; ++j) fft[j-N1/2][i] = tmp_buf[j];

  }

  free(tmp_buf);
}

Points load_points(const char *in_file) {
  FILE *input;
  int i, j, N1, N2;
  double val;
  Points p;

  input = fopen(in_file,"r");   // flat input file
  fscanf(input,"%d %d\n",&N1, &N2);

  p.points = malloc(N1*N2*sizeof(Point));  

  for (i=0; i<N1; ++i)
    for (j=0; j<N2; ++j) {

      p.points[i*N2+j].chan = i;
      p.points[i*N2+j].uv = j;
      fscanf(input, "%lf\n", &val);
      p.points[i*N2+j].val = val;
      p.points[i*N2+j].use = TRUE;
    }

  fclose(input);

  p.min_chan = 0; p.max_chan = N1; p.min_uv = 0; p.max_uv = N2; p.N = N1*N2;
  p.N1 = N1; p.N2 = N2;

  // Normalize
  for (i=0; i<p.N; ++i) {
    p.points[i].x = (p.points[i].chan-p.min_chan)/(p.max_chan-p.min_chan)-0.5;
    p.points[i].y = (p.points[i].uv-p.min_uv)/(p.max_uv-p.min_uv)-0.5; 

  }

  return p;
}



Points load_vis_points(const char *in_file) {
  FILE *input;
  int i;
  double valr, vali, chan, uv;
  double max_channel, max_uv_length, min_uv_length;
  Points p;

  p.N = 0; max_channel = max_uv_length = 0; min_uv_length = 1e39;

  // First find out how many
  input = fopen(in_file,"r");   // flat input file
  while ( fscanf(input,"%lf %lf %lf %lf\n",&chan, &uv, &valr, &vali) != EOF ) { 
    if ( chan > max_channel ) max_channel = chan;
    if ( uv > max_uv_length ) max_uv_length = uv;
    if ( uv < min_uv_length ) min_uv_length = uv;
  
    ++p.N;
  } 
  fclose(input);

  printf("Max channel %f Max UV %f\n", max_channel, max_uv_length);

  p.points = malloc(p.N*sizeof(Point));
  input = fopen(in_file,"r");   // flat input file
  i = 0;
  while ( fscanf(input,"%lf %lf %lf %lf\n",&chan, &uv, &valr, &vali) != EOF ) {
    p.points[i].chan = chan;
    p.points[i].uv = uv;
    p.points[i].val = valr+I*vali;
    p.points[i].use = TRUE;
    ++i;
  }
  fclose(input);

  p.min_chan = 0; p.max_chan = max_channel+1; 
  p.min_uv = min_uv_length; p.max_uv = 1.01*max_uv_length;
  p.N1 = p.max_chan; p.N2 = 2*32768;

  // Normalize
  for (i=0; i<p.N; ++i) {
    p.points[i].x = (p.points[i].chan-p.min_chan)/(p.max_chan-p.min_chan)-0.5;
    p.points[i].y = (p.points[i].uv-p.min_uv)/(p.max_uv-p.min_uv)-0.5; 

  }

  return p;
}

void save_points(Points p) {
  int i;
  FILE *f;

  f = fopen("nfft_points.dat","w");
  for (i=0; i<p.N; ++i)
    if ( p.points[i].use ) fprintf(f, "%f %f %f\n", p.points[i].x, p.points[i].y, cabsf(p.points[i].val));
  fclose(f);
}

void save_points_square(Points p) {
  FILE *f;
  int i, j;
  int N2=512;
  fftw_complex **grid;
  int *chan_counts;
  
  grid = (fftw_complex**)calloc(p.N1, sizeof(fftw_complex*));
  for (i=0; i<p.N1; ++i) grid[i] = (fftw_complex*)calloc(N2, sizeof(fftw_complex));
  chan_counts = (int*)calloc(p.N1, sizeof(int));

  for (i=0; i<p.N; ++i) {
    if ( p.points[i].use && chan_counts[(int)p.points[i].x] < N2 ) {
      grid[(int)p.points[i].x][chan_counts[(int)p.points[i].x]] = p.points[i].val;    
      ++chan_counts[(int)p.points[i].x];
    }
  }

  f = fopen("nfft_points_square.map", "w");
  fprintf(f, "%d %d\n", p.N1, N2);
  for (i=0; i<p.N1; ++i)
    for (j=0; j<N2; ++j) fprintf(f,"%f %f\n", creal(grid[i][j]), cimag(grid[i][j]));
  fclose(f);

  for (i=0; i<p.N1; ++i) free(grid[i]); free(grid);
  free(chan_counts);
}
      



//#define TEST

/** Main routine */
int main() {
  int i, j;
  FILE *output;
  Solution solution;

#ifdef TEST
  Points points = load_points("test_case.map");		// compare with test_image() in test_cases.py
  solution = nfft_solver(points, 3, NULL); 
 
#else
  Points points = load_vis_points("points.dat");
  thin_points(points, 0, 50, 1);
  //save_points(points); 		/ load in gnuplot to look at coverage
  save_points_square(points); 		// use do_nfft_square()


  solution = nfft_solver(points, 10, NULL);  exit(0);

#endif
  invert_one_axis(solution.fft, points.N1, points.N2);

  shift_one_axis(solution.fft, points.N1, points.N2);

  output = fopen("image_n_fft.map", "w");
  fprintf(output, "%d %d\n", points.N1, points.N2); 
  for (i=0; i<points.N1; ++i) 
    for (j=0; j<points.N2; ++j)
      fprintf(output, "%f %f\n", creal(solution.fft[i][j]), cimag(solution.fft[i][j])); 

  for (i=0; i<points.N1; ++i) free(solution.fft[i]); 
  free(solution.fft);
  free(solution.solution);   

  fclose(output);

  return EXIT_SUCCESS;
}
/* \} */
