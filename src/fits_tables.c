/*
  Given a FITS file as argument, dumps information about the headers and tables and data, without dumping the data itself.
  The data is replaced with a print of how many bytes.
  Does not use CFITSIO.
*/

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define TRUE 1
#define FALSE 0

void fold(char *s) {
  int i=0;

  while ( i < 2880 ) {
    putchar(s[i]);
    if ( (i+1)%80 == 0 ) putchar('\n');
    ++i;
  }
  putchar('\n');
} 

int main(int argc, char *argv[]) {
  char file_contents[2881], end[81];
  int f, in_text, bcounter, tcounter, counter, bcounter_all, tcounter_all, i;
  struct stat statbuf;

  if ( argc != 2 ) {
    fprintf(stderr,"Expecting input file name\n");
    return 1;
  }
  file_contents[2880] = '\0';
  for (i=0; i<80; ++i) end[i] = ' '; end[0] = 'E'; end[1] = 'N'; end[2] = 'D'; end[80] = '\0';
  stat(argv[1], &statbuf);

  if ( (f=open(argv[1],O_RDONLY)) == -1 ) {
    fprintf(stderr,"Failed to open %s\n",argv[1]);
    return 1;
  }
  in_text = FALSE; counter = bcounter = tcounter = bcounter_all = tcounter_all = 0;
  while ( read(f, file_contents, 2880) == 2880 ) { 
    counter += 2880;
    if ( strncmp(file_contents,"SIMPLE",6) == 0 || strncmp(file_contents,"XTENSION",8) == 0 ) { 
      in_text = TRUE;
      printf(">>>> %d bytes binary\n",bcounter);
      tcounter = 0;
    }
    if ( in_text ) { fold(file_contents); tcounter += 2880; tcounter_all += 2880; } 
    else { bcounter += 2880; bcounter_all += 2880; } 
    if ( in_text && strstr(file_contents,end) != NULL  ) { 
      in_text = FALSE; 
      printf(">>>> %d bytes text\n",tcounter);
      bcounter = 0; 
    }
  }
  printf(">>>> %d bytes binary\n",bcounter);
  close(f);
  if ( tcounter_all+bcounter_all != counter || counter != statbuf.st_size ) 
    fprintf(stderr,"ERROR: Counts don't tally\n");
  else printf(">>>> %d bytes total\n",counter);
}
