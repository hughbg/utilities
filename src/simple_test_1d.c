/*
 * Copyright (c) 2002, 2017 Jens Keiner, Stefan Kunis, Daniel Potts
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdlib.h>
#include <math.h>
#include <complex.h>

#include "nfft3.h"

#define NFFT_PRECISION_DOUBLE
#include "nfft3mp.h"

/** Swap two vectors. */
#define CSWAP(x,y) {double _Complex * NFFT_SWAP_temp__; \
  NFFT_SWAP_temp__=(x); (x)=(y); (y)=NFFT_SWAP_temp__;}

#define FALSE 0
#define TRUE 1

typedef struct {
  double physical_location;
  double norm_location;	 //  normalized to -0.5 to 0.5
} Location;

typedef struct {
  Location *locations;
  fftw_complex *signal;
  int signal_length;		// M, N
} Points;

void even_layout(Points *p) {
  int i;
  for (i=0; i<p->signal_length; ++i) {
    p->locations[i].physical_location = i;
    p->locations[i].norm_location = -0.5+i*1/(float)p->signal_length;
  }  
}

// Forward FFT
static Points forward_nfft_1d(Points points, int niter) {
  int j, k, l; /**< index for nodes, freqencies,iter*/
  NFFT(plan) p; /**< plan for the nfft               */
  SOLVER(plan_complex) ip; /**< plan for the inverse nfft       */
  const char *error_str;
  Points p_out;


  /** initialise an one dimensional plan */
  NFFT(init_1d)(&p, points.signal_length, points.signal_length);

  /** init pseudo random nodes */
  for(j=0; j<p.M_total; j++) {
    p.x[j] = points.locations[j].norm_location;
  }

  /** precompute psi, the entries of the matrix B */
  if (p.flags & PRE_ONE_PSI)
    NFFT(precompute_one_psi)(&p);

  /** initialise inverse plan */
  SOLVER(init_complex)(&ip, (NFFT(mv_plan_complex)*) (&p));

  /** init pseudo random samples and show them */
  for (j=0; j<p.M_total; ++j) ip.y[j] = points.signal[j];

  /** initialise some guess f_hat_0 and solve */
  for (k = 0; k < p.N_total; k++)
    ip.f_hat_iter[k] = NFFT_K(0.0);


  /** check for valid parameters before calling any trafo/adjoint method */
  error_str = NFFT(check)(&p);
  if (error_str != 0) {
    printf("Error in nfft module: %s\n", error_str);
    p_out.signal_length = 0;
    return p_out;
  }

  NFFT_CSWAP(ip.f_hat_iter, p.f_hat);
  NFFT(trafo)(&p);
  NFFT_CSWAP(ip.f_hat_iter, p.f_hat);

  SOLVER(before_loop_complex)(&ip);
  printf("\n Residual r=%" NFFT__FES__ "\n", ip.dot_r_iter);

  for (l = 0; l < niter; l++)
  {
    SOLVER(loop_one_step_complex)(&ip);
    NFFT_CSWAP(ip.f_hat_iter, p.f_hat);
    NFFT(trafo)(&p);
    NFFT_CSWAP(ip.f_hat_iter, p.f_hat);

    printf("\n Residual r=%"  NFFT__FES__ "\n", ip.dot_r_iter);
  }

  p_out.signal = (fftw_complex*)calloc(p.N_total, sizeof(fftw_complex));
  for (k = 0; k < p.N_total; k++) p_out.signal[k] = ip.f_hat_iter[k];
  p_out.signal_length = p.N_total;
  p_out.locations = (Location*)calloc(p.N_total, sizeof(Location));
  even_layout(&p_out);


  SOLVER(finalize_complex)(&ip);
  NFFT(finalize)(&p);

  return p_out;
}

// Backward FFT
static Points backward_nnfft_1d(Points points, int new_sig_len)
{
  int j,k;                              /**< index for nodes and freqencies   */
  nnfft_plan my_plan;                    /**< plan for the nfft                */
  Points p_out;

  
  int N[1];
  N[0] = points.signal_length;	// what is it?

  /** init an one dimensional plan */
  nnfft_init(&my_plan, 1, points.signal_length, new_sig_len, N);

  /** init pseudo random nodes */
  for(j=0;j<my_plan.M_total;j++) {
    my_plan.x[j] = -0.5+j*1/(float)my_plan.M_total;
  }
  /** init pseudo random nodes */
  for(j=0;j<my_plan.N_total;j++) {
    my_plan.v[j]= points.locations[j].norm_location;
  }

  nnfft_precompute_one_psi(&my_plan);

  /** init pseudo random Fourier coefficients and show them */
  for (k=0; k<my_plan.N_total; ++k) my_plan.f_hat[k] = points.signal[k];

  /** direct trafo and show the result */
  nnfft_trafo_direct(&my_plan);

  p_out.signal = (fftw_complex*) calloc(my_plan.M_total, sizeof(fftw_complex));
  for (k=0; k<my_plan.M_total; ++k) p_out.signal[k] = my_plan.f[k];
  p_out.signal_length = my_plan.M_total;
  p_out.locations = (Location*) calloc(my_plan.M_total, sizeof(Location));
  even_layout(&p_out);

  /** finalise the one dimensional plan */
  nnfft_finalize(&my_plan);


  return p_out;
}

static Points load_test_signal() {
  int i;
  double test_signal[] = { -1, -0.5, 0, -1.5, 3, 1.5, 1, -2.5, 0, 1, 0.2, -1, 2, 3, 5, 2 };
  Points p;

  p.signal_length = sizeof(test_signal)/sizeof(double);
  p.signal = (fftw_complex*)calloc(p.signal_length, sizeof(fftw_complex));  

  for (i=0; i<p.signal_length; ++i) p.signal[i] = test_signal[i];

  p.locations = (Location*)calloc(p.signal_length, sizeof(Location));
  for (i=0; i<p.signal_length; ++i) {
    p.locations[i].physical_location = i;
    p.locations[i].norm_location = -0.5+i*1/(float)p.signal_length;
  }

  return p;

}

static Points vis2points(double *values, double *uv, int len) {
  int i;
  double max_uv_length, min_uv_length;
  Points p;


  p.signal_length = len;

  // First find out min/max
  max_uv_length = -1; min_uv_length = 1e39;
  for (i=0; i<len; ++i) {
    if ( uv[i] > max_uv_length ) max_uv_length = uv[i];
    if ( uv[i] < min_uv_length ) min_uv_length = uv[i];

  } 

  //printf("Max channel %f Max UV %f\n", max_channel, max_uv_length);

  p.signal = (fftw_complex*)calloc(2*p.signal_length, sizeof(fftw_complex));
  p.locations = (Location*)calloc(2*p.signal_length, sizeof(Location));
  for (i=0; i<len; ++i) {
    p.locations[i+p.signal_length].physical_location = uv[i];
    p.signal[i+p.signal_length] = values[2*i]+I*values[2*i+1];
    p.locations[p.signal_length-i-1].physical_location = -uv[i];
    p.signal[p.signal_length-i-1] = values[2*i]+I*values[2*i+1];
  }


  // Normalize. Avoid the Nyquist frequency which would be at -0.5. Therefore bring the max_uv_length just inside 0.5
  for (i=0; i<p.signal_length; ++i) {	// -max_uv_length .. max_uv_length
    p.locations[p.signal_length+i].norm_location = p.locations[p.signal_length+i].physical_location/(max_uv_length*1.01)/2;
    p.locations[p.signal_length-i-1].norm_location = -p.locations[p.signal_length+i].norm_location;
  }

  p.signal_length *= 2;


  return p;
}


static Points load_vis_points(const char *in_file, int channel) {
  const int limiting_factor = 50;
  FILE *input;
  int i, j;
  double valr, vali, chan, uv;
  double max_channel, max_uv_length, min_uv_length;
  Points p;

  max_channel = -1; max_uv_length = -1; min_uv_length = 1e39;

  // First find out how many in channel
  p.signal_length = 0;
  input = fopen(in_file,"r");   // flat input file
  i = 0;
  while ( fscanf(input,"%lf %lf %lf %lf\n",&chan, &uv, &valr, &vali) != EOF ) { 
    if ( chan > max_channel ) max_channel = chan;

    if ( chan == channel && i%limiting_factor == 0 ) { 		// factor 50 strips down values
      if ( uv > max_uv_length ) max_uv_length = uv;
      if ( uv < min_uv_length ) min_uv_length = uv;
  
      ++p.signal_length;
      
    }
    ++i;
  } 
  fclose(input);

  printf("Max channel %f Max UV %f\n", max_channel, max_uv_length);

  p.signal = (fftw_complex*)calloc(p.signal_length, sizeof(fftw_complex));
  p.locations = (Location*)calloc(p.signal_length, sizeof(Location));
  input = fopen(in_file,"r");   // flat input file
  i = 0; j = 0;
  while ( fscanf(input,"%lf %lf %lf %lf\n",&chan, &uv, &valr, &vali) != EOF ) {    
    if ( chan == channel && i%limiting_factor == 0 ) {
      p.signal[j] = valr+I*vali;
      p.locations[j].physical_location = uv;
      ++j;
    }
    ++i;
  }
  fclose(input);

 
  return p;
}

static void free_points(Points p) {
  free(p.locations); free(p.signal);
}


static void save_points(Points p, const char *name) {
  int i;
  FILE *f;

  f = fopen(name,"w");
  for (i=0; i<p.signal_length; ++i)
    fprintf(f, "%f %f\n", p.locations[i].norm_location, cabs(p.signal[i]));
  fclose(f);
}

static void save_signal(fftw_complex *sig, int len, const char *name) {
  int i;
  FILE *f;

  f = fopen(name,"w");
  for (i=0; i<len; ++i)
    fprintf(f, "%d %f\n", i, cabs(sig[i]));
  fclose(f);
}

static void print_points(Points p) {
  int i;
  printf("%d\n", p.signal_length);
  for (i=0; i<p.signal_length; ++i) 
    printf("%f %f %f %f\n", p.locations[i].physical_location, p.locations[i].norm_location, creal(p.signal[i]), cimag(p.signal[i]));
}


// This is preserved when go FFT -> gridded signal -> gridded FFT
// Also, the sum_points of "gridded_signal" will be the same as FFT multiplied by signal length
static double sum_points(Points p) {
  int i;
  double c = 0;
 
  for (i=0; i<p.signal_length; ++i)
    c += creal(p.signal[i]*conj(p.signal[i]));

  return c;
}


void nfft_equispace(double **vis, double **uv, int len, int *new_len) {
  int i;
  Points signal;
  double *new_vis;
  double *new_uv, max_uv_length, max_norm_location;
  Points fft = vis2points(*vis, *uv, len);

  max_norm_location = fft.locations[fft.signal_length-1].norm_location;
  max_uv_length = fft.locations[fft.signal_length-1].physical_location; 

  save_points(fft, "orig.dat");

  printf("Vis points sum %lf\n", sum_points(fft));

  puts("back"); fflush(stdout);
  signal = backward_nnfft_1d(fft, *new_len); free_points(fft);

  printf("Signal len %d\n", signal.signal_length);
  printf("signal %lf\n", sum_points(signal));

  puts("forward"); fflush(stdout);
  fft = forward_nfft_1d(signal, 10);  free_points(signal);

  save_points(fft, "fft.dat");
  printf("Back fft sum %lf\n", sum_points(fft));

  new_vis = (double*)calloc(2*fft.signal_length, sizeof(double));

  for (i=0; i<fft.signal_length; ++i) {	// Flatten complex
    new_vis[2*i] = creal(fft.signal[i]);
    new_vis[2*i+1] = cimag(fft.signal[i]);
  }

			// What about -ve frequencies and DC
  new_uv = (double*)calloc(fft.signal_length, sizeof(double));		
  for (i=0; i<fft.signal_length; ++i)
    new_uv[i] = fft.locations[i].norm_location/max_norm_location*max_uv_length;

  free(*vis); *vis = new_vis;
  free(*uv); *uv = new_uv;

  free_points(fft);

  *new_len = fft.signal_length;;

}

void nfft_equispace_test(double **vis, double **locations, int len, int *new_len) {
  Points test_signal, fft, signal;
  int i;

  test_signal.signal_length = len;
  test_signal.signal = (fftw_complex*)calloc(len, sizeof(fftw_complex));
  test_signal.locations = (Location*)calloc(len, sizeof(Location));
  for (i=0; i<len; ++i) {
    test_signal.signal[i] = (*vis)[2*i]+I*(*vis)[2*i+1];
    test_signal.locations[i].physical_location = i;
    test_signal.locations[i].norm_location = (*locations)[i];
  }

  printf("Test signal sum %lf\n", sum_points(test_signal));

  fft = forward_nfft_1d(test_signal, 3);
  printf("FFT sum %lf\n", creal(sum_points(fft)));
  for (i=0; i<fft.signal_length; ++i) printf("%f %f\n", creal(fft.signal[i]), cimag(fft.signal[i]));

  puts("back");
  signal = backward_nnfft_1d(fft, fft.signal_length);

  for (i=0; i<signal.signal_length; ++i) printf("%f %f\n", creal(signal.signal[i]), cimag(signal.signal[i]));


  printf("Signal len %d\n", signal.signal_length);


  puts("forward"); fflush(stdout);
  fft = forward_nfft_1d(signal, 3);
  for (i=0; i<fft.signal_length; ++i) printf("%f %f\n", creal(fft.signal[i]), cimag(fft.signal[i]));
}


// MAIN

#define TEST
int main(void) {
  int i, len;
  Points test_signal, fft, signal;
  double *locations, *values;


#ifdef TEST
  test_signal = load_test_signal();
  locations = (double*)calloc(test_signal.signal_length, sizeof(double));
  for (i=0; i<test_signal.signal_length; ++i) locations[i] = test_signal.locations[i].norm_location; 
  values = (double*)calloc(2*test_signal.signal_length, sizeof(double));
  for (i=0; i<test_signal.signal_length; ++i) {         // Flatten complex numbers
    values[2*i] = creal(test_signal.signal[i]);
    values[2*i+1] =  cimag(test_signal.signal[i]);
  }
  len = test_signal.signal_length;
  nfft_equispace_test(&values, &locations, test_signal.signal_length, &len);
  printf("%d\n", len);

#else

  fft = load_vis_points("../solver/points.dat", 230);
  locations = (double*)calloc(fft.signal_length, sizeof(double));
  for (i=0; i<fft.signal_length; ++i) locations[i] = fft.locations[i].physical_location;
  len = 2*fft.signal_length;
  values = (double*)calloc(2*fft.signal_length, sizeof(double));
  for (i=0; i<fft.signal_length; ++i) {		// Flatten complex numbers
    values[2*i] = creal(fft.signal[i]);
    values[2*i+1] =  cimag(fft.signal[i]);
  }
  nfft_equispace(&values, &locations, fft.signal_length, &len);
  printf("%d\n", len);
  


#endif

  return 1;
}

