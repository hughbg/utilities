import os, sys

sys.path.append('..')
import make_header

disk_names = [ "ledastorage", "longterm" ]
ledaovro_names = [ "ledaovro1", "ledaovro2", "ledaovro3", "ledaovro4", "ledaovro5", "ledaovro6", "ledaovro7", "ledaovro8", "ledaovro9", "ledaovro10", "ledaovro11","ledaovro12" ]
data_names = [ "data1", "data2" ]
onetwo_names = [ "one", "two" ]
DADA_HEADER_SIZE = 4096
frequency_vals = [ 31.308, 33.924, 36.54, 39.156, 41.772, 44.388, 47.004, 49.62, 52.236, 54.852, 57.468, 60.084, 62.7, 65.316, 67.932, 70.548, 73.164, 75.78, 78.396, 81.012, 83.628, 86.244 ]


def is_integer(x): return int(x) == x

def extract_obs_offset_from_name(fname): 
  return int(os.path.basename(fname)[20:36])

def extract_obs_offset_in_file(fname):
  f = open(fname, 'rb')
  headerstr = f.read(DADA_HEADER_SIZE)
  f.close()
  if len(headerstr) < DADA_HEADER_SIZE: return "UNKNOWN"
  for line in headerstr.split('\n'):
    key, value = line.split()
    if key == "OBS_OFFSET": return int(value)
  
  return "UNKNOWN"

class FileInfo(object):

  def __init__(self, fname):
    hedr = make_header.make_header(fname,write=False,warn=False)
    if hedr["TIME_OFFSET"] == "UNKNOWN" or hedr["N_SCANS"] == "UNKNOWN" or hedr["INT_TIME"] == "UNKNOWN" or hedr["LST"] == "UNKNOWN": 
      t_offset = n_scans = int_time = utc_date = utc_time = 0
    else:
      t_offset = int(hedr["TIME_OFFSET"])	
      n_scans = float(hedr["N_SCANS"])
      int_time = int(hedr["INT_TIME"])

    self.file_name = fname
    self.start_time = t_offset
    self.end_time = t_offset+int(n_scans)*int_time
    self.scans = n_scans
    self.utc_date = hedr["DATE"]
    self.utc_time = hedr["TIME"]
    self.lst = float(hedr["LST"])
    self.freq = float(hedr["FREQCENT"])

    obs1 = extract_obs_offset_from_name(fname)
    obs2 = extract_obs_offset_in_file(fname)
    if obs1 != obs2 and obs1 != "UNKNOWN" and obs2 !="UNKNOWN":
      print "Consistency Error: OBS_OFFSET in file doesn't match name"

    if not is_integer(n_scans): print "CONSISTENCY ERROR, scan:",n_scans
    if not is_integer((self.end_time-self.start_time)/9.0): print "CONSISTENCY ERROR: not 9 sec"

# Gather the file info for all files that have different frequency but the same observation time.
class Observation(object):

     

  def __init__(self, basename):

    self.basename = basename
    self.files = []
    self.start_time_present = []
    self.scans_present = []
    self.freq_present = []
    self.frequency_adjusted = False

    # build and try different names in different directories
    for disk in disk_names:
      for ledaovro in ledaovro_names:
        for data in data_names:
          for onetwo in onetwo_names:
            file_name  = "/nfs/"+disk+"/"+ledaovro+"/"+data+"/"+onetwo+"/"+basename
            if os.access(file_name,os.R_OK): 
              self.files.append(FileInfo(file_name))

    # Gather 
    for f in self.files:
      if f.scans not in self.scans_present: self.scans_present.append(f.scans)
      if f.start_time not in self.start_time_present: self.start_time_present.append(f.start_time)

    if len(self.start_time_present) > 1:
      print "Error: Files with same name have different internal time. Basename:",basename
      sys.exit(1)
    else:

      self.start_time = self.start_time_present[0]
      self.end_time = self.files[0].end_time
      self.lst = self.files[0].lst
      self.utc_date = self.files[0].utc_date
      self.utc_time = self.files[0].utc_time


    self.ok = ( len(self.scans_present) == 1 and is_integer(self.scans_present[0]) and int(self.scans_present[0]) > 0 and self.all_frequencies_present() )

  def find_close_frequency(self, num):
    for fr in frequency_vals:
      if abs(num-fr) < 0.0000001: return True
    return False

  def all_frequencies_present(self):
    num_present = 0
    for f in self.files:
      if f.freq in frequency_vals: num_present += 1
  
    if num_present == 0:	# Trying adjusting
      for f in self.files:
        if self.find_close_frequency(f.freq+5.244): num_present += 1
      if num_present > 0:
        self.frequency_adjusted = True
        for f in self.files: 
          f.freq += 5.244
    return (num_present == 22)

  def report(self):
    frequencies = []
    scans = []
    for f in self.files:
      if f.freq not in frequencies: frequencies.append(f.freq)
      if f.scans not in scans: scans.append(f.scans)
    return frequencies, scans, self.frequency_adjusted

  def contains_file(self, file_name):
    for f in self.files:
      if f.file_name == file_name: return True
    return False

  def has_freq(self, frequencies):
    has = []
    for f in self.files:
      if f.freq in frequencies: has.append(f.freq)
    return has

# A sequence of observations
class Sequence(object):

  def __init__(self, obs):
    self.sets = [obs]

  def add(self, obs):
    self.sets.append(obs)

  def start_time(self): 
    return self.sets[0].start_time 

  def end_time(self): 
    return self.sets[-1].end_time 

  def last_ok(self):
    return self.sets[-1].ok

  def check_consistent(self):
    for i in range(1,len(self.sets)):
      if self.sets[i-1].end_time != self.sets[i].start_time: return False

    for fs in self.sets:
      if fs.ok != self.sets[0].ok: return False

    return True

  def start_utc_time(self): 
    return self.sets[0].utc_time

  def start_utc_date(self): 
    return self.sets[0].utc_date

  def time_length(self):
    return self.sets[-1].end_time-self.sets[0].start_time

  def ok(self): return self.sets[0].ok   # If consistent, then they are all the same, so this one is representative

  def start_lst(self): return self.sets[0].lst

  def start_basename(self):
    return self.sets[0].basename

  def get_obs(self, index):
    return self.sets[index]
 
  def length(self): return len(self.sets)

  def report(self):
    adjusted = False
    out_str = ""
    num_freq = []
    num_this_scans = [ ]
    for obs in self.sets:
      frequencies, scans, adj = obs.report()
      if adj: adjusted = True
      if len(frequencies) not in num_freq:
        num_freq.append(len(frequencies))
      for sc in scans:
        if sc not in num_this_scans: num_this_scans.append(sc)
     
    if len(num_freq) == 1 and num_freq[0] == 22:
      out_str += "All frequencies are present. "
    else: 
      out_str += "Some frequencies missing. Time steps have these numbers of frequencies:"
      for nf in num_freq: out_str += " "+str(nf)
      out_str += "."

    out_str += "<br>Some frequencies must be adjusted by 5.244. "

    out_str += "<br>These numbers of scans are present:"
    for sc in num_this_scans:
      out_str += " "+str(sc)
        
    return out_str

  def contains_file(self, file_name):
    index = 0
    for obs in self.sets:
      if obs.contains_file(file_name): return index
      index += 1
    return -1

  def print_out(self):
    i = 0
    for obs in self.sets:
      print i, obs.start_time, obs.end_time, obs.basename
      i += 1


