import matplotlib.pyplot as plt
import numpy as np
import sys

data = np.loadtxt(sys.argv[1])
dim = int(data[0])
data = data[1:].reshape(dim, dim)

plt.imshow(data)
plt.savefig("x.png")
plt.colorbar()
plt.show()

