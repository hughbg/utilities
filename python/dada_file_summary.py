import zlib
import numpy as np
import os, sys
import make_header
from sets import Set

HEADER_SIZE = 4096

def dada_format(filename):
  if is_dada_h5(filename): return "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX"
  try:
    f = open(filename, 'rb')
    headerstr = f.read(HEADER_SIZE)
    f.close()
  except: return "UNKNOWN"

  for line in headerstr.split('\n'):
    l = line.split()
    if len(l) >= 2 and l[0] == "DATA_ORDER": return l[1]
  return ""  

def is_positive_integer(s):
  if s == "UNKNOWN": return False
  else:
    x = float(s)
    return x > 0 and int(x) == x

def only_positive_integers(values):
  for v in values:
    if not is_positive_integer(v): return False
  return True

def one_value(simple_set):
  for s in simple_set:
    if len(s) > 0: return s
  return "UNKNOWN"


def append_paths(current_paths, add_ons):
  new_paths = []
  for curr in current_paths:
    for add in add_ons:
      new_paths.append(curr+"/"+add)

  return new_paths

def look_for_repositories(path_spec, dirs):
  disk_names = [ "longterm", "jbod00", "jbod01" ]
  ledaovro_names = [ "ledaovro1", "ledaovro2", "ledaovro3", "ledaovro4", "ledaovro5", "ledaovro6", "ledaovro7", "ledaovro8", "ledaovro9", "ledaovro10", "ledaovro11","ledaovro12", "europa1", "europa2" ]
  data_names = [ "data1", "data2" , "data3" ]
  onetwo_names = [ "one", "two" ]

  # build and try different names in different nfs directories. Assuming running on head node.

  sub_dirs = path_spec.split("/")

  possibilities = [ "" ]
  for d in sub_dirs:
    if d == "DISK": possibilities = append_paths(possibilities, disk_names)
    elif d == "LEDAOVRO_N": possibilities = append_paths(possibilities, ledaovro_names)
    elif d == "ONE_TWO": possibilities = append_paths(possibilities, onetwo_names)
    elif d == "DATA_N": possibilities = append_paths(possibilities, data_names)
    else:
      if len(d) > 0: possibilities = append_paths(possibilities, [d])

  for pp in possibilities:
    if os.access(pp,os.X_OK) and pp not in dirs: dirs.append(pp)
    if os.access(pp, os.F_OK) and not os.access(pp,os.X_OK): print "Can't access directory", pp

  if len(pp) == 0: print "Nothing from path spec", path_spec


def size_uncompressed(infilename):
	"""
	Count the number of bytes in a compressed file when it is uncompressed, thus getting its true size
	"""

	print "Unzipping", infilename
	# From Ben's dadazip code.
	nblock = 4

	count = 0
	infilesize = os.path.getsize(infilename)
	if infilesize <= 4096: return infilesize 

 	infilesize -= HEADER_SIZE
	decompress = zlib.decompressobj()
	with open(infilename, 'rb') as fin:
		
		header = fin.read(HEADER_SIZE)
		count += HEADER_SIZE

		max_block_size = (infilesize-1) // nblock + 1
		if max_block_size <= 0: return 
		for offset in xrange(0, infilesize, max_block_size):
			block_size = min(max_block_size, infilesize-offset)
			data = fin.read(block_size)
			insize = len(data)
			try:
			  data = decompress.decompress(data)
 			except: return 0
			count += len(data)

		count += len(decompress.flush())

	return count

def is_dada_zipped(name):
  return name.find("201") >= 0 and name[-8:] == ".dadazip"

def is_dada_h5(name):
  return name.find("201") >= 0 and name[-7:] == ".dadah5"


def is_dada(name):
  return name.find("201") >= 0 and name[-5:] == ".dada"

def get_file_size(filename, zip_file_list):
  """
	There are 3 chances to get the file size.
	1. It's not a zipped file - just "ls"
	2. It's zipped and it's in the file_size list - read the file list.
	3. It's zipped and it'll have to be unzipped to get the true size.
  """

  if is_dada_zipped(filename): 
    if zip_file_list: 
      for line in open(zip_file_list,"r"):
        info = line[:-1].split(",")
        if info[0] == filename: 
          print filename,"got size from catalog",info[6]
          return info[6]

    return size_uncompressed(filename)
  else: return os.path.getsize(filename)


def format_n_scans(n):
  if n == "UNKNOWN": return n
  else: return ( "%.6f" % n)

def process_directory(dir_name, file_sizes_file=None):
  new_catalog = open("catalog.txt", "w")
  print "Process",dir_name
  if os.path.isdir(dir_name):
    for filename in os.listdir(dir_name):
      process_file_name = ""
      if is_dada_zipped(filename):	
        # See if there is an equivalent DADA. Ignore this one if there is.
        dada_name = filename[:-3]      # remove "zip"
        if not os.path.isfile(dada_name):
          process_file_name = dir_name+"/"+filename
      elif is_dada(filename) or is_dada_h5(filename): process_file_name = dir_name+"/"+filename

      if process_file_name != "" and dada_format(process_file_name) in [ "REG_TILE_TRIANGULAR_2x2", "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" ] :
        sys.stdout.flush()	 
        try:
          if is_dada_zipped(filename): 
            size = get_file_size(process_file_name, file_sizes_file)
            info = make_header.make_header(process_file_name, False, False, size)
            new_catalog.write(dir_name+"/"+filename+","+info["DATE"]+","+info["TIME"]+","+info["LST"]+","+info["DATA_ORDER"]+","+str(info["FREQCENT"])+","+str(size)+","+format_n_scans(info["N_SCANS"])+"\n")
          else:
            info = make_header.make_header(process_file_name, False, False)
            new_catalog.write(dir_name+"/"+filename+","+info["DATE"]+","+info["TIME"]+","+info["LST"]+","+info["DATA_ORDER"]+","+str(info["FREQCENT"])+","+str(info["FILE_SIZE"])+","+format_n_scans(info["N_SCANS"])+"\n")
        except: 
          new_catalog.write(dir_name+"/"+filename+",UNKNOWN,UNKNOWN,UNKNOWN,UNKNOWN,UNKNOWN,UNKNOWN,UNKNOWN\n")     # At least the file gets listed

  new_catalog.close()
 
def add_frequency_count():
  # Different file - frequency count
  print "Calculating freq count"

  info = []	# List of records, one for each time stamp of a file
  for line in open("catalog.txt", "rU"):
    l = line[:-1].split(",")
    path = l[0].split("/")
    filename = path[6]
    if is_dada(filename): filename = filename[:-5]	# strip extension
    else: filename = filename[:-8]
    
    frequency = l[5]
    n_scans = l[7]
    
    # find the file in info
    where = -1
    for i in range(len(info)):
      if info[i][0] == filename: 
        where = i
        break
    
    # Insert information about the file in info
    if where >= 0:
      info[where][1].add(str(frequency)+";"+str(n_scans))
      info[where][2].add(n_scans)
    else:
      # Generate a new record which goes on the list
      new_info = ( filename, Set([str(frequency)+";"+str(n_scans)]), Set([n_scans]), Set([]) )    # Create the set from a list
      info.append(new_info)

  # File format: Top level in a line is comma separated. filename (no ext), how many scans, unique scans, code
  # How many scans is colon separated. Each entry is the number of scans for a frequency, of the form <freq>;<n scans>
  # Unique scans is colon separated, it is a list of #scans present
  # Code is GOOD,BAD_{1,2,3,4}
  cross_count = open("tmp_freq_count.txt","w")
  for index, inf in enumerate(info):
    fline = inf[0]+","
    for x in inf[1]: fline += x+":"
    fline = fline[:-1]
    fline += ","
    for x in inf[2]: fline += x+":"
    fline = fline[:-1]
    if not only_positive_integers(inf[2]): 
      fline += ",BAD_2"	     # Number of scans of one of the set is not integer
      info[index][3].add("BAD_2")
    elif len(inf[2]) != 1: 
      fline += ",BAD_1"	# Number of scans in the set aren't all the same
      info[index][3].add("BAD_1")
    elif len(inf[1]) < 22: 
      fline += ",BAD_3"		# There are not 22 frequencies present
      info[index][3].add("BAD_3")
    elif len(inf[1]) > 22: 
      fline += ",BAD_4"         # There are not 22 frequencies present
      info[index][3].add("BAD_4")
    else:
      fline += ",GOOD"
      info[index][3].add("GOOD")
 
    cross_count.write(fline+"\n")

  print "Adding frequency code"
  # Do a partial join of the files. Want the frequency code in catalog too.
  new_catalog= open("x.txt","w")
  for line in open("catalog.txt","rU"):
    path = line[:-1].split(",")[0]
    short_name = path.split("/")[-1]
    if is_dada(short_name): short_name = short_name[:-5]	# strip extension
    else: short_name = short_name[:-8]

    # Find the frequency code
    for inf in info:
      if short_name == inf[0]:
        if len(inf[3]) == 0: print line
        new_catalog.write(line[:-1]+","+one_value(inf[3])+"\n")
  new_catalog.close()
  os.system("sort -t , -k 2,3  -r x.txt > catalog.txt")    # Reverse date/time order
  os.system("sort -t , -k1 -r tmp_freq_count.txt > freq_count.txt")
        

try: os.remove("all_catalog.txt")
except: pass
try: os.remove("all_freq_count.txt")
except: pass


repositories = []
      # build and try different names in different nfs directories. Assuming running on head node.
look_for_repositories("/nfs/DISK/LEDAOVRO_N/DATA_N/ONE_TWO", repositories)

for repository in repositories:
  process_directory(repository, "saved_catalog.txt")
      # Catenate this catalog onto the global one
  os.system("cat catalog.txt >> all_catalog.txt")

os.rename("all_catalog.txt", "catalog.txt")

add_frequency_count()

