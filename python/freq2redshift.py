import sys

f21cm = 1.42040575177e9

def f2z(f):
    return f21cm / (f*1e6) -1

print f2z(float(sys.argv[1]))
