#!/usr/bin/env python
import numpy as np
import sys, os, cmath
from mpl_toolkits.mplot3d import Axes3D
import scipy.stats
import scipy.signal
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

DADA_HEADER_SIZE = 4096

def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header

def make_dada_index(stand1, stand2):    # Ordering of stands is taken care of so they can be input in any order
  if stand2 > stand1:
    return stand2*(stand2+1)//2+stand1
  else: return stand1*(stand1+1)//2+stand2


outrig_ids = [252, 253, 254, 255, 256]
outrig_nbaseline = sum(outrig_ids) # TODO: This only works because the outriggers are the last antennas in the array
filesize = os.path.getsize(sys.argv[1]) - DADA_HEADER_SIZE
print "filesize:     ", filesize
 
outf = open(sys.argv[2], "wb")
f = open(sys.argv[1],"rb")
header_bytes = f.read(DADA_HEADER_SIZE)
outf.write(header_bytes)
header = parse_dada_header(header_bytes)
nchan  = header['NCHAN']
nant   = header['NSTATION']
npol   = header['NPOL']
navg   = header['NAVG']
bps    = header['BYTES_PER_SECOND']
df     = header['BW']*1e6 / float(nchan)
cfreq  = header['CFREQ']*1e6
utc_start = header['UTC_START']
assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
freq0 = cfreq-nchan/2*df
freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
print "bytes per sec:", bps
print "navg:     ", navg
print "nchan:    ", nchan
print "npol:     ", npol
print "nstation: ", nant
nbaseline = nant*(nant+1)//2
print "nbaseline:", nbaseline
noutrig_per_full = int(navg / df + 0.5)
print "noutrig_per_full:", noutrig_per_full
full_framesize   = nchan*nbaseline*npol*npol
print "full_framesize:  ", full_framesize*8
#print nbaseline, noutrig_per_full, outrig_nbaseline
outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
print "outrig_framesize:", outrig_framesize*8
tot_framesize    = (full_framesize + outrig_framesize)
print " tot_framesize:  ",  tot_framesize*8
ntime = float(filesize) / (tot_framesize*8)
print "ntime:", ntime
frame_secs = int(navg / df + 0.5)
print "Frame secs:", frame_secs
time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
print "Time offset:", time_offset

def fix_data(data):
  data = data.reshape((nchan,nbaseline,npol,npol))
  for st1 in range(nant):
    for st2 in range(st1+1):
      bl_index = make_dada_index(st1, st2)
      for chan in range(nchan):
        for p1 in range(2):
          for p2 in range(2):
            if st2%2 == 1 and (st2-st1+1)%2 == 0:
              #print st1, st2, "->", st1+1, st2-1
              other_bl_index = make_dada_index(st1+1, st2-1)
              tmp = data[chan, bl_index, p1, p2]
              data[chan, bl_index, p1, p2] = data[chan, other_bl_index, p1, p2]
              data[chan, other_bl_index, p1, p2] = tmp


for t in xrange(int(ntime)):
  print "Time ",t
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)

  fix_data(full_data)
  full_data.tofile(outf)
  outrig_data.tofile(outf)


f.close()
outf.close()

