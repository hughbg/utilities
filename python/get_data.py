#!/usr/bin/env python
import numpy as np
import sys, os, cmath
from mpl_toolkits.mplot3d import Axes3D
import scipy.stats
import scipy.signal
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

DADA_HEADER_SIZE = 4096

def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header


outrig_ids = [252, 253, 254, 255, 256]
outrig_nbaseline = sum(outrig_ids) # TODO: This only works because the outriggers are the last antennas in the array
filesize = os.path.getsize(sys.argv[1]) - DADA_HEADER_SIZE
print "filesize:     ", filesize
 

f = open(sys.argv[1],"rb")
header = parse_dada_header(f.read(DADA_HEADER_SIZE))
nchan  = header['NCHAN']
nant   = header['NSTATION']
npol   = header['NPOL']
navg   = header['NAVG']
bps    = header['BYTES_PER_SECOND']
df     = header['BW']*1e6 / float(nchan)
cfreq  = header['CFREQ']*1e6
utc_start = header['UTC_START']
assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
freq0 = cfreq-nchan/2*df
freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
print "bytes per sec:", bps
print "navg:     ", navg
print "nchan:    ", nchan
print "npol:     ", npol
print "nstation: ", nant
nbaseline = nant*(nant+1)//2
print "nbaseline:", nbaseline
noutrig_per_full = int(navg / df + 0.5)
print "noutrig_per_full:", noutrig_per_full
full_framesize   = nchan*nbaseline*npol*npol
print "full_framesize:  ", full_framesize*8
#print nbaseline, noutrig_per_full, outrig_nbaseline
outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
print "outrig_framesize:", outrig_framesize*8
tot_framesize    = (full_framesize + outrig_framesize)
print " tot_framesize:  ",  tot_framesize*8
ntime = float(filesize) / (tot_framesize*8)
print "ntime:", ntime
frame_secs = int(navg / df + 0.5)
print "Frame secs:", frame_secs
time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
print "Time offset:", time_offset

bl = [ None for i in range(32896) ]
index = 0
for st1 in range(256):
  for st2 in range(0, st1+1):
    bl[st1*(st1+1)//2+st2] = ( st2, st1)     
    index += 1

for t in xrange(int(1)):
  print "Time ",t
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
  full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
  outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))


  
  for bl_index in range(32896):
    all_zero = True
    for chan in range(109):
      for pi in range(2):
        for pj in range(2):
          if abs(full_data[chan,bl_index,pi,pj]) > 0: all_zero = False
    if all_zero: print bl[bl_index]

#  print "X", abs(full_data[31,bl_index,1,1])*3.2e-2
 # print "X", abs(full_data[31,bl_index,0,1])*3.2e-2
 # print "X", abs(full_data[31,bl_index,1,0])*3.2e-2
 # print "X", abs(full_data[31,bl_index,1,1])*3.2e-2

f.close()

