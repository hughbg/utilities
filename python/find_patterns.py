import os, sys
import numpy as np
import scipy.stats, scipy.signal

NUM_STANDS = 256
NUM_CHANNELS = 109
DADA_HEADER_SIZE = 4096
outrig_nbaseline = 1270


def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string
      
def rms(x):
  sq_sum = 0
  for val in x: 
    if not np.isnan(val): sq_sum += val*val
  return np.sqrt(sq_sum/len(x))
  

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header
      
def make_dada_index(stand1, stand2):    # Ordering of stands is taken care of so they can be input in any order
  if stand2 > stand1:
    return stand2*(stand2+1)//2+stand1
  else: return stand1*(stand1+1)//2+stand2

def sign(x):
  if x < 0: return -1 
  elif x > 0: return 1
  else: return 0


def alternating(a, stand):
  """deviation = sign(a[0]-a[1])

  for i in range(stand-1):
    print i,a[i+1],deviation		
    new_deviation = sign(a[i+1]-a[i])
    if deviation == new_deviation: return False 
    deviation = new_deviation"""

  # Zero has to be off by 1
  if a[stand-1] == 0: where_zero = stand-1
  elif a[stand+1] == 0: where_zero = stand+1
  else: return False

  median_left = np.median(a[where_zero-50:where_zero])
  median_right = np.median(a[where_zero+1:where_zero+1+50])
  if median_left < median_right:
    tmpm = median_left
    median_left = median_right
    median_right = tmpm

  #print where_zero, median_left/median_right
  if median_left/median_right > 2: return True
  else: return False

      
def plot_ant(data, ant, ndumps):

  dada_index = ant*(ant+1)//2+ant
 
  # Set up data arrays for plot
  x = [ i for i in range(NUM_CHANNELS) ]
  y = [ 0.0 for i in range(NUM_CHANNELS) ]
  
  # Plot many lines
  for i in range(ndumps):
    for j in range(NUM_CHANNELS):
      y[j] = abs(data[i][j][dada_index][0][0])
    plt.plot(x, y, color="red")
    for j in range(NUM_CHANNELS):
      y[j] = abs(data[i][j][dada_index][1][1])
      print y[j]
    plt.plot(x, y, color="blue")
    
  plt.ylim(0)

  plt.xlabel('Channel')
  plt.ylabel('Power')
  #ylabel('voltage (mV)')
  plt.title("All for stand "+str(ant)+" (0-based)")
  plt.grid(True)
  plt.savefig("auto_amp_"+str(ant)+".png")
  
  plt.clf()

def correlate(x, y):
  mx = np.mean(x)
  my = np.mean(y)
  bottom_x = np.sqrt(np.sum([ (val-mx)*np.conj(val-mx) for val in x ]))
  bottom_y = np.sqrt(np.sum([ (val-my)*np.conj(val-my) for val in y ]))
  if bottom_x == 0 or bottom_y == 0: return 1000
  top = np.zeros(len(x))
  for i in range(len(x)):
    top[i] = (x[i]-mx)*np.conj(y[i]-my)
  return np.sum(top)/bottom_x/bottom_y

filesize = os.path.getsize(sys.argv[1]) - DADA_HEADER_SIZE 

f = open(sys.argv[1],"rb")
header = parse_dada_header(f.read(DADA_HEADER_SIZE))
nchan  = header['NCHAN']
nant   = header['NSTATION']
npol   = header['NPOL']
navg   = header['NAVG']
bps    = header['BYTES_PER_SECOND']
df     = header['BW']*1e6 / float(nchan)
cfreq  = header['CFREQ']*1e6
utc_start = header['UTC_START']
assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
freq0 = cfreq-nchan/2*df
freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
print "bytes per sec:", bps
print "navg:     ", navg
print "nchan:    ", nchan
print "npol:     ", npol
print "nstation: ", nant
nbaseline = nant*(nant+1)//2
print "nbaseline:", nbaseline
noutrig_per_full = int(navg / df + 0.5)
print "noutrig_per_full:", noutrig_per_full
full_framesize   = nchan*nbaseline*npol*npol
print "full_framesize:  ", full_framesize*8
outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
print "outrig_framesize:", outrig_framesize*8
tot_framesize    = (full_framesize + outrig_framesize)
print " tot_framesize:  ",  tot_framesize*8
ntime = float(filesize) / (tot_framesize*8)
print "ntime:", ntime
frame_secs = int(navg / df + 0.5)
print "Frame secs:", frame_secs
time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
print "Time offset:", time_offset

file_contents = [ [] for i in range(int(ntime)) ]

for t in xrange(int(ntime)):
  print "Time ",t
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
  full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
  outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))

  file_contents[t] = full_data

f.close()



# Check the auto correlations. There are two patterns that can be found: patterns 3 and 4 in the document problem_data.

pattern3 = []
pattern3a = []
pattern4 = []
pattern4a = []
correlations = []
bad_channels = np.zeros(NUM_CHANNELS)
noisy_stands = np.zeros(NUM_STANDS)


print "Analyze stand",
for stand in range(NUM_STANDS):
  print stand,
  sys.stdout.flush()

  dada_index = stand*(stand+1)//2+stand
  
 
  ratios = np.array([ 0.0 for i in range(NUM_CHANNELS) ])
  diff_ratios = np.array([ 0.0 for i in range(NUM_CHANNELS) ])
  
  # Get pattern3/4 from autocorrelations
  for dump in range(int(ntime)):
    for chan in range(NUM_CHANNELS):
      ratios[chan] = diff_ratios[chan] = 0.0
      
      X = abs(file_contents[dump][chan][dada_index][0][0])
      Y = abs(file_contents[dump][chan][dada_index][1][1])
      
      if not (X == 0.0 or Y == 0.0 ): 
	ratios[chan] = X/Y
	diff_ratios[chan] = abs(X-Y)/Y
      
    ratio = ratios.mean()
    if ratio > 1: ratio = 1/ratio
    if ratio < 0.03 and ratio != 0.0 and stand not in pattern3: pattern3.append(stand)      # Antenna near 0
    #if stand in [ 72, 104, 141, 145, 150, 163, 168, 215 ]: print "X", stand, ratio
      
    diff_ratio = diff_ratios.mean()
    #if stand in [ 4,5,20,21,36,37,52,53,68,69,84,85,100,101,116,117,132,133,148,149,164,165,180,181,196,197,212,213,228,229,244,245 ]: print "X",diff_ratios.mean()
    if diff_ratio < 0.001 and diff_ratio != 0.0 and stand not in pattern4: pattern4.append(stand)	  # Antennas identical
      
  # Correlation too high, another manifestation of 4
  for dump in range(int(ntime)):
    where_peak = -1
    baseline_peak = 0
    maxima = np.zeros((NUM_STANDS))
    for other_stand in range(NUM_STANDS):
 
      # We want the maximum value out of all channels, XY cross-correlation first
      values = np.zeros((NUM_CHANNELS))
      for chan in range(NUM_CHANNELS):
        values[chan] = abs(file_contents[dump][chan][make_dada_index(stand,other_stand)][0][1])
      maxima[other_stand] = np.max(values)
      if maxima[other_stand] > baseline_peak:
        baseline_peak = maxima[other_stand]
        where_peak = other_stand
   
    if np.mean(maxima) > 0: ratio = maxima.max()/np.mean(maxima)
    
    if ratio > 15 and where_peak == stand and stand not in pattern4a: 
      pattern4a.append(stand)
  
  # 3a, cut YX
  for dump in range(int(ntime)):
    x = [ 0 for i in range(NUM_STANDS) ]
    for i in range(NUM_STANDS):
      dada_index = make_dada_index(stand,i)
      mmax = 0.0
      for j in range(NUM_CHANNELS):
        if abs(file_contents[0][j][dada_index][1][0]) > mmax: mmax = abs(file_contents[dump][j][dada_index][1][0]) 

      #print i, mmax
      x[i] = mmax

    if stand > 50 and stand < NUM_STANDS-50: 
      if alternating(x, stand) and stand not in pattern3a: pattern3a.append(stand)

  # Spikes. Discount noisy stands.
  auto_signal = np.zeros(NUM_CHANNELS)
  dada_index = make_dada_index(stand,stand)
  for dump in range(int(ntime)):
    for pol in [ 0, 1 ]:
      for chan in range(NUM_CHANNELS): 
        auto_signal[chan] = abs(file_contents[dump][chan][dada_index][pol][pol])
      y = scipy.signal.medfilt(auto_signal, 5)
      z = auto_signal-y
      a, lower, upper = scipy.stats.sigmaclip(z)
      for i in range(NUM_CHANNELS):
        if z[i] < lower or z[i] > upper: bad_channels[i] += 1

  # Noisy. Discount the stands with other patterns, because they can give a high rms.
      noisy_stands[stand] += rms(z/y)

  # Compare XX and YY cross-correlation
  xx = []
  yy = []
  for other_stand in range(NUM_STANDS):
    dada_index = make_dada_index(stand, other_stand)
    if stand != other_stand:
      for ch in range(NUM_CHANNELS):
        if stand < other_stand:
          xx.append(file_contents[0][ch][dada_index][0][0])
          yy.append(file_contents[0][ch][dada_index][1][1])
        else:
          xx.append(file_contents[0][ch][dada_index][1][1])
          yy.append(file_contents[0][ch][dada_index][0][0])

  correlations.append((stand, correlate(xx,yy)))

cs =  sorted(correlations, key=lambda x: x[1])
for x in cs: print x
sys.exit(0)
print 
print
print "Diagnostics (over all dumps)"

print "Pattern 3, one antenna very low"
if len(pattern3) > 0:
  print "Stands",
  for i in pattern3: print i,
  print

print "Pattern 3a, pairs with pattern3 and will cover some of those stands"
if len(pattern3a) > 0:
  print "Stands",
  for i in pattern3a: print i,
  print

print "Pattern 4, antennas almost identical"
if len(pattern4) > 0:
  print "Stands",
  for i in pattern4: print i,
  print
  
print "Pattern 4a, correlation too high; suspected to match with pattern 4"
if len(pattern4a) > 0:
  print "Stands",
  for i in pattern4a: print i,
  print

print "Bad channels"
for i in range(len(bad_channels)):
  print i, bad_channels[i]
print

print "Noisy stands"
for i in range(len(noisy_stands)):
  print i, noisy_stands[i]
  
print "-------------------"
print "FYI: Number patterns"
  
print "Every 8th"
for n in range(1,33): print 8*n-1,
print 

print "16N+4/5"
for n in range(16): print str(16*n+4)+":"+str(16*n+5),
print 



