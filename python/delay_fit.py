#!/usr/bin/env python

"""
  TODO: [Done]Re-do the ttcal-like calibration code
             Do _not_ zero the autocorrelations for the firstguess
               (The maths only makes sense to me if the autos are included,
                  as per the comment in the ttcal source, even though it's
                  subsequently ignored there!)
               Use eigsh.
             Use just one flags array of (nchan,nant)
               Per-baseline 'flags' should be weights instead
                 This allows zeroing the autos as well as, e.g., down-weighting short baselines
        [Done]Apply the gain solutions and image the visibilities
        Compute the observation time and known source positions
           How to deal with multiple sources?
             Option 1: Use known beam pattern and source fluxes to generate an accurate model
             Option 2: Phase to one source and frequency-scrunch to wash out other sources
"""

import numpy as np
import matplotlib.pyplot as plt
import os, cmath
from scipy.sparse.linalg import eigsh#, eigs
import scipy.stats
from scipy.optimize import minimize
import ephem, math
import datetime
import json
import stands, flags

def sign(x):
  if x < 0: return -1
  else: return 1

# Records from phase extraction
class PhaseStats(object):
  def __init__(self, what, data_line):
	#f.write(str(stand)+" "+str(other_stand)+" "+str(0)+" "+str(delay)+" "+str(delay_from_slope)+" "+str(st.subband_phase_range(stand, other_stand, float(header["CFREQ"])))+" "+str(phase_range)+" "+str(slope)+" "+str(stderr)+"\n")

    self.what = what
    self.st1 = int(data_line[0])
    self.st2 = int(data_line[1])
    self.pol = int(data_line[2])
    self.delay = data_line[3]
    self.delay_from_slope = data_line[4]
    self.max_phase_range = data_line[5]
    self.observed_phase_range = data_line[6]
    self.phase_slope = data_line[7]
    self.phase_slope_error = data_line[8]

    assert(self.st1 >= self.st2)
    self.baseline_index = self.st1*(self.st1+1)//2+self.st2


  def excessive_range(self):
    return abs(self.observed_phase_range) > (self.max_phase_range)

# From quick look
def _cast_to_type(string):
	try: return int(string)
	except ValueError: pass
	try: return float(string)
	except ValueError: pass
	return string
DADA_HEADER_SIZE = 4096
def parse_dada_header(headerstr, cast_types=True):
	header = {}
	for line in headerstr.split('\n'):
		try:
			key, value = line.split(None, 1)
		except ValueError:
			break
		key = key.strip()
		value = value.strip()
		if cast_types:
			value = _cast_to_type(value)
		header[key] = value
	return header

def power(x):
	return (x*x.conj()).real
	#p = np.empty_like(x.real)
	#p[...] = x[...].real * x[...].real
	#p     += x[...].imag * x[...].imag
	#return p
def db(x):
	return 10*np.log10(x)
def power_db(x):
	return db(power(x))

def bid(i,j):
	assert(j<=i)
	return i*(i+1)//2+j

def build_baseline_ants(nant):
	nbaseline = nant*(nant+1)/2
	baseline_ants = np.empty((nbaseline,2), dtype=np.int32)
	for i in xrange(nant):
		for j in xrange(i+1):
			b = i*(i+1)/2 + j
			baseline_ants[b,0] = i
			baseline_ants[b,1] = j
	return baseline_ants

def horizontal2cartesian(az, alt, r=1):
	x = r * np.cos(alt) * np.sin(az)
	y = r * np.cos(alt) * np.cos(az)
	z = r * np.sin(alt)
	return [x, y, z]
class CalSource(object):
	def __init__(self, name, position, flux_ref, freq_ref=None, spec_indices=None,
				 form='point'):
		self.name = name
		# String coord tuple => make FixedBody, else either local coord tuple or Body instance
		if isinstance(position, tuple) and isinstance(position[0], basestring):
			self.position = ephem.FixedBody()
			self.position._ra  = position[0]
			self.position._dec = position[1]
		else:
			self.position = position
		self.flux_ref = flux_ref
		self.freq_ref = freq_ref
		# Allow value or iterable for spec_indices
		try:
			it = iter(spec_indices)
			self.spec_indices = spec_indices
		except TypeError:
			if spec_indices is not None:
				self.spec_indices = (spec_indices,)
			else:
				self.spec_indices = None
		self.form = form
	def horizontal(self, observer):
		if isinstance(self.position, tuple):
			az, alt = self.position[0], self.position[1]
		else:
			self.position.compute(observer)
			az, alt = self.position.az, self.position.alt
		return az, alt
	def flux(self, freqs):
		# As appears in TTCal by Michael Eastwood
		log_flux = np.log(self.flux_ref)
		if len(self.spec_indices):
			log_freq = np.log(freqs/self.freq_ref)
			for i, ind in enumerate(self.spec_indices):
				log_flux += ind * log_freq**(i+1)
		flux = np.exp(log_flux)
		#print self.name, flux
		print self.name, "flux", np.mean(flux)
		return flux
	def direction_weights(self, observatory, stations, freqs):
		az, alt = self.horizontal(observatory)
 		print self.name, "altitude", alt/3.14159*180
		if alt < 0:
			raise ValueError("Source is below the horizon")
		pointing = np.array(horizontal2cartesian(az, alt), dtype=stations.dtype)
		dists = np.sum(stations * pointing, axis=-1)
		geo_delay = dists / 299792458.
		geo_delay_weight = np.exp(-1j * 2*np.pi * geo_delay * freqs[:,None])
		return geo_delay_weight
		
	def model(self, observatory, stations, freqs):
		if self.form == 'point':
			point = self.direction_weights(observatory, stations, freqs)
			flux  = self.flux(freqs)[:,None,None]
			#vis   = flux/(bw*area) * np.einsum('icp,jcq->ijcpq', point, point.conj())
			n = len(stations)
			vis   = flux/n**2 * np.einsum('ci,cj->cij', point, point.conj())
			return vis
		else:
			raise ValueError('Unknown source form "%s"' % self.form)
Cyg    = CalSource('Cyg', ('19:59:28.4', '+40:44:02.1'),   22827,   47e6, (0))
#Hyd     = CalSource('Hyd', ( '9:18:05.65', '-12:05:44.0'),   10571.0,   58e6, (-0.2046))
#Gem     = CalSource('Gem', ('06:18:02.70','+22:39:36.0'),   10571.0,   58e6, (-0.2046))
Cas    = CalSource('Cas', ('23:23:27.8', '+58:48:34'),      13391+12501,   47e6, (0))
Her    = CalSource('Her', ('16:51:11.1', '+05:04:58'),       230.0,   58e6, (+0.2635))
Tau    = CalSource('Tau', ('05:34:31.94','+22:00:52.2'),     2595,   47e6, (0))
Vir    = CalSource('Vir', ('12:30:49.42','+12:23:28.04'),    105438.69,   1e6, (-0.856))
Sun    = CalSource('Sun', ephem.Sun(),      250.0,   20e6, (+1.9920))
Zenith = CalSource('Zenith', (np.radians(0),np.radians(90)), 1.0)

def load_telescope(filename):
	with open(filename, 'r') as telescope_file:
		telescope = json.load(telescope_file)
	coords_local = np.array(telescope['coords']['local']['__data__'], dtype=np.float32)
	# Reshape into ant,column
	coords_local = coords_local.reshape(coords_local.size/4,4)
	ant_coords = coords_local[:,1:]
	inputs = np.array(telescope['inputs']['__data__'], dtype=np.float32)
	# Reshape into ant,pol,column
	inputs      = inputs.reshape(inputs.size/7/2,2,7)
	delays      = inputs[:,:,5]*1e-9
	dispersions = inputs[:,:,6]*1e-9
	return telescope, ant_coords, delays, dispersions


# Compare a fit of delays to delay matrix
def compare(delays_approx):
  diff = 0.0
  num = 0
  for i in range(len(delays_approx)):
    for j in range(i):
      if delay_matrix[i, j] != 0:
        delay = delays_approx[i]-delays_approx[j]
        num += 1
        diff += (delay_matrix[i, j]-delay)**2

  #print math.sqrt(diff/num)
  return math.sqrt(diff)

# Post process delays and plot them.
def post_process(delays_approx, how_many):		# input: result of the fit
  # Plotting and RMS

  flat_matrix = np.zeros(how_many)		# flat matrix is delay matrix i.e. baseline delays flattened
  flat_matrix_fit = np.zeros(how_many)
  for i in range(256):
    for j in range(i):
      if delay_matrix[i, j] != 0:
        bl_index = i*(i+1)//2+j
        if bl_index < len(flat_matrix):
          flat_matrix[bl_index] = delay_matrix[i, j] 
          flat_matrix_fit[bl_index] = delays_approx[i]-delays_approx[j]   # Difference between stand delays is the delay present in the visibilities

  # Change RMS of fit to match RMS of input data, because it doesn't get set by fit properly
  rms = np.sqrt(np.mean(np.square(flat_matrix[flat_matrix!=0])))
  rms_fit = np.sqrt(np.mean(np.square(flat_matrix_fit[flat_matrix_fit!=0])))
  delays_approx *= rms/rms_fit        # delays modified here

  for i in range(256):
    for j in range(i):
      if delay_matrix[i, j] != 0:
        bl_index = i*(i+1)//2+j
        if bl_index < len(flat_matrix):
          flat_matrix[bl_index] = delay_matrix[i, j]
          flat_matrix_fit[bl_index] = delays_approx[i]-delays_approx[j]

  diff = np.subtract(flat_matrix, flat_matrix_fit)
  diff = diff[diff != 0.]
  print "RMS diff orig and fit", np.sqrt(np.mean(np.square(diff)))

  plt.plot(np.subtract(flat_matrix, flat_matrix_fit), label="Orig data", linewidth=0.4)
  plt.xlabel("Baseline")
  plt.title("Error in Fit Baseline delays")
  plt.ylabel("Delay (s)")
  np.savetxt("delay_matrix_error.dat", np.subtract(flat_matrix, flat_matrix_fit))
  plt.savefig("delay_matrix_error.png")
  plt.clf()

  gmin = min(min(flat_matrix_fit),min(flat_matrix))*1.1
  gmax = max(max(flat_matrix_fit),max(flat_matrix))*1.1
  plt.ylim(gmin, gmax)
  plt.plot(flat_matrix, label="Orig data", linewidth=0.4) 
  plt.xlabel("Baseline")
  plt.title("Baseline delays")
  plt.ylabel("Delay (s)")
  np.savetxt("delay_matrix.dat", flat_matrix)  
  plt.savefig("delay_matrix.png")
  plt.clf()

  plt.ylim(gmin, gmax)
  plt.plot(flat_matrix_fit, label="Fitted data", linewidth=0.4, color="green") 
  plt.xlabel("Baseline")
  plt.title("Baseline delays")
  plt.ylabel("Delay (s)")
  #plt.legend()
  plt.savefig("delay_matrix_fit.png")
  np.savetxt("delay_matrix_fit.dat", flat_matrix_fit)
  plt.clf()

  return delays_approx
        

if __name__ == '__main__':
	import sys
	
	telescope, coords, delays, dispersions = load_telescope("lwa_ovro.telescope.json")
	
	outrig_ids = [252, 253, 254, 255, 256]
	outrig_nbaseline = sum(outrig_ids) # TODO: This only works because the outriggers are the last antennas in the array

	filename = sys.argv[1]
	filesize = os.path.getsize(filename) - DADA_HEADER_SIZE
	print "filesize:     ", filesize
	with open(filename, 'rb') as f:
		header = parse_dada_header(f.read(DADA_HEADER_SIZE))
		#nn = 15
		#nn = 7
		#f.read((nn*(nn+1)/2+nn)*109*8*4)
		nchan  = header['NCHAN']
		nant   = header['NSTATION']
		npol   = header['NPOL']
		navg   = header['NAVG']
		bps    = header['BYTES_PER_SECOND']
		df     = header['BW']*1e6 / float(nchan)
		cfreq  = header['CFREQ']*1e6
		utc_start = header['UTC_START']
		assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
		freq0 = cfreq-nchan/2*df
		freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
		print "bytes per sec:", bps
		print "navg:     ", navg
		print "nchan:    ", nchan
		print "npol:     ", npol
		print "nstation: ", nant
		nbaseline = nant*(nant+1)//2
		print "nbaseline:", nbaseline
		noutrig_per_full = int(navg / df + 0.5)
		print "noutrig_per_full:", noutrig_per_full
		full_framesize   = nchan*nbaseline*npol*npol
		print "full_framesize:  ", full_framesize*8
		#print nbaseline, noutrig_per_full, outrig_nbaseline
		outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
		print "outrig_framesize:", outrig_framesize*8
		tot_framesize    = (full_framesize + outrig_framesize)
		print " tot_framesize:  ",  tot_framesize*8
		ntime = float(filesize) / (tot_framesize*8)
		print "ntime:", ntime
		print filesize % (tot_framesize*8)
		frame_secs = int(navg / df + 0.5)
		print "Frame secs:", frame_secs
		time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
		#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
		print "Time offset:", time_offset
		for t in xrange(int(sys.argv[2])+1):
			full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
			outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
		full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
		outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))
		print full_data.shape
		print outrig_data.shape
		
		R = np.empty((nchan,nant,nant,npol,npol), dtype=np.complex64)
		#R = np.empty((nchan,nant,nant,npol,npol), dtype=np.complex128)
		baseline_ants = build_baseline_ants(nant)
		ants_i = baseline_ants[:,0]
		ants_j = baseline_ants[:,1]
		chans = np.arange(nchan)[:,None]
		R[chans,ants_i,ants_j,:,:] = full_data
		R[chans,ants_j,ants_i,:,:] = full_data.conj()
		
		# Compute cable delays for freq,ant,pol
		freqs_ant_pol = freqs[:,None,None]
		#extra = np.loadtxt("delays_extra.dat")
		#delays[...] = 0; dispersions[...] = 0
                #delays[:, 0] = np.add(delays[:, 0], extra)/2; delays[:, 1] = np.add(delays[:, 1], extra)/2
		cable_delays = (delays + dispersions / np.sqrt(freqs_ant_pol/1e6))
 		#for i in range(nant):
                #  print i, ( "%.2f" % (cable_delays[cable_delays.shape[0]/2, i, 0]*1e9) ), cable_delays[cable_delays.shape[0]/2, i, 0]*299792458
		#exit()
		# Compute corresponding complex phase weights
		cable_delay_weights = np.exp(1j*2*np.pi*cable_delays*freqs_ant_pol)
		
		apply_cable_delays = True
		R[...] = 1 #to just get cable_delays for fitting
		if apply_cable_delays:
			R[...,0,0] *= cable_delay_weights[:,None,:,0].conj()
			R[...,1,1] *= cable_delay_weights[:,None,:,1].conj()
			R[...,0,0] *= cable_delay_weights[:,:,None,0]
			R[...,1,1] *= cable_delay_weights[:,:,None,1]
			print "Apply cable delays"
		else: print "No cable delay added"
			
		R = R.conj()
		
		coord_attrs = telescope['coords']['local']['__attrs__']
		lat = coord_attrs['latitude']
		lon = coord_attrs['longitude']
		alt = coord_attrs['altitude']
		observer = ephem.Observer()
		observer.long      = np.radians(lon)
		observer.lat       = np.radians(lat)

		observer.elevation = alt
		print observer.long, observer.lat, observer.elevation
		observer.temp      = 20
		observer.pressure  = 0 # Note: Disables optical refraction corrections
		#2015-01-01-04:05:06
		utc_date, utc_time = utc_start[:10], utc_start[11:]
		utc_date = utc_date.replace('-','/')
		observer.date = ephem.Date(utc_date+' '+utc_time)
		print "*****", observer.date
		bpa = 154609088
                tsamp      = float(header["TSAMP"]) * 1e-6   # Sampling time per channel, in microseconds
                navg       = int(header["NAVG"])             # Number of averages per integration
                int_tim    = tsamp * navg                    # Integration time is tsamp * navg
                byte_offset = int(header["OBS_OFFSET"])
                num_int_since_obs_start = byte_offset / bpa
                time_offset_since_obs_start = num_int_since_obs_start * int_tim 
                observer.date = datetime.datetime.strptime(header["UTC_START"], "%Y-%m-%d-%H:%M:%S")+datetime.timedelta(seconds=time_offset_since_obs_start)
		print "*****", observer.date
		
		which_pol = 0
		print "Use pol", which_pol
		R = R[...,which_pol,which_pol] # Extract xx pol
		nchan = 109 #0#09 # Extract subset of channels
		freqs = freqs[:nchan]
		R = R[:nchan] # Extract subset of channels
		
		# Construct model visibilities
		#M = np.ones_like(R)#Cas.model(ovro, stations, freqs)
		M = np.zeros_like(R)
		for source in [Cyg, Cas, Sun, Her, Vir, Tau]:
			try:
				#M += source.model(observer, coords, freqs) * (1.3 if source is Cas else 1.0)
				#M += source.model(observer, coords, freqs) * (1.787 if source is Cas else 1.0)
				M += source.model(observer, coords, freqs)
			except ValueError:
				pass
                # M[0, i, j] is the conjugate of M[0, j, i]
                
                spec_flags = flags.Flags()
		spec_flags.select_leda_outriggers()
		spec_flags.disable()
                print len(spec_flags.unflagged()), "stands used", spec_flags.unflagged()
		st = stands.Stands()

		# Capture delays. These are baseline delays. What we want to get is stand delays (from fitting).

		print "Stands",
                f = open("delay_peaks_divided.dat", "w")
                fft_length = 2*8192
                for stand in range(256):
		  print stand,
		  sys.stdout.flush()
                  for other_stand in range(stand):
                    if stand != other_stand and not spec_flags.stand_flagged(stand) and not spec_flags.stand_flagged(other_stand) and not spec_flags.bl_flagged(stand, other_stand):
                      fft = [ complex(0,0) for i in range(fft_length) ]
                      for j in range(109):
                        fft[j] = R[j, stand, other_stand]   #/M[j, stand, other_stand]
                        if abs(fft[j]) != 0: fft[j] /= abs(fft[j])     # strip amplitude

                      efft = abs(np.fft.fft(fft))
                      if stand == 208 and other_stand == 58:
                        b = [ cmath.phase(c) for c in fft[:109] ]
                        np.savetxt("phase_divided.dat", np.unwrap(b))
                        np.savetxt("delay_divided.dat", efft)
                      positive_freq = efft[1:len(efft)/2]     # https://docs.scipy.org/doc/numpy/reference/routines.fft.html#module-numpy.fft
                      negative_freq = efft[len(efft)/2+1:]
                      pos_max = np.max(positive_freq)
                      neg_max = np.max(negative_freq)

                      if pos_max > neg_max: where_peak = np.argmax(positive_freq)+1    # +1 because of DC taken out
                      else: where_peak = np.argmax(negative_freq)-len(negative_freq)

                      phases = np.unwrap([ cmath.phase(c) for c in fft[:109] ])
                      x = [ i for i in range(109) ]
                      slope, b, c, d, stderr = scipy.stats.linregress(x, phases)
                      phase_range = max(phases)-min(phases)
		      delay_from_slope = (phases[-1]-phases[0])/2.616e6/(2*cmath.pi)
                      delay = (float(where_peak)/len(efft))*(109/2.616e6)
                      f.write(str(stand)+" "+str(other_stand)+" "+str(0)+" "+str(delay)+" "+str(delay_from_slope)+" "+str(st.subband_phase_range(stand, other_stand, float(header["CFREQ"])))+" "+str(phase_range)+" "+str(slope)+" "+str(stderr)+"\n")
                f.close()
		print
		"""
		
		sky = image_visibilities(M[0], freqs[0], coords)
		plt.imshow(sky, interpolation='nearest', vmax=sky.max()/2)
		plt.colorbar()
		plt.show()
		exit()
	 	"""	
		
		# Select good delays - smooth phase ramps.

		data = np.loadtxt("delay_peaks_divided.dat")
		divided = [ PhaseStats("model", d) for d in data ]
	
		num = 0
		max_bl = -1
		max_diff = -1
		divided_delays = [ 0 for i in range(32896) ]
		stands1 = [ 0 for i in range(32896) ]
		stands2 = [ 0 for i in range(32896) ]
		chosen = []
		slope_threshold = 0.001  # LEDA core
		#slope_threshold = 0.02  # caltech
		for i in range(len(divided)):
		  # Print what fails
		  #if not (divided[i].phase_slope_error < slope_threshold): print "Failed slope not accurate", divided[i].phase_slope_error
		  #if not (abs((divided[i].delay-divided[i].delay_from_slope)/divided[i].delay) < 0.1):
		  #  print "Failed slope doesn't match FFT", divided[i].delay, divided[i].delay_from_slope
		  #if not (sign(divided[i].phase_slope) == sign(divided[i].delay)): print "Failed sign"

		  # Do the checking
		  if divided[i].phase_slope_error < slope_threshold and abs((divided[i].delay-divided[i].delay_from_slope)/divided[i].delay) < 0.1  \
		  			and sign(divided[i].phase_slope) == sign(divided[i].delay):
		        #print st1, st2, model[i][0], model[i][1], o[3], model[i][3]
		     divided_delays[divided[i].baseline_index] = divided[i].delay_from_slope
		     stands1[divided[i].baseline_index] = divided[i].st1
		     stands2[divided[i].baseline_index] = divided[i].st2
		
		     num += 1
		     if divided[i].baseline_index > max_bl: max_bl = divided[i].baseline_index
		
		     if abs(divided[i].delay) > max_diff:
		       max_diff = abs(divided[i].delay)
		       where_max = (divided[i].st1, divided[i].st2)
		
		     #if divided[i].excessive_range(): print "Divided", divided[i].st1, divided[i].st2, divided[i].observed_phase_range, divided[i].max_phase_range
		     chosen.append(i)

		if not apply_cable_delays:
                  f = open("delay_peaks_divided.dat", "w")
		  for i in range(data.shape[0]):
		    f.write(str(int(data[i, 0]))+" "+str(int(data[i, 1]))+" "+str(int(data[i, 2]))+" "+str(data[i, 3])+" "+str(data[i, 4])+" "+str(data[i, 5])+" "+str(data[i, 6])+" "+str(data[i, 7])+" "+str(data[i, 8]))
		    if i in chosen: f.write(" GOOD\n")
                    else: f.write("\n")
                  f.close()
 		
		
                good_delays = zip(stands1[:max_bl+1], stands2[:max_bl+1], divided_delays[:max_bl+1])
                np.savetxt("good_delays.dat", good_delays)

                dump_gd = [ x for x in good_delays if x[2] != 0 ]   # JUst used for writing
                good_stands = []
                for s in [ x[0] for x in dump_gd ]:
                  if int(s) not in good_stands: good_stands.append(int(s))
                for s in [ x[1] for x in dump_gd ]:
                  if int(s) not in good_stands: good_stands.append(int(s))

                print "Got", num, "good baselines"
                print "Choose baseline", where_max, "for an example"
                print "Got", len(good_stands), "good stands", sorted(good_stands, key=int)

		plt.xlabel("Baseline index")
		plt.ylabel("Delay (s)")
		plt.plot(divided_delays[:max_bl+1], label="Divided", linewidth=0.4)
		plt.legend()
		plt.savefig("delay_matrix.png")

		
		# Run minimization to fit delays

		print "Running minimization"
		data = np.loadtxt("good_delays.dat")

		delay_matrix = np.zeros((256, 256))
		for i in range(data.shape[0]):
  		  delay_matrix[int(data[i][0]), int(data[i][1])] = data[i][2]   # Load observed

		delays = np.zeros(256)
		result = minimize(compare, delays, method="Powell")
                delays = result.x   
		mask = [ not spec_flags.stand_flagged(i)  for i in range(len(delays)) ]
		delays = np.where(mask, delays, 0)   # changed sense of this
		delays = post_process(delays, data.shape[0])
		plt.xlabel("Stand")
		plt.ylabel("Delay (ns)")
		plt.title("Stand delays")
		plt.plot(delays*1e9)    
		plt.savefig("delays.png")
		np.savetxt("delays.dat", delays)    # This is the ultimate result: a list of delays by stand, in seconds, like the JSON

		# Result files: delays.dat (fit to stand delays), delay_matrix.dat (original data, baseline delays) delay_matrix_fit.dat (fit to baseline delays)


		# Create new cuwarp config

		f = open("instr_config_fit.txt", "w")

		sequence = 0
		for i in range(len(delays)):
  		  f.write(str(sequence)+"\t"+str(i)+"\tX\t"+"EL_"+( "%.2f" % (delays[i]*299792458))+"\t0\n")
		  sequence += 1
  		  f.write(str(sequence)+"\t"+str(i)+"\tY\t"+"EL_"+( "%.2f" % (delays[i]*299792458))+"\t0\n")
  		  sequence += 1

		f.close()

		# vel factor
		graph = zip(np.loadtxt("physical.dat"), delays*299792458)
		graph = [ el for el in graph if el[1] != 0 ]
		graph = np.array(sorted(graph, key=lambda tup: tup[0]))

		np.savetxt("vfactor.dat",graph)
		slope, b, c, d, stderr = scipy.stats.linregress(graph[:, 0], graph[:, 1])
		print "Vel factor", 1/slope

