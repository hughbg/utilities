#!/usr/bin/env python
import numpy as np
import random
import sys, os, cmath, math,scipy
from scipy import stats
import stands
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from skimage.restoration import  unwrap_phase


NUM_CHANNELS = 109
NUM_STANDS = 256
NUM_BASELINES = NUM_STANDS*(NUM_STANDS+1)/2
DADA_HEADER_SIZE = 4096


outrig_nbaseline = 1270

def sqr(x): return x*x

def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header
      
def num2pol(x):
  if x == 0: return "X"
  else: return "Y"

def make_html():
  every_eight = [ 8*i-1 for i in range(1,33)]
  sixteen_four = [ 16*i+4 for i in range(16)]
  sixteen_five = [ 16*i+5 for i in range(16)]

  f = open("stands.html", "w")
  for stand in range(NUM_STANDS):
    tip = ""
    if stand in every_eight: tip += " 8N "
    if stand in sixteen_four: tip += " 16N+4 "
    if stand in sixteen_five: tip += " 16N+5 "
    if len(tip) > 0: tip = "Pattern: "+tip
    f.write("<h2>"+str(stand)+"</h2>"+tip+"<p>The autocorrelation plot covers all correlator dumps in the file, and XX, YY pols.<p><img src=auto_amp_"+str(stand)+".png><p>\n")
    f.write("What follows  covers the first correlator dump only.<p><h3>Amplitude summary</h3>\n")
    f.write("<table><tr>\n")
    #for pol1 in range(2):
    #  for pol2 in range(2):
    #    f.write("<td><img src=cross_phase_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".png width=400></td>\n")
    #f.write("</tr><tr>\n")
    for pol1 in range(2):
      for pol2 in range(2):
	
        f.write("<td align=center>Pols "+num2pol(pol1)+num2pol(pol2)+"</td>\n")
    f.write("</tr><tr>\n")
    for pol1 in range(2):
      for pol2 in range(2):
        f.write("<td align=center><img src=cross_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".png width=400></td>\n")
    f.write("</tr><tr>\n")
    for pol1 in range(2):
      for pol2 in range(2):
        f.write("<td align=center><img src=peaks_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".png width=400></td>\n")
    f.write("</tr><tr>\n")
    for pol1 in range(2):
      for pol2 in range(2):
        f.write("<td align=center><img src=peaks_by_distance_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".png width=400></td>\n")
    f.write("</tr></table>\n")
    f.write("<h3>Amplitude and Phase in detail</h3>\n")
    f.write("<img src=amp_phase_"+str(stand)+".gif><p>\n")
  f.close

def x_unwrap_phase(phases):
  mod_phases = phases[:]
  for i in range(len(mod_phases)-1):
    if mod_phases[i+1] > mod_phases[i]:
      if abs(mod_phases[i+1]-2*np.pi-mod_phases[i]) < abs(mod_phases[i+1]-mod_phases[i]):
        for j in range(i+1,len(mod_phases)): mod_phases[j] -= 2*np.pi
    else:
      if abs(mod_phases[i+1]+2*np.pi-mod_phases[i]) < abs(mod_phases[i+1]-mod_phases[i]):
        for j in range(i+1,len(mod_phases)): mod_phases[j] += 2*np.pi
  return mod_phases
      
def plot_ant(data, ant, ndumps):

  dada_index = ant*(ant+1)//2+ant
 
  # Set up data arrays for plot
  x = [ i for i in range(NUM_CHANNELS) ]
  y = [ 0.0 for i in range(NUM_CHANNELS) ]
  
  # Plot many lines
  for i in range(ndumps):
    for j in range(NUM_CHANNELS):
      y[j] = abs(data[i][j][dada_index][0][0])
    plt.plot(x, y, color="red")
    for j in range(NUM_CHANNELS):
      y[j] = abs(data[i][j][dada_index][1][1])
    plt.plot(x, y, color="blue")

    
  plt.ylim(0)

  plt.xlabel('Channel')
  plt.ylabel('Log(Power)')
  #ylabel('voltage (mV)')
  plt.title("All for stand "+str(ant)+" (0-based)")
  plt.grid(True)
  plt.savefig("auto_amp_"+str(ant)+".png")
  
  plt.clf()
  
def plot_channel(data, channel, dump, pol):
  # Set up data arrays for plot
  x = [ i for i in range(NUM_BASELINES) ]
  y = [ 0.0 for i in range(NUM_BASELINES) ]
  
  f = open("x.dat","w")
  for i in xrange(256):
    for j in xrange(i+1):
      index = make_dada_index(i,j)    
      val = abs(data[dump][channel][index][pol][pol])
      if val > 0: y[index] = math.log(val)
      f.write(str(y[index])+"\n")
  f.close()
  plt.plot(x, y, "r.")

    
  plt.ylim(0)
  plt.xlim(0,33000)

  plt.xlabel('Baseline')
  plt.ylabel('Visibility')
  #ylabel('voltage (mV)')
  plt.title("Visibilities for channel "+str(channel)+" (0-based)")
  plt.grid(True)
  plt.savefig("channel_"+str(channel)+"_pol_"+str(pol)+".png")
  
  plt.clf()

  
  
def sign(x):
  if x < 0: return "N"   # negative
  else: return "P"       # positive

def plot_arc(x1, y1, x2, y2, magnitude):
  #print x1, x2, y1, y2
  # For atan2, (1,0) is angle 0. Going anticlockwise is positive
  # is negative
  xs = np.array([])
  ys = np.array([])
  
  angle1 = math.atan2(y1, x1)
  angle2 = math.atan2(y2, x2)
 
  # If both angles are the same sign then go from one to the other
  if sign(angle1) == sign(angle2):
    start_angle = min(angle1, angle2)
    end_angle = max(angle1, angle2) 
  else:
    # Convert angles to 2 pi anticlockwise from horizontal
    if angle1 < 0: angle1 = 2*math.pi+angle1
    if angle2 < 0: angle2 = 2*math.pi+angle2

    start_angle = min(angle1, angle2)
    end_angle = max(angle1, angle2) 

    # Now work out distance going clockwise or anticlockwise
    distance_anticlockwise = end_angle-start_angle
    distance_clockwise = 2*math.pi-distance_anticlockwise

    if distance_clockwise < distance_anticlockwise: 
      tmp = start_angle
      start_angle = end_angle
      end_angle = tmp+2*math.pi

  theta = start_angle
  d_theta = (end_angle-start_angle)/20
  if d_theta == 0.0: return xs, ys
  while theta <= end_angle:
    x = magnitude*math.cos(theta)
    y = magnitude*math.sin(theta)
    xs = np.append(xs,x)
    ys = np.append(ys,y)
    theta += d_theta
    
  return xs, ys
  
# Get current size
#fig_size = plt.rcParams["figure.figsize"]
 
# Prints: [8.0, 6.0]
#print "Current size:", fig_size
 
# Set figure width to 12 and height to 9
#fig_size[0] = 6
#fig_size[1] = 4
#plt.rcParams["figure.figsize"] = fig_size

def make_dada_index(stand1, stand2):	# Ordering of stands is taken care of so they can be input in any order
  if stand2 > stand1:
    return stand2*(stand2+1)//2+stand1
  else: return stand1*(stand1+1)//2+stand2

def image_stand(data, dump, stand, pol1, pol2):
 
  # Create graph of baseline max 
  dat_file = open("peaks.dat", "w")
  where_peak = -1
  baseline_peak = 0
  maxima = np.zeros((NUM_STANDS))
  for i in range(NUM_STANDS):
    values = np.zeros((109))
    for j in range(NUM_CHANNELS):
      if stand == i and pol1 == pol2: values[j] = 0			# Case: autocorrelation
      elif stand == i and pol1 == 1 and pol2 == 0: values[j] = 0  	# Case: YX conjugate of XY on the same stand, set it to 0
      #elif j not in [ 99, 100, 101, 102 ]: all_amp[i, j] = 0		# Case: flagged out
      else: values[j] = abs(data[dump][j][make_dada_index(stand,i)][pol1][pol2])
    maxima[i] = np.max(values)
    if maxima[i] > baseline_peak:
      baseline_peak = maxima[i]
      where_peak = i
    dat_file.write(str(maxima[i])+"\n")
  dat_file.close()
  
  ratio = maxima.max()/np.mean(maxima)

  if ratio > 20 and where_peak == stand: print "peaks_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".png","F", stand

  gp_file = open("mk.gp", "w")
  gp_file.write("set terminal png\n")
  gp_file.write("set output \"x.png\"\n")
  gp_file.write("set title \"Stand "+str(stand)+" baseline peaks\n")
  gp_file.write("unset key\n")
  gp_file.write("set yrange[0:]\n")
  gp_file.write("set xrange [0:256]\n")
  gp_file.write("set label \"Peak at stand "+str(where_peak)+"\" at graph 0.6, graph 0.95\n")
  gp_file.write("set label \"Ratio "+( "%.3f" % ratio )+"\" at graph 0.6, graph 0.9\n")
  gp_file.write("set xlabel \"Other Stand Number\"\n")
  gp_file.write("set ylabel \"Visibility Amplitude\"\n")
  gp_file.write("plot \"peaks.dat\" with lines\n")
  gp_file.close()
  os.system("gnuplot mk.gp")
  os.system("mv x.png "+"peaks_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".png")
  os.system("cp peaks.dat "+"peaks_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".dat")

  # Create graph of peaks ordered by distance
  stand_locations = stands.Stands()
  dat_file = np.loadtxt("peaks.dat")
  by_distance = np.zeros((NUM_STANDS, 2))
  where_peak_distance = -1
  for i in range(NUM_STANDS):
    by_distance[i][0] = stand_locations.distance(stand,i)
    by_distance[i][1] = dat_file[i]
    if i == where_peak: where_peak_distance = by_distance[i][0]  

  
  sorted_by_distance = sorted(by_distance, key=lambda tup: tup[0])
  np.savetxt("peaks_by_distance.dat",sorted_by_distance)
  
  gp_file = open("mk.gp", "w")
  gp_file.write("set terminal png\n")
  gp_file.write("set output \"x.png\"\n")
  gp_file.write("set title \"Stand "+str(stand)+" baseline peaks\n")
  gp_file.write("unset key\n")
  gp_file.write("set yrange[0:]\n")
  gp_file.write("set xrange [0:256]\n")
  gp_file.write("set label \"Peak at distance "+str( "%.2f" % where_peak_distance)+"m \" at graph 0.5, graph 0.95\n")
  gp_file.write("set xlabel \"Distance to other stand (m)\"\n")
  gp_file.write("set ylabel \"Visibility Amplitude\"\n")
  gp_file.write("plot \"peaks_by_distance.dat\" with lines\n")
  gp_file.close()
  os.system("gnuplot mk.gp")
  os.system("mv x.png "+"peaks_by_distance_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".png")
  
  # Map of amplitudes
  all_amp = np.zeros((NUM_CHANNELS, NUM_STANDS))
  for j in range(NUM_CHANNELS):   
    for i in range(NUM_STANDS):
      if stand == i and pol1 == pol2: all_amp[j, i] = 0			# Case: autocorrelation
      else: all_amp[j, i] = abs(data[dump][j][make_dada_index(stand,i)][pol1][pol2])
  plt.xlabel("Stand Number")
  plt.ylabel("Channel")
  plt.title("Visibility Amplitude")
  plt.imshow(all_amp, cmap="binary")
  plt.savefig("x.png")
  os.system("convert x.png -trim "+"cross_"+str(stand)+"_"+str(pol1)+"_"+str(pol2)+".png")

  

  
def stand_details(data, dump, stand):

  # Animated gifs of amplitude and phase
  # Need to know min/max amplitudes
  print "Animated GIF"
  max_amp = 0
  for i in range(NUM_STANDS):
    dada_index = make_dada_index(stand, i)

    for j in range(NUM_CHANNELS):
      for pol1 in [0, 1]:
        for pol2 in [0, 1]:
	  if not ( stand == i and pol1 == pol2):
            amp = abs(data[dump][j][dada_index][pol1][pol2])
            if amp > max_amp: 
	      max_amp = amp
	      which_stand = i
	      which_pol1 = pol1
	      which_pol2 = pol2
  print "Max amp",max_amp,"occurs at stand", which_stand, "pol1",pol1,"pol2",pol2

  stand_locations = stands.Stands()
  x = np.array([ j for j in range(NUM_CHANNELS) ])

  for pol1 in [0, 1]:
    for pol2 in [ 0, 1 ]:
      pol_name = num2pol(pol1)+num2pol(pol2)
      print pol_name
    
      print "Phase",
      for i in range(NUM_STANDS):
        print i, 
        sys.stdout.flush()
  
        dada_index = make_dada_index(stand, i)

        y = np.zeros(NUM_CHANNELS)
	all_zero = True
        if not (stand == i and pol1 == pol2):
          for j in range(NUM_CHANNELS):

	    phase = cmath.phase(data[dump][j][dada_index][pol1][pol2])
	    y[j] = phase
	    if y[j] != 0.0: all_zero = False

        y = x_unwrap_phase(unwrap_phase(y))
      

        #slope = np.zeros((108))
        #for k in range(108):
	  #slope[k] = y[k+1]-y[k]
	  #print y[k]
        #print "\nPhase stat", np.mean(slope), (np.max(y)-np.min(y))/109, np.min(y), np.max(y), np.max(y)-np.min(y), stand_locations.distance(stand,i)
      
        plt.title("Stand "+str(stand)+" to "+str(i)+" Phase - Pol "+pol_name)
        plt.xlim(0,110)
        
        if stand == i and pol1 == pol2: 
	  plt.figtext(0.4,0.4, "AUTOCORRELATION NOT PLOTTED", color="blue")
        else: 
          # Shift to 0
          ymin = np.min(y)
          for j in range(len(y)): y[j] -= ymin 
          if np.max(y) < np.pi: plt.ylim(0,np.pi)
	  if all_zero: plt.figtext(0.4,0.4, "ALL ZERO", color="blue")
	  plt.plot(x, y)

        plt.xlabel("Channel")
        plt.ylabel("Phase")
        plt.savefig("x.png")
        os.system("convert  x.png -resize 340 "+"phase_"+str(i)+"_"+pol_name+".png")
        plt.clf()
      
      print
  
      print "Amplitude",  	# Don't actually use when stand == i, i.e autocorrelation
      for i in range(NUM_STANDS):
        print i, 
        sys.stdout.flush()
  
        dada_index = make_dada_index(stand, i)

        y = np.zeros(NUM_CHANNELS)
	all_zero = True
        if not (stand == i and pol1 == pol2):
          for j in range(NUM_CHANNELS):

	    amplitude = abs(data[dump][j][dada_index][pol1][pol2])
	    y[j] = amplitude
	    if y[j] != 0.0: all_zero = False


        plt.title("Stand "+str(stand)+" to "+str(i)+" Amplitude - Pol "+pol_name)
        plt.xlim(0,110)
        
        if stand == i and pol1 == pol2: 
	  plt.figtext(0.4,0.4, "AUTOCORRELATION NOT PLOTTED", color="blue")
        else: 
	  if all_zero: plt.figtext(0.4,0.4, "ALL ZERO", color="blue")
	  plt.plot(x, y)
        plt.ylim(0, max_amp)
        plt.xlabel("Channel")
        plt.ylabel("Amplitude")
        plt.savefig("x.png")
        os.system("convert  x.png -resize 340 "+"amplitude_"+str(i)+"_"+pol_name+".png")
        plt.clf()
      
      print

  x = np.array([ i for i in range(512) ])
  fft = [ complex(0,0) for i in range(512) ]
  print "Back phase"
  for other_stand in range(NUM_STANDS):
    dada_index = make_dada_index(stand, other_stand)
    print other_stand, 
    for pol1 in [0, 1]:
      for pol2 in [ 0, 1]:
        pol_name = num2pol(pol1)+num2pol(pol2)
	print pol_name, 
        for j in range(NUM_CHANNELS):

	  #mag = abs(data[dump][j][dada_index][pol1][pol2])
	  #if mag == 0: mag = 1
          fft[j] = complex(data[dump][j][dada_index][pol1][pol2].real, data[dump][j][dada_index][pol1][pol2].imag) 
	
          #phase = j/109.0*6*math.pi
          #fft[j] = complex(math.cos(phase), math.sin(phase)) 
        efft = abs(np.fft.fft(fft))
        where_peak = np.argmax(efft)

        plt.figtext(0.4,0.85, "Peak at "+str(where_peak))
        plt.xlim(-10, 515)
        plt.title("Stand "+str(stand)+" to "+str(other_stand)+" delay")
        plt.plot(x, efft)
	    

        plt.xlabel("Time")
        plt.ylabel("FFT")
        plt.savefig("x.png")
        os.system("convert  x.png -resize 340 "+"back_phase_"+str(other_stand)+"_"+pol_name+".png")
        plt.clf()

  print

  for i in range(NUM_STANDS):
    os.system("convert "+"amplitude_"+str(i)+"_XX.png"+" amplitude_"+str(i)+"_XY.png"+" +append "+"amp_"+str(i)+"_XX_XY.png")
    os.system("convert "+"amp_"+str(i)+"_XX_XY.png"+" amplitude_"+str(i)+"_YX.png"+" +append "+"amp_"+str(i)+"_XX_XY_YX.png")
    os.system("convert "+"amp_"+str(i)+"_XX_XY_YX.png"+" amplitude_"+str(i)+"_YY.png"+" +append "+"amp_"+str(i)+"_XX_XY_YX_YY.png")
    os.system("convert "+"phase_"+str(i)+"_XX.png"+" phase_"+str(i)+"_XY.png"+" +append "+"phase_"+str(i)+"_XX_XY.png")
    os.system("convert "+"phase_"+str(i)+"_XX_XY.png"+" phase_"+str(i)+"_YX.png"+" +append "+"phase_"+str(i)+"_XX_XY_YX.png")
    os.system("convert "+"phase_"+str(i)+"_XX_XY_YX.png"+" phase_"+str(i)+"_YY.png"+" +append "+"phase_"+str(i)+"_XX_XY_YX_YY.png")
    os.system("convert "+"back_phase_"+str(i)+"_XX.png"+" back_phase_"+str(i)+"_XY.png"+" +append "+"back_phase_"+str(i)+"_XX_XY.png")
    os.system("convert "+"back_phase_"+str(i)+"_XX_XY.png"+" back_phase_"+str(i)+"_YX.png"+" +append "+"back_phase_"+str(i)+"_XX_XY_YX.png")
    os.system("convert "+"back_phase_"+str(i)+"_XX_XY_YX.png"+" back_phase_"+str(i)+"_YY.png"+" +append "+"back_phase_"+str(i)+"_XX_XY_YX_YY.png")


    os.system("convert "+"amp_"+str(i)+"_XX_XY_YX_YY.png"+" phase_"+str(i)+"_XX_XY_YX_YY.png"+" back_phase_"+str(i)+"_XX_XY_YX_YY.png"+" -append "+"amp_phase_"+str(i)+".png")

  command = "convert -delay 40 "
  for i in range(NUM_STANDS):
    if stand != i:       # skip autocorrelation
     command += "amp_phase_"+str(i)+".png "
  command += "amp_phase_"+str(stand)+".gif"
  os.system(command)
  os.system("rm amplitude*.png phase*.png amp_phase*.png")



def find_best_phase_ramp(data, dump, stand, flags):
  y = np.zeros(NUM_CHANNELS)
  x = np.array([i for i in range(NUM_CHANNELS)])
  best_fit = 1e39
  which_best = -1
  which_best_pol = -1

  for other_stand in range(NUM_STANDS):
    if stand != other_stand and other_stand not in flags:
      dada_index = make_dada_index(stand, other_stand)
      for pol in [ 0, 1 ]:
        for j in range(NUM_CHANNELS):
          y[j] = cmath.phase(data[dump][j][dada_index][pol][pol])
        y = x_unwrap_phase(unwrap_phase(y))
      
	slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
        if np.max(y) > 0 and std_err < best_fit: 
          best_fit = std_err
          which_best = other_stand
          which_best_pol = pol

          
  print stand, std_err, which_best
  dada_index = make_dada_index(stand, which_best)
  for j in range(NUM_CHANNELS):
    y[j] = cmath.phase(data[dump][j][dada_index][which_best_pol][which_best_pol])
  y = x_unwrap_phase(unwrap_phase(y))
  ymin = np.min(y)
  for j in range(len(y)): y[j] -= ymin 
  plt.figtext(0.4,0.85, "Fit "+str(which_best)+" "+str(which_best_pol)+" "+str(std_err), color="blue")
  plt.title("Best phase for stand "+str(stand))
  plt.plot(x, y)
  if np.max(y) <= np.pi:plt.ylim(0, np.pi)
  plt.xlabel("Channel")
  plt.ylabel("Phase")
  plt.savefig("best.png")
  os.system("convert  best.png -resize 340 "+"best_phase_"+str(stand)+".png")
  plt.clf()


def back_phases(data, dump, stand):
  x = np.array([ i for i in range(NUM_CHANNELS) ])
  fft = [ complex(0,0) for i in range(NUM_CHANNELS) ]
  print "Back phase"
  for other_stand in range(NUM_STANDS):
    print other_stand
    dada_index = make_dada_index(stand, other_stand)
    for pol1 in [0, 1]:
      for pol2 in [ 0, 1]:
        pol_name = num2pol(pol1)+num2pol(pol2)
	print pol_name, 
        for j in range(NUM_CHANNELS):
	  mag = abs(data[dump][j][dada_index][pol1][pol2])
	  if mag == 0: mag = 1
          fft[j] = complex(data[dump][j][dada_index][pol1][pol2].real, data[dump][j][dada_index][pol1][pol2].imag) 
	
          phase = math.sqrt(j/109.0*6*math.pi)  # +2*(random.random()-0.5)
          fft[j] = complex(math.cos(phase), math.sin(phase)) 
        efft = abs(np.fft.fft(fft))
        #efft[500] = complex(50,50)
        #efft[501] = complex(70, 70)
	#efft[502] = complex(30, 30)

        where_peak = np.argmax(efft)

        plt.figtext(0.4,0.85, "Peak at "+str(where_peak))
        plt.title("Stand "+str(stand)+" to "+str(other_stand)+" signal combined")
        plt.plot(x, efft)
	    

        plt.xlabel("Time")
        plt.ylabel("Amplitude")
	plt.xlim(-6)
        plt.savefig("x.png")
        os.system("convert  x.png -resize 340 "+"back_phase_"+str(other_stand)+"_"+pol_name+".png")
        plt.clf()
	sys.exit(0)
    print


  command = "convert -delay 40 "
  for i in range(NUM_STANDS):
  
    if stand != i:       # skip autocorrelation
     command += "back_phase_"+str(other_stand)+"_"+pol_name+".png "
  command += "back_phase_"+str(stand)+".gif"
  os.system(command)

filesize = os.path.getsize(sys.argv[1]) - DADA_HEADER_SIZE 

f = open(sys.argv[1],"rb")
header = parse_dada_header(f.read(DADA_HEADER_SIZE))
nchan  = header['NCHAN']
nant   = header['NSTATION']
npol   = header['NPOL']
navg   = header['NAVG']
bps    = header['BYTES_PER_SECOND']
df     = header['BW']*1e6 / float(nchan)
cfreq  = header['CFREQ']*1e6
utc_start = header['UTC_START']
assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
freq0 = cfreq-nchan/2*df
freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
print "bytes per sec:", bps
print "navg:     ", navg
print "nchan:    ", nchan
print "npol:     ", npol
print "nstation: ", nant
nbaseline = nant*(nant+1)//2
print "nbaseline:", nbaseline
noutrig_per_full = int(navg / df + 0.5)
print "noutrig_per_full:", noutrig_per_full
full_framesize   = nchan*nbaseline*npol*npol
print "full_framesize:  ", full_framesize*8
outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
print "outrig_framesize:", outrig_framesize*8
tot_framesize    = (full_framesize + outrig_framesize)
print " tot_framesize:  ",  tot_framesize*8
ntime = float(filesize) / (tot_framesize*8)
print "ntime:", ntime
frame_secs = int(navg / df + 0.5)
print "Frame secs:", frame_secs
time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
print "Time offset:", time_offset

file_contents = [ [] for i in range(int(ntime)) ]

for t in xrange(int(ntime)):
  print "Time ",t
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
  full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
  outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))

  file_contents[t] = full_data

f.close()

# ffmpeg -i amp_phase_1.gif movie.mp4


#for stand in range(NUM_STANDS):
#  print "Autocorrelation",stand
#  plot_ant(file_contents, stand, int(ntime))

#flags = [ 76,77,90,91,92,93,105,106,107,108,109,110,128,72,73,104,105,144,145,148,149,156,157,160,161,164,165,168,169,196,197,224,225,9,159,177,219,246,247,248,249,250,251,252,252,253,254,255 ]

#for i in range(NUM_STANDS): 
#  if i not in flags: find_best_phase_ramp(file_contents, 0, i, flags)

which_correlator_dump = 0
#for i in range(NUM_STANDS):
#  print "Correlation stand",i
#  for pol1 in range(2):
#    for pol2 in range(2):
#      image_stand(file_contents, which_correlator_dump, i, pol1, pol2)
      
for i in range(NUM_STANDS):
  print "Animated", i
  stand_details(file_contents, which_correlator_dump, i)

#back_phases(file_contents, 0, 1)
#make_html()
  

# Channel running through all baseline i.e. x-axis is baselines
#print "Channel",
#plt.figure(figsize=(20,5))
#for chan in range(NUM_CHANNELS): 
#  print chan,
#  plot_channel(file_contents, chan, 0, 0)
#  plot_channel(file_contents, chan, 0, 1)
#  sys.stdin.flush()
#print


       
		
