import sys, math
import numpy as np

all_sources = []
def process_line(l):
  if float(words[6]) < 50: return

  h = float(words[0])
  m = float(words[1])
  s = float(words[2])
  ra = (h*3600+m*60+s)/3600    # Not to degrees, just decimal hrs

  d = float(words[3])
  m = float(words[4])
  s = float(words[5])
  if d < 0:
    m = -m
    s = -s
  dec = (d*3600+m*60+s)/3600

  name = words[0]+words[1]+words[3]+words[4]

  if float(words[7]) > 10:
    all_sources.append((float(words[7]), name, ra, dec))

def print_it(s, what):
    if what == "SOURCE": print what, s[1], ( "%.4f" % s[2] ), ( "%.4f" % s[3] )
    else: print what, ( "%.4f" % s[2] ), ( "%.4f" % s[3] )

    # Add in 20MHz at spectral index of -0.7
    print "FREQ 20e+06   ", ( "%.2f" % ( s[0]*math.pow(20e6/74e6, -0.7) ) ), 0, 0, 0
    print "FREQ 74e+06   ", s[0], 0, 0, 0


def find_clusters():
  distances = np.zeros((len(all_sources), len(all_sources)))
  for i in range(len(all_sources)):
    for j in range(len(all_sources)):
      distances[i][j] = math.sqrt(((all_sources[i][2]*15)-(all_sources[j][2]*15))**2 + (all_sources[i][3]-all_sources[j][3])**2)

  clusters = [ None for i in range(distances.shape[0]) ]
  chosen = []
  for i in range(distances.shape[0]):
    if i not in chosen:
      cluster = [ i ]
      for j in range(distances.shape[1]):
        if i != j and distances[i, j] < 0.1 and j not in chosen:
          cluster.append(j)
          chosen.append(j)
      clusters[i] = cluster

  # Consistency check
  num = 0
  for i in range(len(clusters)):
    if clusters[i]: num += len(clusters[i])
  if num != len(all_sources):
    print "Clustering failed"
    exit(1)
      
  for cluster in clusters:
    if cluster:
      print "#", len(cluster)
      print_it(all_sources[cluster[0]], "SOURCE")
      for i in range(1, len(cluster)):
        print_it(all_sources[cluster[i]], "COMPONENT")
        print "ENDCOMPONENT"
      print "ENDSOURCE"

def print_all():
  for s in all_sources:
    print "SOURCE", s[1], ( "%.4f" % s[2] ), ( "%.4f" % s[3] )
    print "FREQ 74e+06   ", s[0]

    # Add in 20MHz at spectral index of -0.7
    print "FREQ 20e+06   ", ( "%.2f" % ( s[0]*math.pow(20e6/74e6, -0.7) ) )
    print "ENDSOURCE"
  

#SOURCE 0000+5539 0.00568056 55.6517
#FREQ 20e+06    41.06 0.0 0.0 0.0
#FREQ 74e+06    16.43 0.0 0.0 0.0
#ENDSOURCE




i = 0
for line in open(sys.argv[1],"r"):
  if i > 8 and len(line) > 0 and line[0] != " " : 
    words = line.split()
    try:
      process_line(words)
    except:
      pass
  i += 1

all_sources.sort(key=lambda tup: tup[0], reverse=True)
all_sources = all_sources[:200]

find_clusters()

#print_all()
