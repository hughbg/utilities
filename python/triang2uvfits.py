import os, sys, shutil



def is_dada(name):
  return name[-5:] == ".dada"

if len(sys.argv) != 3:
  print "Usage: triang2uvfits <DADA file> <UVFits file>"
  sys.exit(1)

if not os.path.isfile(sys.argv[1]):
  print "File doesn't exist"
  sys.exit(1)

if not is_dada(sys.argv[1]):
  print "File is not dada"
  sys.exit(1)

i = 0
for line in open(sys.argv[1],"rU"):
  l = line.split()
  if l[0] == "DATA_ORDER" and l[1] != "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX":
    print "File is not TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX format"
    sys.exit(1)
  i += 1
  if i > 30: break


print "Make header"
os.system("python /home/leda/hgarsden/make_header.py "+sys.argv[1])
# Only the header file and the L files are important for the input to corr2uvfits
command = "./triang2uvfits header.txt antenna_locations.txt instr_config.txt "+sys.argv[1]+" "+sys.argv[2]
print command
os.system(command)



