import ephem, make_header, os, sys, random,numpy as np
import time, math
from datetime import datetime
from dateutil import tz
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


disk_names = [ "ledastorage", "longterm" ]
HOURS_IN_DAY = 24
ONE_HOUR = 1.0/HOURS_IN_DAY
FOUR_MINUTES = ONE_HOUR/15
SIX_MINUTES = ONE_HOUR/10
BIN_SIZE_AZ = 10
BIN_SIZE_ALT = 5

yticks = []
yticklabels = [ 90, 75, 60, 45, 30, 15, 0 ]
for x in yticklabels:
  yticks.append(math.cos(x*math.pi/180))

ovro_location = ('37.2397808', '-118.2816819', 1183.4839)
ovro = ephem.Observer(); (ovro.lat, ovro.lon, ovro.elev) = ovro_location

cas = ephem.FixedBody()
cas._ra  =  '23:23:24'
cas._dec = '+58:48:54'
cas.name = "Cas"

cyg = ephem.FixedBody()
cyg._ra  =  '19 59 28.357'
cyg._dec = '+40 44 02.10'
cyg.name = "Cyg"

sun = ephem.Sun()

def time_at_timezone(dt_string, zone):
  # Incoming format must be 2016/1/31 22:56:01.
  # ephem.Date() does that
  from_zone = tz.gettz('UTC')
  to_zone = tz.gettz(zone)

  dt = datetime.strptime(dt_string, "%Y/%m/%d %H:%M:%S")

  # Tell the datetime object that it's in UTC time zone since 
  # datetime objects are 'naive' by default
  dt = dt.replace(tzinfo=from_zone)

  # Convert time zone
  return ( "%s" % ephem.Date(dt.astimezone(to_zone)) )

def time_at_ovro(dt_string): return time_at_timezone(dt_string,"America/Los_Angeles")
def time_at_boston(dt_string): return time_at_timezone(dt_string,"America/New_York")

def bastard(fr):
  return fr == 47.004 or fr == 47.004-5.244 or fr == 47.004-0.012

def nightly_observation():		# Run this during the day
  time_to_go_20_degrees = 20/360.0*(23*60*60+56*60)/(24*60*60)      #Days
  gal_center = ephem.FixedBody()	
  gal_center._ra  =  '17 45 40.04'
  gal_center._dec = '-29 00 28.1'
  gal_center.name = "Galactic Center"

  ovro1 = ephem.Observer(); (ovro1.lat, ovro1.lon, ovro1.elev) = ovro_location
  ovro1.date = datetime.now().replace(hour=20, minute=0, second=0, microsecond=0)     # about midday OVRO time, remember ephem dates are UTC

  sun = ephem.Sun()  
  sun.compute(ovro1)
  gal_center.compute(ovro1)

  next_sunset = ephem.Date(ovro1.next_setting(sun)+ephem.hour) # Add 1 hour
  next_sunrise = ovro1.next_rising(sun)	

  previous_galaxyset = ephem.Date(ovro1.previous_setting(gal_center)+time_to_go_20_degrees)
  previous_galaxyrise = ovro1.previous_rising(gal_center)
  next_galaxyset = ephem.Date(ovro1.next_setting(gal_center)+time_to_go_20_degrees)
  next_galaxyrise = ovro1.next_rising(gal_center)	

  times = []
  times.append(("Sunset+1h  ", next_sunset))
  times.append(("Sunrise    ", next_sunrise))
  times.append(("Galaxy Set-20&deg;", previous_galaxyset))
  times.append(("Galaxy Rise", previous_galaxyrise))
  times.append(("Galaxy Set-20&deg;", next_galaxyset))
  times.append(("Galaxy Rise", next_galaxyrise))


  times = sorted(times, key=lambda tup: tup[1])
  
  where_sunset = -1
  for i in range(len(times)):
    if times[i][0][:6] == "Sunset": where_sunset = i
    i += 1
  if where_sunset > 0: start_sequence = where_sunset-1
  else: start_sequence = where_sunset

  start_time = end_time = None

  for i in range(start_sequence, start_sequence+4): print times[i]

  if times[start_sequence][0][:11] == "Galaxy Rise" and times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:7] == "Sunrise" and times[start_sequence+3][0][:10] == "Galaxy Set":
    pass     # Galaxy up all night
  elif times[start_sequence][0][:10] == "Galaxy Set" and times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:7] == "Sunrise" and times[start_sequence+3][0][:11] == "Galaxy Rise":
    end_time = next_sunrise; start_time = next_sunset
  elif times[start_sequence][0][:11] == "Galaxy Rise" and times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:10] == "Galaxy Set" and times[start_sequence+3][0][:7] == "Sunrise":
    end_time = next_sunrise; start_time = times[start_sequence+2][1]
  elif times[start_sequence][0][:10] == "Galaxy Set" and times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:11] == "Galaxy Rise" and times[start_sequence+3][0][:7] == "Sunrise" :
    end_time = times[start_sequence+2][1]; start_time = next_sunset
  elif times[start_sequence][0][:6] == "Sunset" and times[start_sequence+1][0][:10] == "Galaxy Set" and times[start_sequence+2][0][:11] == "Galaxy Rise" and times[start_sequence+3][0][:7] == "Sunrise" :
    end_time = times[start_sequence+2][1]; start_time = times[start_sequence+1][1]
  elif times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:10] == "Galaxy Set" and times[start_sequence+3][0][:11] == "Galaxy Rise" and times[start_sequence+4][0][:7] == "Sunrise" :
    end_time = times[start_sequence+3][1]; start_time = times[start_sequence+2][1]

  print "Selected", start_time, "to", end_time, "(all times UTC)"
  ovro1.date = start_time
  start_lst = ovro1.sidereal_time()
  ovro1.date = end_time
  end_lst = ovro1.sidereal_time()

  # Create the capture interval for today
  schedules = []
  for line in open("/home/leda/leda_server/control/server_control/schedules/leda_schedule.txt","rU"):
    l = line.split()
    if l[0] != "LEDA_WINDOW": schedules.append(line)

  f = open("/home/leda/leda_server/control/server_control/schedules/leda_schedule.txt","w")
  for sc in schedules: f.write(sc)
  f.write("LEDA_WINDOW\t"+":".join(("%s" % start_lst).split(":")[:2])+"\t"+":".join(("%s" % end_lst).split(":")[:2])+"\n")
  f.close()


def track_cyg():
  cyg_alt_az = [ [ [] for j in range(360/(BIN_SIZE_AZ)) ] for i in range(90/BIN_SIZE_ALT) ]  
  cas_alt_az = [ [ [] for j in range(360/(BIN_SIZE_AZ)) ] for i in range(90/BIN_SIZE_ALT) ]  

  for fl in open("/var/www/catalog.txt"):
    file_info = fl.split(",")
    file_name = file_info[0]
    d = file_info[1]
    t = file_info[2]
    order = file_info[4]
    if file_info[5] == "UNKNOWN": freq = 0
    else: freq = float(file_info[5])

    if d != "UNKNOWN" and t != "UNKNOWN":
      ovro.date = d[0:4]+"/"+d[4:6]+"/"+d[6:8]+" "+t[0:2]+":"+t[2:4]+":"+t[4:6]
      cas.compute(ovro)
      cyg.compute(ovro)
      sun.compute(ovro)
    
      cas_alt = float(cas.alt)*180/np.pi
      cyg_alt = float(cyg.alt)*180/np.pi
      sun_alt = float(sun.alt)*180/np.pi
      cas_az = float(cas.az)*180/np.pi
      cyg_az = float(cas.az)*180/np.pi
      if order == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" and bastard(freq) and cas_alt > 5 and sun_alt < 0:
        cas_alt_az[int(cas_alt)/BIN_SIZE_ALT][int(cas_az)/(BIN_SIZE_AZ)].append(os.path.basename(file_name))

      if order == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" and bastard(freq) and cyg_alt > 5 and sun_alt < 0:
        cyg_alt_az[int(cyg_alt)/BIN_SIZE_ALT][int(cyg_az)/(BIN_SIZE_AZ)].append(os.path.basename(file_name))

  selections = []

  print "<html><head><title>Past Cyg/Cas observing times</title></head><body>"
  print "Past Cyg/Cas observations at particular elevation and azimuth. These are what we have observed."
  print "The tables are not in time order, but Elev/Az order. Cas and Cyg must be 5 degrees elevation, and the Sun must be below the horizon."
  print "I look for frequncy 47.004 only (but take into account some shifts that incorrectly labelled frequencies)."

  print "<h2>Cas</h2>"
  print "<table><tr valign=top><td><table cellpadding=10 border><tr><th>Elevation</th><th>Azimuth</th><th>File</th></tr>"
  for i in range(90/BIN_SIZE_ALT):
    for j in range(360/(BIN_SIZE_AZ)):
      if len(cas_alt_az[i][j]) > 0: 
        print "<tr><td>",i*BIN_SIZE_ALT, "</td><td>",j*BIN_SIZE_AZ, "</td><td>"," ".join(sorted(cas_alt_az[i][j][:50])),"... etc.","</td></tr>"
  print "</table></td><td><img src=cas_track.png></td></tr></table>"

  print "<h2>Cyg</h2>"
  print "<table><tr valign=top><td><table cellpadding=10 border><tr><th>Elevation</th><th>Azimuth</th><th>File</th></tr>"
  for i in range(90/BIN_SIZE_ALT):
    for j in range(360/(BIN_SIZE_AZ)):
      if len(cyg_alt_az[i][j]) > 0: 
        print "<tr><td>",i*BIN_SIZE_ALT, "</td><td>",j*(BIN_SIZE_AZ), "</td><td>"," ".join(sorted(cyg_alt_az[i][j][:50])),"... etc.","</td></tr>"
        for k in range(len(cyg_alt_az[i][j])): selections.append(cyg_alt_az[i][j][k])

  print "</table></td><td><img src=cyg_track.png></td></tr></table>"
  print "Schedule<p>"
  for line in open("/home/leda/leda_server/control/server_control/schedules/leda_schedule.txt","rU"):
    sys.stderr.write(line)
    print line,"<br>"

  print "<p>Created by "+sys.argv[0]+"</body></html>"

  #print selections

  # Make a picture

  # Make pictures at higher resolution than sampling for schedule and table

  # Cas
  r = []
  theta = []
  for fl in open("/var/www/catalog.txt"):
    file_info = fl.split(",")
    d = file_info[1]
    t = file_info[2]
    if d != "UNKNOWN" and t != "UNKNOWN":
      ovro.date = d[0:4]+"/"+d[4:6]+"/"+d[6:8]+" "+t[0:2]+":"+t[2:4]+":"+t[4:6]
      cas.compute(ovro)
      sun.compute(ovro)

      cas_alt = float(cas.alt)*180/np.pi
      sun_alt = float(sun.alt)*180/np.pi

      if cas_alt > 5 and sun_alt < 0:
        r.append(np.cos(cas.alt))
        theta.append(cas.az)


  ax = plt.subplot(111, projection='polar')
  ax.plot(theta, r, 'r.', linewidth=3)
  ax.set_theta_zero_location('N')  
  ax.set_rmax(1.0)
  ax.set_rticks(yticks)
  ax.set_yticklabels(yticklabels)
  ax.grid(True)
  ax.set_title("Cas Locations", va='bottom')
  plt.savefig("cas_track.png")
  plt.clf()

  # Cyg
  r = []
  theta = []
  for fl in open("/var/www/catalog.txt"):
    file_info = fl.split(",")
    d = file_info[1]
    t = file_info[2]
    if d != "UNKNOWN" and t != "UNKNOWN":
      ovro.date = d[0:4]+"/"+d[4:6]+"/"+d[6:8]+" "+t[0:2]+":"+t[2:4]+":"+t[4:6]
      cyg.compute(ovro)
      sun.compute(ovro)

      cyg_alt = float(cas.alt)*180/np.pi
      sun_alt = float(sun.alt)*180/np.pi

      if cyg_alt > 5 and sun_alt < 0:
        r.append(np.cos(cyg.alt))
        theta.append(cyg.az)


  ax = plt.subplot(111, projection='polar')
  ax.plot(theta, r, 'b.', linewidth=3)
  ax.set_theta_zero_location('N')  
  ax.set_rmax(1.0)
  ax.set_rticks(yticks)
  ax.set_yticklabels(yticklabels)
  ax.grid(True)
  ax.set_title("Cyg Locations", va='bottom')
  plt.savefig("cyg_track.png")
  plt.clf()

  os.rename("cas_track.png","/var/www/cas_track.png")
  os.rename("cyg_track.png","/var/www/cyg_track.png")
	

def request_cyg():
  cyg_alt_az = [ [ None for j in range(360/(BIN_SIZE_AZ)) ] for i in range(90/BIN_SIZE_ALT) ]  
  cas_alt_az = [ [ None for j in range(360/(BIN_SIZE_AZ)) ] for i in range(90/BIN_SIZE_ALT) ]  
  cyg_lst = [ [ None for j in range(360/(BIN_SIZE_AZ)) ] for i in range(90/BIN_SIZE_ALT) ]  
  cas_lst = [ [ None for j in range(360/(BIN_SIZE_AZ)) ] for i in range(90/BIN_SIZE_ALT) ]  
  utc_times = []
  ovro_date = ovro.date
 
  time_ahead = ovro_date

  min_alt = 1e39
  max_alt = -1e39
  for i in range(100*HOURS_IN_DAY):
    ovro.date = time_ahead+float(i)*ONE_HOUR
    cas.compute(ovro)
    cyg.compute(ovro)
    sun.compute(ovro)
    
    cas_alt = float(cas.alt)*180/np.pi
    cyg_alt = float(cyg.alt)*180/np.pi
    sun_alt = float(sun.alt)*180/np.pi

    cas_az = float(cas.az)*180/np.pi 
    cyg_az = float(cyg.az)*180/np.pi

    if cas_alt > 5 and sun_alt < 0:
      if not cas_alt_az[int(cas_alt)/BIN_SIZE_ALT][int(cas_az)/(BIN_SIZE_AZ)]: 
        cas_alt_az[int(cas_alt)/BIN_SIZE_ALT][int(cas_az)/(BIN_SIZE_AZ)] = ("%s" % time_at_boston(("%s" % ovro.date)))
        cas_lst[int(cas_alt)/BIN_SIZE_ALT][int(cas_az)/(BIN_SIZE_AZ)]= ("%s" % ovro.sidereal_time())
        if ovro.date not in utc_times: utc_times.append(ovro.date)

    if cyg_alt > 5 and sun_alt < 0:
      if not cyg_alt_az[int(cyg_alt)/BIN_SIZE_ALT][int(cyg_az)/(BIN_SIZE_AZ)]: 
        cyg_alt_az[int(cyg_alt)/BIN_SIZE_ALT][int(cyg_az)/(BIN_SIZE_AZ)] = ("%s" % time_at_boston(("%s" % ovro.date)))
        cyg_lst[int(cyg_alt)/BIN_SIZE_ALT][int(cyg_az)/(BIN_SIZE_AZ)] = ("%s" % ovro.sidereal_time())
        if ovro.date not in utc_times: utc_times.append(ovro.date)


  print "<html><head><title>Future Cyg/Cas observing times</title></head><body>"
  print "Future Cyg/Cas observing times to find them at particular elevations over the sky. Future means the next 100 days. The sun must be below the horizon, and Cas and Cyg must be at least 5 degrees above."
  print "Observations are binned into the bins implied by the tables below. The plots show all the locations that are useful, but only those in the table are sampled (for now)."
  print "The tables are not in time order, but Elev/Az order. See at the end for the next observing time slots."

  print "<h2>Cas</h2>"
  print "<table><tr valign=top><td><table cellpadding=10 border><tr><th>Elevation</th><th>Azimuth</th><th>Date/Time in Cambridge</th><th>LST</th></tr>"
  for i in range(90/BIN_SIZE_ALT):
    for j in range(360/(BIN_SIZE_AZ)):
      if cas_alt_az[i][j]: print "<tr><td>",i*BIN_SIZE_ALT, "</td><td>", j*(BIN_SIZE_AZ), "</td><td>", cas_alt_az[i][j],"</td><td>", cas_lst[i][j],"</td></tr>"
  print "</table></td><td><img src=cas_request.png></td></tr></table>"

  print "<h2>Cyg</h2>"
  print "<table><tr valign=top><td><table cellpadding=10 border><tr><th>Elevation</th><th>Azimuth</th><th>Date/Time in Cambridge</th><th>LST</th></tr>"
  for i in range(90/BIN_SIZE_ALT):
    for j in range(360/(BIN_SIZE_AZ)):
      if cyg_alt_az[i][j]: print "<tr><td>",i*BIN_SIZE_ALT, "</td><td>", j*(BIN_SIZE_AZ), "</td><td>", cyg_alt_az[i][j],"</td><td>", cyg_lst[i][j],"</td></tr>"
  print "</table></td><td><img src=cyg_request.png></td></tr></table>"

  print "<p><b>Times for today",ephem.Date(datetime.utcnow()),"UTC </b><p><pre>"

  # Create the capture interval for today
  schedules = []
  for line in open("/home/leda/leda_server/control/server_control/schedules/leda_schedule.txt","rU"):
    l = line.split()
    if l[0] != "CYG_CAS_OFF_ZENITH": schedules.append(line)

  f = open("/home/leda/leda_server/control/server_control/schedules/leda_schedule.txt","w")
  for sc in schedules: f.write(sc)
  now = ephem.Date(datetime.utcnow())
  for t in utc_times: 
    if int(now) == int(t):     # same day
      ovro.date = t-SIX_MINUTES
      lst_start = ovro.sidereal_time()
      ovro.date = t+SIX_MINUTES
      lst_end = ovro.sidereal_time()
      f.write("CYG_CAS_OFF_ZENITH\t"+":".join(("%s" % lst_start).split(":")[:2])+"\t"+":".join(("%s" % lst_end).split(":")[:2])+"\n")
  f.close()

  for line in open("/home/leda/leda_server/control/server_control/schedules/leda_schedule.txt","rU"):
    sys.stderr.write(line)
    print line,
    
  #print min_alt, max_alt
    
  print "</pre><p>Created by "+sys.argv[0]+"</body></html>"

  # Make pictures at higher resolution than sampling for schedule and table

  # Cas
  r = []
  theta = []
  time_ahead = ovro_date
  for i in range(100*HOURS_IN_DAY):
    ovro.date = time_ahead+float(i)*ONE_HOUR
    cas.compute(ovro)
    sun.compute(ovro)
    cas_alt = float(cas.alt)*180/np.pi
    cas_az = float(cas.az)*180/np.pi 
    sun_alt = float(sun.alt)*180/np.pi

    if cas_alt > 5 and sun_alt < 0:
      r.append(np.cos(cas.alt))
      theta.append(cas.az)

  ax = plt.subplot(111, projection='polar')
  ax.plot(theta, r, 'r.', linewidth=3)
  ax.set_theta_zero_location('N')  
  ax.set_rmax(1.0)
  ax.set_rticks(yticks)
  ax.set_yticklabels(yticklabels)
  ax.grid(True)
  ax.set_title("Cas Locations", va='bottom')
  plt.savefig("cas_request.png")
  plt.clf()

  # Cyg
  r = []
  theta = []
  time_ahead = ovro_date
  for i in range(100*HOURS_IN_DAY):
    ovro.date = time_ahead+float(i)*ONE_HOUR
    cyg.compute(ovro)
    sun.compute(ovro)
    cyg_alt = float(cyg.alt)*180/np.pi
    cyg_az = float(cyg.az)*180/np.pi
    sun_alt = float(sun.alt)*180/np.pi

    if cyg_alt > 5 and sun_alt < 0:
      r.append(np.cos(cyg.alt))
      theta.append(cyg.az)

  ax = plt.subplot(111, projection='polar')
  ax.plot(theta, r, 'b.', linewidth=3)
  ax.set_theta_zero_location('N')  
  ax.set_rmax(1.0)
  ax.set_rticks(yticks)
  ax.set_yticklabels(yticklabels)
  ax.grid(True)
  ax.set_title("Cyg Locations", va='bottom')
  plt.savefig("cyg_request.png")
  plt.clf()

  os.rename("cas_request.png","/var/www/cas_request.png")
  os.rename("cyg_request.png","/var/www/cyg_request.png")



sys.stderr.write(time.strftime("%d/%m/%Y %H:%M:%S")+" "+sys.argv[1]+" ---------------------------\n")
if sys.argv[1] == "request": request_cyg()
elif sys.argv[1] == "track": track_cyg()
elif sys.argv[1] == "night": nightly_observation()


