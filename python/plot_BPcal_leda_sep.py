#!/usr/bin/python
#
# script that reads in a TLE file, estimates the power output by the beamformer, and plots the result.
#
# Requires:
#   - PyEphem
#   - Matplotlib
#

import sys, getopt, string, re
from pylab import *

#----------------------------------------------------------------------------------------------------------------------#
# check for command-line options

def help_message():
    print '''
Options: --file=name -- Optional. bandpass file to plot.
Options: --phases    -- Optional. plot phases rather than amplitudes.
Options: --chan      -- Optional. use channel number as x axis.
Options: --stands    -- Optional. List of stands, 1..256 to display
Options: --raw       -- Optional. use channel number as x axis, with no ordering.
                                  i.e., if above pfb channel 128.
    '''
    sys.exit(0)

#----------------------------------------------------------------------------------------------------------------------#
# initialise command-line variables.

bp_file = 'BandpassCalibration_node001.dat';
sel_offset = 1
plot_chan = 0
plot_raw  = 0
stand_list = None

#----------------------------------------------------------------------------------------------------------------------#
# read command-line arguments

try:
    options, xarguments = getopt.getopt(sys.argv[1:], '', ['file=','phases','chan','raw','stands='])
except getopt.error:
    help_message()

for a in options[:]:
    if a[0] == '--file' and a[1] != '':
        bp_file = a[1]
        options.remove(a)
        break
    elif a[0] == '--file' and a[1] == '':
        print '--file expects an argument'
        sys.exit(0)
for a in options[:]:
    if a[0] == '--phases':
        sel_offset = 2
        options.remove(a)
        break
for a in options[:]:
    if a[0] == '--chan':
        plot_chan = 1
        options.remove(a)
        break
for a in options[:]:
    if a[0] == '--raw':
        plot_raw  = 1
        plot_chan = 1
        options.remove(a)
        break

for a in options[:]:
    if a[0] == '--stands' and a[1] != '':
        stand_list = a[1]
        options.remove(a)
        break
    elif a[0] == '--stands' and a[1] == '':
        print '--stand expects an argument'
        sys.exit(0)

if stand_list != None:
  stand_list = [ int(el) for el in stand_list.split(",") ]

#----------------------------------------------------------------------------------------------------------------------#
# MAIN

# open the tle file
try:
    fid = open(bp_file, 'r')
except IOError:
    print 'Can\'t open file \'' + bp_file + '\' for reading.'
    sys.exit(0)

# read the file into an array
lines = fid.readlines()

# check that something was read in
if len(lines)==0:
    print "Error reading bandpass file: no lines read from the file."
    sys.exit(0)
elif len(lines)%8!=1:
    print "Error reading bandpass file: number of lines should be 1 plus a multiple of 8."
    sys.exit(0)


if stand_list != None:
  new_stand_list = []
  missing = []
  stand_present = []
  for l in lines[1:]:
    stand_present.append(int(l.split(",")[0]))

  for stand in stand_list:
    if stand not in stand_present: missing.append(stand)
    else: new_stand_list.append(stand)

  if len(missing) > 0:
    for m in missing:
      print str(m)+",",
    print "are missing"
    print "New list",
    for a in new_stand_list: print str(a)+",",
    print 

  stand_list = new_stand_list

# close the file
fid.close()

# initialise the body list
PX_lsq = []
PY_lsq = []
QX_lsq = []
QY_lsq = []
PX_fit = []
PY_fit = []
QX_fit = []
QY_fit = []

tmp1 = string.split( lines[0], ',' )
tmp2 = []
for val in tmp1:
    tmp2.append( float(val) )
N_ch = len(tmp2)

freq = zeros(N_ch)
for k in range(0,N_ch):
    freq[k] = tmp2[k]

if plot_raw:
    ch = range(0,N_ch)
else:
    ch = zeros(N_ch)
    for k in range(0,N_ch):
        ch[k] = freq[k]/0.04

freq_idx = argsort(freq)

chan_sel = sel_offset + 2*array(freq_idx)

for lineIndex in range(1, len(lines), 8):  # get 0, 8, 16, ...

    tmp = string.split( lines[lineIndex+0], ',' ); PX_lsq.append([]); PX_lsq[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PX_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+1], ',' ); PX_fit.append([]); PX_fit[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PX_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+2], ',' ); PY_lsq.append([]); PY_lsq[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PY_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+3], ',' ); PY_fit.append([]); PY_fit[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PY_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+4], ',' ); QX_lsq.append([]); QX_lsq[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QX_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+5], ',' ); QX_fit.append([]); QX_fit[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QX_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+6], ',' ); QY_lsq.append([]); QY_lsq[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QY_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+7], ',' ); QY_fit.append([]); QY_fit[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QY_fit[-1][k] = float(tmp[k])

N_stand = len(PX_lsq)

print "plotting Jones matrices for %d frequency channels and %d stands" % (N_ch, N_stand)

# Step through the channels and print spectra

fig1 = figure(figsize=(14,8))
fig2 = figure(figsize=(14,8))
#fig3 = figure(figsize=(14,8))
#fig4 = figure(figsize=(14,8))

if sel_offset == 1:

    figure(1); fig1.canvas.set_window_title('Amp. of fits for the sky X -> instr. P elements of the Jones matrices') 
    figure(2); fig2.canvas.set_window_title('Amp. of fits for the sky Y -> instr. Q elements of the Jones matrices') 
#    figure(3); fig3.canvas.set_window_title('Amp. of fits for the sky Y -> instr. P elements of the Jones matrices') 
#    figure(4); fig4.canvas.set_window_title('Amp. of fits for the sky X -> instr. Q elements of the Jones matrices') 

    line1 = '-b'
    line2 = '-r'

elif sel_offset == 2:

    figure(1); fig1.canvas.set_window_title('Phase of fits for the sky X -> instr. P elements of the Jones matrices') 
    figure(2); fig2.canvas.set_window_title('Phase of fits for the sky Y -> instr. Q elements of the Jones matrices') 
#    figure(3); fig3.canvas.set_window_title('Phase of fits for the sky Y -> instr. P elements of the Jones matrices') 
#    figure(4); fig4.canvas.set_window_title('Phase of fits for the sky X -> instr. Q elements of the Jones matrices') 

    line1 = 'b'
    line2 = '-r'

    # unwrap 2pi phase jumps
    for stand in range(0, N_stand):
        last_PX_lsq_phase = PX_lsq[stand][2]
        last_PY_lsq_phase = PY_lsq[stand][2]
        last_QX_lsq_phase = QX_lsq[stand][2]
        last_QY_lsq_phase = QY_lsq[stand][2]
        last_PX_fit_phase = PX_fit[stand][2]
        last_PY_fit_phase = PY_fit[stand][2]
        last_QX_fit_phase = QX_fit[stand][2]
        last_QY_fit_phase = QY_fit[stand][2]
        PX_lsq[stand][2] *= 180/pi
        PY_lsq[stand][2] *= 180/pi
        QX_lsq[stand][2] *= 180/pi
        QY_lsq[stand][2] *= 180/pi
        PX_fit[stand][2] *= 180/pi
        PY_fit[stand][2] *= 180/pi
        QX_fit[stand][2] *= 180/pi
        QY_fit[stand][2] *= 180/pi

        for offset in range(4,2*N_ch+1,2):

            #if PX_lsq[stand][offset]-last_PX_lsq_phase > pi: PX_lsq[stand][offset] -= 2*pi
            #if PY_lsq[stand][offset]-last_PY_lsq_phase > pi: PY_lsq[stand][offset] -= 2*pi
            #if QX_lsq[stand][offset]-last_QX_lsq_phase > pi: QX_lsq[stand][offset] -= 2*pi
            #if QY_lsq[stand][offset]-last_QY_lsq_phase > pi: QY_lsq[stand][offset] -= 2*pi
            #if PX_fit[stand][offset]-last_PX_fit_phase > pi: PX_fit[stand][offset] -= 2*pi
            #if PY_fit[stand][offset]-last_PY_fit_phase > pi: PY_fit[stand][offset] -= 2*pi
            #if QX_fit[stand][offset]-last_QX_fit_phase > pi: QX_fit[stand][offset] -= 2*pi
            #if QY_fit[stand][offset]-last_QY_fit_phase > pi: QY_fit[stand][offset] -= 2*pi

            #if PX_lsq[stand][offset]-last_PX_lsq_phase < -pi: PX_lsq[stand][offset] += 2*pi
            #if PY_lsq[stand][offset]-last_PY_lsq_phase < -pi: PY_lsq[stand][offset] += 2*pi
            #if QX_lsq[stand][offset]-last_QX_lsq_phase < -pi: QX_lsq[stand][offset] += 2*pi
            #if QY_lsq[stand][offset]-last_QY_lsq_phase < -pi: QY_lsq[stand][offset] += 2*pi
            #if PX_fit[stand][offset]-last_PX_fit_phase < -pi: PX_fit[stand][offset] += 2*pi
            #if PY_fit[stand][offset]-last_PY_fit_phase < -pi: PY_fit[stand][offset] += 2*pi
            #if QX_fit[stand][offset]-last_QX_fit_phase < -pi: QX_fit[stand][offset] += 2*pi
            #if QY_fit[stand][offset]-last_QY_fit_phase < -pi: QY_fit[stand][offset] += 2*pi

            last_PX_lsq_phase = PX_lsq[stand][offset]
            last_PY_lsq_phase = PY_lsq[stand][offset]
            last_QX_lsq_phase = QX_lsq[stand][offset]
            last_QY_lsq_phase = QY_lsq[stand][offset]
            last_PX_fit_phase = PX_fit[stand][offset]
            last_PY_fit_phase = PY_fit[stand][offset]
            last_QX_fit_phase = QX_fit[stand][offset]
            last_QY_fit_phase = QY_fit[stand][offset]

            PX_lsq[stand][offset] *= 180/pi
            PY_lsq[stand][offset] *= 180/pi
            QX_lsq[stand][offset] *= 180/pi
            QY_lsq[stand][offset] *= 180/pi
            PX_fit[stand][offset] *= 180/pi
            PY_fit[stand][offset] *= 180/pi
            QX_fit[stand][offset] *= 180/pi
            QY_fit[stand][offset] *= 180/pi

if plot_chan==1: freq=ch

band_start = freq[0]
quart_bw = ( freq[-1] - band_start ) / 4.0

# Work out mapping from stands to subplots
if stand_list == None: N_subplots = N_stand
else: N_subplots = len(stand_list)
rows = int(math.ceil(sqrt(N_subplots)))
cols = int(math.trunc(sqrt(N_subplots)))
while rows*cols < N_subplots: cols += 1
print rows,"rows",cols,"cols"

rc('font', size=8)
legendfont = matplotlib.font_manager.FontProperties(size=8)

done = []
print "stand",
for index in range(0, N_stand):

    stand = PX_lsq[index][0]		# These are 1-based

    if stand_list != None and stand not in stand_list: continue

    print int(stand),
    sys.stdout.flush()

    if stand_list == None:
      which_subplot = index+1
    else: 
      # Find the index of this stand in the list
      which_subplot = stand_list.index(stand)+1
      done.append(stand)

    figure(1);
    ax = subplot(rows, cols,which_subplot);
    plot(freq[freq_idx],PX_lsq[index][chan_sel], line1, label='input ID=%2d'%stand)
    plot(freq[freq_idx],PX_fit[index][chan_sel], line2 )
    legend(loc=[0,0.85],prop=legendfont)
    ax.xaxis.set_major_locator(MaxNLocator(4))
    ax.yaxis.set_major_locator(MaxNLocator(5))
    if plot_chan==1: ax.xaxis.set_major_formatter(FormatStrFormatter("%d"))
    if N_stand-index<=8 and plot_chan==0: xlabel( "MHz" )
    if N_stand-index<=8 and plot_chan==1: xlabel( "dumb chan # (doesn't skip flagged channels)" )

    if sel_offset == 1 and index==4: title( "gain relative to band average" )
    if sel_offset == 2 and index==4: title( "phase in degrees" )
    #if sel_offset == 2: ylim(-180,180)
    grid(True)

    figure(2);
    ax = subplot(rows, cols,which_subplot)
    plot(freq[freq_idx],QY_lsq[index][chan_sel], line1, label='input ID=%2d'%stand)
    plot(freq[freq_idx],QY_fit[index][chan_sel], line2)
    legend(loc=[0,0.85],prop=legendfont)
    ax.xaxis.set_major_locator(MaxNLocator(4))
    ax.yaxis.set_major_locator(MaxNLocator(5))
    if plot_chan==1: ax.xaxis.set_major_formatter(FormatStrFormatter("%d"))
    if N_stand-index<=8 and plot_chan==0: xlabel( "MHz" )
    if N_stand-index<=8 and plot_chan==1: xlabel( "dumb chan # (doesn't skip flagged channels)" )

    if sel_offset == 1 and index==4: title( "gain relative to band average" )
    if sel_offset == 2 and index==4: title( "phase in degrees" )
    #if sel_offset == 2: ylim(-180,180)
    grid(True)

#    figure(3);
#    ax = subplot(16,8,index)
#    plot(freq[freq_idx],PY_lsq[index][chan_sel], line1, label='input ID=%2d'%index)
#    plot(freq[freq_idx],PY_fit[index][chan_sel], line2)
#    legend(loc=[0,0.85],prop=legendfont)
#    ax.xaxis.set_major_locator(MaxNLocator(4))
#    ax.yaxis.set_major_locator(MaxNLocator(5))
#    if plot_chan==1: ax.xaxis.set_major_formatter(FormatStrFormatter("%d"))
#    if N_stand-index<=8 and plot_chan==0: xlabel( "MHz" )
#    if N_stand-index<=8 and plot_chan==1: xlabel( "dumb chan # (doesn't skip flagged channels)" )
#    if sel_offset == 2: ylim(-180,180)
#
#    figure(4);
#    ax = subplot(16,8,index)
#    plot(freq[freq_idx],QX_lsq[index][chan_sel], line1, label='input ID=%2d'%index)
#    plot(freq[freq_idx],QX_fit[index][chan_sel], line2)
#    legend(loc=[0,0.85],prop=legendfont)
#    ax.xaxis.set_major_locator(MaxNLocator(4))
#    ax.yaxis.set_major_locator(MaxNLocator(5))
#    if plot_chan==1: ax.xaxis.set_major_formatter(FormatStrFormatter("%d"))
#    if N_stand-index<=8 and plot_chan==0: xlabel( "MHz" )
#    if N_stand-index<=8 and plot_chan==1: xlabel( "dumb chan # (doesn't skip flagged channels)" )
#    if sel_offset == 2: ylim(-180,180)
#




print
show()
