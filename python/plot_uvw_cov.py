import matplotlib.pyplot as plt, sys
import scipy.interpolate
import numpy as np
import random, os

FFT_SIZE = 1024
MAX_COORD_VAL = 410

def conj_is_same(c1, c2):
  return abs(c1.real-c2.real) < 0.0000001 and abs(c1.imag+c2.imag) < 0.0000001


class FFTMap(object):
  def __init__(self, incoming):			# Incoming can be the result of fft2, or a size
    if type(incoming) is int:
      if incoming%2 != 0:
        print "ERROR: size must be even for standard map"
        sys.exit(1)
      self.dimension = incoming		
      self.data = np.zeros((self.dimension, self.dimension), dtype=np.complex64)
    else:
      self.dimension = incoming.shape[0]		
      self.data = np.zeros((self.dimension, self.dimension), dtype=np.complex64)
      for i in range(self.dimension):
        for j in range(self.dimension): self.data[i, j] = incoming[i, j]

  # Standard 2-D FFT format has negative frequencies at the tops of the array. 
  # Should be same as fftshift and ifftshift for even-sized arrays. A shift followed
  # by shift returns the array to the orginal state
  def shift(self):        
    standard_format = self.data.copy()     
    for i in range(self.dimension):
      for j in range(self.dimension):
        u = i-self.dimension/2
        v = j-self.dimension/2
        if 0 <= u: x = u
        else: x = u+self.dimension
        if 0 <= v: y = v
        else: y = v+self.dimension
        #print i, j, "->", x, y
        standard_format[x, y] = self.data[i, j]       # Should be += for the sum of Nyquist

    self.data = standard_format       

  def check_consistent(self):       # Only for format where DC is at 0, 0
    i = 0
    for j in range(1, self.dimension/2):     # symmetry along the i=0 axis
      conj_j = self.dimension-j
      if not conj_is_same(self.data[i, j], self.data[i, conj_j]): 
         print "Bad conjugate 1", i-self.dimension/2, j-self.dimension/2, self.data[i, j], i-self.dimension/2, conj_j-self.dimension/2, self.data[i, conj_j]

    j = 0
    for i in range(1, self.dimension/2):     # symmetry along the j=0 axis
      conj_i = self.dimension-i
      if not conj_is_same(self.data[i, j], self.data[conj_i, j]): 
         print "Bad conjugate 2", i-self.dimension/2, j-self.dimension/2, self.data[i, j], conj_i-self.dimension/2, j-self.dimension/2, self.data[conj_i, j]
 
    for j in range(1, self.dimension/2+1):		# symmetry along columns
      conj_j = self.dimension-j
      for i in range(1, self.dimension):
        conj_i = self.dimension-i
        if not conj_is_same(self.data[i, j], self.data[conj_i, conj_j]): 
           print "Bad conjugate 3", i-self.dimension/2, j-self.dimension/2, self.data[i, j], conj_i-self.dimension/2, conj_j-self.dimension/2, self.data[conj_i, conj_j]

    if self.data[0, 0].imag != 0.0:
      print "0, 0 component is not 0 in imaginary", self.data[0, 0].imag
    if self.data[self.dimension/2, self.dimension/2].imag != 0.0:
      print self.dimension/2, self.dimension/2, " component is not 0 in imaginary", self.data[self.dimension/2, self.dimension/2].imag


  def print_map(self):
    for i in range(self.dimension):
      for j in range(self.dimension):
        print "("+( "%.3f" % self.data[i, j].real )+","+( "%.3f" % self.data[i, j].imag )+")",
      print



# fftshift changes locations from 
#-4 -3 -2 -1 0 1 2 3
#to
#0 1 2 3 -4 -3 -2 -1
#but does not change values

def do_grid(title, flags, output_file, zoom=False, extras=False):
  grid = FFTMap(FFT_SIZE)
  done = []
  for line in open("uvw.dat"):	# Run sort | uniq on this file to get rid of duplicates. Conjugates (-u, -v) do not appear in the file.
    a, st1, st2, u, v = line.split()     # a is tag "UVW"
    u = float(u)
    v = float(v)

    if int(st1) not in flags and int(st2) not in flags:
      x = int(round(u))
      y = int(round(v))
 
      quick = False
      if quick:
        if -(FFT_SIZE/2) <= x and x < FFT_SIZE/2 and -(FFT_SIZE/2) <= y and y < FFT_SIZE/2: grid.data[x+FFT_SIZE/2, y+FFT_SIZE/2] = 1024
        x = -x
        y = -y
        if -(FFT_SIZE/2) <= x and x < FFT_SIZE/2 and -(FFT_SIZE/2) <= y and y < FFT_SIZE/2: grid.data[x+FFT_SIZE/2, y+FFT_SIZE/2] = 1024
      else:

        if (x, y) not in done and -(FFT_SIZE/2) <= x and x < FFT_SIZE/2 and -(FFT_SIZE/2) <= y and y < FFT_SIZE/2:
          sum_gauss = 0.0
          for i in range(x-1, x+2, 1):
            for j in range(y-1, y+2, 1):
              sum_gauss += np.exp(-(((i-x)**2)/0.5+((j-y)**2))/0.5)
        
          for i in range(x-1, x+2, 1):
            for j in range(y-1, y+2, 1):
              if -(FFT_SIZE/2) <= i and i < FFT_SIZE/2 and -(FFT_SIZE/2) <= j and j < FFT_SIZE/2: grid.data[i+FFT_SIZE/2, j+FFT_SIZE/2] += 100*FFT_SIZE*np.exp(-(((i-x)**2)/0.5+((j-y)**2))/0.5)/sum_gauss

          done.append((x, y))

        x = -x
        y = -y

        if (x, y) not in done and -(FFT_SIZE/2) <= x and x < FFT_SIZE/2 and -(FFT_SIZE/2) <= y and y < FFT_SIZE/2:
          sum_gauss = 0.0
          for i in range(x-1, x+2, 1):
            for j in range(y-1, y+2, 1):
              sum_gauss += np.exp(-(((i-x)**2)/0.5+((j-y)**2))/0.5)

          for i in range(x-1, x+2, 1):
            for j in range(y-1, y+2, 1):
              if -(FFT_SIZE/2) <= i and i < FFT_SIZE/2 and -(FFT_SIZE/2) <= j and j < FFT_SIZE/2: grid.data[i+FFT_SIZE/2, j+FFT_SIZE/2] += 100*FFT_SIZE*np.exp(-(((i-x)**2)/0.5+((j-y)**2))/0.5)/sum_gauss

          done.append((x, y))

  grid.check_consistent()

  if False and extras:
    radius = 2
    while radius < 200:
      empty = True
      full = True
      for i in range(-radius, radius+1, 1):
        for j in range(-radius, radius+1, 1):
          if radius <= np.sqrt(i**2+j**2) and np.sqrt(i**2+j**2) < radius+1:
            x = i+FFT_SIZE/2
            y = j+FFT_SIZE/2
            if grid.data[x, y] > 0: 
              empty = False
            else: full = False

      print radius, empty, full
      radius += 1

    num = 0
    for i in range(1024):
      for j in range(1024):
        if grid.data[i, j] > 0: num += 1
    print "Num 1s", num

  grid.shift()
  image = np.fft.fftshift(np.real(np.fft.ifft2(grid.data)))

  clip_val = 50

  if extras:
    x = np.zeros(image.shape[0])
    y = np.zeros(image.shape[0])
    for i in range(image.shape[0]):
       x[i] = i
       y[i] = image[i, image.shape[1]/2]
       if y[i] > clip_val: y[i] = clip_val 
       if y[i] < -clip_val: y[i] = -clip_val   
    plt.xlim(x[0], x[-1])
    plt.title("PSF Profile")
    plt.plot(x, y)
    np.savetxt("y.dat", y)
    plt.savefig("profile.png")
  
  print np.min(image), np.max(image)
  if zoom: image = image[FFT_SIZE/2-50:FFT_SIZE/2+50, FFT_SIZE/2-50:FFT_SIZE/2+50]
  else:
    offset = 8
    for i in range(image.shape[0]):
      for j in range(image.shape[1]):
        if abs(i-FFT_SIZE/2) < offset and abs(j-FFT_SIZE/2) < offset: image[i, j] = -0.1

  # Clip and square
  for i in range(image.shape[0]):
    for j in range(image.shape[1]):
      if image[i, j] > clip_val: image[i,j] = clip_val
      if image[i, j] < -clip_val: image[i,j] = -clip_val
      
      if not zoom:
        if image[i, j] > 0: image[i, j] = image[i, j]**10
        else: image[i, j] = -(abs(image[i, j])**10)

  

  f, ax = plt.subplots()
  ax.set_aspect("equal")
  plt.title(title)
  if zoom: plt.zlim = ( -clip_val, clip_val ) 
  else: plt.zlim = (-(clip_val**10), clip_val**10)
  plt.imshow(image, cmap="gray")
  plt.savefig(output_file)
  plt.clf()
  

def do_plot(title, flags, output_file):

  max_d = 0
  x = []
  y = []

  print 56 in flags
  for line in open("uvw.dat"):
    a, st1, st2, u, v = line.split()     # a is tag "UVW"
    u = float(u)
    v = float(v)
    if int(st1) not in flags and int(st2) not in flags:
      x.append(u)
      x.append(-u)
      x.append(u)
      x.append(-u)
      y.append(v)
      y.append(v)
      y.append(-v)
      y.append(-v)

      if abs(u) > max_d: max_d = abs(u)
      if abs(v) > max_d: max_d = abs(v)

  print max_d
    
  print len(x), len(flags)
      
  f, ax = plt.subplots()
  ax.set_aspect("equal")
  plt.plot(x, y, "r.", ms=0.8)
  plt.xlim(-50, 50)
  plt.ylim(-50, 50)
  plt.title(title)
  plt.xlabel("U (metres)")
  plt.ylabel("V (metres)")
  plt.savefig(output_file)
  plt.clf()
  print "Created",output_file

outriggers = [251, 252, 253, 254, 255 ]
caltech = [ 56,57,58,59,60,61,62,63, 120,121,122,123,124,125,126,127, 184,185,186,187,188,189,190,191, 238,239,240,241,242,243,244,245  ]
bad_stands = [ 0,56,57,58,59,60,61,62,63,72,74,75,76,77,78,82,83,84,85,86,87,91,92,93,104,120,121,122,123,124,125,126,127,128,145,148,157,161,164,168,184,185,186,187,188,189,190,191,197,220,224,225,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255 ]
#do_plot("UV Coverage - all stands", [], "all_stands.png")
#do_plot("UV Coverage - all stands minus LEDA outriggers", outriggers, "no_outriggers.png")
#do_plot("UV Coverage - all stands minus Caltech stands", caltech, "no_caltech.png")
#do_plot("UV Coverage - all stands minus Caltech stands and LEDA outriggers", outriggers+caltech, "no_outriggers_or_caltech.png")
#do_plot("UV Coverage - all stands minus bad stands, Caltech stands, LEDA outriggers", outriggers+caltech+bad_stands, "no_bad.png")

do_grid("PSF - all stands", [], "all_stands_psf.png", False, True)
do_grid("PSF - all stands, zoomed", [], "all_stands_psf_zoomed.png", True, False)
do_grid("PSF - all stands minus LEDA outriggers", outriggers, "no_outriggers_psf.png", False, False)
do_grid("PSF - all stands minus LEDA outriggers, zoomed", outriggers, "no_outriggers_psf_zoomed.png", True, False)
do_grid("PSF - all stands minus Caltech stands", caltech, "no_caltech_psf.png", False, False)
do_grid("PSF - all stands minus Caltech stands, zoomed", caltech, "no_caltech_psf_zoomed.png", True, False)
do_grid("PSF - all stands minus Caltech stands and\nLEDA outriggers", outriggers+caltech, "no_outriggers_or_caltech_psf.png", False, False)
do_grid("PSF - all stands minus Caltech stands and\nLEDA outriggers, zoomed", outriggers+caltech, "no_outriggers_or_caltech_psf_zoomed.png", True, False)
do_grid("PSF - all stands minus bad stands, Caltech\nstands, LEDA outriggers", outriggers+caltech+bad_stands, "no_bad_psf.png", False, False)
do_grid("PSF - all stands minus bad stands, Caltech\nstands, LEDA outriggers, zoomed", outriggers+caltech+bad_stands, "no_bad_psf_zoomed.png", True, False)


# Negative frequencies are just conjugates of the positive ones for real input. The frequency at A[1] has a negative frequency at A[N-1] in the 1-D case.
# The negative frequencies double the output in the real-valued signal. In 2-D the frequency at u, v should have a negative frequency at -u, -v that is the conjugate.
# This also applies when u or v is 0. In fact it applies to the whole FFT. ??
# https://docs.scipy.org/doc/numpy-1.10.1/reference/routines.fft.html
# Frequencies for a signal of length 10 go as        0 1 2 3 4 N -4 -3 -2 -1    where N is nyquist - but is it frequency doubles. It should bereal, also. See numpy fftshift

"""
OK.  Re your previous note,
recall that we want to assess
the filling of the UV plane
after gridding for a wide fov.

To quantify what you have found
in the images, I suggest cuts
for each plot and reducing the
plot y-axis range so that details
can be seen (de-emphasizing the
central peak).

I don't understand your comment
that you use log scaling since the
yaxis goes from <0 to +120.  Is
the peak 10^120 or e^120?

Lincoln
"""
