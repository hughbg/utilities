import scipy.io as sio
import numpy as np
import struct, random
import os, sys, scipy.signal

#random.seed()

def binary(x):	# Assuming np.int64.
  s = ""
  for i in range(64):
    val = np.bitwise_and(x, np.int64(0b0000000000000000000000000000000000000000000000000000000000000001))
    if val == 1: s = "1"+s
    else: s = "0"+s
    x = np.right_shift(x, 1)
  return s

def smooth(data):	# smooth perturbations out of gains
  x = np.linspace(0, BP_END-BP_START-1, BP_END-BP_START)
  z = np.polyfit(x, data[BP_START:BP_END], 15)
  p = np.poly1d(z)
  result = list(data)
  result[BP_START:BP_END] = p(x)

  return result

def gains_as_lines(gains):
  data = gains[:BP_START]
  data = np.append(data, np.linspace(gains[BP_START], np.min(gains), 1330-BP_START))
  data = np.append(data, np.linspace(np.min(gains), np.min(gains), 2150-1330))
  data = np.append(data, np.linspace(np.min(gains), gains[BP_END-1], BP_END-2150))
  data = np.append(data, gains[BP_END:])
  
  return data

  
def bit_round(x):	# Using the bottom 32 bits as the fraction, round the number. Assuming np.int64.

  # If the number is -ve then work on the +ve version.
  neg = False 
  if x < 0: 
    x = -x
    neg = True

  # For rounding select the remainder which is bit 31 down, and put it after the decimal point. 
  # This simple bit op would be messy if the number is -ve due to complements etc.
  remainder = np.bitwise_and(x, np.int64(0b0000000000000000000000000000000011111111111111111111111111111111))     # Bottom 32 bits, i.e. 31:0

  # Get the number that is in the top 32 bits of x
  number = np.right_shift(x, 32)

  #if neg: xx = -(number+remainder)
  #else: xx = (number+remainder)
  #print xx 
  
  if "rounding:on" in sys.argv:		# Otherwise truncation
    # Doing rounding.
    if remainder > np.int64(0b0000000000000000000000000000000010000000000000000000000000000000) or \
	  (remainder == np.int64(0b0000000000000000000000000000000010000000000000000000000000000000) and ((number+1)%2 == 0)):
      number += 1 
  
  #if neg: xx = -x
  #else: xx = x
  #print xx
  

  x = np.left_shift(number, 32)
  
  if neg: x = -x

  return x


print sys.argv


NUM_SAMPLES = int(sys.argv[3])
BP_START = 600
BP_END = 3650
WHICH_BOARD = 9		# 1 to 16

scale_fac =  0.63025
shift_fac = 27

digitalGainDict=sio.loadmat('/home/hgarsden/tmp/gainsSolved.mat')
digitalGains=digitalGainDict['gains']
gains = digitalGains[WHICH_BOARD-1,3,:]

if "line_gains:on" in sys.argv:
  print "Equalizer gains as lines"
  gains = gains_as_lines(gains)

# Unequalized
odata = np.ones(4096, 'l') * (2 ** shift_fac - 1) * scale_fac*1
cstr = struct.pack('>4096l', *odata)

# Equalized
odata3=odata*gains
cstr3 = struct.pack('>4096l', *odata3)
        
# Generate smooth equalizer gains for creating bandpass. Smooth the gains to get rid of small perturbations that can appear as sawtooth later on
if "line_gains:on" in sys.argv:
  smoothed_gains = gains
else: smoothed_gains = smooth(struct.unpack('>4096l', cstr3))

bandpass = np.full(4096, 1, dtype=np.float64)/smoothed_gains*1e9
bandpass[:BP_START] = 0     
bandpass[BP_END:] = 0

if "equalization:on" in sys.argv:			# use channel independent
  print "Channel dependent gain"

  gains = smoothed_gains     # still integer

  # Now have to smooth the floats
  if "line_gains:on" in sys.argv:
    float_gains = gains
  else: float_gains = smooth(odata3)

else:
  print "Channel gain all same"
  gains = struct.unpack('>4096l', cstr)
  float_gains = odata

# Perturb bandpass
if "perturb_bandpass:on" in sys.argv:
  x = np.linspace(0, BP_END-BP_START-1, BP_END-BP_START)
  z = np.polyfit(x, bandpass[BP_START:BP_END], 8)
  p = np.poly1d(z)
  bandpass[BP_START:BP_END] = p(x)

np.savetxt("bandpass.dat", bandpass)
np.savetxt("gains.dat", gains)

saturation_positive = saturation_negative = saturation_check = 0    
           
signal_equiv = np.zeros(len(gains), dtype=np.int64)			# Signal multiplied by gain
signal_equivf = np.zeros(len(gains), dtype=np.float64)			

max_raw = -1e39

# Find out what the values are like, so can scale
for j in range(10):
  random_amplitude = np.random.normal(size=len(gains))		          
  for i in range(BP_START,BP_END):
    val_raw = bandpass[i]*random_amplitude[i]*gains[i]
    if val_raw > max_raw: max_raw = val_raw
scale = 2**35/(max_raw)
print "Scale", scale

sys.stdout.flush()

max_raw = -1e39
min_raw = 1e39  
max_sat = max_raw
min_sat = min_raw        

for j in range(NUM_SAMPLES):
            
  random_amplitude = np.random.normal(size=len(gains))		
           
  for i in range(BP_START,BP_END):
    
    val_raw = bandpass[i]*random_amplitude[i]*scale*float(sys.argv[1])		# Make sure is a high valued integer for resolution
    if val_raw > max_raw: max_raw = val_raw
    if val_raw < min_raw: min_raw = val_raw
       
    gain = gains[i]
            
    # Multiply by an unsigned 32b coefficient ("digital gain") producing a signed 50b product.
    val = np.multiply(np.int64(np.round(val_raw)), np.int64(gain))
            
    #val = np.int64(0b0000000001000000000000000000010101010101011101000000010001110110)
    
    # Select bits 35:32 (of 49:0), rounding symmetrically using the value of bit 31.
    val_round = bit_round(val) 
    val_result = np.right_shift(val_round, 32)	# Select the number. Down shift preserves negative. Leave top bits for saturation check.
    #print "X", val_result
            
    val_sat = val_result         # save it
    if val_sat > max_sat: max_sat = val_sat
    if val_sat < min_sat: min_sat = val_sat
    

    # Saturation: If bits 49:35 are not all the same, force the 4b result to 0b0111 (+7) if it is positive and to 0b1001 (-7) if it is negative.
    # If the result is now 0b1000 (-8), force it to 0b1001 (-7). 
    if "saturation_check:on" in sys.argv:
      saturation_check += 1
      if  val_result < -7 or val_result > 7:
        #print "Saturation",
        if val_round > 7: 
          #print "positive"
          val_result = np.int64(7)
          saturation_positive += 1
        else: 
          #print "negative"
          saturation_negative += 1
          val_result = np.int64(-7)
                
    """
    print "Val/Val Saturated", val_sat, val_result
    print "Val ", binary(val)
    print "    ", " "*32+"*"*32
    print "ValR", binary(val_round)
    shadow = np.zeros(64, dtype=np.int)
    shadow[28:32] = 1
    s = ""
    for si in range(64): 
      if shadow[si] == 1: s += "*"
      else: s += " "            
    print "    ", s
    print "Res ", binary(val_result)
    """
	
    signal_equiv[i] += val_result**2

	      # Float version
    valf = (np.float64(val_raw)*np.float64(float_gains[i]))/2**32
    signal_equivf[i] += valf**2

              
  if j%10000 == 0 and j > 0: 
    print "Wrote", j
    np.savetxt("signal_equiv_"+str(j)+".dat", signal_equiv, fmt="%ld")
    np.savetxt("signal_equivf_"+str(j)+".dat", signal_equivf, fmt="%lf")
    print "Saturation positive", saturation_positive, "Saturation negative", saturation_negative
    print "Data range", min_raw, max_raw
    print "Equalized data range", min_sat, max_sat
    sys.stdout.flush();
            
np.savetxt("signal_equiv.dat", signal_equiv, fmt="%ld")
np.savetxt("signal_equivf.dat", signal_equivf, fmt="%lf")
print "Saturation positive", saturation_positive, "Saturation negative", saturation_negative, "checked", saturation_check
print "Data range", min_raw, max_raw
    
    
