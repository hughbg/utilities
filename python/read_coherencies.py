import scipy.optimize
import random, copy

STANDS = 256

# Class to make matrix ops easier. 2-D  matrix
class Matrix(object): 
  def __init__(self, a, b, c, d):
    self.matrix = [ [ a, b ], [ c, d ]  ]

  def multiply(self, m):
    a = self.matrix[0][0]*m.matrix[0][0]+self.matrix[0][1]*m.matrix[1][0]
    b = self.matrix[0][0]*m.matrix[0][1]+self.matrix[0][1]*m.matrix[1][1]
    c = self.matrix[1][0]*m.matrix[0][0]+self.matrix[1][1]*m.matrix[1][0]
    d = self.matrix[1][0]*m.matrix[0][1]+self.matrix[1][1]*m.matrix[1][1]
    return Matrix(a, b, c, d)

  def transpose(self):	# conj transpose
    a = self.matrix[0][0].conjugate()
    b = self.matrix[0][1].conjugate()
    c = self.matrix[1][0].conjugate()
    d = self.matrix[1][1].conjugate()
    return Matrix(a, c, b, d)

  def subtract(self, m):
    a = self.matrix[0][0]-m.matrix[0][0]
    b = self.matrix[0][1]-m.matrix[0][1]
    c = self.matrix[1][0]-m.matrix[1][0]
    d = self.matrix[1][1]-m.matrix[1][1]
    return Matrix(a, b, c, d)

  def add(self, m):
    a = self.matrix[0][0]+m.matrix[0][0]
    b = self.matrix[0][1]+m.matrix[0][1]
    c = self.matrix[1][0]+m.matrix[1][0]
    d = self.matrix[1][1]+m.matrix[1][1]
    return Matrix(a, b, c, d)

  def add_in(self, m):
    self.matrix[0][0]+=m.matrix[0][0]
    self.matrix[0][1]+=m.matrix[0][1]
    self.matrix[1][0]+=m.matrix[1][0]
    self.matrix[1][1]+=m.matrix[1][1]


  def inverse(self):
    a = self.matrix[0][0]
    b = self.matrix[0][1]
    c = self.matrix[1][0]
    d = self.matrix[1][1]
    det = a*d-b*c
    return Matrix(d/det, -b/det, -c/det, a/det)


  def squared_norm(self):    # Squared frobenius norm
    a = self.matrix[0][0]
    b = self.matrix[0][1]
    c = self.matrix[1][0]
    d = self.matrix[1][1]
    return a*a.conjugate()+b*b.conjugate()+c*c.conjugate()+d*d.conjugate()  

  def multiply_scalar(self, val):
    a = self.matrix[0][0]*val
    b = self.matrix[0][1]*val
    c = self.matrix[1][0]*val
    d = self.matrix[1][1]*val
    return Matrix(a, b, c, d)    
 
  def multiply_by_scalar(self, val):
    self.matrix[0][0]*=val
    self.matrix[0][1]*=val
    self.matrix[1][0]*=val
    self.matrix[1][1]*=val

  def zero(self):
    return abs(self.matrix[0][0]) ==0 and abs(self.matrix[0][1]) == 0 and abs(self.matrix[1][0]) == 0 and abs(self.matrix[1][1]) == 0

  def printm(self, newline=True):
    print "[",
    for i in range(2):
      for j in range(2):
        print self.matrix[i][j],
    print "]",
    if newline: print

def perturb(m):
    m.matrix[0][0] *= 1.0+(random.random()-0.5)/2
    m.matrix[0][1] *= 1.0+(random.random()-0.5)/2
    m.matrix[1][0] *= 1.0+(random.random()-0.5)/2
    m.matrix[1][1] *= 1.0+(random.random()-0.5)/2

def residual(Vin, Jin, Pin):  # Between V and J.P.Jt
  res = 0
  for j in range(STANDS):
    for k in range(STANDS):
      if j in stands_used and k in stands_used:
        print "X",j, k
        Vin[j][k].printm()
        Vresidual = Vin[j][k].subtract(Jin[j].multiply(Pin[j][k]).multiply(Jin[k].transpose()))
        res += Vresidual.squared_norm()

  return res

def normalize(a):
  summ = Matrix(complex(0,0), complex(0,0), complex(0,0), complex(0,0))
  for i in range(STANDS):
    for j in range(STANDS):
      if not a[i][j].zero(): summ.add_in(a[i][j])

  norm = summ.squared_norm()
  
  for i in range(STANDS):
    for j in range(STANDS):
      a[i][j].multiply_by_scalar(1e13/norm)

  
def compare_vis(j_matrices_elements):
  J_in = [ Matrix(complex(0,0), complex(0,0), complex(0,0), complex(0,0)) for j in range(STANDS) ]

  for j in range(len(stands_used)):
    J_in[stands_used[j]] = Matrix(complex(j_matrices_elements[j*8+0], j_matrices_elements[j*8+1]), complex(j_matrices_elements[j*8+2], j_matrices_elements[j*8+3]),
			complex(j_matrices_elements[j*8+4], j_matrices_elements[j*8+5]), complex(j_matrices_elements[j*8+6], j_matrices_elements[j*8+7]))

  res = abs(residual(visibilities, J_in, model))
  #print res
  return res

"""

fprintf(coherencies, "%d %d %f ", st1, st2, weights[k]);
          fprintf(coherencies, "%f %f %f %f %f %f %f %f ",
                crealf(MeasuredVisCoherency[0]), cimagf(MeasuredVisCoherency[0]),
                crealf(MeasuredVisCoherency[1]), cimagf(MeasuredVisCoherency[1]),
                crealf(MeasuredVisCoherency[2]), cimagf(MeasuredVisCoherency[2]),
                crealf(MeasuredVisCoherency[3]), cimagf(MeasuredVisCoherency[3]));

          fprintf(coherencies, "%f %f %f %f %f %f %f %f\n",
"""

stands_used = []


visibilities = [ [ Matrix(complex(0,0), complex(0,0), complex(0,0), complex(0,0)) for i in range(STANDS) ] for j in range(STANDS) ]
model = [ [ Matrix(complex(0,0), complex(0,0), complex(0,0), complex(0,0))  for i in range(STANDS) ] for j in range(STANDS) ]

for line in open("coherencies.dat"):
  l = line.split()
  st1 = int(l[0])
  st2 = int(l[1])

  if st1 not in stands_used: stands_used.append(st1)
  if st2 not in stands_used: stands_used.append(st2)



  visibilities[st1][st2] = Matrix(complex(float(l[3]), float(l[4])), complex(float(l[5]), float(l[6])), 
				complex(float(l[7]), float(l[8])), complex(float(l[9]), float(l[10])))

  model[st1][st2] = Matrix(complex(float(l[11]), float(l[12])), complex(float(l[13]), float(l[14])), 
				complex(float(l[15]), float(l[16])), complex(float(l[17]), float(l[18])))


normalize(visibilities)
normalize(model)

print stands_used
accumulated_J_result = [ None for i in range(STANDS) ]
stands_step = 3
all_stands_used = stands_used
s_index = 0
while s_index < len(all_stands_used):

  stands_used = all_stands_used[s_index:s_index+stands_step]
  jones = [ Matrix(complex(1,0), complex(0,0), complex(0,0), complex(1,0)) for i in range(STANDS) ]

  # Generate dummy data
  dummy_data = False
  if dummy_data:
  
  
    for j in range(STANDS):
      jones = [ Matrix(complex(random.random(),random.random()), complex(random.random(),random.random()), complex(random.random(),random.random()),
  		 complex(random.random(),random.random())) for i in range(STANDS) ]
  
    # Generate visibilities from jones
    for j in range(STANDS):
      for k in range(j+1,STANDS):
        visibilities[j][k] = jones[j].multiply(model[j][k]).multiply(jones[k].transpose())
  
    print "Dummy residual before perturbation", residual(visibilities, jones, model)
    
  
    # Perturb jones
    J_perturb = [ None for i in range(STANDS) ]
    for j in range(STANDS):
      J_perturb[j] = Matrix(jones[j].matrix[0][0], jones[j].matrix[0][1], jones[j].matrix[1][0], jones[j].matrix[1][1])   
      perturb(J_perturb[j])
    
    # Calculate perturbed visibilities
    V_perturb =[ [ Matrix(complex(0,0), complex(0,0), complex(0,0), complex(0,0)) for i in range(STANDS) ] for j in range(STANDS) ]
    for j in range(STANDS):
      for k in range(j+1,STANDS):
        V_perturb[j][k] = J_perturb[j].multiply(model[j][k]).multiply(J_perturb[k].transpose())
  
    print "Dummy residual after perturbation", residual(V_perturb, jones, model)
  
    for i in range(STANDS):
      for j in range(STANDS):
        visibilities[i][j] = V_perturb[i][j]
    
  
  print stands_used, s_index
  print "Initial residual", residual(visibilities, jones, model)
  
  if s_index == 3: exit()
  
      
  flat = [ None for i in range(len(stands_used)*8) ]
  for j in range(len(stands_used)):
    flat[j*8+0] = jones[stands_used[j]].matrix[0][0].real
    flat[j*8+1] = jones[stands_used[j]].matrix[0][0].imag
    flat[j*8+2] = jones[stands_used[j]].matrix[0][1].real
    flat[j*8+3] = jones[stands_used[j]].matrix[0][1].imag
    flat[j*8+4] = jones[stands_used[j]].matrix[1][0].real
    flat[j*8+5] = jones[stands_used[j]].matrix[1][0].imag
    flat[j*8+6] = jones[stands_used[j]].matrix[1][1].real
    flat[j*8+7] = jones[stands_used[j]].matrix[1][1].imag
  
  
  res = scipy.optimize.minimize(compare_vis, flat)
  print "Success?", res.success, res.message  
  
  result_J = res.x
  
  J_result = [ None for j in range(STANDS) ]
  
  
  for j in range(len(stands_used)):
    J_result[stands_used[j]] = Matrix(complex(result_J[j*8+0], result_J[j*8+1]), complex(result_J[j*8+2], result_J[j*8+3]),
  			complex(result_J[j*8+4], result_J[j*8+5]), complex(result_J[j*8+6], result_J[j*8+7]))
  
  
  print "Residual", residual(visibilities, J_result, model)
  jones[stands_used[0]].printm()
  if dummy_data: J_perturb[stands_used[0]].printm()
  J_result[stands_used[0]].printm()
  
  for j in range(STANDS):
    if accumulated_J_result[i] and J_result[i]:
      print "ERROR: Looks like J_result already has a result for stand", i
    else if J_result[i]: accumulated_J_result[i] = copy.copy(J_result[i])

  s_index += stands_step
  

