import sys
import numpy as np
def hms2decimal(hh, mm, ss):
  return hh+mm/60+ss/3600

v0 = 1e6

def json2source(name, h, m, s, d, dm, ds, I0, index):

  log_I_74 = np.log10(I0)
  for i in range(len(index)):
    log_I_74 += index[i]*np.log10(np.power(74e6/v0,i+1))

  log_I_20 = np.log10(I0)
  for i in range(len(index)):
    log_I_20 += index[i]*np.log10(np.power(20e6/v0,i+1))

  print "//", name
  if d < 0: sign = "-"
  else: sign = "+"
  print "COMPONENT", str(h)+str(m)+sign+str(d)+str(dm), hms2decimal(h, m, s), hms2decimal(d, dm, ds)
  print "FREQ 20e+06", np.power(10, log_I_20), "0.0", "0.0", "0.0"
  print "FREQ 74e+06", np.power(10, log_I_74), "0.0", "0.0", "0.0"
  print "ENDCOMPONENT"

json2source("Cyg1", 19, 59, 29.990, +40, 43, 57.53, 43170.55527073293, [0.085, -0.178])
json2source("Cyg2", 19, 59, 24.316, +40, 44, 50.70, 6374.4647292670625, [0.085, -0.178])
json2source("Cas1", 23,23,12.780, +58,50,41.00, 205291.01635813876, [-0.77])
json2source("Cas2", 23,23,28.090 , +58,49,18.10 , 191558.43164385832, [-0.77])
json2source("Cas3", 23,23,20.880 , +58,50,49.92 , 159054.81199800296, [-0.77])
