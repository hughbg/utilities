import numpy as np
import scipy.io as sio
samp = 196e6

leda = np.loadtxt("delays.dat")
caltech = np.loadtxt("../experiment2/delays.dat")

delays = np.add(leda, caltech)
delays = -delays
delays = np.where(delays==0, 0, delays-np.min(delays))
delays *= samp
delays += 1

delays = {"delaysEst2": [ [x] for x in delays]}
sio.savemat("leda_delays.mat", delays)

x = sio.loadmat("leda_delays.mat")["delaysEst2"]
print x[:5]
print np.min(x), np.max(x)

x = sio.loadmat("../lwa_delay_estimates_mar18_full.mat")["delaysEst2"]
print x[:5]
print np.min(x), np.max(x)
