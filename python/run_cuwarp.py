import sys, os, pyfits, numpy, math, scipy.stats
sys.path.append('/home/leda/hgarsden')
import make_header

ROOT = "/nfs/ledastorage"

def is_dada_obs(name):
  return name.find("/201") > -1 and name[-5:] == ".dada"

def write_cuwarp_parameters(number_of_dumps, dump_time, ra, dec, freq_base):
  f = open("cuwarp.in","w\n")

  f.write("// this helps a bit with the stability problem...  A low value keeps the Jones matrix values down.\n")
  f.write("// See http://mysupercomputerexperience.blogspot.com/2015/10/visscale-affect-on-jones-matrix-values.html\n")
  f.write("VisScale=0.00001\n")

  f.write("// ArrayIsLWA sets the max baseline as 350m\n")
  f.write("ArrayIsLWA1=1\n")


  f.write("// a simple mfs gridding is available in cpu-only mode, so average all of the channels together\n")
  f.write("FscrunchChan=109\n")


  f.write("// generate completely new starting solutions\n")
  f.write("generateDIjones=1\n")
  f.write("useStoredCalibrationFiles=0\n")

  f.write("applyDIcalibration=1\n")
  f.write("doRFIflagging=1\n")

  f.write("CorrDumpTime="+str(dump_time)+"\n")

  f.write("// process each time step as a separate snapshot\n")
  f.write("CorrDumpsPerCadence=1\n")
  f.write("NumberOfIntegrationBins=1\n")
  f.write("NumberOfIterations="+str(number_of_dumps)+"\n")


  f.write("ReadAllFromSingleFile=1\n")

  f.write("BaseFilename=WholeSkyL64_47.004_d20150203_utc181702.uvfits   // Not used\n")
  f.write("NumberOfChannels=109\n")

  f.write("ChannelBandwidth=0.024\n")
  f.write("ObservationFrequencyBase="+str(freq_base)+"\n")

  f.write("ObservationImageCentreRA="+str(ra)+"\n")
  f.write("ObservationImageCentreDec="+str(dec)+"\n")


  f.write("DoCalibration=1\n")

  f.write("SourceCatalogueFile=VLSScatalog07Jun26_10JyCutoff.txt\n")

  f.write("NumberOfCalibrators=1\n")


  f.write("MakeImage=1\n")
  f.write("FieldOfViewDegrees=90\n")

  f.write("StorePixelMatrices=1\n")

  f.write("ImageOversampling=5\n")

  f.close()

def analyze_images1():
  best_stat = -1
  where_best_stat = ""

  for filename in os.listdir("."):

    if filename[-5:] == ".fits":
      
      hdulist = pyfits.open(filename)
      im = hdulist[0].data
      #ImagMagik FITS has a third dimension
      if len(im.shape) == 3:
        tmp = im[0,::]
        im = tmp
  
      # Get rms background
      sum = 0.0
      num = 0
      for i in range(80,180):
        for j in range(300, 400):
          if not numpy.isnan(im[i, j]):
            sum += im[i, j]*im[i, j]
	    #im[i, j] = 0
            num += 1
	  else: return 0
      if num == 0: return 0
      else: stat = im.max()/math.sqrt(sum/num)
      
      if stat > best_stat: 
        best_stat = stat
        where_best_stat = filename
   
      hdulist.close()
      #make_ppm(im)

  return where_best_stat, best_stat

def analyze_images():
  best_stat = -1
  where_best_stat = ""

  for filename in os.listdir("."):

    if filename[-5:] == ".fits":

      hdulist = pyfits.open(filename)
      im = hdulist[0].data
      #ImagMagik FITS has a third dimension
      if len(im.shape) == 3:
        tmp = im[0,::]
        im = tmp
      
      if numpy.isnan(numpy.min(im)): stat = -1
      else: 
        clipped = scipy.stats.sigmaclip(im)[0]
        stat = im.max()/numpy.std(clipped)

      if stat > best_stat:
        best_stat = stat
        where_best_stat = filename

      hdulist.close()
      #make_ppm(im)

  return where_best_stat, best_stat
      

def process_file(fname):
  header = make_header.make_header(fname, True)
  if header["N_SCANS"] == "UNKNOWN" or header["DATA_ORDER"] != "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX": 
    print "Skip", fname,
    return
  print "File", fname
  write_cuwarp_parameters(int(header["N_SCANS"]), int(header["INT_TIME"]), header["RA_HRS"], header["DEC_DEGS"], float(header['FREQCENT'])-float(header['BANDWIDTH'])/2)
  os.remove("leda.dada")		# sym link
  os.symlink(fname, "leda.dada")
  os.system("./rts_node_cpu cuwarp.in")
  where, best = analyze_images()
  if best != -1:
    save_file = (fname[5:-5]+".jpg").replace("/","_")      # assume path starts with /nfs
    print "DONE",fname, save_file, best, where, header["LST"], header["FREQCENT"]
    os.system("convert "+where+" -resize 400 "+save_file)
    sys.stdout.flush()

for ovro in range(1,12):
  for sub_dir in [ "data1", "data2" ]:
    for num in [ "one", "two" ]: 
      dir_name = ROOT+"/ledaovro"+str(ovro)+"/"+sub_dir+"/"+num
      print dir_name
      sys.stdout.flush()
      if os.path.isdir(dir_name):
        for filename in os.listdir(dir_name):
	  if is_dada_obs(dir_name+"/"+filename): 
	    os.system("rm *.fits *.log")
	    process_file(dir_name+"/"+filename)

