import ephem, datetime, numpy as np

position = ('-26.70331940', '116.67081524', 0)

# d is a date/time string
def date_to_julian(d):
  return ephem.Date(datetime.datetime.strptime(d, "%Y-%m-%d-%H:%M:%S"))+2415020.0

# jl is Julian date (float)
def julian_to_lst(jl):
  d = ephem.Date(jl-2415020.0)
  obs = ephem.Observer()
  (obs.lat, obs.lon, obs.elev) = position
  obs.date = d
  return obs.sidereal_time()*180/np.pi/15

# jl is Julian date (float)
def julian_to_date(jl):
  d = ephem.Date(jl-2415020.0)
  return d

print julian_to_lst(2456528.22648), julian_to_date(2456528.22648)
print julian_to_date(2451544.869936)
