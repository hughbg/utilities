import ephem, os, subprocess
import math, sys, datetime

lock_file = "/home/leda/.lock_bf_cal.txt"
ovro_location = ('37.2397808', '-118.2816819', 1183.4839)

ovro = ephem.Observer()
(ovro.lat, ovro.lon, ovro.elev) = ovro_location
ovro.date = datetime.datetime.utcnow()


# http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NAME+CYGNUS+A
cyg = ephem.FixedBody()
cyg._ra  =  '19 59 28.357'
cyg._dec = '+40 44 02.10'
cyg.name = "Cyg"


ovro.date -= 1/24.0	# Go back one hour
cyg.compute(ovro)
alt_past = cyg.alt*180/math.pi

ovro.date = datetime.datetime.utcnow()	# Go now
cyg.compute(ovro)
alt_now = cyg.alt*180/math.pi

  # If conditions are right, start the ball rolling
if alt_past < alt_now and alt_now > 30 and not os.path.exists(lock_file):
  print "Start beamformer calibration", alt_past, alt_now, datetime.datetime.now()
  print "Set lock file",lock_file

  # Stop it from running till next time, create a lock file
  f = open(lock_file, "w")
  f.close()
  
  # Comment this line to stop the calibration running
  #subprocess.call(["/home/leda/hgarsden/run_bf_cal.sh"])

  # If Cyg is setting, remove the lock file
elif alt_past > alt_now and alt_now < 30 and os.path.exists(lock_file):
  print "Unlock beam former calibration, remove", lock_file, ",", alt_past, alt_now, "Current OVRO time", datetime.datetime.now()
  os.remove(lock_file)

else: print "No action", "Last Cyg elev.", alt_past, "Cyg elev. now",alt_now, "Current OVRO time", datetime.datetime.now()



 
