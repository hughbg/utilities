import os, sys
sys.path.append('..')
import make_header
from sets import Set
from datetime import datetime
import observation

DADA_HEADER_SIZE = 4096
NUM_FILES_TIME_DIRECTION = 2
NUM_FILES_FREQUENCY_DIRECTION = 2

# Find a window that centres around the index of interest.
# What's in the list is not used, only indexes based on the boundaries.
def window(list_len, window_len, index_of_interest):
  start = index_of_interest-window_len//2
  if start < 0: start = 0
  end = start+window_len
  if end > list_len: 
    end = list_len
    start = end-window_len
    if start < 0: start = 0

  return start, end
        
def triangular(fname):
  f = open(fname, 'rb')
  headerstr = f.read(DADA_HEADER_SIZE)
  f.close()
  if len(headerstr) < DADA_HEADER_SIZE: return False
  for line in headerstr.split('\n'):
    words = line.split()
    if len(words) >= 2:
      if words[0] == "DATA_ORDER" and words[1] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX": return True

  return False

def both_good(a, b):
  return a and b

def format_date(d):
  if d == "UNKNOWN": return d
  else: return d[0:4]+"/"+d[4:6]+"/"+d[6:8]

def format_time(t):
  if t == "UNKNOWN": return t
  else: return t[0:2]+":"+t[2:4]+":"+t[4:6]


def all_times_have_this(matrix, freq_index, start_index, end_index):
  for i in range(start_index, end_index):
    if not matrix[i][freq_index]: return False
  return True


# MAIN

def sequence_all(input_date=None, html=True):
 # input_date is stripped off the file, like 2015-03-27-23_08_41

  if html:
    print "<html><head><title>Time sequences in DADA File Storage</title></head><body>"
    print "<center><h2>Sequences of DADA files</h2>"
    print "Last updated", datetime.now().strftime("%Y-%m-%d %H:%M:%S"),"OVRO time <p>"
    print "</center>Interpreting the table:<p>"
    print "The data is from 2016 only. 2015 can be included but there are zip files that need to be processed. Only /nfs/ledastorage is scanned.<p>"
    print "Each row summarizes a sequence of DADA files. The rows are in date/time order from oldest to newest. Firstly, the files in a sequence must be contiguous in time, i.e the end time of one file lines up exactly with the start time of the next file. Secondly, the sequence must be either usable or not usable. Usable means that DADA files exist for all 22 frequency sub-bands, and that the files all contain a valid number of correlator dumps (i.e. no partial, empty files). This means the sequence can be merged into a single observation over the time of the sequence and over the whole frequency band (otherwise, it's not usable).<p>"
    print "\"Overlap\" means that the end of one sequence overlaps the next sequence in time. This shouldn't happen.<p>The other columns are hopefully easy to understand.<p>"
  
    print "<table border=1 cellspacing=0 cellpadding=10>"
    print "<tr><th>Start date/time (UTC)</th><th>Start LST</th><th>Length of sequence<br>in time</th><th>Usable</th><th>Does <b>not</b> overlap next sequence</th><th>Number of files in<br> the time direction</th><th>First file name</th><th>Report</th></tr>"
  
  # Get dates of all the runs 
  file_list = "/tmp/file_list"+str(os.getpid())+".txt"

  command = "find "
  for d in observation.disk_names: command += "/nfs/"+d+" "
  command += " | egrep '(/2016-.+dada)$' "
  if input_date: command += "| grep "+input_date
  command += " > "+file_list
  os.system(command)    # what about dadazip
  dates = []
  for fl in open(file_list):
    # Ignore  empty files
    if os.path.getsize(fl[:-1]) > DADA_HEADER_SIZE:
      dt = os.path.basename(fl)[:19]
      if dt[4] == "-" and dt[7] == "-" and triangular(fl[:-1]) and dt not in dates: dates.append(dt)
  
  dates = sorted(dates)


  #dates = [ "2016-02-06-02_12_19" ]
  #file_list = "/tmp/files_list.txt"

  # Do the runs in time sequence 
  for dt in dates: 

    # Get all files in the run with this start date/time in their name
    observations = []	   # List of file sets. A set is a set of 22 across frequency.
    basenames = []         # Just to make the list unique,keep track ofteh names used
    for f in open(file_list):
      if f.find(dt) >= 0:
        basename = os.path.basename(f)[:-1]
        if basename not in basenames: 
          basenames.append(basename)

	  # Insert into the observation list in order of start time
          new_observations = []
          obs = observation.Observation(basename)     # This combines and checks all frequencies
          i = 0
          while i < len(observations) and observations[i].start_time < obs.start_time:
            new_observations.append(observations[i])
            i += 1
          new_observations.append(obs)
          while i < len(observations):
            new_observations.append(observations[i])
            i += 1
	  observations = new_observations

    # Now sequence these file basenames and calculated sizes/frequencies. example_file_in_run contains one full filename
    # for every basename that has been found.
  
    sequences = [ ]
  
    for obs in observations: 
	# How to decide if to start a new sequence
      if len(sequences) > 0 and obs.start_time == sequences[-1].end_time() and ( ( obs.ok and sequences[-1].last_ok() ) or
			( not obs.ok and not sequences[-1].last_ok() ) ):
        sequences[-1].add(obs)
      else: sequences.append(observation.Sequence(obs))	# Start new one
  

    # Print sequences
  
    for i, seq in enumerate(sequences):
      # Consistency check: should all be good or all be bad, relating to frequencies and size
      
      consistent = seq.check_consistent()
      if not consistent:
        print "CONSISTENCY ERROR: Sequence starting with", seq.start_time(), "is inconsistent"
  
      if html:
        # Summarize the sequence and print
        start_date_str = format_date(seq.start_utc_date())
        start_time_str = format_time(seq.start_utc_time())
        time_length = seq.time_length()
        m, s = divmod(time_length, 60)
        h, m = divmod(m, 60)
        time_length_str = ( "%dh%02dm%02ds" % (h, m, s) )
        m, s = divmod(seq.start_lst()*3600, 60)
        h, m = divmod(m, 60)
        lst_str = ( "%dh%02dm%02ds" % (h, m, s) )
        usable = seq.ok() and consistent    
        basename = seq.start_basename()
        overlap = False
        if i < len(sequences)-1:
          if sequences[i+1].end_time() < sequences[i].start_time(): overlap = True
        print "<tr align=center><td>",start_date_str,start_time_str,"</td><td>"+lst_str+"</td><td>",time_length_str,\
		"</td><td>",usable,"</td><td>",not overlap,"<td>",seq.length(),"</td><td>"+basename+"</td><td>"+seq.report()+"</td></tr>"
  
        #for seq in sequences:
        #  print "-----"
        #  for s in seq: print s

     
  
  if html: print "</table></body></html>"
  
  # Overlaps
  #('/nfs/ledastorage/ledaovro7/data1/two/2016-02-06-02_12_19_0000409250255936.dada', 23823, 23913, 10.0, '20160206', '084922', True, True)
  #-----
  #('/nfs/ledastorage/ledaovro7/data1/two/2016-02-06-02_12_19_0000410641737728.dada', 23904, 23994, 10.0, '20160206', '085043', True, True)
  
  
  
  os.remove(file_list)

  

def sequence_includes(fname):

  if not os.access(fname,os.R_OK):
    print fname,"is unreadable"
    sys.exit(1)
  
  dir_name = os.path.dirname(fname)
  file_name = os.path.basename(fname)
  run_time = file_name[:19]   # start of the run we are interested in
  
  # Get dada files only, in order, only for the run
  # This gets files from the same directory, which may (unlikely) be missing some
  observations = [] 
  for f in sorted(os.listdir(dir_name)):
    if len(f) >= 19 and f[-5:] == ".dada" and run_time == f[:19]: observation.append(observation.Observation(f))  

  
  # Create list
  for obs in observations: 
        # How to decide if to start a new sequence
    if len(sequences) > 0 and obs.start_time == sequences[-1].end_time() and ( ( obs.ok and sequences[-1].last_ok() ) or
			( not obs.ok and not sequences[-1].last_ok() ) ):
      sequences[-1].add(obs)
    else: sequences.append(observation.Sequence(obs))	# Start new one
 
       
  # find the sequence the file of interest is in
  where_i = where_j = -1
  for i, seq in enumerate(sequences):
    index = seq.contains_file(fname)
    if index >= 0:
      where_i = i
      where_j = index
      break
  
  # find what times we can get, around the time of the file of interest
  start_time_index, end_time_index = window(sequences[where_i].length(), NUM_FILES_TIME_DIRECTION, where_j)
 
  # Find what frequencies we want, around the frequency of the file of interest
  freq = get_frequency(fname)
  where_i_f = -1		# interesting frequency
  for i in range(len(frequency_vals)):
    if frequency_vals[i] == freq: where_i_f = i
  start_freq_index, end_freq_index = window(len(frequency_vals), NUM_FILES_FREQUENCY_DIRECTION, where_i_f)  
  
  # Now check if we have those frequencies and consistent sizes for the files selected in the time direction.
  # Restrict the freq range further.
  scans = sequences[where_i].get_obs(where_j).get_file(fname).scans
  time_step_good = [ [ True for j in range(len(frequency_vals)) ] for i in range(len(sequences[where_i])) ]  # matrix of those present
  for i in range(start_time_index, end_time_index):
    obs = sequences[where_i].get_obs(j)
    has = obs.has_freq(frequency_vals[start_freq:end_freq])

    # Record what's good
    for j in range(start_freq, end_freq):
      if frequency_vals[j] not in has: 
        time_step_good[i][j] = False

  if not time_step_good[where_j][where_i_f]:
    print "Failed. The file is not in a valid time step"
    sys.exit(1)


  # Now reduce the array
  # Find the set of frequencies that occur in all times
  # start with the freq of file of interest and go out in the matrix while frequencies exist

  restricted_start_freq = restricted_end_freq = where_i_f
  while restricted_start_freq >= start_freq and all_times_have_this(time_step_good, restricted_start_freq, start_time, end_time): restricted_start_freq -= 1
  while restricted_end_freq < end_freq and all_times_have_this(time_step_good, restricted_end_freq, start_time, end_time): restricted_end_freq += 1
  start_freq = restricted_start_freq+1  # Because starts are on the start, but end times are 1 beyond the actual end
  end_freq = restricted_end_freq

  # Check size and number of scans

  #for i  in range(start_time,end_time):
  #  for ff in find_frequencies(sequences[where_i][i][0], frequency_vals[start_freq:end_freq])
  #  print files
 
# MAIN
  
sequence_all(None, True)
#sequence_includes("/nfs/ledastorage/ledaovro5/data1/two/2016-02-20-01_36_24_0001902310218752.dada")

