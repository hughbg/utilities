"""
About the baselines in RTS/cuwarp and UVFits:

In Align Visibilities in the RTS code, stands 90,180 have a baseline index of 19034.
When the uvfits file is being dumped in writeUVinstant(), you can run DecodeBaseline() on
the MIRIAD baseline index 19034 and get 91,181, subtract 1 (because MIRIAD indexes from 1)
and get the correct stand indexes.

However 19034 does not correspond to the indexing scheme used in DuCT/corr2uvfits, which does include
autocorrelations. The scheme in DuCT is illustrated by the code:

    for st1 in range(256):
      for st2 in range(st1, 256):
        indexes[st1, st2] = index
        index += 1

The scheme in cuwarp is illustrated by the code:

   for st1 in range(256):
      for st2 in range(st1+1,256):
        indexes[st1, st2] = index
        index += 1

This indexing scheme is used in the code in this script.

The visibilities that are dumped to UV Fits have two operations performed on them:
- AlignVisibilities as above
- a phase rotation. I'm not sure why this is done. The code says (in writeUVinstant)
	    // apply phase centre. Not sure what cal should be here, it is the index of the source used
            // for the cal solution, so probably zero in this context.
  It looks lik it phases to the image center specified in the parameters, but normally this
  is zenith, and it already should be at zenith.  I see a shift of about 6 degrees in a test case.
- conjugation


"""


import sys, pyfits, numpy as np

class UVFits(object):
  def __init__(self, fnami, number_stands):
    self.corr = pyfits.open(sys.argv[1])
   
    # Make indexes
    self.indexes = np.zeros((number_stands, number_stands), dtype=np.int)

    index = 0
    for st1 in range(number_stands):
      for st2 in range(st1+1, number_stands):
        self.indexes[st1, st2] = index
        index += 1

  def get_by_stand(self, st1, st2, chan, pol):
    bl_index = self.indexes[st1][st2]
  
    # Real/imag, weight
    return self.corr[0].data.data[bl_index][0][0][chan][pol][0], self.corr[0].data.data[bl_index][0][0][chan][pol][1], self.corr[0].data.data[bl_index][0][0][chan][pol][2]

  def get_by_bl(self, bl_index, chan, pol):
  
    # Real/imag, weight
    return self.corr[0].data.data[bl_index][0][0][chan][pol][0], self.corr[0].data.data[bl_index][0][0][chan][pol][1], self.corr[0].data.data[bl_index][0][0][chan][pol][2]

uvfits = UVFits(sys.argv[1], 64)

print uvfits.get_by_stand(1, 62, 16, 0)



