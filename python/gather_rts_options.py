import sys

# Run this in the RTS/src directory. Run as python ~/tmp/gather_rts_options.py |sort |sed s/++++/"\\n\/\/"/ | grep -v '^//$'

set_option_lines = []		#  opts->n_iter=1;
got_them = True
for line in open("rts_options.c","r"):
  if line.find("SetDefaultRTSOptions") > -1:
    got_them = True
  if line.find("}") == 0: got_them = False
    
  if got_them and line.strip().find("opts->") == 0: set_option_lines.append(line.strip())


define_options = []	# ( OPT_N_ITER,  NumberOfIterations )
for line in open("rts_options_def.c","r"):
  if line.find("{OPT_") > -1: 
    opt = line.split()[0][1:-1]
    param_name = line.split()[2][1:-1]
    define_options.append((opt, param_name))

usage_lines = []
got_them = True
for line in open("rts_options.c","r"):
  if line.find("void usage(void)") > -1:
    got_them = True
  if line.find("}") == 0: got_them = False
    
  if got_them and line.find("printf") > -1: usage_lines.append(line[:-1])

option_mappings = []	# ('OPT_N_ITER', 'opts->n_iter')
f = open("rts_options_def.c","r")
lines = f.readlines()
f.close()
for index, line in enumerate(lines):
  if line.find("case OPT") > -1: 
    i = index
    while lines[i].strip().find("opts->") != 0: i += 1
    opt = line.split()[1][:-1]
    option_mappings.append((opt, lines[i].split()[0]))
f.close()


for defined in define_options:
  print defined[1], defined[0],

  found = 0
  for option in option_mappings:
    if option[0] == defined[0]:
      for s in set_option_lines:
        if s.find(option[1]) > -1: 
          print s,
	  found += 1
  if found == 0: print "[no default]",
  if found > 1: 
    print "error: more than one parameter namefor an option"
    sys.exit(1) 

  found = 0
  for u in usage_lines:
    where = u.find(defined[1]+"=")
    if where > -1: 
      found += 1
      print "++++",u[where:-5]
  if found > 1: 
    print "error: more than one usage for an option"
    sys.exit(1) 
  if found == 0: print "++++"      # Separator



