import sys, os
import numpy as np

class DadaFile(object):
  def __init__(self, fname, tm, fr):
    print "Loading", fname

    DADA_HEADER_SIZE = 4096

    self.fname = fname
    self.dada_name = str(tm)+"_"+str(fr)+"_"+os.path.basename(fname)[:-2]

    if os.path.exists(self.dada_name): os.remove(self.dada_name)
    os.system("./hdf52dada "+fname+" "+self.dada_name)
    if not os.path.exists(self.dada_name):
      print self.fname, "failed to decompress"
      exit(1)

    # Ben's loading code
    outrig_ids = [252, 253, 254, 255, 256]
    outrig_nbaseline = sum(outrig_ids) # TODO: This only works because the outriggers are the last antennas in the array
    filesize = os.path.getsize(self.dada_name) - DADA_HEADER_SIZE
    #print "filesize:     ", filesize
 
  
    self.file_handle = open(self.dada_name,"rb")
    header = self.parse_dada_header(self.file_handle.read(DADA_HEADER_SIZE))
    nchan  = header['NCHAN']
    nant   = header['NSTATION']
    npol   = header['NPOL']
    navg   = header['NAVG']
    bps    = header['BYTES_PER_SECOND']
    df     = header['BW']*1e6 / float(nchan)
    cfreq  = header['CFREQ']*1e6
    utc_start = header['UTC_START']
    assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
    freq0 = cfreq-nchan/2*df
    freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
    #print "bytes per sec:", bps
    #print "navg:     ", navg
    #print "nchan:    ", nchan
    #print "npol:     ", npol
    #print "nstation: ", nant
    nbaseline = nant*(nant+1)//2
    #print "nbaseline:", nbaseline
    noutrig_per_full = int(navg / df + 0.5)
    #print "noutrig_per_full:", noutrig_per_full
    full_framesize   = nchan*nbaseline*npol*npol
    #print "full_framesize:  ", full_framesize*8
    #print nbaseline, noutrig_per_full, outrig_nbaseline
    outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
    #print "outrig_framesize:", outrig_framesize*8
    tot_framesize    = (full_framesize + outrig_framesize)
    #print " tot_framesize:  ",  tot_framesize*8
    ntime = float(filesize) / (tot_framesize*8)
    #print "ntime:", ntime
    frame_secs = int(navg / df + 0.5)
    #print "Frame secs:", frame_secs
    time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
    #time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
    #print "Time offset:", time_offset

    self.header = header
    self.freq0 = freq0
    self.cfreq = cfreq
    self.full_framesize = full_framesize
    self.outrig_framesize = outrig_framesize
    self.nchan = nchan
    self.nbaseline = nbaseline
    self.npol = npol
    self.noutrig_per_full = noutrig_per_full
    self.outrig_nbaseline = outrig_nbaseline
    self.full_framesize = full_framesize
    self.outrig_framesize = outrig_framesize
    self.nscans = int(ntime)

    print self.nscans, "scans"
    

  def _cast_to_type(self, string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

  def parse_dada_header(self, headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = self._cast_to_type(value)
                header[key] = value
        return header

  def read_data(self):
    full_data   = np.fromfile(self.file_handle, dtype=np.complex64, count=self.full_framesize)
    outrig_data = np.fromfile(self.file_handle, dtype=np.complex64, count=self.outrig_framesize)
    self.full_data   =   full_data.reshape((self.nchan,self.nbaseline,self.npol,self.npol))
    self.outrig_data = outrig_data.reshape((self.noutrig_per_full,self.nchan,self.outrig_nbaseline,self.npol,self.npol))


# ==================================

class StitchedDada(object):		# Only 1 correlator dump cached, i.e. 1 scan

  def __init__(self, fname, freq0, full_dims, outrig_dims, header_template):
    if full_dims[0]%2 == 0:		# Even number of channels, need to make it odd
      middle_channel_index = full_dims[0]//2-1
      CFREQ = (freq0+middle_channel_index*24e3)/1e6
      BW = (full_dims[0]-1)*0.024
      NCHAN = full_dims[0]-1
    else: 
      middle_channel_index = full_dims[0]//2
      CFREQ = (freq0+middle_channel_index*24e3)/1e6
      BW = full_dims[0]*0.024
      NCHAN = full_dims[0]

    header = ""
    for key in header_template:
      if key == "CFREQ": 
        header += key + " " + str(CFREQ)+"\n"
      elif key == "BW": 
        header += key + " " + str(BW)+"\n"
      elif key == "NCHAN": 
        header += key + " " + str(NCHAN)+"\n"
      else: header += key + " " + str(header_template[key])+"\n"
    print "New center frequency", CFREQ*1e6

    header = (header+"\0"*4096)[:4096]

    self.file_handle = open(fname, "wb")
    self.file_handle.write(header)

    self.full_cache = np.zeros(full_dims, dtype=np.complex64)
    self.outrig_cache = np.zeros(outrig_dims, dtype=np.complex64)   

    self.num_accum = 0


  def accumulate(self, which_subband, data, outrig):
    self.num_accum += 1
    self.full_cache[which_subband*109:which_subband*109+109] += data

  def insert(self, which_subband, data, outrig): 
    self.full_cache[which_subband*109:which_subband*109+109] = data

  def scale(self):
    self.full_cache /= self.num_accum

  def write_cache(self):	# When a full dump is present
    if self.full_cache.shape[0]%2 == 0:		# Even number, make it odd
      self.full_cache[:-1, ...] .tofile(self.file_handle)
      self.outrig_cache[:, :-1, ...].tofile(self.file_handle)
    else:
      self.full_cache.tofile(self.file_handle)
      self.outrig_cache.tofile(self.file_handle)

num_files_in_time = 0
num_files_in_frequency = 0

# Initialize input caches
with open("file_list.txt") as fl:
  line = fl.readline()
  l = line.split()
  num_files_in_time = int(l[0])
  num_files_in_frequency = int(l[1])

  file_cache = [ [ None for j in range(num_files_in_frequency) ] for i in range(num_files_in_time) ]
  for nt in range(num_files_in_time):
    for nf in range(num_files_in_frequency):
       line = fl.readline()
       file_cache[nt][nf] = DadaFile(line[:-1], nt, nf)

# Here need to do consistency check
for nt in range(num_files_in_time):
  for nf in range(num_files_in_frequency): 
    if file_cache[nt][nf].nscans != file_cache[nt][0].nscans:
      print "Number of scans not the same in time", nt, file_cache[nt][nf].nscans, file_cache[nt][0].nscans

# Initialize output cache
num_channels = num_files_in_frequency*109
full_shape = ( num_channels, file_cache[0][0].nbaseline, file_cache[0][0].npol, file_cache[0][0].npol)
outrig_shape = ( file_cache[0][0].noutrig_per_full, num_channels, file_cache[0][0].outrig_nbaseline, file_cache[0][0].npol, file_cache[0][0].npol)
stitch_out = StitchedDada("stitched_11_subbands.dada", file_cache[0][0].freq0, full_shape, outrig_shape, file_cache[0][0].header)

do_summing = True		# over time

# Accumulate data into the output
for nt in range(num_files_in_time):
  for nscan in range(file_cache[nt][0].nscans):
    for nf in range(num_files_in_frequency):    # Need to reinitialize caches for new nt files
       print file_cache[nt][nf].fname, "scan", nscan
       file_cache[nt][nf].read_data()
       if do_summing: stitch_out.accumulate(nf, file_cache[nt][nf].full_data, file_cache[nt][nf].outrig_data)
       else: stitch_out.insert(nf, file_cache[nt][nf].full_data, file_cache[nt][nf].outrig_data)

    if not do_summing: stitch_out.write_cache()


if do_summing: 
  stitch_out.scale()
  stitch_out.write_cache()
