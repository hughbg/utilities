import ephem
from datetime import datetime
from dateutil import tz

time_to_go_20_degrees = 20/360.0*(23*60*60+56*60)/(24*60*60)      #Days


def day_string(d):
  st = ( "%s" % d )
  day = st.split()[0]
  return day

def pad(s):
  if len(s) == 1: return "0"+s
  else: return s

def dh_str(t):
  t *= 24
  return pad(str(int(t)))+":"+pad(str(int((t-int(t))*60)))

def time_at_timezone(dt, zone):
  # Incoming format must be 2016/1/31 22:56:01.
  # ephem.Date() does that
  from_zone = tz.gettz('UTC')
  to_zone = tz.gettz(zone)

  dt = datetime.strptime(("%s" % dt), "%Y/%m/%d %H:%M:%S")

  # Tell the datetime object that it's in UTC time zone since 
  # datetime objects are 'naive' by default
  dt = dt.replace(tzinfo=from_zone)

  # Convert time zone
  return ( "%s" % ephem.Date(dt.astimezone(to_zone)) )

def time_at_ovro(dt): return time_at_timezone(dt,"America/Los_Angeles")
def time_at_boston(dt): return time_at_timezone(dt,"America/New_York")

gal_center = ephem.FixedBody()	
gal_center._ra  =  '17 45 40.04'
gal_center._dec = '-29 00 28.1'
gal_center.name = "Galactic Center"



ovro_location = ('37.2397808', '-118.2816819', 1183.4839)

ovro = ephem.Observer(); (ovro.lat, ovro.lon, ovro.elev) = ovro_location
ovro.date = datetime.now().replace(hour=10, minute=0, second=0, microsecond=0)
ovro.date +=1 	# Tomorrow, 2am at OVRO

sun = ephem.Sun()


print "<table cellpadding=10 cellspacing=0 border=1>"
print "<tr><th>Date</th><th>Observing Window Length hours:mins<br>(Accumulated in brackets)</th><th>What's happening</th><th align=center colspan=4>Sequence of Events</td></tr>"
print "<tr><th><p></th><th><p> </th> <th><p> </th><br><th>Event 1</th><th>Event 2</th><th>Event 3</th><th>Event 4</th></tr>"
time_sum = 0.0


csv = open("/tmp/windows.csv", "w")
csv.write("Subject, Start Date, Start Time, End Date, End Time, Description\n")
for i in range(365):
  sun.compute(ovro)
  gal_center.compute(ovro)

  previous_sunset = ephem.Date(ovro.previous_setting(sun)+ephem.hour) # Add 1 hour
  next_sunrise = ovro.next_rising(sun)	

  previous_galaxyset = ephem.Date(ovro.previous_setting(gal_center)+time_to_go_20_degrees)
  previous_galaxyrise = ovro.previous_rising(gal_center)
  next_galaxyset = ephem.Date(ovro.next_setting(gal_center)+time_to_go_20_degrees)
  next_galaxyrise = ovro.next_rising(gal_center)	

  times = []
  times.append(("Sunset+1h  ", previous_sunset))
  times.append(("Sunrise    ", next_sunrise))
  times.append(("Galaxy Set-20&deg;", previous_galaxyset))
  times.append(("Galaxy Rise", previous_galaxyrise))
  times.append(("Galaxy Set-20&deg;", next_galaxyset))
  times.append(("Galaxy Rise", next_galaxyrise))


  times = sorted(times, key=lambda tup: tup[1])
  
  where_sunset = -1
  for i in range(len(times)):
    if times[i][0][:6] == "Sunset": where_sunset = i
    i += 1
  if where_sunset > 0: start_sequence = where_sunset-1
  else: start_sequence = where_sunset

  start_time = end_time = None

  print "<tr><td>",day_string(time_at_ovro(( "%s" % ovro.date))),"</td>"
  if times[start_sequence][0][:11] == "Galaxy Rise" and times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:7] == "Sunrise" and times[start_sequence+3][0][:10] == "Galaxy Set":
    time_length = 0
    print "<td>0</td><td>Galaxy is up all night</td>"
    subject = "Galaxy is up all night"
  elif times[start_sequence][0][:10] == "Galaxy Set" and times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:7] == "Sunrise" and times[start_sequence+3][0][:11] == "Galaxy Rise":
    time_length = float(next_sunrise)-float(previous_sunset); time_sum += time_length
    print "<td>",dh_str(float(next_sunrise)-float(previous_sunset)),"("+dh_str(time_sum)+")","</td><td>Galaxy is down all night</td>"
    end_time = next_sunrise; start_time = previous_sunset
    desc = "Galaxy is down all night"
  elif times[start_sequence][0][:11] == "Galaxy Rise" and times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:10] == "Galaxy Set" and times[start_sequence+3][0][:7] == "Sunrise":
    time_length = float(next_sunrise)-float(times[start_sequence+2][1]); time_sum += time_length
    print "<td>",dh_str(float(next_sunrise)-float(times[start_sequence+2][1])),"("+dh_str(time_sum)+")","</td><td>Galaxy sets during the night</td>"
    end_time = next_sunrise; start_time = times[start_sequence+2][1]
    desc = "Galaxy sets during the night"
  elif times[start_sequence][0][:10] == "Galaxy Set" and times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:11] == "Galaxy Rise" and times[start_sequence+3][0][:7] == "Sunrise" :
    time_length = float(times[start_sequence+2][1])-float(previous_sunset); time_sum += time_length
    print "<td>",dh_str(float(times[start_sequence+2][1])-float(previous_sunset)),"("+dh_str(time_sum)+")","</td><td>Galaxy rises during the night</td>"
    end_time = times[start_sequence+2][1]; start_time = previous_sunset
    desc = "Galaxy rises during the night"
  elif times[start_sequence][0][:6] == "Sunset" and times[start_sequence+1][0][:10] == "Galaxy Set" and times[start_sequence+2][0][:11] == "Galaxy Rise" and times[start_sequence+3][0][:7] == "Sunrise" :
    time_length = float(times[start_sequence+2][1])-float(times[start_sequence+1][1]); time_sum += time_length
    print "<td>",dh_str(float(times[start_sequence+2][1])-float(times[start_sequence+1][1])),"("+dh_str(time_sum)+")","</td><td>Galaxy sets and rises during the night</td>"  
    end_time = times[start_sequence+2][1]; start_time = times[start_sequence+1][1]
    desc = "Galaxy sets and rises during the night"
  elif times[start_sequence+1][0][:6] == "Sunset" and times[start_sequence+2][0][:10] == "Galaxy Set" and times[start_sequence+3][0][:11] == "Galaxy Rise" and times[start_sequence+4][0][:7] == "Sunrise" :
    time_length = float(times[start_sequence+3][1])-float(times[start_sequence+2][1]); time_sum += time_length
    print "<td>",dh_str(float(times[start_sequence+3][1])-float(times[start_sequence+2][1])),"("+dh_str(time_sum)+")","</td><td>Galaxy sets and rises during the night</td>"
    end_time = times[start_sequence+3][1]; start_time = times[start_sequence+2][1]
    desc = "Galaxy sets and rises during the night"
    start_sequence += 1
  else:
    print "<td colspan=2>Error, unknown window pattern</td>"

  for i in range(start_sequence,start_sequence+4):
    print "<td>",times[i][0],"<br>",time_at_ovro(times[i][1]),"</td>"
  print "</tr>"

  if start_time and end_time:
    start_time = time_at_boston(start_time)
    end_time = time_at_boston(end_time)
    csv.write("LEDA observing, "+str(start_time).split()[0]+", "+str(start_time).split()[1]+", "+str(end_time).split()[0]+", "+str(end_time).split()[1]+", "+desc+". Gives an observing window of length "+dh_str(time_length)+" (h:m)\n")

  ovro.date += 1

print "</table>"

csv.close()

