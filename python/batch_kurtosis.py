import sys, os, numpy as np
import random

DADA_HEADER_SIZE = 4096

class Buffer(object):
    """ Allows the caller to add values to buffer in blocks of indeterminate size compared to the buffer size.
        If the buffer overflows the remainder is returned to the caller, who can add it later. 
        In this way the caller can eat up data.  """

    def __init__(self, size, typ):
       self.dtype = typ
       self.buffer = np.zeros(size, dtype=self.dtype)
       self.top = 0

    def full(self):
        return self.top == len(self.buffer)

    def clear(self):
        self.top = 0
        
    def append(self, values):
        # Cater for just one value
        if np.isscalar(values):
            if self.top == len(self.buffer): return values      # Can't do it
            else:
                self.buffer[self.top] = values
                self.top += 1
                return None
        else:
          
            # Find out how much we can add in
            available_slots = len(self.buffer)-self.top
            if available_slots > len(values):
                available_slots = len(values)
            self.buffer[self.top:self.top+available_slots] = values[:available_slots]
            self.top += available_slots
            return values[available_slots:]             # remainder


def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string
  
def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header

stand = 10
bl_index = stand*(stand+1)//2+stand
  
FFT_SIZE = 8192
N = 216000
M = 1000
  
outrig_ids = [252, 253, 254, 255, 256]
outrig_nbaseline = sum(outrig_ids) # TODO: This only works because the outriggers are the last antennas in the array

def get_data(file_name):

  filesize = os.path.getsize(file_name) - DADA_HEADER_SIZE
  print "filesize:     ", filesize
   
  
  f = open(file_name,"rb")
  header = parse_dada_header(f.read(DADA_HEADER_SIZE))
  nchan  = header['NCHAN']
  nant   = header['NSTATION']
  npol   = header['NPOL']
  navg   = header['NAVG']
  bps    = header['BYTES_PER_SECOND']
  df     = header['BW']*1e6 / float(nchan)
  cfreq  = header['CFREQ']*1e6
  utc_start = header['UTC_START']
  assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
  freq0 = cfreq-nchan/2*df
  freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
  print "bytes per sec:", bps
  print "navg:     ", navg
  print "nchan:    ", nchan
  print "npol:     ", npol
  print "nstation: ", nant
  nbaseline = nant*(nant+1)//2
  print "nbaseline:", nbaseline
  noutrig_per_full = int(navg / df + 0.5)
  print "noutrig_per_full:", noutrig_per_full
  full_framesize   = nchan*nbaseline*npol*npol
  print "full_framesize:  ", full_framesize*8
  #print nbaseline, noutrig_per_full, outrig_nbaseline
  outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
  print "outrig_framesize:", outrig_framesize*8
  tot_framesize    = (full_framesize + outrig_framesize)
  print " tot_framesize:  ",  tot_framesize*8
  ntime = float(filesize) / (tot_framesize*8)
  print "ntime:", ntime
  frame_secs = int(navg / df + 0.5)
  print "Frame secs:", frame_secs
  time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
  #time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
  print "Time offset:", time_offset
  
  data_stream = [ 0j for i in range(int(ntime)) ]
  for t in xrange(int(ntime)):
    print "Time ",t
    full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
    outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
    full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
  
    data_stream[t] = full_data[50, bl_index, 0, 0]
  f.close()
  
  return data_stream

def get_data_pretend(junk):
  global N

  ndump = 10
  data_stream = np.zeros(ndump, dtype=np.complex64)
  for i in range(ndump): 
    fft = np.fft.fft(np.random.normal(0, 1, FFT_SIZE))
    data_stream[i] = fft[1000]*np.conj(fft[1000])/FFT_SIZE
 
  N = 1		# force it

  return data_stream

def kurtosis(data):
    #print "Doing kurtosis on", len(data), "values"
    M = len(data)
    data = 2.0*abs(data)/FFT_SIZE         # Data has been correlated, but these factors need to be added in
    S1 = np.sum(data)
    S2 = np.sum(data**2.0)      
    #Compute spectral kurtosis estimator
    Vk_square = (N*M+1)/(M-1)*(M*(S2/(S1**2.0))-1)      # eqn 8 http://mnrasl.oxfordjournals.org/content/406/1/L60.full.pdf
                                                        # and eqn 50 https://web.njit.edu/~gary/assets/PASP_Published_652409.pdf
    print Vk_square
    return Vk_square



M_buffer = Buffer(M, np.complex64)

kurtosis_values = []

for line in sys.stdin:
  #os.system("./hdf52dada "+line[:-1]+" x.dada")
  kd = get_data_pretend("x.dada")
  remainder = M_buffer.append(kd)	# remainder will be empty if buffer is not full
  while M_buffer.full():
    kurtosis_values.append(kurtosis(M_buffer.buffer))
    M_buffer.clear()
    remainder = M_buffer.append(remainder)   # At the end of the loop, remainder will be empty i.e. eaten up. The buffer may have something in it.
  

np.savetxt("kurtosis.dat", kurtosis_values)
  


