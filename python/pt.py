import shutil

importuvfits(fitsfile="new_dec_no_precession.uvfits",vis="new_dec_no_precession.MS")
print "Show no precession from corr2uvfits"
ms.open("new_dec_no_precession.MS")
for i in range(10):
  print str(ms.getdata(['uvw'])['uvw'][0][i])+" "+str(ms.getdata(['uvw'])['uvw'][1][i])+" "+str(ms.getdata(['uvw'])['uvw'][2][i])
ms.close()

print "Show no precession from dada2ms"
ms.open("dada2ms.ms")
for i in range(10):
  print str(ms.getdata(['uvw'])['uvw'][0][i])+" "+str(ms.getdata(['uvw'])['uvw'][1][i])+" "+str(ms.getdata(['uvw'])['uvw'][2][i])
ms.close()

importuvfits(fitsfile="new_dec_with_precession.uvfits",vis="new_dec_with_precession.MS")
print "Show with precession from corr2uvfits"
ms.open("new_dec_with_precession.MS")
for i in range(10):
  print str(ms.getdata(['uvw'])['uvw'][0][i])+" "+str(ms.getdata(['uvw'])['uvw'][1][i])+" "+str(ms.getdata(['uvw'])['uvw'][2][i])
ms.close()

importuvfits(fitsfile="old_dec_with_precession.uvfits",vis="old_dec_with_precession.MS")
print "Show with precession from corr2uvfits using old dec"
ms.open("old_dec_with_precession.MS")
for i in range(10):
  print str(ms.getdata(['uvw'])['uvw'][0][i])+" "+str(ms.getdata(['uvw'])['uvw'][1][i])+" "+str(ms.getdata(['uvw'])['uvw'][2][i])
ms.close()


ms.open("new_dec_no_precession.MS")
f = open("new_dec_no_precession.txt","w")
u = ms.getdata(['uvw'])['uvw'][0]
v = ms.getdata(['uvw'])['uvw'][1]
w = ms.getdata(['uvw'])['uvw'][2]
for i in range(32896):
  f.write(str(u[i])+" "+str(v[i])+" "+str(w[i])+"\n")
f.close()
ms.close()

ms.open("new_dec_with_precession.MS")
f = open("new_dec_with_precession.txt","w")
u = ms.getdata(['uvw'])['uvw'][0]
v = ms.getdata(['uvw'])['uvw'][1]
w = ms.getdata(['uvw'])['uvw'][2]
for i in range(32896):
  f.write(str(u[i])+" "+str(v[i])+" "+str(w[i])+"\n")
f.close()
ms.close()


print "Doing fixvis on corr2uvfits (reuse fails so no difference)"
fixvis(vis="new_dec_no_precession.MS",outputvis="Y.MS",refcode="J2000")
#shutil.copytree("new_dec_no_precession.MS","Y.MS")
ms.open("Y.MS")
f = open("new_dec_added_precession.txt","w")
u = ms.getdata(['uvw'])['uvw'][0]
v = ms.getdata(['uvw'])['uvw'][1]
w = ms.getdata(['uvw'])['uvw'][2]
for i in range(32896):
  f.write(str(u[i])+" "+str(v[i])+" "+str(w[i])+"\n")
f.close()
ms.close()

print "Show fixvis added precession from corr2uvfits"
ms.open("Y.MS")
for i in range(10):
  print str(ms.getdata(['uvw'])['uvw'][0][i])+" "+str(ms.getdata(['uvw'])['uvw'][1][i])+" "+str(ms.getdata(['uvw'])['uvw'][2][i])
ms.close()

ms.open("dada2ms.ms")
f = open("dada_no_precession.txt","w")
u = ms.getdata(['uvw'])['uvw'][0]
v = ms.getdata(['uvw'])['uvw'][1]
w = ms.getdata(['uvw'])['uvw'][2]
for i in range(32896):
  f.write(str(u[i])+" "+str(v[i])+" "+str(w[i])+"\n")
f.close()
ms.close()

print "Doing fixvis on dada2ms"
fixvis(vis="dada2ms.ms",outputvis="D.MS",refcode="J2000",reuse=False)
#shutil.copytree("dada2ms.ms","D.MS")
print "Show fixvis added precession from dada2ms"

ms.open("D.MS")
for i in range(10):
  print str(ms.getdata(['uvw'])['uvw'][0][i])+" "+str(ms.getdata(['uvw'])['uvw'][1][i])+" "+str(ms.getdata(['uvw'])['uvw'][2][i])
ms.close()

ms.open("D.MS")
f = open("dada_added_precession.txt","w")
u = ms.getdata(['uvw'])['uvw'][0]
v = ms.getdata(['uvw'])['uvw'][1]
w = ms.getdata(['uvw'])['uvw'][2]
for i in range(32896):
  f.write(str(u[i])+" "+str(v[i])+" "+str(w[i])+"\n")
f.close()
ms.close()


exit()
