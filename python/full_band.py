import os, numpy, sys, math
import matplotlib.pyplot as plt

DADA_HEADER_SIZE = 4096
NUM_CHANNELS = 109
NUM_BASELINES = 32896
CHANNEL_WIDTH = 0.024

def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header


def chan2freq(chan, cfreq): return cfreq+(chan-NUM_CHANNELS/2)*CHANNEL_WIDTH


stand = int(sys.argv[2])
file_by_frequency = []
dirs = sys.argv[1].split("/")
file_name = dirs[-1]
for index in range(12):
  for data in [ "data1", "data2", "/data3" ]:
    for num in ["one", "two" ]:
      generated_name = "/"+dirs[1]+"/"+dirs[2]+"/ledaovro"+str(index)+"/"+data+"/"+num+"/"+file_name
      if os.path.isfile(generated_name): 
        f = open(generated_name,"rb")
	header = parse_dada_header(f.read(DADA_HEADER_SIZE))
        f.close()
        file_by_frequency.append((float(header["CFREQ"]), generated_name))

file_by_frequency.sort(key=lambda tup: tup[0])

print len(file_by_frequency), "files"

print "Gathering data"
plt.figure(figsize=(20,5))

x = numpy.zeros(NUM_CHANNELS*len(file_by_frequency))
y = numpy.zeros(NUM_CHANNELS*len(file_by_frequency))
for i, files in enumerate(file_by_frequency):
  print files[0], files[1]
  sys.stdout.flush()
  f = open(files[1],"rb")
  f.read(DADA_HEADER_SIZE)
  full_data   = numpy.fromfile(f, dtype=numpy.complex64, count=NUM_CHANNELS*NUM_BASELINES*4)
  f.close()
  full_data   =   full_data.reshape((NUM_CHANNELS,NUM_BASELINES,2,2))
  bl_index = stand*(stand+1)//2+stand
  for chan in range(NUM_CHANNELS):
    x[i*NUM_CHANNELS+chan] = chan2freq(chan, files[0])
    y[i*NUM_CHANNELS+chan] = abs(full_data[chan,bl_index,0,0])


f = open("band.dat", "w")
for i in range(len(x)):
  f.write(str(x[i])+" "+str(y[i])+"\n")
f.close()
  

plt.plot(x,y)
plt.xlabel("Frequency (MHz)")
plt.ylabel("Amplitude")
plt.ylim(ymin=0)
plt.title("Stand "+str(stand))
#plt.yscale('log')
plt.xticks(range(30, 91, 2))
# Subband boundaries
ymax = numpy.max(y)
for i in range(len(file_by_frequency)):
  top_freq = chan2freq(108, file_by_frequency[i][0])
  plt.plot([top_freq, top_freq], [0, ymax], "r")
print "Plotting"
plt.show()
