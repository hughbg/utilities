import numpy as np
import random
import sys, os, cmath, math
import stands
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from skimage.restoration import  unwrap_phase



NUM_CHANNELS = 109
NUM_STANDS = 256
NUM_BASELINES = NUM_STANDS*(NUM_STANDS+1)/2
DADA_HEADER_SIZE = 4096

outrig_nbaseline = 1270

def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header

def make_dada_index(stand1, stand2):	# Ordering of stands is taken care of so they can be input in any order
  if stand2 > stand1:
    return stand2*(stand2+1)//2+stand1
  else: return stand1*(stand1+1)//2+stand2

def num2pol(x):
  if x == 0: return "X"
  else: return "Y"

def stand_details(data, dump, stand1, stand2):

  for pol1 in [0, 1]:
    for pol2 in [ 0, 1 ]:
      x = [ i for i in range(109) ]

      pol_name = num2pol(pol1)+num2pol(pol2)
      print pol_name,
      sys.stdout.flush()
    
      dada_index = make_dada_index(stand1, stand2)

      y = np.zeros(NUM_CHANNELS)
      all_zero = True
      for j in range(NUM_CHANNELS):
        phase = cmath.phase(data[dump][j][dada_index][pol1][pol2])
	y[j] = phase
	if y[j] != 0.0: all_zero = False

      y = unwrap_phase(y)
      
      plt.title("Stand "+str(stand1)+" to "+str(stand2)+" Phase - Pol "+pol_name)
      plt.xlim(0,110)
        
      if all_zero: plt.figtext(0.4,0.4, "ALL ZERO", color="blue")
      plt.plot(x, y)
      plt.xlabel("Channel")
      plt.ylabel("Phase")
      plt.savefig("x.png")
      os.system("convert  x.png -resize 340 "+"phase_"+str(stand1)+"_"+pol_name+".png")
      plt.clf()
      
 
      y = np.zeros(NUM_CHANNELS)
      all_zero = True
      for j in range(NUM_CHANNELS):
        amplitude = abs(data[dump][j][dada_index][pol1][pol2])
        y[j] = amplitude
        if y[j] != 0.0: all_zero = False

      plt.title("Stand "+str(stand1)+" to "+str(stand2)+" Amplitude - Pol "+pol_name)
      plt.xlim(0,110)
        
      if all_zero: plt.figtext(0.4,0.4, "ALL ZERO", color="blue")
      else: plt.plot(x, y)
      plt.xlabel("Channel")
      plt.ylabel("Amplitude")
      plt.savefig("x.png")
      os.system("convert  x.png -resize 340 "+"amplitude_"+str(stand1)+"_"+pol_name+".png")
      plt.clf()


      x = np.array([ i for i in range(512) ])
      fft = [ complex(0,0) for i in range(512) ]
      for j in range(NUM_CHANNELS):
        fft[j] = complex(data[dump][j][dada_index][pol1][pol2].real, data[dump][j][dada_index][pol1][pol2].imag) 
	
          #phase = j/109.0*6*math.pi
          #fft[j] = complex(math.cos(phase), math.sin(phase)) 
      efft = abs(np.fft.fft(fft))
      plt.xlim(-10, 515)
      plt.title("Stand "+str(stand1)+" to "+str(stand2)+" delay (FFT visibilities)")
      plt.plot(x, efft)
	    
      plt.xlabel("Time")
      plt.ylabel("FFT Amplitude")
      plt.savefig("x.png")
      os.system("convert  x.png -resize 340 "+"delay_"+str(stand1)+"_"+pol_name+".png")
      plt.clf()

  os.system("convert amplitude_"+str(stand1)+"_XX.png amplitude_"+str(stand1)+"_XY.png amplitude_"+str(stand1)+"_YX.png amplitude_"+str(stand1)+"_YY.png +append amplitude_"+str(stand1)+"_XX_XY_YX_YY.png")
  os.system("convert phase_"+str(stand1)+"_XX.png phase_"+str(stand1)+"_XY.png phase_"+str(stand1)+"_YX.png phase_"+str(stand1)+"_YY.png +append phase_"+str(stand1)+"_XX_XY_YX_YY.png")
  os.system("convert delay_"+str(stand1)+"_XX.png delay_"+str(stand1)+"_XY.png delay_"+str(stand1)+"_YX.png delay_"+str(stand1)+"_YY.png +append delay_"+str(stand1)+"_XX_XY_YX_YY.png")
  os.system("convert "+"amplitude_"+str(stand1)+"_XX_XY_YX_YY.png"+" phase_"+str(stand1)+"_XX_XY_YX_YY.png"+" delay_"+str(stand1)+"_XX_XY_YX_YY.png"+" -append "+"baseline_"+str(stand1)+"_"+str(stand2)+".png")


if len(sys.argv) != 5:
  print "Usage: plot_baseline.py <dump index> <stand1> <stand2> <file>"
  sys.exit()

filesize = os.path.getsize(sys.argv[4]) - DADA_HEADER_SIZE 

f = open(sys.argv[4],"rb")
header = parse_dada_header(f.read(DADA_HEADER_SIZE))
nchan  = header['NCHAN']
nant   = header['NSTATION']
npol   = header['NPOL']
navg   = header['NAVG']
bps    = header['BYTES_PER_SECOND']
df     = header['BW']*1e6 / float(nchan)
cfreq  = header['CFREQ']*1e6
utc_start = header['UTC_START']
assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
freq0 = cfreq-nchan/2*df
freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
print "bytes per sec:", bps
print "navg:     ", navg
print "nchan:    ", nchan
print "npol:     ", npol
print "nstation: ", nant
nbaseline = nant*(nant+1)//2
print "nbaseline:", nbaseline
noutrig_per_full = int(navg / df + 0.5)
print "noutrig_per_full:", noutrig_per_full
full_framesize   = nchan*nbaseline*npol*npol
print "full_framesize:  ", full_framesize*8
outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
print "outrig_framesize:", outrig_framesize*8
tot_framesize    = (full_framesize + outrig_framesize)
print " tot_framesize:  ",  tot_framesize*8
ntime = float(filesize) / (tot_framesize*8)
print "ntime:", ntime
frame_secs = int(navg / df + 0.5)
print "Frame secs:", frame_secs
time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
print "Time offset:", time_offset

file_contents = [ [] for i in range(int(ntime)) ]

for t in xrange(int(ntime)):
  print "Time ",t
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
  full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
  outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))

  file_contents[t] = full_data

f.close()


stand_details(file_contents, int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
os.system("eog baseline_"+sys.argv[2]+"_"+sys.argv[3]+".png")

