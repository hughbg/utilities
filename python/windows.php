<html>
<head><title>OVRO Observing Windows</title></head>
<body>
<center><h1>OVRO Observing Windows</h1></center>

An observing window can start 1 hour after sunset, and after the galaxy has set and advanced 20&deg; below the horizon. It finishes at sunrise or galaxy rise. In the table, "Galaxy" means the Galactic centre. Sunset+1h means one hour after sunset, and Galaxy Set-20&deg; means the galaxy has set and dropped 20&deg; below the horizon. The window length is the length of time that is available for an observing window given the above rules.
<p>
What follows are the observing windows for the next year. All times are the local time at OVRO. (Should be LST?)
<center>

<?php 
putenv("MPLCONFIGDIR=/tmp");
putenv("HOME=/tmp");
$command = escapeshellcmd('python /var/www/windows.py');
$output = shell_exec($command);
echo $output;

?>

</center>

</body>
</html>
