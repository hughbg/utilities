import math, cmath
import numpy as np
import random
import matplotlib.pyplot as plt

c = 2.998e+8

def random_phase():		# In degrees
  return np.random.random()*180

def random_angle():		# In degrees, from zenith
  return (np.random.random()-0.5)*180

def random_amplitude():		
  return np.random.random()*10

class Source(object):
  def __init__(self, amplitude, phase_offset, angle, frequency):	# angle is from zenith, angle and phase in deg, freq in Hz
    self.phase = phase_offset*math.pi/180
    self.angle = angle*math.pi/180
    self.amplitude = amplitude
    self.frequency = frequency
    self.wavelength = c/self.frequency
    self.period = 1/frequency


 
class Antenna(object):
  def __init__(self, dist):
    self.distance_from_centre = dist

  def signal_at_time(self, source, time):
    # If the antenna is a +ve distance from the centre and the angle is +ve, then the signal is backwards in distance.
    # http://mysupercomputerexperience.blogspot.com/2014/11/correlator.html
    distance = -self.distance_from_centre*math.sin(source.angle)
    return source.amplitude*math.sin(2*math.pi*distance*source.frequency/c-time*2*math.pi*source.frequency+source.phase)

  def signal_over_time(self, sources, periods):
    signals = [ None for i in range(len(sources)) ]
    for i in range(len(sources)):
      times = np.linspace(0, periods*sources[i].period, periods*128)
      signals[i] = [ self.signal_at_time(sources[i], t) for t in times ]
 
    return [ sum(x) for x in zip(*signals) ]

  def phase_diff(self, source):	# phase diff from antenna at centre
    distance = -self.distance_from_centre*math.sin(source.angle)
    return 2*math.pi*distance*source.frequency/c

def correlate(antennas, sources):
  sig1 = antennas[0].signal_over_time(sources, 16)
  fft1 = np.fft.fft(sig1)
  sig2 = antennas[1].signal_over_time(sources, 16)
  fft2 = np.fft.fft(sig2)
  return np.conj(fft1[16])*fft2[16]

def subband_phase_range(distance, freq):	# Calculate phase range for source on horizon
  phases = np.zeros(109)
  frequencies = np.linspace(47e6-2.616e6/2, 47e6+2.616e6/2, num=109, endpoint=False)
  for i, freq in enumerate(frequencies):
    wavelength = c/freq
    phases[i] = 2*math.pi*distance/wavelength 	# = 2 pi u. For the phase to change, u must change.
  phases = np.unwrap(phases)
  return max(phases)-min(phases)

def delay():	# show how to get delay between antennas
  antenna1 = Antenna(0.0)
  antenna2 = Antenna(-115.618746893)

  channel_visibilities = np.zeros(109, dtype=np.complex64)

  frequencies = np.linspace(47e6-2.616e6/2, 47e6+2.616e6/2, num=109, endpoint=False)
  for i, freq in enumerate(frequencies):		# traverse sub-band frequencies
    #Cyg 48.7684763764
    #Cas 24.4218452415
    #Sun 21.6937099959
    #Her 57.2387991962
    #Vir 32.4608532037
    source = Source(100, 0, 90, freq)
    channel_visibilities[i] = correlate((antenna1, antenna2), [source])


  fft = np.zeros(8192, dtype=np.complex64)
  fft[:len(channel_visibilities)] = channel_visibilities
  fft = abs(np.fft.fft(fft))

  positive_freq = fft[1:len(fft)/2]     # https://docs.scipy.org/doc/numpy/reference/routines.fft.html#module-numpy.fft
  negative_freq = fft[len(fft)/2+1:]
  pos_max = np.max(positive_freq)
  neg_max = np.max(negative_freq)
  if pos_max > neg_max: where_peak = np.argmax(positive_freq)+1    # +1 because of DC taken out
  else: where_peak = np.argmax(negative_freq)-len(negative_freq)

  phases = np.unwrap([ cmath.phase(vis) for vis in channel_visibilities ])
  print "Light travel time between antennas:", antenna2.distance_from_centre/c, "Delay:", (float(where_peak)/len(fft))*(109/2.616e6),
  print "Delay from phase slope:", (phases[-1]-phases[0])/(max(frequencies)-min(frequencies))/(2*math.pi)

  print "Phase range from light travel:", subband_phase_range(antenna2.distance_from_centre, 47e6), "Phase range from correlation:", max(phases)-min(phases)
  np.savetxt("phases.txt", phases)

def closure():
  antenna1 = Antenna(0.0)
  antenna2 = Antenna(50.0)
  antenna3 = Antenna(75.0)

  source = Source(1, 30, 60, 47e6)

  p1 = cmath.phase(correlate((antenna1, antenna2), [source]))
  p2 = cmath.phase(correlate((antenna2, antenna3), [source]))
  p3 = cmath.phase(correlate((antenna1, antenna3), [source]))

  print "Should be 0:", p1+p2-p3	


def sim_21cm():

  def amplitude(A0, f0, fi, si):
    return A0*(fi/f0)**si


  channels = 436	# Make it even
  channel_width = 24e3
  center_freq = 47e6
  num_sources = 10
  separation = 1000.0		# meters

  frequencies = np.linspace(center_freq-channels/2*channel_width, center_freq+channels/2*channel_width, num=channels, endpoint=False)
  
  antenna1 = Antenna(0.0)
  antenna2 = Antenna(separation)	# metres from 0

  print "Delay from c", separation/c*1e9
  calc_delay = separation/c*channels*channel_width
  print "FFT index of horizon from calc", calc_delay
  if calc_delay > channels/2:
    print "Can't fit that delay in FFT"
    exit(1)


  # Get horizon
  vis = np.zeros(len(frequencies), dtype=np.complex64)

  for i, f in enumerate(frequencies):
    s = Source(1, 0, 0, f)
    vis[i] += correlate((antenna1, antenna2), [s])
  fft = np.fft.fftshift(np.fft.fft(vis))
  h_zenith = np.argmax(np.abs(fft))		# Find the index for 0 delay

  vis = np.zeros(len(frequencies), dtype=np.complex64)

  for i, f in enumerate(frequencies):
    s = Source(1, 0, 90, f)
    vis[i] += correlate((antenna1, antenna2), [s])

  fft = np.fft.fftshift(np.fft.fft(vis))
  horizon = np.argmax(np.abs(fft))-h_zenith
  print "FFT index for source on horizon from sim", abs(horizon)


  # Generate sources
  sources_spec = []
  for i in range(num_sources):
    sources_spec.append(( random_amplitude(), random_phase(), random_angle() ))


  # Fill the baseline data
  vis = np.zeros(len(frequencies), dtype=np.complex64)
  u_vals = np.loadtxt("u.dat")
  for spec in sources_spec:
    si = (np.random.random()-0.5)*5
    hvis = np.zeros(len(frequencies), dtype=np.complex64)
    for i, f in enumerate(frequencies):
      s = Source(amplitude(spec[0], frequencies[0], f, si), spec[1], spec[2], f)

      antenna2.distance_from_centre = c*u_vals[i]/f

      vis[i] += correlate((antenna1, antenna2), [s])
      hvis[i] = correlate((antenna1, antenna2), [s])  


      
    

    print "Source delay index",  np.argmax(np.abs(np.fft.fftshift(np.fft.fft(hvis))))-h_zenith, "from angle", separation/c*channels*channel_width*math.sin(spec[2]*math.pi/180); 
 
  # Add noise
  for i in range(len(vis)):
    vis[i] += vis[i]*(np.random.random()-0.5)

  fft = np.fft.fft(vis)

  plt.plot(np.fft.fftshift(np.abs(fft)))
  plt.plot([h_zenith-calc_delay, h_zenith-calc_delay], [0, np.max(np.abs(fft))], "r")
  plt.plot([h_zenith+calc_delay, h_zenith+calc_delay], [0, np.max(np.abs(fft))], "r")
  plt.show()
  

sim_21cm()
exit()

#y = [ 0.0 for i in range(4096) ]
#for i in range(4096):

#delay()
#exit()
antenna1 = Antenna(0.0)
antenna2 = Antenna(50.0)

num_sources = 2
source_list = []
visibility_sum = 0j
for i in range(num_sources):
  s = Source(random_amplitude(), random_phase(), random_angle(), 46e6)
  source_list.append(s)

  sig1 = antenna1.signal_over_time([s], 16)
  fft1 = np.fft.fft(sig1)
  sig2 = antenna2.signal_over_time([s], 16)
  fft2 = np.fft.fft(sig2)
  visibility_sum += np.conj(fft1[16])*fft2[16]


sig1 = antenna1.signal_over_time(source_list, 16)
fft1 = np.fft.fft(sig1)
sig2 = antenna2.signal_over_time(source_list, 16)
fft2 = np.fft.fft(sig2)
x = np.conj(fft1[16])*fft2[16]

print "S combined", x, abs(x), cmath.phase(x)
print "S added", visibility_sum, abs(visibility_sum), cmath.phase(visibility_sum)
