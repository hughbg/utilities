import pyfits, numpy, sys, scipy.signal

def signal_to_noise(image):
      
 if numpy.isnan(numpy.min(im)): stat = -1
 else: 
   clipped = scipy.stats.sigmaclip(im)[0]
   stat = im.max()/numpy.std(clipped)
 return stat

def estimate_noise(data):
    """ Estimate the RMS noise of an image

    from http://stackoverflow.com/questions/2440504/
                noise-estimation-noise-measurement-in-image

    Reference: J. Immerkaer, 'Fast Noise Variance Estimation',
    Computer Vision and Image Understanding,
    Vol. 64, No. 2, pp. 300-302, Sep. 1996 [PDF]

    """

    H, W = data.shape

    data = numpy.nan_to_num(data)

    M = [[1, -2, 1],
         [-2, 4, -2],
         [1, -2, 1]]

    sigma = numpy.sum(numpy.sum(numpy.abs(scipy.signal.convolve2d(data, M))))
    sigma = sigma * numpy.sqrt(0.5 * numpy.pi) / (6 * (W - 2) * (H - 2))

    return sigma

hdulist = pyfits.open(sys.argv[1])
im = hdulist[0].data
#ImagMagik FITS has a third dimension
if len(im.shape) == 3:
  tmp = im[0,::]
  im = tmp

print ( "Sum: %.3f Min: %.3f Max: %.3f Median: %.3f Mean: %.3f Std dev: %.3f Noise estimate (std dev): %.3f S/N: %.3f" % 
	( im.sum(), im.min(), im.max(), numpy.median(im) ,im.mean(), im.std(), estimate_noise(im), signal_to_noise(im)) )
hdulist.close()



