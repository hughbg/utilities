import os

def get_complex_matrix(l):	# from line
  fields = l.split("{")
  matrix = fields[1].split("}")[0].split("\n")[0].split(",")
  for i in range(len(matrix)):
    matrix[i] = complex(matrix[i].replace(" ","").replace("i","j"))
  return matrix



for filename in os.listdir("."):
  if filename[-4:] == ".log": log_file = filename

self_calibrated = []  
model = []
for line in open(log_file,"rU"):
  if line.find("self-calibrated dft") >= 0: 
    flux = get_complex_matrix(line)
    self_calibrated.append(flux)
  if line.find("  visibility model dft  ") >= 0: 
    flux = get_complex_matrix(line)
    model.append(flux)

if len(self_calibrated) != len(model):
  print "Error: not the same number of calibration values"
  sys.exit(0)

ratio_sum = 0
num = 0
best = 1e39
where_best = -1
worst = -1
where_worst = -1
for i in range(len(model)):
  ratio_i = 0
  for j in [ 0, 3 ]:
    ratio_i += abs(self_calibrated[i][j]/model[i][j]-1) # ratio should be near 1
    print self_calibrated[i][j],model[i][j]

  ratio_i /= 2
  num += 1
  ratio_sum += ratio_i
  if ratio_i < best:
    best = ratio_i
    where_best = i
  if ratio_i > worst:
    worst = ratio_i
    where_worst = i
      
    
print "Average",( "%.3f" % (ratio_sum/num) )
print "Best",("%.3f" % best),"at", where_best, "Worst", ("%.3f" % worst), "at", where_worst
