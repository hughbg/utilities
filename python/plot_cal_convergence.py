import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d

with open("cal_ratios.dat") as myfile:
  head = [float(next(myfile).split()[0]) for x in xrange(32896)]
num_used_baselines = np.argmax(head)+1

with open("cal_ratios.dat") as myfile:
  head = [float(next(myfile).split()[1]) for x in xrange(num_used_baselines*50)]

data = np.reshape(head, (50, num_used_baselines))
print data.shape

X, Y = np.meshgrid([i for i in range(data.shape[1])], [i for i in range(data.shape[0])])
print data.shape,X.shape

ax = plt.figure().gca(projection='3d')
ax.azim = 18
ax.elev = 25
plt.xlabel("Baseline")
plt.ylabel("Time")
plt.title("Calibration convergence for all unflagged stands")
ax.plot_surface(X,Y,data, cmap="rainbow")
plt.show()
