import numpy as np
import matplotlib.pyplot as plt

length = 256 # MUST BE EVEN. 

def percent_diff(x1, x2):      # real-valued inputs
    return np.abs((x1-x2)/x1)*100


def normalized_fft(a):
    return np.fft.fft(a)/a.size

def normalized_ifft(f):
    return np.fft.ifft(f)*f.size

def sine_wave_power(fft_modes, mode):
    # fft_modes must be even length. Must be normalized.
    # Mode selects a sine wave. Positive/negative frequencies must be taken into account.
    # This is more accurate the longer the fft is. Accuracy degrades around <16

    assert 0 <= mode and mode <= fft_modes.size//2, "Bad mode"+str(mode)
    
    if mode == 0 or mode == fft_modes.size//2:      # DC or Nyquist
        new_fft = np.zeros_like(fft)
        new_fft[mode] = fft[mode]    
        inv = normalized_ifft(new_fft)
        if mode == 0:                               
            return np.max(np.abs(inv))**2         # DC is not a sine wave
        else:
            return np.max(np.abs(inv))**2/2           # Nyquist is a sine wave. Factor of 2 is to average
    
    else:     # Not DC or nyquist
        new_fft = np.zeros_like(fft_modes)

        new_fft[mode] = fft_modes[mode]
        new_fft[-mode] = fft_modes[-mode]
        inv = normalized_ifft(new_fft)
        return np.max(np.abs(inv))**2/2             # Factor of two to average the sine wave
    

def mode_value_power(fft_modes, mode):
    # fft_modes must be in shifted form with DC in the middle. Must be even length. Must be normalized.
    # Mode selects a sine wave. Positive/negative frequencies must be taken into account.

    assert 0 <= mode and mode <= fft_modes.size//2
    
    if mode == 0  or mode == fft_modes.size//2:      # DC or Nyquist
        if mode == 0:                               
            return np.abs(fft_modes[mode])**2     # DC is not a sine wave
        else:
            return np.abs(fft_modes[mode])**2/2       # Nyquist is a sine wave. Factor of 2 is to average    
    
    else:     # Not DC or nyquist
        amplitude = np.abs(fft_modes[-mode])+np.abs(fft_modes[mode])            # Factor of two to average the sine wave
        return amplitude**2/2

def data_to_power(data):
    
    fft = normalized_fft(data)
    
    amplitude = np.abs(fft[:length//2+1])    # DC to nyquist mode


    # Combine +/- frequencies. Add their amp
    amplitude[1:fft.size//2] += np.abs(fft[-1:-fft.size//2:-1])

    # Now power. Because all modes except DC are sine waves, we do A^2/2 as in https://uk.mathworks.com/matlabcentral/answers/402487-power-of-sine-wave-plot. For the DC we just do A^2.

    power = amplitude**2
    power[1:] /= 2
    
    return power

data = np.random.normal(scale=50, size=length) #+np.random.normal(scale=50, size=length)*1j
fft = normalized_fft(data)


print("Negative and positive frequencies")

mode = np.random.randint(1, fft.size//2)
print("Random mode:", mode)
print("Check same:", sine_wave_power(fft, mode), mode_value_power(fft, mode))

print()
print("Nyquist")


print("Check same:", sine_wave_power(fft, fft.size//2), mode_value_power(fft, fft.size//2))


print()
print("DC")

print("Check same:", sine_wave_power(fft, 0), mode_value_power(fft, 0))

power = data_to_power(data)

# Now go through every mode, and check the power against different methods
power_values = np.zeros(data.size//2+1)
sep_mode_values = np.zeros(data.size//2+1)
sep_sine_wave_values = np.zeros(data.size//2+1)

limit = 5
for i in range(fft.size//2+1):
    power_values[i] = power[i]
    sep_sine_wave_values[i] = sine_wave_power(fft, i)
    sep_mode_values[i] = mode_value_power(fft, i)
    
plt.rcParams['figure.figsize'] = [20, 10]

plt.plot(power_values, label="Power", linewidth=0.5)
plt.plot(sep_sine_wave_values, label="Sine wave", linewidth=0.5)
plt.plot(sep_mode_values, label="Mode value", linewidth=0.5)
plt.legend()
plt.savefig("x")





