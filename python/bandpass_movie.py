import sys, numpy, math, Image, ImageDraw
import matplotlib.pyplot as plt

class Plotter(object):
  SIZE = 512

  def __init__(self, max_amp):
    self.max_amp = max_amp
    self.accumulated = numpy.zeros((self.SIZE, self.SIZE))

  def reset(self):
    self.img = Image.new("1", (self.SIZE, self.SIZE), "white")
    self.draw = ImageDraw.Draw(self.img)
    

  def line(self, amp, phase):
    y = (amp/self.max_amp)*math.cos(phase)*self.SIZE/2
    x = (amp/self.max_amp)*math.sin(phase)*self.SIZE/2
    self.draw.line((self.SIZE/2, self.SIZE/2, self.SIZE/2+x, self.SIZE/2+y), "black")


  def save_image(self, name, stand, channel):
    data = numpy.zeros((self.SIZE, self.SIZE))
    for i in range(self.SIZE):
      for j in range(self.SIZE):
	data[self.SIZE-1-i, j] = self.img.getpixel((i, j))

        # White is 255 black is 0
        self.accumulated[self.SIZE-1-i, j] += 255.0-self.img.getpixel((i, j))
   
    extent=(-math.ceil(self.max_amp), math.ceil(self.max_amp), -math.ceil(self.max_amp), math.ceil(self.max_amp))
    plt.imshow(data, cmap=plt.get_cmap("bone"), interpolation='nearest', extent=extent )
    plt.figtext(0.01,0.97,"Bandpass Values")
    plt.figtext(0.01,0.016,"Stand: "+( "%03d" % stand )+" Channel: "+( "%03d" % channel ))
    plt.savefig(name+"_"+( "%03d" % stand )+"_"+( "%03d" % channel )+".jpg")
    plt.clf()

  def save_accumulated_image(self, colormap):

    for i in range(self.SIZE):
      for j in range(self.SIZE):
        if colormap == "binary": 
          if self.accumulated[i, j] > 0: self.accumulated[i, j] = 1
        else:
          if self.accumulated[i, j] > 0: self.accumulated[i, j] = math.log(self.accumulated[i, j])
    

    plt.imshow(self.accumulated, cmap=plt.get_cmap(colormap), interpolation='nearest', extent=(-math.ceil(self.max_amp), math.ceil(self.max_amp), -math.ceil(self.max_amp), math.ceil(self.max_amp)))

    if colormap != "binary": plt.colorbar()
    plt.figtext(0.01,0.97,"Bandpass Values")
    plt.savefig("all_"+colormap+".jpg")
    plt.clf()

def SQR(x):
    return x*x


# Look at the paper for PX PY etc meanings.
# Amplitude roll-off by freq. Phase variesslowly. 
# With cable delay removed. OR will get a slope with cable delay in.
# Find an observation with only 1 source - easier  to deal with (but then do needto peel it?)s
# Plot PX etc across frequency for antennas individually. PX^2+PY^2 in the end might equal X^
# Check te peeling before bandpass his makes a difference. MWAnot dominatedby single point source.
# For the MWA is stuff being peeled out in order to get the bandpass. - this is critical.
# Think MWA should hev lots of sources but some dominant ones that get peeled.
# DI is just getting gains in different directions.

#Plot amp etc for 1 antenna connected to all other

NUM_PX = 8	# and PY etc.

# OVRO
NUM_STANDS = 256
NUM_CHANNELS = 108

# MWA
#NUM_STANDS = 128
#NUM_CHANNELS = 31

"""
    for k in range(0,len(tmp)): PX_lsq[-1][k] = float(tmp[k])

    tmp = string.split( lines[lineIndex+1], ',' ); PX_fit.append([]); PX_fit[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PX_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+2], ',' ); PY_lsq.append([]); PY_lsq[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PY_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+3], ',' ); PY_fit.append([]); PY_fit[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PY_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+4], ',' ); QX_lsq.append([]); QX_lsq[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QX_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+5], ',' ); QX_fit.append([]); QX_fit[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QX_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+6], ',' ); QY_lsq.append([]); QY_lsq[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QY_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+7], ',' ); QY_fit.append([]); QY_fit[-1]=zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QY_fit[-1][k] = float(tmp[k])
"""

values = numpy.zeros((NUM_STANDS, NUM_PX, NUM_CHANNELS, 2))
eight_count = numpy.zeros((NUM_STANDS))

max_amp = 0
line_num = 0
for line in open(sys.argv[1],"r"):
  if line_num > 0:
    
    if len(line.split()) != NUM_CHANNELS+1:
      print "WARNING: Bandpass line doesn't have ",NUM_CHANNELS, "has",len(line.split())

    stand = int(line.split(",")[0])-1	# and make zero-based
    if stand >= NUM_STANDS:
      print "WARNING: got stand number",stand


    for i, pair in enumerate(line.split()[1:]):
      p = pair.split(",")
      amp = float(p[0])
      phase = float(p[1])

      #if amp < 0 or 1 < amp:
      #  print "WARNING: amp is not in [0,1]:", amp
      if phase < -math.pi or math.pi < phase:
        print "WARNING: phase is not in [-pi,pi]:",phase

      y = abs(amp*math.cos(phase))
      x = abs(amp*math.sin(phase))
      if x > max_amp: max_amp = x
      if y > max_amp: max_amp = y
      
      values[stand, eight_count[stand], i, 0] = amp
      values[stand, eight_count[stand], i, 1] = phase

    eight_count[stand] += 1


  line_num += 1

# Hack here
def mean(x):
  sum=0
  for n in x: sum += n
  return sum/len(x)

for i in range(NUM_PX):
  nums = []  
  for j in range(NUM_STANDS):
    for k in range(NUM_CHANNELS):
      val = values[j, i, k, 0]	# amp PX_fit^2+PY_fit^2
      nums.append(val)
      if values[j, i, k, 0] > 1.2: print values[j, i, k, 0]
  print i, mean(nums)

sys.exit(0) 

max_amp = 1.5
image = Plotter(max_amp)


all_amp = numpy.zeros((NUM_STANDS*NUM_CHANNELS*NUM_PX))
all_phase = numpy.zeros((NUM_STANDS*NUM_CHANNELS*NUM_PX))
index = 0
for stand in range(NUM_STANDS):
  if eight_count[stand] > 0:
    print "Stand",stand
    for channel in range(NUM_CHANNELS):
      image.reset()
      for i in range(NUM_PX):
        image.line(values[stand, i, channel, 0], values[stand, i, channel, 1])
	all_amp[index] = values[stand, i, channel, 0]
	all_phase[index] = values[stand, i, channel, 1]
	index += 1
        #print values[stand, i, channel, 0], values[stand, i, channel, 1]
      image.save_image("bp",stand, channel)

all_amp = all_amp[:index]
all_phase = all_phase[:index]
print "Amp stats"
print "Mean", numpy.mean(all_amp),"Std:",numpy.std(all_amp),"Min:",numpy.min(all_amp),"Max:",numpy.max(all_amp)
print "Phase stats"
print "Mean", numpy.mean(all_phase),"Std:",numpy.std(all_phase),"Min:",numpy.min(all_phase),"Max:",numpy.max(all_phase)


for stand in range(NUM_STANDS):
  if eight_count[stand] > 0:
    print "Stand",stand
    for channel in range(NUM_CHANNELS):
      for i in range(NUM_PX):
        image.line(values[stand, i, channel, 0], values[stand, i, channel, 1])
        #print values[stand, i, channel, 0], values[stand, i, channel, 1]
      
image.save_accumulated_image("jet")
image.save_accumulated_image("binary")

