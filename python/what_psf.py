import math

vlight = 299792458	# m/s
baseline = 750	# metres
freq = 140e6

wavelength = vlight/freq
psf = 180*wavelength/baseline/math.pi
print "Approx PSF",psf,"(degrees)"

NSIDE = 512
pix_size = 180*math.sqrt(4*math.pi/12/NSIDE/NSIDE)/math.pi
print "Healpix NSIDE",NSIDE,"gives pixel width",pix_size,"(degrees)"
print "PSF covers this many pixels across",psf/pix_size

# Freq range

cfreq = 3.9156E+07	# Hz
bandwidth = 2616000	# Hz

print "Bottom freq",cfreq-(bandwidth/2),"top freq",cfreq+bandwidth/2
