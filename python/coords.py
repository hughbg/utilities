import math, numpy.linalg

def matrix_vector_mult(m, v):
  out = [ 0, 0, 0]  
  for i in range(3):
    for j in range(3):
      out[i] += m[i][j]*v[j]
  return out
 

def ENH2XYZ_local(E,N,H,lat): 
  sl = math.sin(lat)
  cl = math.cos(lat)

  m = [ [0, -sl, cl], [1, 0, 0], [0, cl, sl] ]
  return matrix_vector_mult(m, [ E, N, H])

def iENH2XYZ_local(x,y,z,lat):
  sl = math.sin(lat)
  cl = math.cos(lat)

  m = [ [0, -sl, cl], [1, 0, 0], [0, cl, sl] ]
  inv_m = numpy.linalg.inv(m)
  return matrix_vector_mult(inv_m, [ x, y, z])

lat = math.pi*30/180

#res = ENH2XYZ_local(5, 7, 2, lat)
#print iENH2XYZ_local(res[0], res[1], res[2],lat)

def calcUVW(ha, dec, x, y, z):
  sh = math.sin(ha); sd = math.sin(dec)
  ch = math.cos(ha); cd = math.cos(dec)

  m = [ [sh, ch, 0], [-sd*ch, sd*sh, cd], [cd*ch, -cd*sh, sd] ]
  return matrix_vector_mult(m, [ x, y, z])

def icalcUVW(ha, dec, u, v, w):
  sh = math.sin(ha); sd = math.sin(dec)
  ch = math.cos(ha); cd = math.cos(dec)

  m = [ [sh, ch, 0], [-sd*ch, sd*sh, cd], [cd*ch, -cd*sh, sd] ]
  inv_m = numpy.linalg.inv(m)
  return matrix_vector_mult(inv_m, [ u, v, w])


#res = calcUVW(math.pi*30/180, math.pi*60/180, 0, 1, 0)
print icalcUVW(0, math.pi*60/180, 1, 0, 0)
print icalcUVW(0, math.pi*60/180, 0, 1, 0)
print icalcUVW(0, math.pi*60/180, 0, 0, 1)


