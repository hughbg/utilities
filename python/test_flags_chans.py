import os, sys

def save_flags(fl):
  f = open("flagged_channels.txt","w")
  for i in fl: f.write(str(i)+"\n")
  f.close()

def is_nan():
  for filename in os.listdir("."):
    if filename[-4:] == ".log":
      for line in open(filename):
        if line.find("visibility model dft") > -1 and line.find("nan") > -1: return True
  return False


flags = []
for i in range(109):
  if i < 55 or i > 58: flags.append(i)

#sys.exit(0)

for i in range(109):
  new_flags = list(flags)
  if i in flags: new_flags.remove(i)
  save_flags(new_flags)
  os.system("rm *.log *.fits; /data2/hg/RTS/bin/rts_node_gpu cuwarp.in")
  os.system("rm *.fits; /data2/hg/RTS/bin/rts_node_gpu xx.in")
  if not is_nan(): 
    flags = list(new_flags)
    print i, "ok"
  else: print i, "not ok"

save_flags(flags)



