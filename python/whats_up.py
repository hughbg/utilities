import ephem, os
from datetime import datetime
from dateutil import tz
import math, sys

import matplotlib
matplotlib.use('Agg')
from pygsm import GSMObserver
import pylab as plt


# All in degrees
HORIZON_AZ = [ 0,  5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 65, 70,
             75, 80, 85, 90, 95,100,105,110,115,120,125,130,145,150,
            155,175,180,185,190,195,200,205,210,230,235,240,245,250,
            260,265,270,280,285,290,295,300,305,310,350,355,360 ]

HORIZON_EL = [  3,  5,  5,  7,  9, 10, 12, 13, 13, 14, 15, 15, 13, 12,
             11, 10,  9,  8,  7,  6,  6,  5,  4,  3,  3,  4,  4,  3,
              2,  2,  3,  3,  4,  5,  5,  6,  7,  7,  6,  7,  8,  9,
              9,  8,  7,  7,  5,  4,  4,  3,  3,  2,  2,  3,  3 ]

ovro_location = ('37.2397808', '-118.2816819', 1183.4839)

# Global sky model - setup for sky generation
ov_gsm = GSMObserver()
ov_gsm.lon = ovro_location[1]
ov_gsm.lat = ovro_location[0]
ov_gsm.elev = ovro_location[2]

def get_utc_now():
  if len(sys.argv) == 2:
    try:
      utc = datetime.strptime(sys.argv[1], "%Y/%m/%d %H:%M:%S")
    except:
      utc = None
  else: utc = datetime.utcnow()
  return utc  

def time_at_timezone(dt_string, zone):
  # Incoming format must be 2016/1/31 22:56:01.
  # ephem.Date() does that
  from_zone = tz.gettz('UTC')
  to_zone = tz.gettz(zone)

  dt = datetime.strptime(dt_string, "%Y/%m/%d %H:%M:%S")

  # Tell the datetime object that it's in UTC time zone since 
  # datetime objects are 'naive' by default
  dt = dt.replace(tzinfo=from_zone)

  # Convert time zone
  return ( "%s" % ephem.Date(dt.astimezone(to_zone)) )

def time_at_ovro(dt_string): return time_at_timezone(dt_string,"America/Los_Angeles")
def time_at_boston(dt_string): return time_at_timezone(dt_string,"America/New_York")

def above_horizon(azimuth, elevation):
  azimuth *= 180/math.pi
  elevation *= 180/math.pi

  # Find the closest azimuth in list
  closest = 1e39
  where_closest = -1
  for i in range(len(HORIZON_AZ)):
    dist = abs(HORIZON_AZ[i]-azimuth)
    if dist < closest: 
      where_closest = i
      closest = dist

  return elevation > HORIZON_EL[where_closest]

def day_fraction_to_hs(d):
  negative = d<0
  d = abs(d)
  h = int(d*24)
  m = int((d*24-h)*60)
  if negative: return "-"+str(h)+"h"+str(m)+"m"
  else: return "+"+str(h)+"h"+str(m)+"m"

def name_with_link(body):
  if hasattr(body,"link"):
    return "<a href=\""+obj.link+"\">"+str(obj.name)+"</a>"
  else: return obj.name

def ra_dec(body):
  if hasattr(body,"link"):
    return "RA: "+str(obj._ra)+"<br>DEC: "+str(obj._dec)
  else: return "RA: "+str(obj.ra)+"<br>DEC: "+str(obj.dec)

class FixedBody(ephem.FixedBody): pass

ovro = ephem.Observer(); (ovro.lat, ovro.lon, ovro.elev) = ovro_location

now_utc_now = get_utc_now()
if not now_utc_now:
  print "<i>Pretend UTC date",sys.argv[1]," is invalid</i>"
  sys.exit(1)

if len(sys.argv) == 2:
  print "<i>Pretending that the current UTC time is",sys.argv[1],"</i>"
  ovro.date = now_utc_now
  print "<a href=whats_up.php>Remove the pretence</a>"
else: print "<a href=whats_up.php>Reload</a>"
# http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Cassiopeia+A
cas = FixedBody()
cas._ra  =  '23:23:24'
cas._dec = '+58:48:54'
cas.name = "Cas"
cas.link = "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Cassiopeia+A"
cas.compute(ovro)
# http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NAME+CYGNUS+A
cyg = FixedBody()
cyg._ra  =  '19 59 28.357'
cyg._dec = '+40 44 02.10'
cyg.name = "Cyg"
cyg.link = "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NAME+CYGNUS+A"
cyg.compute(ovro)
# M1 http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Taurus%20A
tau = FixedBody()	# Taurus A, Crab
tau._ra  =  '05 34 31.94'
tau._dec = '+22 00 52.2'
tau.name = "Tau"
tau.link = "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Taurus%20A"
tau.compute(ovro)
# http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Hercules+A
herc = FixedBody()	
herc._ra  =  '16 51 11.4'
herc._dec = '+04 59 20'
herc.name = "Herc"
herc.link = "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Hercules+A"
herc.compute(ovro)
# http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NAME+HYDRA+A
hydra = FixedBody()	
hydra._ra  =  '09 18 05.651'
hydra._dec = '-12 05 43.99'
hydra.name = "Hyd"
hydra.link = "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NAME+HYDRA+A"
hydra.compute(ovro)
# http://simbad.u-strasbg.fr/simbad/sim-id?Ident=IC++443
gemini = FixedBody()	
gemini._ra  =  '06 18 02.7'
gemini._dec = '+22 39 36'
gemini.name = "Gem"
gemini.link = "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=IC++443"
gemini.compute(ovro)
# http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Virgo%20A&NbIdent=1&Radius=10&Radius.unit=arcmin
virgo = FixedBody()	
virgo._ra  =  '12 30 49.423'
virgo._dec = '+12 23 28.04'
virgo.name = "Vir"
virgo.link = "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Virgo%20A&NbIdent=1&Radius=10&Radius.unit=arcmin"
virgo.compute(ovro)
sun = ephem.Sun()
sun.compute(ovro)
moon = ephem.Moon()
moon.compute(ovro)


# Print HTML
print "<h2>LST at OVRO</h2>"
print "<font size=+2>",ovro.sidereal_time(),"</font><p>"

# Finally, generate GSM sky
ov_gsm.date = now_utc_now
ov_gsm.generate(50)
sky = ov_gsm.view(logged=True, show=False, min=9, max=20)
plt.savefig('/home/leda/leda_server/roachnest/files/img/current_gsm.png')
os.system("convert /home/leda/leda_server/roachnest/files/img/current_gsm.png -trim current_gsm.png")

print "<table cellpadding=20><tr align=center><td colspan=2><h2>The location of several objects relative to the horizon at OVRO</h2>\
	The altitude is the angle in degrees of the object above 0 elevation. Objects that are below the horizon have altitudes in red. The horizon <br>includes the terrain (mountains) around the OVRO site,\
    so it is possible to have positive altitude but still be below the horizon.</td></tr>"
print "<tr><td> <img src=current_gsm.png></td><td>"

print "<table cellpadding=10 cellspacing=0 border>"
print "<tr><td></td><th>RA/DEC</th><th>Current Altitude (degrees)</th></tr>"
objects = [ sun, cas, cyg, tau, herc, hydra, virgo, gemini ]
for obj in objects:
  if above_horizon(obj.az, obj.alt):
    print "<tr><td>"+name_with_link(obj)+"</td><td>"+ra_dec(obj)+"</td><td>"+( "%.2f" % (obj.alt/math.pi*180))+"</td></tr>"
  else:
    print "<tr><td>"+name_with_link(obj)+"</td><td>"+ra_dec(obj)+"</td><td><font color=red>"+( "%.2f" % (obj.alt/math.pi*180))+"</font></td></tr>"
 
print "</table>"
print "</td></tr></table>"

print "<h2>Useful Times</h2>"
print "<table cellpadding=10 cellspacing=0 border>"
print "<tr><td></td><th>UTC Date/Time</th><th>OVRO Date/Time</th><th>Boston Date/Time</th><th>Time to event</th></tr>"
now = ( "%s" % ephem.Date(now_utc_now))
print "<tr><td>Current Time</td><td>"+now+"</td><td>"+time_at_ovro(now)+"</td><td>"+time_at_boston(now)+"</td><td></td></tr>"
next_sun_set = ovro.next_setting(sun)
next_sun_set_str = ( "%s" % ovro.next_setting(sun))
print "<tr><td>Next Sunset at OVRO</td><td>"+next_sun_set_str+"</td><td>"+time_at_ovro(next_sun_set_str)+"</td><td>"+time_at_boston(next_sun_set_str)+"</td><td>"+\
       day_fraction_to_hs((float)(next_sun_set-ephem.Date(now_utc_now)))+"</td></tr>"
objects = [ ("Cas", cas ), ( "Cyg", cyg ), ( "Tau", tau ), ( "Herc", herc ), ( "Hydra", hydra ), ("Virgo", virgo), ("Gemini", gemini), ("Moon", moon) ]
for obj in objects:
  previous_transit = ovro.previous_transit(obj[1])
  next_transit = ovro.next_transit(obj[1])
  previous_transit_str = ( "%s" % previous_transit )
  next_transit_str = ( "%s" % next_transit )

  print "<tr><td>Prev / Next "+obj[0]+" transit</td><td>"+previous_transit_str+" / <br>"+next_transit_str+"</td>\
	<td>"+time_at_ovro(previous_transit_str)+" / <br>"+time_at_ovro(next_transit_str)+"</td><td>" +\
	time_at_boston(previous_transit_str)+" / <br>"+time_at_boston(next_transit_str)+"</td><td>"+\
        day_fraction_to_hs((float)(previous_transit-ephem.Date(now_utc_now)))+" / <br>"+day_fraction_to_hs((float)(next_transit-ephem.Date(now_utc_now)))+"</td></tr>"


print "</table>"



 
