# Kurtosis on Fast Capture data loaded from DADA files.

import numpy as np
import os, scipy.stats 
import flags, random
import matplotlib.pyplot as plt

M = 10320/2
FFT_SIZE = 8192

def get_pretend_data(nc):
  data_out = np.zeros((nc, 16, 4, 2, 109, 4, 2), dtype=np.complex64)
  for i in range(nc): 
    data_out[i, 0, 0, 0, 0, 0, 0] = complex(np.round(np.random.randn()*1.7), 0)
  for i in range(nc):
    data_out[i, 0, 0, 0, 0, 0, 0] = complex(data_out[i, 0, 0, 0, 0, 0, 0].real, np.round(np.random.randn()*1.7))

  return data_out

def print_bin(xx):
  s = ""
  for i in range(8):
    s = str(xx&0b00000001)+s
    xx = xx>>1
  return  s

def parse_complex(x):    # from 8 bit complex
  if type(x) == np.int8:
    #print x, print_bin(x)
    re = np.bitwise_and(x, np.int8(0b11110000)) / 16
    im = np.int8(np.left_shift(np.bitwise_and(x, np.int8(0b00001111)), 4)) /16
  else:         # pretend data
    return complex(x[0], x[1])  
  #print re, im

  return complex(im, re)



def kurtosis():
  histogram = np.zeros(13)

  auto_corr = np.zeros(M, dtype=np.complex64)
  for i in range(M):
    auto_corr[i] = kbuff[i]*np.conj(kbuff[i])/FFT_SIZE
    histogram[kbuff[i].real+6] += 1

  psd = 2.0*np.real(auto_corr)/FFT_SIZE         # Data has been correlated, but these factors need to be added in
  S1 = np.sum(psd)
  S2 = np.sum(psd**2.0)      
  #Compute spectral kurtosis estimator
  Vk_square = (M+1)/(M-1)*(M*(S2/(S1**2.0))-1)      # eqn 8 http://mnrasl.oxfordjournals.org/content/406/1/L60.full.pdf
                                                        # and eqn 50 https://web.njit.edu/~gary/assets/PASP_Published_652409.pdf
    
  #np.savetxt("x.dat", np.real(kbuff))
  #print Vk_square, scipy.stats.kurtosis(np.real(kbuff)), scipy.stats.kurtosis(np.imag(kbuff)), np.mean(np.real(kbuff)), np.mean(np.imag(kbuff)), np.max(kbuff)-np.min(kbuff)
  return Vk_square, scipy.stats.kurtosis(np.real(kbuff)), scipy.stats.kurtosis(np.imag(kbuff)), histogram

def plot(ant, ch, sk_val, true_real, true_imag, histogram, mess):
  plt.title("Stand "+str(ant//2)+", Pol "+str(ant%2)+", Ch "+str(ch)+", SK "+( "%.2f" % sk )+", True K "+( "%.2f" % true_real ))
  plt.xlabel("Real 4-bit value")
  plt.ylabel("Count")
  plt.plot([ i-6 for i in range(13) ], histogram)
  plt.savefig("hist_"+str(ant)+"_"+str(ch)+".png")
  plt.clf()

  f = open("histograms.html", "a")
  f.write("<img src=hist_"+str(ant)+"_"+str(ch)+".png><br>SK = "+( "%.2f" % sk )+", True K "+( "%.2f" % true_real )+" "+mess+"<hr>\n")
  f.close()


# Main

# Only one file

f = "/data2/fc/ledaovro1/one/2017-06-08-06:22:40_0000000000000000.000000.dada"
fid=open(f, "rb")
header=fid.read(4096)
data_all=np.fromfile(fid, dtype=np.int8, count=os.path.getsize(f)-4096)
fid.close()

# Array dimensions from C code,want to convert
# [N sequence numbers][16 roaches][4 input groups][2 time samples][109 chans][4 stations][2 pols][2 dims][4 bits]
# dims is real/imag

# Buffer for kurtosis windows
kbuff = np.array([ 0j for i in range(M) ])
kbuff_index = 0

chunk_size = 16*4*2*109*4*2	# in bytes
num_chunks = data_all.shape[0]/chunk_size
print "Num chunks", num_chunks, "Chunk size (bytes)", chunk_size
print "Threshold?", 4.0*M**2/((M-1)*(M+2)*(M+3))

# Matlab
#data_rs=reshape(data_all_c,2,ant_per_f,numch,timePerChunk,numf,numcheck/timePerChunk);
#data_perm=permute(data_rs,[1,2,5,3,4,6]);
#data_perm_rs=reshape(data_perm,2,numf*ant_per_f,numch,numcheck);
# Python copy of Matlab
data_all = data_all.reshape(num_chunks, 64, 2, 109, 8)
data_all = np.transpose(data_all, (0, 2, 3, 1, 4))
data_all = data_all.reshape((num_chunks*2, 109, 512))	# Data converted into nice form
#data_all = get_pretend_data(num_chunks)

#Do one
"""
for i in range(num_chunks):
  kbuff[kbuff_index] = parse_complex(data_all[i, 0, 0, 0, 13, 0, 0 ])    # (1289, 0, 0, 0, 13, 0, 0)
  histogram[kbuff[kbuff_index].real+8] += 1
  kbuff_index += 1

  kbuff[kbuff_index] = parse_complex(data_all[i, 0, 0, 1, 13, 0, 0 ])    # (1289, 0, 0, 0, 13, 0, 0)
  histogram[kbuff[kbuff_index].real+8] += 1
  kbuff_index += 1


  if kbuff_index == M:
    print kurtosis()
    for i in range(-8, 8):
      if histogram[i+8] != 0: print i, histogram[i+8]
    exit()

    kbuff_index = 0

for i in range(-8, 8):
  if histogram[i+8] != 0: print i, histogram[i+8]

exit()
"""

if os.path.exists("histograms.html"):
  os.remove("histograms.html")

fl = flags.Flags()	# Flag bad ones
fl.select_leda_core()

# Scan all
best_k = 1e9
where_best_k = ()
where_best_sk = 0
for antenna in range(512):
  random_chan = int(np.floor(random.random()*109))	# for plotting an example
  for chan in range(109):
      stand = antenna//2
      if fl.stand_flagged(stand): message = "FLAGGED"
      else: message = "NOT_FLAGGED"

      print antenna, chan
      kbuff = np.array([ 0j for i in range(M) ])
      kbuff_index = 0

      random_chunk = int(np.floor(random.random()*num_chunks*2))
      for i in range(num_chunks*2):
        c = parse_complex(data_all[i, chan, antenna])
	if c.real not in [ -8, -7, 7 ] and c.imag not in [ -8, -7, 7 ]:		# Clip edge values, we don't trust them
	  # Add to buffer
          kbuff[kbuff_index] = parse_complex(data_all[i, chan, antenna])
          kbuff_index += 1
	#else: print "Got -8 value"

        if kbuff_index == M:   # Buffer is full
          sk, k1, k2, hist = kurtosis()		# Nita/Gary kurtosis
	  if chan == random_chan and i >= random_chunk: plot(antenna, chan, sk, k1, k2, hist, message); random_chunk = 1e39
          print "K", sk, k1, k2, stand, message
          if abs(k1) < best_k:
            best_k = abs(k1)
            where_best_sk = sk
	    where_best_k = ( i, chan, antenna )
          if abs(k2) < best_k:
            best_k = abs(k2)
            where_best_sk = sk
            where_best_k = ( i, chan, antenna )

          kbuff_index = 0
 
      print "Res", best_k, where_best_sk, where_best_k
