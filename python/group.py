import sys, pyfits
from sets import Set


def DecodeBaselineCalTech(blcode):
  b2 = int(blcode) % 256;
  b1 = (blcode - b2)/256;
  return (b1, b2)

def DecodeBaseline(blcode):
  if blcode > 65536:
    blcode -= 65536;
    b2 = int(blcode) % 2048;
    b1 = (blcode - b2)/2048;
  else: 
    b2 = int(blcode) % 256;
    b1 = (blcode - b2)/256;
  return (b1, b2)


def equal(c1, c2):
  return abs(c1[0]-c2[0]) < 1 and abs(c1[1]-c2[1]) < 1

def is_conj(c1, c2):  
  c3 = [ c2[0], -c2[1] ]
  return equal(c1,c3)


def print_pols(b, ch, casa_vis_list, corr_vis_list):
  for pol in range(4):
    print b, ch, pol,
    print "(", casa_vis_list[pol][0], casa_vis_list[pol][1], ")", "(", corr_vis_list[pol][0], corr_vis_list[pol][1], ")", 
    print DecodeBaseline(casa[0].data.par('baseline')[b]),
    print DecodeBaseline(corr[0].data.par('baseline')[b])


def all_vis_match(casa_vis_list, corr_vis_list):
  for pol in range(4):
    if not equal(casa_vis_list[pol], corr_vis_list[pol]): return None
  return "all_vis_match"

def all_vis_conj(casa_vis_list, corr_vis_list):
  for pol in range(4):
    if not is_conj(casa_vis_list[pol], corr_vis_list[pol]): return None
  return "all_vis_conj"
  
def auto_corr_pattern(casa_vis_list, corr_vis_list):
  if not ( equal(casa_vis_list[0], corr_vis_list[0]) and equal(casa_vis_list[1], corr_vis_list[1]) and corr_vis_list[0][1] == 0 and corr_vis_list[1][1] == 0
		and casa_vis_list[0][1] < 1 and casa_vis_list[1][1] < 1 ): return None
  if not is_conj(casa_vis_list[2], corr_vis_list[2]): return None
  if not ( equal(corr_vis_list[3], [ 0.0, 0.0 ]) ): return None
  if not equal(casa_vis_list[3], corr_vis_list[2]): return None

  # I think our 0'dvalues would be the same as CalTech if we didn't zero them. 
  # Can't tell because the imag is thrown away in the L file.
  # Yes vis is conjugated if baseline reversed.
  # The auto_corr_pattern conjugation has nothing to do with baseline_reversal. dada2ms has to be in error
  # because our value is only subject to the global conjugation.

  return "auto_corr_pattern"

def examine_channel(b, ch):
  casa_vis_list = [ [], [], [], [] ]
  corr_vis_list = [ [], [], [], [] ]
  result = Set()
  for pol in range(4):
    casa_vis_list[pol] = casa[0].data.data[b][0][0][0][ch][pol]
    corr_vis_list[pol] = corr[0].data.data[b][0][0][ch][pol]    # This shows how to index

  match = all_vis_match(casa_vis_list, corr_vis_list)
  result.add(match)
  if match: return result
  result.add(all_vis_conj(casa_vis_list, corr_vis_list))
  result.add(auto_corr_pattern(casa_vis_list, corr_vis_list))
  result.discard(None)
  if len(result) == 0: result.add("rogue")

  #print ch, result

  return result


def examine_baseline(b):
  # Some channels may satisfy multiple patterns. Record them.
  counts = dict(rogue=0, auto_corr_pattern=0, all_vis_conj=0, all_vis_match=0)
  for ch in range(109):
    for pattern in examine_channel(b, ch):
      counts[pattern] += 1

  # Check that only one pattern appears 109 times
  result = ""
  for key in counts: 
    #print key, counts[key]
    if counts[key] == 109:
      if result != "": return "rogue"			# Problems, already got a result
      else: result = key

  return result


casa = pyfits.open('/data1/hg/2015-02-03-18:17:02.uvfits')

corr = pyfits.open("/data2/hg/interfits/lconverter_for_caltech/WholeSkyL64_47.004_d20150203_utc181702/WholeSkyL64_47.004_d20150203_utc181702.uvfits")

if abs(casa[0].data.par('date')[0]-corr[0].data.par('date')[0]) > 1e-7:
  print "Dates don't match",casa[0].data.par('date')[0]-corr[0].data.par('date')[0]
  sys.exit(0)

#c = 299792458
#for baseline in range(32896*6,32896*7):
#  casa_uvw = ( casa[0].data.par(0)[baseline]*c, casa[0].data.par(1)[baseline]*c, casa[0].data.par(2)[baseline]*c )
#  corr_uvw = ( corr[0].data.par(0)[baseline]*c, corr[0].data.par(1)[baseline]*c, corr[0].data.par(2)[baseline]*c )
#  #print casa_uvw[0]-corr_uvw[0], casa_uvw[1]-corr_uvw[1], casa_uvw[2]-corr_uvw[2]
#  print  corr_uvw[0],corr_uvw[1],corr_uvw[2]

#sys.exit(0)

# For how to index, see examine_channel

for baseline in range(32896):

  pattern = examine_baseline(baseline)
  casa_ant = DecodeBaselineCalTech(casa[0].data.par('baseline')[baseline])
  casa_ant = "( "+str(casa_ant[0])+" "+str(casa_ant[1])+" "+str(casa[0].data.par('baseline')[baseline])+" )"
  corr_ant = DecodeBaseline(corr[0].data.par('baseline')[baseline])
  corr_ant = "( "+str(corr_ant[0])+" "+str(corr_ant[1])+" "+str(corr[0].data.par('baseline')[baseline])+" )"


  print baseline, casa_ant, corr_ant, pattern

