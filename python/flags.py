import numpy as np

# Takes care of flagging
class Flags():
  def __init__(self):
    self.flags = np.loadtxt("flagged_stands.dat", dtype=np.int)
    self.flags_bl = np.loadtxt("flagged_baselines.dat", dtype = np.int)
    self.enabled = True
    self.hyperriggers = [ 57, 58, 59, 60, 61, 62, 63, 64, 121, 122, 123, 124, 125, 126, 127, 128, 185, 186, 187, 188, 189, 190, 191, 192, 239, 240, 241, 242, 243, 244, 245, 246 ]       # 1 -based
    self.hyperriggers = [ i-1 for i in self.hyperriggers ]	# 0-based
    self.outriggers = [ 251, 252, 253, 254, 255 ]

  def select_caltech(self):
    print "Flags: select caltech stands"
    for i in range(256):
      if i not in self.hyperriggers: self.flags = np.append(self.flags,i)

  def select_leda_core(self):
    print "Flags: select LEDA core"
    for i in range(256):
      if i in self.hyperriggers or i in self.outriggers: self.flags = np.append(self.flags,i)

  def select_leda_outriggers(self):
    print "Flags: select LEDA outriggers"
    for i in range(256):
      if i not in self.outriggers: self.flags = np.append(self.flags,i)

  def select_leda_all(self):
    print "Flags: select LEDA all"
    for i in range(256):
      if i in self.hyperriggers: self.flags = np.append(self.flags,i)

  def unflagged(self):
    result = []
    for i in range(256):
      if not self.stand_flagged(i): result.append(i)
    return result

  def stand_flagged(self, st):
    if not self.enabled: return False

    return st in self.flags

  def bl_flagged(self, st1, st2):
    if not self.enabled: return False

    for ii in range(len(self.flags_bl)):
      if (st1 == self.flags_bl[ii, 0] and st2 == self.flags_bl[ii, 1]) or (st2 == self.flags_bl[ii, 0] and st1 == self.flags_bl[ii, 1]): return True
    return False

  def disable(self):
    self.enabled = False    

