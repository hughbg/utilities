import numpy, sys

data = []
data_min = 1e29
data_max = -1e29
for line in sys.stdin:
	if float(line) < data_min: data_min = float(line) 
	if float(line) > data_max: data_max = float(line)
	data.append(float(line))

hist = numpy.histogram(data,50)[0]
bins = numpy.histogram(data,50)[1]
for i in range(len(hist)):
	print bins[i],hist[i] 

sys.exit(0)
# How to use a fixed bin width
bin_sequence = []
x = data_min
while x <= data_max+0.5599459171300003:
	bin_sequence.append(x)
	x += 0.5599459171300003


hist = numpy.histogram(data,bin_sequence)[0]
bins = numpy.histogram(data,bin_sequence)[1]
for i in range(len(hist)):
        print bins[i],hist[i]

