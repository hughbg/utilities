import math, numpy as np
import scipy.optimize


# Delay assuming source at horizon staring down the baseline, i.e. maximum delay

c = 299792458
distance = 100.0	# metres

print "Delay from light travel time", distance/c

# Setup frequency band
f0 = 44e6
freqs = [ f0+i*.024e6 for i in range(109) ]

# Do phases
phases = np.zeros(109)
for i in range(len(freqs)):
  wavelength = c/freqs[i]

  phases[i] = 2*math.pi/wavelength*distance

phases = np.unwrap(phases)
wavelength1 = c/freqs[0]
wavelength2 = c/freqs[-1]
print "Delay from phase", (phases[-1]-phases[0])/(freqs[-1]-freqs[0])/(2*math.pi)


# Do complex visibilities with the same phases
visibilities = np.zeros(2*8192, dtype=np.complex64)
for i in range(len(freqs)):
  wavelength = c/freqs[i]

  visibilities[i] = complex(math.cos(2*math.pi/wavelength*distance), math.sin(2*math.pi/wavelength*distance))

efft = abs(np.fft.fft(visibilities))

where_peak = np.argmax(np.fft.fftshift(efft))
where_peak -= len(visibilities)/2
print "Delay from FFT", (float(where_peak)/len(efft))*(109/2.616e6)



