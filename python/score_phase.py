import numpy as np
import os, sys, math
from skimage.restoration import unwrap_phase
from scipy import stats
import cmath

NUM_STANDS = 256
NUM_CHANNELS = 109
DADA_HEADER_SIZE = 4096
outrig_nbaseline = 1270

def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header

def print_stats(a):
  print "Min:",a.min(),"Mean:",np.mean(a),"Max:",a.max()

def normalize(a):	# Max the maximum = 1
  b = a.copy()
  m = b.max()
  if m == 0: return 0
  for i in range(len(b)): b[i] /= m
  return b

def rms(a):       # not std which is relative to the mean
  summ = 0.0
  for x in a: summ += x*x
  return math.sqrt(summ/len(a))

def make_dada_index(stand1, stand2):	# Ordering of stands is taken care of so they can be input in any order
  if stand2 > stand1:
    return stand2*(stand2+1)//2+stand1
  else: return stand1*(stand1+1)//2+stand2

def gauss(x, *p):
  A, mu, sigma = p

  if mu < 0 or mu >= len(x): return x*0
  else: return A*np.exp(-(x-mu)**2/(2.0*sigma**2))

def score(data, dump, stand, flagged):
  
  PADDED_LENGTH = 512
  c_vis = [ complex(0, 0) for i in range(PADDED_LENGTH) ]		# Padded with zeroes. The channels.
  ok_stands = [ ]
  for other_stand in range(NUM_STANDS):
    dada_index = make_dada_index(stand, other_stand)
    effts = []   					# ffts of the vis for 2 pols		
    pol_score = [ [0,0,0,0,0,0], [0,0,0,0,0,0] ]     	# 6 values for pols 0,0 and 1,1
    for pol in [0, 1]:
      for j in range(NUM_CHANNELS):
        c_vis[j] = complex(data[dump][j][dada_index][pol][pol].real, data[dump][j][dada_index][pol][pol].imag)   # includes amplitude
	

      efft = abs(np.fft.fft(c_vis))
      max_efft = efft.max()
      if max_efft > 0: efft /= max_efft

      peak_left = np.max(efft[:PADDED_LENGTH/2])
      peak_right = np.max(efft[PADDED_LENGTH/2:])

      if peak_left < peak_right:	# When the ramp is backwards
        tmpp = peak_right
        peak_right = peak_left
        peak_left = tmpp
        efft = efft[::-1]	# flip, so biggest peak is nearest 0

      effts.append(efft)

      # Peak ratio, noise, width
      if peak_left > 0: 
        pol_score[pol][0] = rms(efft[100:400])/peak_left
        pol_score[pol][1] = peak_right/peak_left

        # Second score is about width
        # Find where the peak goes over half from the left
        section = efft[:PADDED_LENGTH/2]
        i = 0
        while section[i] < peak_left/2: i += 1
        start_peak = i

        # Find where the peak goes over half from the right
        i = len(section)-1
        while section[i] < peak_left/2: i -= 1
        end_peak = i

	# Calculate width
        fwhm = end_peak-start_peak
        if fwhm == 0: fwhm = 0.5
        pol_score[pol][2] = float(fwhm)

      # Fit polynomial to amplitudes
      vis = np.zeros(NUM_CHANNELS)
      x = np.zeros(NUM_CHANNELS)
      for j in range(NUM_CHANNELS):
        x[j] = j
        vis[j] = abs(data[dump][j][dada_index][pol][pol]) 

      if np.mean(vis) > 0: vis /= np.mean(vis)   # Normalize - do this better

      coeff = np.polyfit(x,vis,5)
      p = np.poly1d(coeff)

      diff = np.zeros(len(vis))
      for j in range(len(vis)): diff[j] = vis[j]-p(j)
      pol_score[pol][3] = rms(diff)

      # Now fit a straight line to the polynomial and see how well that fits. Measure of waviness. Wavieness is not a bad thing - don't use this.
      y = np.zeros(NUM_CHANNELS)
      for j in range(NUM_CHANNELS): y[j] = p(j)
      x = np.array([ i for i in range(NUM_CHANNELS) ])
      slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
      pol_score[pol][4] = std_err

      # Fit line to phase. Not normalising the data because it's phase unwrapped. Phase line error. Is the phase a straight line.
      y = np.zeros(NUM_CHANNELS)
      for j in range(NUM_CHANNELS): y[j] = cmath.phase(complex(data[dump][j][dada_index][pol][pol].real, data[dump][j][dada_index][pol][pol].imag))
      y = unwrap_phase(y)
      x = np.array([ i for i in range(NUM_CHANNELS) ])
      slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
      pol_score[pol][5] = std_err
      
    
    closeness = rms(effts[0]-effts[1])

      
    if other_stand not in flagged:
      ok_stands.append((stand, other_stand, pol_score[0][0], pol_score[1][0], pol_score[0][1], pol_score[1][1], pol_score[0][2], pol_score[1][2], pol_score[0][3], pol_score[1][3], pol_score[0][4], pol_score[1][4], pol_score[0][5], pol_score[1][5], closeness))


					
  
  return ok_stands

filesize = os.path.getsize(sys.argv[1]) - DADA_HEADER_SIZE 

f = open(sys.argv[1],"rb")
header = parse_dada_header(f.read(DADA_HEADER_SIZE))
nchan  = header['NCHAN']
nant   = header['NSTATION']
npol   = header['NPOL']
navg   = header['NAVG']
bps    = header['BYTES_PER_SECOND']
df     = header['BW']*1e6 / float(nchan)
cfreq  = header['CFREQ']*1e6
utc_start = header['UTC_START']
assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
freq0 = cfreq-nchan/2*df
freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
print "bytes per sec:", bps
print "navg:     ", navg
print "nchan:    ", nchan
print "npol:     ", npol
print "nstation: ", nant
nbaseline = nant*(nant+1)//2
print "nbaseline:", nbaseline
noutrig_per_full = int(navg / df + 0.5)
print "noutrig_per_full:", noutrig_per_full
full_framesize   = nchan*nbaseline*npol*npol
print "full_framesize:  ", full_framesize*8
outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
print "outrig_framesize:", outrig_framesize*8
tot_framesize    = (full_framesize + outrig_framesize)
print " tot_framesize:  ",  tot_framesize*8
ntime = float(filesize) / (tot_framesize*8)
print "ntime:", ntime
frame_secs = int(navg / df + 0.5)
print "Frame secs:", frame_secs
time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
print "Time offset:", time_offset

file_contents = [ [] for i in range(int(ntime)) ]

for t in xrange(int(ntime)):
  print "Time ",t
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
  full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
  outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))

  file_contents[t] = full_data

f.close()



stand_scores = [ 0 for i in range(NUM_STANDS) ]

flags = []
if os.path.exists("flagged_tiles.txt"):
  flags = np.loadtxt("flagged_tiles.txt")
  print "Loaded flags"


all_scores = []
print "Stands:",
for stand in range(NUM_STANDS):
  if stand not in flags:
    all_scores += score(file_contents, 0, stand, flags)
    print stand,
    sys.stdout.flush()
print

print "Rearranging tables"

middle_rms_noise = np.zeros(len(all_scores)*2)	# Big is bad, means a lot of RMS
peak_ratio = np.zeros(len(all_scores)*2)	# Big is bad, mean peaks are the same
peak_width = np.zeros(len(all_scores)*2)        # Big is bad, peak is wide
amplitude_noise = np.zeros(len(all_scores)*2)   # All bad if big
amplitude_waviness = np.zeros(len(all_scores)*2)
phase_line_error = np.zeros(len(all_scores)*2)
closeness = np.zeros(len(all_scores))

for i in range(len(all_scores)):
  middle_rms_noise[i*2] = all_scores[i][2]
  middle_rms_noise[i*2+1] = all_scores[i][3]
  peak_ratio[i*2] = all_scores[i][4]
  peak_ratio[i*2+1] = all_scores[i][5]
  peak_width[i*2] = all_scores[i][6]
  peak_width[i*2+1] = all_scores[i][7]
  amplitude_noise[i*2] = all_scores[i][8]
  amplitude_noise[i*2+1] = all_scores[i][9]
  amplitude_waviness[i*2] = all_scores[i][10]
  amplitude_waviness[i*2+1] = all_scores[i][11]
  phase_line_error[i*2] = all_scores[i][12]
  phase_line_error[i*2+1] = all_scores[i][13]
  closeness[i] = all_scores[i][14]

print_stats(middle_rms_noise)
print_stats(peak_ratio)
print_stats(peak_width)
print_stats(amplitude_noise)
print_stats(amplitude_waviness)
print_stats(phase_line_error)
print_stats(closeness)

np.savetxt("A.txt",middle_rms_noise,fmt="%f")
np.savetxt("B.txt",peak_ratio,fmt="%f")
np.savetxt("C.txt",peak_width,fmt="%f")
np.savetxt("D.txt",amplitude_noise,fmt="%f")
np.savetxt("E.txt",amplitude_waviness,fmt="%f")
np.savetxt("F.txt",phase_line_error,fmt="%f")
np.savetxt("G.txt",closeness,fmt="%f")

middle_rms_noise_threshold = 0.05
peak_ratio_threshold = 0.8
for sc in all_scores:
  if sc[2] > middle_rms_noise_threshold or sc[3] > middle_rms_noise_threshold or sc[4] > peak_ratio_threshold or sc[5] > peak_ratio_threshold: print sc[0], sc[1]

      #print "A",sc[2]     # cut > 0.5, peak 0.06
      #print "A",sc[6]
      ##print "B",sc[3]     # two peaks at 0.2 0.92
      #print "B",sc[7]
      #print "C",sc[4]     # peak 0.02, cut >0.1
      #print "C",sc[8]
      #print "D",sc[5]    
      #print "D",sc[9]     # peak 0.02, cut >0.1

      #print "E",sc[10]    # peak 0.04, cut 0.5



