import pyfits, shutil, os, numpy, sys, string, scipy.stats

# Routines for bandpass score and calibration score

def do_line_stat(numbers):
  j = 1
  mean_amplitude = 0
  num = 0
  while j < len(numbers):
    mean_amplitude += numbers[j]
    num += 1
    j += 2	# skip phase

  return mean_amplitude/num


def obtain_bandpass_score():
  file_name = "BandpassCalibration_node001.dat"

  # open the tle file
  try:
    fid = open(file_name, 'r')
  except IOError:
    print 'Can\'t open file \'' + bp_file + '\' for reading.'
    sys.exit(0)

  # read the file into an array
  lines = fid.readlines()

  # check that something was read in
  if len(lines)==0:
    print "Error reading bandpass file: no lines read from the file."
    sys.exit(0)
  elif len(lines)%8!=1:
    print "Error reading bandpass file: number of lines should be 1 plus a multiple of 8."
    sys.exit(0)

  # close the file
  fid.close()

  # initialise the body list
  PX_lsq = []
  PY_lsq = []
  QX_lsq = []
  QY_lsq = []
  PX_fit = []
  PY_fit = []
  QX_fit = []
  QY_fit = []

  tmp1 = string.split( lines[0], ',' )
  tmp2 = []
  for val in tmp1:
    tmp2.append( float(val) )
  N_ch = len(tmp2)

  freq = numpy.zeros(N_ch)
  for k in range(0,N_ch):
    freq[k] = tmp2[k]

  ch = numpy.zeros(N_ch)
  for k in range(0,N_ch):
    ch[k] = freq[k]/0.04

  freq_idx = numpy.argsort(freq)

  for lineIndex in range(1, len(lines), 8):  # get 0, 8, 16, ...

    tmp = string.split( lines[lineIndex+0], ',' ); PX_lsq.append([]); PX_lsq[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PX_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+1], ',' ); PX_fit.append([]); PX_fit[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PX_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+2], ',' ); PY_lsq.append([]); PY_lsq[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PY_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+3], ',' ); PY_fit.append([]); PY_fit[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PY_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+4], ',' ); QX_lsq.append([]); QX_lsq[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QX_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+5], ',' ); QX_fit.append([]); QX_fit[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QX_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+6], ',' ); QY_lsq.append([]); QY_lsq[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QY_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+7], ',' ); QY_fit.append([]); QY_fit[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QY_fit[-1][k] = float(tmp[k])

  N_ant = len(PX_lsq)

  print "Stats for Jones matrices for %d frequency channels and %d antennas" % (N_ch, N_ant)

  vals = []
  for i in range(len(PX_fit)):
    mean_amp = do_line_stat(PX_fit[i])
    vals.append(mean_amp)
  px_range = numpy.max(vals)-numpy.min(vals)

  vals = []
  for i in range(len(QY_fit)):
    mean_amp = do_line_stat(QY_fit[i])
    vals.append(mean_amp)
  qy_range = numpy.max(vals)-numpy.min(vals)

  return px_range+qy_range

def get_complex_matrix(l):	# from line
  fields = l.split("{")
  matrix = fields[1].split("}")[0].split("\n")[0].split(",")
  for i in range(len(matrix)):
    matrix[i] = complex(matrix[i].replace(" ","").replace("i","j"))
  return matrix
 
def obtain_cal_score():
  for filename in os.listdir("."):
    if filename[-4:] == ".log": log_file = filename

  self_calibrated = []  
  model = []
  for line in open(log_file,"rU"):
    if line.find("self-calibrated dft") >= 0: 
      if line.find("nan") >= 0: return -1
      flux = get_complex_matrix(line)
      self_calibrated.append(flux)
    if line.find("  visibility model dft  ") >= 0: 
      if line.find("nan") >= 0: return -1
      flux = get_complex_matrix(line)
      model.append(flux)

  if len(self_calibrated) != len(model):
    print "Error: not the same number of calibration values"
    sys.exit(0)

  ratio_sum = 0.0
  num = 0
  for i in range(len(model)):
    for j in [ 0, 3 ]:
      ratio_sum += abs(self_calibrated[i][j]/model[i][j]-1)	# ratio should be near 1, so get residual
      print self_calibrated[i][j],model[i][j]
      num += 1

  return ratio_sum/num	

def test_vis_scale(vis_scale):
  print vis_scale,"-------------------"

  outfile = open("x.in","w")
  for line in open("cuwarp.in","rU"):
    if line.strip()[:9] == "VisScale=": outfile.write("VisScale="+str(vis_scale)+"\n")
    else: outfile.write(line)
  outfile.close()

  shutil.copyfile("x.in","cuwarp.in")

  os.system("rm *.fits *.jpg *.log Bandpass*; ../rts_node_cpu cuwarp.in")

  cal_score = obtain_cal_score()
  bandpass_score = obtain_bandpass_score()

  os.system("mkdir scale_"+str(vis_scale)+"; sh /home/hgarsden/bin/panel; mv panel.html *.jpg scale_"+str(vis_scale))

  return cal_score, bandpass_score

if not os.access("cuwarp.in",os.R_OK):
  print "cuwarp.in missing"
  sys.exit(1)

html_file = open("vis_scale_tests.html","w")
html_file.write("<head>\n<title>Vis Scale Tests</title></head>\n<body>\n\n")
scales = [ 10000, 1000, 10, 5, 1, 0.5, 0.1, 0.05, 0.001, 0.00001 ]
for scale in scales:
  cal_score, bandpass_score = test_vis_scale(scale)

  html_file.write("<h2>VisScale "+str(scale)+"</h2>\n")
  html_file.write("Cal score: "+str(cal_score)+"<p>\n")
  html_file.write("Bandpass score: "+str(bandpass_score)+"<p>\n")

  for f in sorted(os.listdir("scale_"+str(scale))):
    if f[-4:] == ".jpg" and f.find("XX") > -1: html_file.write("<img src=scale_"+str(scale)+"/"+f+" width=200>\n")
  html_file.flush()


html_file.write("</body></html>\n")
html_file.close()

