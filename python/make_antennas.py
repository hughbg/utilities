# Load a JSON file containing antenna locations and cable layout, and generate input files for
# corr2uvfits and other programs to use.

import json, math

JSON_FILE = "lwa_ovro.telescope.json"

# Load the data

data_file = open(JSON_FILE)
data_json = json.load(data_file)

datestamp = data_json["coords"]["local"]["__attrs__"]["date_modified"]

# Create anntenna_locations.tpl file -----------
# Header first, then traverse the data and write it.

out = open("antenna_locations.tpl","w")
out.write("""# LEDA-512 Antenna Positions AS BUILT
#
# https://docs.google.com/spreadsheet/ccc?key=0ApcypIpF5JopdFF6S2VvZWdaREtxd3dVb3MySTc5b3c&usp=sharing
#
# lines beginning with '#' and blank lines are ignored. Do not leave spaces in empty lines.            
# locations of antennas relative to the centre of the array in local topocentric            
# "east", "north", "height". Units are meters.            
# Format: Antenna_name east north height            
# antenna names must be 8 chars or less            
# fields are separated by white space.\n""")
out.write("#\n# Last updated "+datestamp+" from file "+JSON_FILE+"\n#\n# StandID    East    North    Elev\n") 

data_index = 0				
coords = data_json["coords"]["local"]["__data__"]		
while data_index < len(coords):
	out.write( "%s %10s %10s %10s\n" % 
	( "Stand"+str(int(coords[data_index+0])),
	coords[data_index+1],coords[data_index+2],
	coords[data_index+3] ) )
	data_index += 4			# This matrix is flattened so we need to step through it in rows

out.close()


# Create instr_config.tpl file -----------------

pol = [ "", "X", "Y" ]     # Map 1,2 -> X,Y
antennas = [ "" for i in range(512) ]
SPEED_OF_LIGHT = 299792458

def el_delay(delay, dispersion):
	REF_FREQ = 55 	# Need a frequency, in MHz, for calculating dispersion.
			# This is a flaw because it should be the frequency actually being operated on
			# for calibration or whatever.
	delay += dispersion/math.sqrt(REF_FREQ)
	return "EL_"+( "%.2f" % ( float(delay)*1e-9*SPEED_OF_LIGHT ) )


# Store the values in array so they can be re-ordered by input. 
# The re-order is not necessary ifor coor2uvfitsbut makes it easier for a human
# to read. The mapping is changed to input->antennas (columns swapped)
data_index = 0
cable_inputs = data_json["inputs"]["__data__"]
while data_index < len(cable_inputs):	# input columns are: stand pol input flag area delay dispersion
	antennas[int(cable_inputs[data_index+2])-1] = \
		( "%10d %6s %12s %6s" % ( 
		int(cable_inputs[data_index+0])-1,
		pol[int(cable_inputs[data_index+1])],
		el_delay(cable_inputs[data_index+5], cable_inputs[data_index+6]),
		cable_inputs[data_index+3] ) )
	data_index += 7			# step though it in chunks


# Data is in an array - dump it
out = open("instr_config.tpl","w")
out.write("# this file maps inputs into the receiver/correlator to antennas and polarisations.\n")
out.write("# in addition, a cable length delta (in meters) can be specified\n")
out.write("# the first column is not actually used by the uvfits writer, but is there as\n")
out.write("# an aide to human readers. Inputs are ordered from 0 to n_inp-1\n")
out.write("# lines beginning with '\#' and blank lines are ignored. Do not leave spaces in empty lines.\n")
out.write("#\n")
out.write("# Input flagging: put a 1 in the flag column to flag all data from that input.\n")
out.write("#                 0 means no flag. 2 means disconnected\n")
out.write("# Last updated "+datestamp+" from file "+JSON_FILE+"\n")
out.write("# input  antenna  pol   delta       flag\n")
for i in range(len(antennas)):
	out.write(str(i)+antennas[i]+"\n")
out.close()


