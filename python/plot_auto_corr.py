#!/usr/bin/env python
import numpy as np
import sys, os, cmath
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

DADA_HEADER_SIZE = 4096

outrig_nbaseline = 1270

def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header

filesize = os.path.getsize(sys.argv[1]) - DADA_HEADER_SIZE 

f = open(sys.argv[1],"rb")
header = parse_dada_header(f.read(DADA_HEADER_SIZE))
nchan  = header['NCHAN']
nant   = header['NSTATION']
npol   = header['NPOL']
navg   = header['NAVG']
bps    = header['BYTES_PER_SECOND']
df     = header['BW']*1e6 / float(nchan)
cfreq  = header['CFREQ']*1e6
utc_start = header['UTC_START']
assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
freq0 = cfreq-nchan/2*df
freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
print "bytes per sec:", bps
print "navg:     ", navg
print "nchan:    ", nchan
print "npol:     ", npol
print "nstation: ", nant
nbaseline = nant*(nant+1)//2
print "nbaseline:", nbaseline
noutrig_per_full = int(navg / df + 0.5)
print "noutrig_per_full:", noutrig_per_full
full_framesize   = nchan*nbaseline*npol*npol
print "full_framesize:  ", full_framesize*8
#print nbaseline, noutrig_per_full, outrig_nbaseline
outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
print "outrig_framesize:", outrig_framesize*8
tot_framesize    = (full_framesize + outrig_framesize)
print " tot_framesize:  ",  tot_framesize*8
ntime = float(filesize) / (tot_framesize*8)
print "ntime:", ntime
frame_secs = int(navg / df + 0.5)
print "Frame secs:", frame_secs
time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
print "Time offset:", time_offset

f.close()


stand = int(sys.argv[2])
use_flags = sys.argv[3]
if use_flags == "f": use_flags = True
else: use_flags = False
pol = int(sys.argv[4])

	   
dada_index = stand*(stand+1)/2+stand

flags = np.zeros((int(ntime), nchan))
for line in open("flagged.dat","r"):
  time, stand1, stand2, channel = line.split()
  time = int(time); channel = int(channel); stand1 = int(stand1); stand2 = int(stand2); 
  if stand == stand1 and stand1 == stand2: flags[time, channel] = 1

times = [ i for i in range(int(ntime)) ]
channels = [ i for i in range(nchan) ]
data = np.zeros((nchan, int(ntime)))


f = open(sys.argv[1],"rb")

f.read(DADA_HEADER_SIZE)

for t in xrange(int(ntime)):
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
  full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
  outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))
  
  # Get max/min of 0 and 1
  for i in range(nchan): 
    data[i, t] = abs(full_data[i, dada_index, 0, 0])
  min0 = data[:,t].min()
  max0 = data[:,t].max()
  for i in range(nchan): 
    data[i, t] = abs(full_data[i, dada_index, 1, 1])
  min1 = data[:,t].min()
  max1 = data[:,t].max()
  min_both = min([min0, min1])
  max_both = max([max0, max1])

  # Pol 0, 0 (DADA) is pol 0  in 0..3 numbering
  #   0, 1  is  2
  #   1, 0  is  3
  #   1, 1  is  1
  for i in range(nchan): 
    if flags[t, i] == 1 and use_flags: data[i, t] = None 
    else: data[i, t] = abs(full_data[i, dada_index, pol, pol])
    
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.azim = 18
ax.elev = 25


X, Y = np.meshgrid(times, channels)
ax.plot_wireframe(X,Y,data)
ax.set_xlabel('Time')
ax.set_ylabel('Channel')
ax.set_zlabel('Antenna amplitude')
ax.set_zlim(0.98*min_both, 1.02*max_both)

if use_flags: s = "Flagged"
else: s = "Unflagged"
plt.figtext(0.01,0.97,s+" Stand "+str(stand+1)+" Pols "+str(pol)+" "+str(pol))
plt.savefig(s+"_"+str(stand+1)+"_"+str(pol)+"_"+str(pol)+".jpg")
#plt.show()
plt.clf()

f.close()



