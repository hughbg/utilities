import math, cmath
import numpy as np

print "Experiment 1 -----------------"

c = 299792458.0
el = 30*math.pi/180

chan_width = 24e3
for distance in [40]: #range(1024):
  viss = np.zeros(8192, dtype=np.complex64)
  freqs = []
  i = 0
  freq = 47e6-2.616e6/2
  while freq < 47e6+2.616e6/2:
    freqs.append(freq)
    wavelength = c/freq
    dist = distance*math.cos(el)
    vis = complex(math.cos(2*math.pi/wavelength*dist), math.sin(2*math.pi/wavelength*dist))
    viss[i] = vis/abs(vis)
    i += 1
    freq += chan_width

  phases = np.unwrap([ cmath.phase(x) for x in viss[:len(freqs)] ])	# phase array not same length as FFT array
  #print max(phases)-min(phases)

  # Now delay calculation
  fft = abs(np.fft.fftshift(np.fft.fft(viss)))**2

  delay1 = dist/c									# Light travel time
  delay2 = (max(phases)-min(phases))/(max(freqs)-min(freqs))/(2*math.pi)		# From phase
  print "Exact location in FFT would be", delay2*len(fft)*(max(freqs)-min(freqs))/(float(len(freqs)-1)), "but appears as", np.argmax(fft)-len(fft)/2
  delay3 = (float(np.argmax(fft)-len(fft)/2)/len(fft))*(float(len(freqs)-1)/(max(freqs)-min(freqs)))	# From FFT. Note second term is 1/24e3
  print "X",delay1, delay2, delay3	# all should be the same
  print "Horizon delay", distance/c

# In a wave equation the phase is the argument of the cos/sin or is in the exponential e^i phase. Phase = kx-wt.
# If the phase is calculated at the same time t=0 then the phase is a linear function
# of the distance and frequency. The phase is 2 pi/lambda d,  d is the distance between them assuming horizontal wave. This is
# 2 pi f d /c = 2 pi f delay  where delay is travel time. As a function of frequency, the slope of the phase
# is 2 pi delay.


# This experiment fiddles with values and forces the delay to sit precisely on index 20
# of the FFT, which is now the same length as the data but only 108 channels. 
# It then shows that the delays calculated from the phase match up.
# Also uses chan_width explicitly.

print "\nExperiment 2 ----------------"
chan_width = 2*80132.06313
distance = 400
viss = np.zeros(108, dtype=np.complex64)
freqs = []
i = 0
freq = 47e6
while i < 108:
  freqs.append(freq)
  wavelength = c/freq
  dist = distance*math.cos(el)
  viss[i] = complex(math.cos(2*math.pi/wavelength*dist), math.sin(2*math.pi/wavelength*dist))
  i += 1
  freq += chan_width

phases = np.unwrap([ cmath.phase(x) for x in viss ])  
#print max(phases)-min(phases)

# Now delay calculation
fft = abs(np.fft.fftshift(np.fft.fft(viss)))**2

delay1 = dist/c                                                                       # Light travel time
delay2 = (max(phases)-min(phases))/(max(freqs)-min(freqs))/(2*math.pi)                # From phase
print "Exact location in FFT would be", delay2*len(fft)*chan_width, "and appears as", np.argmax(fft)-len(fft)/2
delay3 = (float(np.argmax(fft)-len(fft)/2)/len(fft))/chan_width       # From FFT. Note second term is 1/24e3
print "X",delay1, delay2, delay3      # all should be the same

