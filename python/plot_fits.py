import aplpy
import numpy
import sys
import string 
import math
import pyfits
import os


if len(sys.argv) == 2:
  set_min = None
  set_max = None
  contours = 0
  filename = sys.argv[1]
elif len(sys.argv) == 4:
  set_min = float(sys.argv[1])
  set_max = float(sys.argv[2])
  filename = sys.argv[3]

    
gc = aplpy.FITSFigure(filename)
if set_min == None: gc.show_colorscale(pmin=0,pmax=100)
else: gc.show_colorscale(vmin=set_min, vmax=set_max)
gc.add_colorbar()
gc.ticks.set_xspacing(30)
gc.ticks.set_length(10)
gc.ticks.set_color("black")
gc.ticks.show()


if filename[-5:] == ".fits": out_filename = filename[:-5]+".png"
else: out_filename = filename+".png"

gc.save(out_filename)

