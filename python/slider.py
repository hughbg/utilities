from pylab import *
from matplotlib.widgets import Slider
import numpy as np
import sys

NUM_BASELINES = 32896
NUM_CHANNELS = 109

flags = np.zeros((NUM_BASELINES, NUM_CHANNELS))


for line in open("flags.dat"):
  time, baseline, channel, flag = line.split()
  if time == "0": flags[int(baseline), int(channel)] = int(flag)
  

# Set up the layout

figure(figsize=(15,5))
xlabel('Baseline')
ylabel('Channel')
title('$Flags$')
grid(True)

ax = subplot(111)
subplots_adjust(left=0.1, bottom=0.2)

baselines = []
channels = []
for baseline in range(NUM_BASELINES):
  for channel in range(109):
    if flags[baseline][channel] == 1:
      baselines.append(baseline)
      channels.append(channel)
      
plot(baselines, channels, "r.")


ax.set_xlim(-1, 1028)
ax.set_ylim(-1, 110)


show()
