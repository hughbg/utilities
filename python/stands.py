import math

def sqr(x): return x*x

class Stands(object):
  locations = [ () for i in range(256) ]
  
  def __init__(self):
    for line in open("antenna_locations.txt", "rU"):
      if len(line) > 0 and line[0] != "#":
        stand, x, y, z = line.split() 
        stand = int(stand[5:])-1
        self.locations[stand] = ( float(x), float(y), float(z) )
        
  def distance(self, stand1, stand2):
    return math.sqrt(sqr(self.locations[stand1][0]-self.locations[stand2][0])+sqr(self.locations[stand1][1]-self.locations[stand2][1])+sqr(self.locations[stand1][2]-self.locations[stand2][2]))

