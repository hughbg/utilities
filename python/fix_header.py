import sys, os

SNAPSHOT_HEADER_SIZE = 2880	# Won't work if header goes over this
telescope_key = "TELESCOP= 'LWA-OVRO'                                                            "
frequency_keys = "CTYPE3  = 'FREQ    '                                                            CRVAL3  =   1.476806640625E+08                                                  CDELT3  =   1.464843750000E+05                                                  CRPIX3  =   1.000000000000E+00                                                  CUNIT3  = 'HZ      '                                                            "
history_key = "HISTORY = 'Fixed by H.G.'                                                       "
date_key = "DATE-OBS= '2010-06-01T11:15:59.272000'    /Date of observation                  TIMESYS = 'UTC     '           /Time system for HDU                             "
unit_key = "CUNIT1  = 'deg     '                                                            CUNIT2  = 'deg     '                                                            "      

def format_date(d): 
  return d[0:4]+"-"+d[4:6]+"-"+d[6:8]

def format_time(t): 
  return t[0:2]+":"+t[2:4]+":"+t[4:6] 
  
def set_frequency():
  global frequency_keys
  if os.path.isfile("header.txt"):
    for line in open("header.txt","rU"):
      fields = line.split()
      if len(fields) > 1 and fields[0] == "FREQCENT": freq = fields[1]
      if len(fields) > 1 and fields[0] == "BANDWIDTH": bandwidth = fields[1]
  else:
    print "Header file missing"
    sys.exit(1)
    
  place_to_insert = frequency_keys.find("CRVAL3")
  to_insert = "CRVAL3  =   "+str(float(freq)*1e6)+"         "
  lfreq = list(frequency_keys)
  for i in range(len(to_insert)): lfreq[place_to_insert+i] = to_insert[i]
  frequency_keys = "".join(lfreq)

  place_to_insert = frequency_keys.find("CDELT3")
  to_insert = "CDELT3  =   "+str(float(bandwidth)*1e6)+"         "
  lfreq = list(frequency_keys)
  for i in range(len(to_insert)): lfreq[place_to_insert+i] = to_insert[i]
  frequency_keys = "".join(lfreq)

	
def set_date_obs():
  global date_key
  if os.path.isfile("header.txt"):
    for line in open("header.txt","rU"):
      fields = line.split()
      if len(fields) > 1 and fields[0] == "DATE": date = fields[1]
      if len(fields) > 1 and fields[0] == "TIME": time = fields[1]
  else:
    print "Header file missing"
    sys.exit(1)
    
  place_to_insert = 0
  to_insert = "DATE-OBS= '"+format_date(date)+"T"+format_time(time)+"'          "
  ldate = list(date_key)
  for i in range(len(to_insert)): ldate[place_to_insert+i] = to_insert[i]
  date_key = "".join(ldate)



set_frequency()
set_date_obs()

f = open(sys.argv[1],"rb")
header = f.read(SNAPSHOT_HEADER_SIZE)

where_end = header.find(" END ")

header = list(header)
for i in range(len(frequency_keys)):
  header[where_end+i+1] = frequency_keys[i]

for i in range(len(telescope_key)):
  header[where_end+len(frequency_keys)+i+1] = telescope_key[i]
  
for i in range(len(history_key)):
  header[where_end+len(frequency_keys)+len(telescope_key)+i+1] = history_key[i]

for i in range(len(date_key)):
  header[where_end+len(frequency_keys)+len(telescope_key)+len(history_key)+i+1] = date_key[i]

for i in range(len(unit_key)):
  header[where_end+len(frequency_keys)+len(telescope_key)+len(history_key)+len(date_key)+i+1] = unit_key[i]
  
header[where_end+len(frequency_keys)+len(telescope_key)+len(history_key)+len(date_key)+len(unit_key)+1] = "E"
header[where_end+len(frequency_keys)+len(telescope_key)+len(history_key)+len(date_key)+len(unit_key)+2] = "N"
header[where_end+len(frequency_keys)+len(telescope_key)+len(history_key)+len(date_key)+len(unit_key)+3] = "D"



out = open(sys.argv[2],"wb")
out.write("".join(header))
done = False
while not done: 
  data = f.read()
  if len(data) > 0: 
    out.write(data)
  else: done = True
    


