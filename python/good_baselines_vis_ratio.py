# Printing coherency ratio into file ratios.txt from cuwarp. Then run this.
import numpy, sys
bl_indexes = numpy.zeros((256,256))

index = 0
for i in range(256):
  for j in range(i,256):
    bl_indexes[i][j] = index
    index += 1

ratios = numpy.loadtxt("ratios.txt")
coherencies = [ [] for i in range(32896) ]
for r in ratios:
  if r[3] != "nan" and r[4] != "nan" and r[3] != "inf" and r[4] != "inf" and r[3] != "-inf" and r[4] != "-inf" and r[5] != "nan" and r[6] != "nan" and r[5] != "inf" and r[6] != "inf" and r[5] != "-inf" and r[6] != "-inf":
    index = int(bl_indexes[int(r[1]), int(r[2])])
    coherencies[index].append(r) 


#index = int(bl_indexes[8][24])
#for r in coherencies[index]: print abs(complex(r[2],r[3]))
#sys.exit(0)



for i in range(256):
  for j in range(i,256):
    index = int(bl_indexes[i][j])
    if len(coherencies[index]) > 0:
      num_vals = len(coherencies[index])/2
      vals = []
      for k in range(num_vals): vals.append(0j)
      vals = numpy.array(vals)

      # Check for convergence
      for k in range(len(coherencies[index])-num_vals,len(coherencies[index])):
        vals[k-(len(coherencies[index])-num_vals)] = complex(coherencies[index][k][3],coherencies[index][k][4])+complex(coherencies[index][k][5],coherencies[index][k][6])
      mean = numpy.mean(vals)
      if mean != 0:
        for k in range(num_vals):
          vals[k] /= mean
        #print numpy.std(vals)
        if numpy.std(vals) > 1.2: print i, j
