import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy, sys, os, math

sky = numpy.loadtxt(sys.argv[1]).reshape(1792,1792)
sky_min = sky.min()

for i in range(1792):
  for j in range(1792):
    x = float(i)-1792.0/2
    y = float(j)-1792.0/2
    if  math.sqrt(x*x+y*y) > 570: sky[i,j] = float('nan')

for i in range(1792):
  for j in range(1792):
    sky[i, j] -= sky_min
    if sky[i, j] < 0: sky[i,j] = 0

sky_max = sky.max()

for i in range(1792):
  for j in range(1792):
    #sky[i, j] = math.sqrt(sky[i, j])
    if sky[i, j] > 0: sky[i,j] = math.log(sky[i, j])

plt.figure(figsize=(24,24))
plt.imshow(sky, interpolation='hamming')  #, vmin=math.sqrt(sky_max/100), vmax=math.sqrt(sky_max-sky_max/100))

plt.set_cmap("gist_heat")
plt.colorbar()
plt.savefig(sys.argv[1].replace("txt","jpg"))
