# This goes through the whole band and generates the list of evenly spaced channel frequencies which also run through the CFREQ frequencies (centre frequencies) specified in the DADA files.
# It prints CFREQ beside each centre frequency, and also prints the channel number of the frequency within the band it is in.
# Column 1 is channel number, col 2 is frequency.
frequency_vals = [ 31.308, 33.924, 36.54, 39.156, 41.772, 44.388, 47.004, 49.62, 52.236, 54.852, 57.468, 60.084, 62.7, 65.316, 67.932, 70.548, 73.164, 75.78, 78.396, 81.012, 83.628, 86.244 ] # CFREQ
frequency_vals = [28.680,31.296,33.912,36.528,39.144,41.760,44.376,46.992,49.608,52.224,54.840,57.456,60.072,62.688,65.304,67.920,70.536,73.152,75.768,78.384,81.000,83.616]

lower_freq = frequency_vals[0]
chan_sep = 0.024

def close(x, y): return abs(x-y) < 1e-5

def find_freq(f):
  for fr in frequency_vals:
    if close(fr,f): return True
  return False

print "<table cellpadding=4 border><tr><th>Sub-band index</th><th>Channel index from frequency 0</th><th>Channel index from band bottom</th><th>Channel index from sub-band bottom</th><th>Frequency</th><th>Is sub-band center frequency?</th></tr>"

band_bottom_freq = lower_freq+(-54)*chan_sep
index_from_0 = int(band_bottom_freq/0.024)
#for i in range(index_from_0):
#  #print i, "-", "-", "-"
#  print "<tr><td>", i, "</td><td>-</td><td>-</td><td>-</td>-</td></tr>"

chan = 0
subband_index = 0
for i in range(22*109):
  frequency = lower_freq+(i-54)*chan_sep
  #print index_from_0+i, i, chan, frequency,
  print "<tr><td>", subband_index, "</td><td>", index_from_0+i, "</td><td>", i, "</td><td>", chan, "</td><td>", frequency, "</td>"
  if find_freq(frequency): 
    #print "CFREQ"
    print "<td>YES</td></tr>" 
  else:
    #print 
    print "<td></td></tr>"
  chan += 1
  if chan == 109: 
    chan = 0
    subband_index += 1
print "</table>"
