import os

cuwarp_stations = [ False for i in range(256) ] 
cuwarp_channels = [ False for i in range(109) ]
corr2uvfits_stations = [ [0, 0] for i in range(256) ]

if os.path.exists("/home/hgarsden/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702/flagged_tiles.txt"):
  print "Examining","/home/hgarsden/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702/flagged_tiles.txt"
  for line in open("/home/hgarsden/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702/flagged_tiles.txt"):
    cuwarp_stations[int(line[:-1])] = True
if os.path.exists("/home/hgarsden/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702/flagged_channels.txt"):
  print "Examining","/home/hgarsden/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702/flagged_channels.txt"
  for line in open("/home/hgarsden/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702/flagged_channels.txt"):
    cuwarp_channels[int(line[:-1])] = True

if os.path.exists("/home/hgarsden/interfits/lconverter/template/instr_config.tpl"):
  print "Examining","/home/hgarsden/interfits/lconverter/template/instr_config.tpl"
  for line in open("/home/hgarsden/interfits/lconverter/template/instr_config.tpl"):
    l = line.split()
    if line[0] != "#" and len(l) == 5 and l[4] != "0": 
      if l[2] == "X": corr2uvfits_stations[int(l[1])][0] = int(l[4])
      elif l[2] == "Y": corr2uvfits_stations[int(l[1])][1] = int(l[4])
      print line


print "\nWHAT FLAGGED\nStations (numbering begins at 1):"
for i in range(256):
  if cuwarp_stations[i] or corr2uvfits_stations[i][0] != 0 or corr2uvfits_stations[i][1] != 0:
    print i+1,
    if cuwarp_stations[i]: print "(flagged_tiles)"
    else: print 


  if corr2uvfits_stations[i][0] != 0: 
    print "  X (instr_config)",
    if corr2uvfits_stations[i][0] == 1: print 
    else: print "disconnected"
  if corr2uvfits_stations[i][1] != 0: 
    print "  Y (instr_config)",
    if corr2uvfits_stations[i][1] == 1: print 
    else: print "disconnected"

print "\nChannels (numbering begins at 1):"
for i in range(109):
  if cuwarp_channels[i]: print i+1
