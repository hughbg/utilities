import  numpy as np

data = [ complex(1,2), complex(-1,3), complex(4,0), complex(1,1), complex(2, 0), complex(-2,1), complex(3,2), complex(0,1) ]
data = [ complex(-1.28691e+06, 728064.0), complex(-1.36858e+06, -365824.0), complex(-1.43821e+06, 1.03347e+06), complex(-1.58054e+06, 499712.0), complex(-1.48966e+06, 866048.0), complex(-1.92819e+06, 1.03091e+06), complex(-1.78893e+06, 1.34656e+06), complex(-1.14176e+06, 1.17094e+06), complex(-886528.0, 956928.0), complex(-1.22701e+06, 857600.0), complex(-2.17984e+06, 1.08723e+06), complex(-846336.0, 589056.0), complex(-1.728e+06, 708864.0), complex(-1.74182e+06, 1.0647e+06), complex(-1.11232e+06, 1.11232e+06), complex(-2.20058e+06, 657920.0), complex(-1.16224e+06, 333824.0), complex(-1.46509e+06, 348928.0), complex(-2.46221e+06, 1.70624e+06), complex(-1.3417e+06, 994816.0), complex(-1.43155e+06, 1.27488e+06), complex(-1.79994e+06, 265984.0), complex(-1.41517e+06, 408064.0), complex(-1.57466e+06, -100864.0), complex(-1.23469e+06, 1.2672e+06), complex(-2.64038e+06, -719872.0), complex(-4.49974e+07, -1.07463e+08), complex(-2.56e+06, -56064.0), complex(-1.65043e+06, 581632.0), complex(-780032.0, 467456.0), complex(-1.41235e+06, 860160.0), complex(-2.08307e+06, 6656.0), complex(-855296.0, 363008.0), complex(-1.47942e+06, 218112.0), complex(-1.11667e+06, 164864.0), complex(-1.64224e+06, -181248.0), complex(-1.41696e+06, -704768.0), complex(-2.42867e+06, 459264.0), complex(-1.72339e+06, 301056.0), complex(-1.11846e+06, 134912.0), complex(-883968.0, 513280.0), complex(-1.92512e+06, 600320.0), complex(-1.91974e+06, 51456.0), complex(-2.05466e+06, 270592.0), complex(-2.04877e+06, -1536.0), complex(-1.89286e+06, 442368.0), complex(-1.60666e+06, 399360.0), complex(-1.60307e+06, 345600.0), complex(-1.87213e+06, -352512.0), complex(-1.46278e+06, 732672.0), complex(-1.79917e+06, -96000.0), complex(-1.34144e+06, -18688.0), complex(-2.06899e+06, 163584.0), complex(-2.07514e+06, 685312.0), complex(-1.55878e+06, 316416.0), complex(-1.62842e+06, -80640.0), complex(-2.10458e+06, -624896.0), complex(-1.72211e+06, -82176.0), complex(-1.35168e+06, -691200.0), complex(-1.88467e+06, 157952.0), complex(-1.82426e+06, -217856.0), complex(-1.23341e+06, 115200.0), complex(-1.504e+06, -44032.0), complex(-1.73338e+06, 62464.0), complex(-2.2633e+06, 113152.0), complex(-2.14605e+06, -263168.0), complex(-1.97146e+06, 454912.0), complex(-2.25869e+06, -248320.0), complex(-1.45843e+06, -734720.0), complex(-2.54413e+06, -268032.0), complex(-2.05926e+06, 59136.0), complex(-2.3849e+06, -218880.0), complex(-1.62586e+06, 111104.0), complex(-1.71827e+06, -185600.0), complex(-1.9392e+06, -332288.0), complex(-2.48986e+06, -1.1392e+06), complex(-2.50086e+06, -781312.0), complex(-1.65402e+06, -1.24109e+06), complex(-1.70752e+06, -977664.0), complex(-1.55264e+06, -486400.0), complex(-2.1847e+06, -674560.0), complex(-1.33325e+06, -764160.0), complex(-1.38317e+06, -893696.0), complex(-1.68346e+06, -878592.0), complex(-1.77971e+06, -619008.0), complex(-927744.0, -583936.0), complex(-1.20218e+06, -913408.0), complex(-1.22291e+06, -591872.0), complex(-1.4743e+06, -929536.0), complex(-1.14432e+06, -358912.0), complex(-973312.0, -635136.0), complex(-451840.0, -553984.0), complex(-772608.0, -548096.0), complex(-197376.0, -323840.0), complex(-482560.0, -328192.0), complex(-660736.0, 101120.0), complex(225792.0, -1.15917e+06), complex(58112.0, -191232.0), complex(-250112.0, -76800.0), complex(-83200.0, 232192.0), complex(-97280.0, -335616.0), complex(-229632.0, -643072.0), complex(-442112.0, -162304.0), complex(-847104.0, 249344.0), complex(-491520.0, -768.0), complex(-1.11565e+06, 1.07776e+06), complex(-1.15354e+06, 133120.0), complex(-980480.0, -82688.0), complex(-1.0601e+06, 582400.0) ]

import cmath
def compute_dft_complex(input):
	n = len(input)
	output = []
	for k in range(n):  # For each output element
		s = complex(0)
		for t in range(n):  # For each input element
                  if input[t] != 0j:
			angle = 2j * cmath.pi * t * k / n
			s += input[t] * cmath.exp(-angle)
		output.append(s)

	return output

def compute_dft_complex_fast(data):
	n = len(data)

        return [ np.sum(np.multiply(data, np.exp(-2j*cmath.pi *np.arange(n)*k/n))) for k in range(n) ]


def difference(c1,c2):
  return abs((c1-c2)/c2)


fft = (np.fft.fft(data))

#for c in fft: print ( "%.3f" % np.real(c) ), ( "%.3f" % np.imag(c) )

data[20] = 0j
data[89] = 0j
data[100] = 0j
data[50] = 0j
dft = compute_dft_complex_fast(data)


#for c in dft: print ( "%.3f" % np.real(c) ), ( "%.3f" % np.imag(c) )


rms = rms_mean = 0
for i in range(len(dft)): 
  print ( "%.3f" % np.real(fft[i]) ), ( "%.3f" % np.imag(fft[i]) ), ( "%.3f" % np.real(dft[i]) ), ( "%.3f" % np.imag(dft[i]) ), ( "%.3f" % abs(fft[i]-dft[i]) )
  rms += abs(fft[i]-dft[i])**2
  rms_mean += fft[i]**2

print "RMS", np.sqrt(rms/len(dft)), "RMS Mean val", np.sqrt(abs(rms_mean)/len(dft)), "Ratio", np.sqrt(rms/len(dft))/np.sqrt(abs(rms_mean)/len(dft))
  


"""

8.000 10.000
-1.586 1.000
-2.000 4.000
2.414 0.172
12.000 -2.000
-4.414 1.000
-6.000 -4.000
-0.414 5.828
----------
8.000 10.000
-0.414 5.828
-6.000 -4.000
-4.414 1.000
12.000 -2.000
2.414 0.172
-2.000 4.000
-1.586 1.000
"""
