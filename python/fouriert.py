from math import cos, sin, pi, pow
import numpy as np
from cmath import exp
import matplotlib.pyplot as plt

data = np.zeros(256, dtype=np.complex64)
for i in range(len(data)):
  print pow(2000/(2000+float(i*5)), 2)
  data[i] = len(data)/float(i+1)*0.001 #*complex(cos(2*pi*6*float(i)/len(data)), sin(2*pi*6*float(i)/len(data)))



fft = np.fft.fft(data)
plt.plot(np.real(fft))
plt.show();exit()


# Do a Fourier transform using the equation

# Define the data at high resolution
data = np.zeros(128, dtype=np.complex64)

x_0 = 0
x_N = len(data)			# x ranges from start to end
a = 6				# Data has equation e^(i k x)    
for i in range(len(data)):
  x = (x_N-x_0)*float(i)/len(data)+x_0
  rand = (np.random.random()-0.5)/100
  rand = 0
  data[i] = complex(cos(2*pi*(a+rand)*x), sin(2*pi*(a+rand)*x))


plt.plot(np.real(data))
plt.show(); exit()
# Now implement the fourier transform equation (integral) with various frequencies
"""
f = 0
while f < 10: 
  fsum = 0.0
  for i in range(len(data)):
    x = float(i)/len(data)
    dx = 1.0/len(data)
    fsum += data[i]*complex(cos(2*pi*x*f), -sin(2*pi*x*f))*dx
  #print f, fsum.real

  f += 0.01

"""
# Peak is going to be at frequency where 2*pi*f = 2*pi*k, or in this case, k
fft = np.fft.fftshift(np.fft.fft(data))
plt.plot(np.abs(fft))
plt.show()


fft = np.fft.fft(data)
print np.argmax(np.abs(fft)), a*(x_N-x_0)

