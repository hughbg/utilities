#!/usr/bin/env python
import numpy as np
import sys, os, cmath
from mpl_toolkits.mplot3d import Axes3D
import scipy.stats
import scipy.signal
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

DADA_HEADER_SIZE = 4096

def _cast_to_type(string):
        try: return int(string)
        except ValueError: pass
        try: return float(string)
        except ValueError: pass
        return string

def parse_dada_header(headerstr, cast_types=True):
        header = {}
        for line in headerstr.split('\n'):
                try:
                        key, value = line.split(None, 1)
                except ValueError:
                        break
                key = key.strip()
                value = value.strip()
                if cast_types:
                        value = _cast_to_type(value)
                header[key] = value
        return header

def sequence4_ok(a):
  sequences = []	# group into sequences of the same values True or False
  previous = not a[0]
  for i in range(len(a)):
    if a[i] != previous: 
      sequences.append([])
    sequences[-1].append(a[i])
    previous = a[i]

  if ( sequences[0][0] and len(sequences[0]) > 4 ) or ( not sequences[0][0] and len(sequences[0]) > 11 ) \
	or ( sequences[-1][0] and len(sequences[-1]) > 4 ) or ( not sequences[-1][0] and len(sequences[-1]) > 11 ):
    return False

  # The first/last can be ignored, but the center ones must have sequence of 4 true
  # Must be separated by 11
  for i in range(1, len(sequences)-1):
    if ( sequences[i][0] and len(sequences[i]) != 4 ) or ( not sequences[i][0] and len(sequences[i]) != 11 ):
      return False

  return True

outrig_ids = [252, 253, 254, 255, 256]
outrig_nbaseline = sum(outrig_ids) # TODO: This only works because the outriggers are the last antennas in the array
filesize = os.path.getsize(sys.argv[1]) - DADA_HEADER_SIZE
print "filesize:     ", filesize
 

f = open(sys.argv[1],"rb")
header = parse_dada_header(f.read(DADA_HEADER_SIZE))
nchan  = header['NCHAN']
nant   = header['NSTATION']
npol   = header['NPOL']
navg   = header['NAVG']
bps    = header['BYTES_PER_SECOND']
df     = header['BW']*1e6 / float(nchan)
cfreq  = header['CFREQ']*1e6
utc_start = header['UTC_START']
assert( header['DATA_ORDER'] == "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" )
freq0 = cfreq-nchan/2*df
freqs = np.linspace(freq0, freq0+(nchan-1)*df, nchan)
print "bytes per sec:", bps
print "navg:     ", navg
print "nchan:    ", nchan
print "npol:     ", npol
print "nstation: ", nant
nbaseline = nant*(nant+1)//2
print "nbaseline:", nbaseline
noutrig_per_full = int(navg / df + 0.5)
print "noutrig_per_full:", noutrig_per_full
full_framesize   = nchan*nbaseline*npol*npol
print "full_framesize:  ", full_framesize*8
#print nbaseline, noutrig_per_full, outrig_nbaseline
outrig_framesize = noutrig_per_full*nchan*outrig_nbaseline*npol*npol
print "outrig_framesize:", outrig_framesize*8
tot_framesize    = (full_framesize + outrig_framesize)
print " tot_framesize:  ",  tot_framesize*8
ntime = float(filesize) / (tot_framesize*8)
print "ntime:", ntime
frame_secs = int(navg / df + 0.5)
print "Frame secs:", frame_secs
time_offset = float(header['OBS_OFFSET']) / (tot_framesize*8) * frame_secs
#time_offset = header['OBS_OFFSET'] / (full_framesize*8) * frame_secs
print "Time offset:", time_offset

# Find the state which has the sky

yX = np.zeros(int(ntime)*9)
yY = np.zeros(int(ntime)*9)

stand1 = 253	# reference antenna
stand2 = 10
bl_index = stand1*(stand1+1)//2+stand2 - 251*(251+1)//2

for t in xrange(int(ntime)):
  print "Time ",t
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
  outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))

  for sky_state in range(0,9):
    for j in range(nchan):
      yX[t*9+sky_state] += abs(outrig_data[sky_state][j][bl_index][0][0])/nchan    # Average over channels
    for j in range(nchan):
      yY[t*9+sky_state] += abs(outrig_data[sky_state][j][bl_index][1][1])/nchan
  
f.close()

if np.max(yX) > np.max(yY):
  chosen = yX
else: chosen = yY
factor = 1
mask = np.where(chosen > max(chosen)*factor, True, False)
while not sequence4_ok(mask) and factor > 0:
  print factor
  factor -= 0.01
  mask  = np.where(chosen > max(chosen)*factor, True, False)

print "Choose", mask

# Now write the sky state into the main part

out_dada = open(sys.argv[2], "w")

f = open(sys.argv[1],"rb")
header = f.read(DADA_HEADER_SIZE)
out_dada.write(header)

for t in xrange(int(ntime)):
  print "Time ",t
  full_data   = np.fromfile(f, dtype=np.complex64, count=full_framesize)
  outrig_data = np.fromfile(f, dtype=np.complex64, count=outrig_framesize)
  full_data   =   full_data.reshape((nchan,nbaseline,npol,npol))
  outrig_data = outrig_data.reshape((noutrig_per_full,nchan,outrig_nbaseline,npol,npol))

  # Find out how many states we'll use in this dump, for scaling
  num = 0
  for sky_state in range(0,9):
    if mask[t*9+sky_state]: num += 1

  if num == 0:
    print "Warning: no outriggers for dump", int(t)
    scale = 1
  else: scale = 9.0/num
  
  for sky_state in range(0,9):
    if mask[t*9+sky_state]:
      print "Adding in sky_state index", t*9+sky_state
      for stand1 in range(251, nant):
        for stand2 in range(stand1+1):
          full_bl_index = stand1*(stand1+1)//2+stand2
          out_bl_index = full_bl_index - 251*(251+1)//2
          for ch in range(nchan):
            for p1 in range(2):
              for p2 in range(2):
                full_data[ch, full_bl_index, p1, p2] += outrig_data[sky_state, ch, out_bl_index, p1, p2]*scale

  full_data.tofile(out_dada)
  outrig_data.tofile(out_dada)
              
out_dada.close()
f.close()
