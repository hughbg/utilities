import pyfits, os, numpy, scipy.stats

best_stat = -1
where_best_stat = ""

def make_png(fname, image):
    num = 0

    # Create ppm first
    output_file = open(fname[:-5]+".ppm","w")
    output_file.write("P3\n")
    output_file.write(str(image.shape[0])+" "+str(image.shape[1])+"\n255\n")
    im_max = numpy.max(image)
    im_min = numpy.min(image)
    for i in range(image.shape[0]):
      for j in range(image.shape[1]):
        num += 1
        if num % ((image.shape[0]*image.shape[1])/10) == 0: print "Processed",num
        gray = str(int((image[image.shape[0]-i-1][j]-im_min)/(im_max-im_min)*255))
        output_file.write(gray+" "+gray+" "+gray+"\n")

    output_file.close()   

    # Convert to jpg
    os.system("convert   "+fname[:-5]+".ppm  "+fname[:-5]+".png")
    os.remove(fname[:-5]+".ppm")

def clip(image, limits):
  im_min = numpy.min(image)
  for i in range(image.shape[0]):
    for j in range(image.shape[1]):
      if image[i][j] < limits[1]: image[i][j] = limits[1]
      if limits[2] < image[i][j]: image[i][j] = limits[2]

for filename in sorted(os.listdir(".")):

  if filename[-5:] == ".fits":

    hdulist = pyfits.open(filename)
    im = hdulist[0].data
    #ImagMagik FITS has a third dimension
    if len(im.shape) == 3:
      tmp = im[0,::]
      im = tmp
      
    if numpy.isnan(numpy.min(im)): stat = -1
    else: 
      result = scipy.stats.sigmaclip(im, 2.4, 2.4)   # Found by experiment
      stat = numpy.percentile(im,99)/numpy.std(result[0])
      
    if stat > best_stat:
      best_stat = stat
      where_best_stat = filename

    hdulist.close()

    #clip(im, result)
    #make_png(filename, im)



print where_best_stat, best_stat

