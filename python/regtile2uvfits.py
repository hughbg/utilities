import os, sys, shutil



def is_dada(name):
  return name[-5:] == ".dada"

if len(sys.argv) != 3:
  print "Usage: regtile2uvfits <DADA file> <UVFits file"
  sys.exit(1)

if not os.path.isfile(sys.argv[1]):
  print "File doesn't exist"
  sys.exit(1)

if not is_dada(sys.argv[1]):
  print "File is not dada"
  sys.exit(1)

i = 0
for line in open(sys.argv[1],"rU"):
  l = line.split()
  if l[0] == "DATA_ORDER" and l[1] != "REG_TILE_TRIANGULAR_2x2":
    print "File is not REG_TILE format"
    sys.exit(1)
  i += 1
  if i > 30: break

file_name = os.path.basename(sys.argv[1])
if not os.path.exists(file_name):
  shutil.copyfile(sys.argv[1], file_name)
print "Make header"
os.system("python /home/leda/hgarsden/make_header.py "+file_name)
command = "./lconvert512OV "+file_name
print command
os.system(command)
# Only the header file and the L files are important for the input to corr2uvfits
command = "./corr2uvfits -a "+file_name+"_0.LA -c "+file_name+"_0.LC -o "+sys.argv[2]+" -H header.txt -I instr_config.txt -S antenna_locations.txt -l"
print command
os.system(command)
os.system("rm *.LA *.LC "+file_name)


