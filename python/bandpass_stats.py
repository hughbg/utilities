import numpy, sys, string
from operator import itemgetter

def do_line_stat(numbers):
  j = 1
  mean_amplitude = 0
  num = 0
  while j < len(numbers):
    mean_amplitude += numbers[j]
    num += 1
    j += 2	# skip phase

  return mean_amplitude/num

def array_stats(a):
  print "Mean:",( "%.2f" % numpy.mean(a)), "Stddev:", ( "%.2f" % (numpy.std(a)) ), "Median:", ( "%.2f" % numpy.median(a) ),
  print "Min:", ( "%.2f" % numpy.min(a) ), "Max:", ( "%.2f" % numpy.max(a) )

# open the tle file
try:
    fid = open(sys.argv[1], 'r')
except IOError:
    print 'Can\'t open file \'' + bp_file + '\' for reading.'
    sys.exit(0)

# read the file into an array
lines = fid.readlines()

# check that something was read in
if len(lines)==0:
    print "Error reading bandpass file: no lines read from the file."
    sys.exit(0)
elif len(lines)%8!=1:
    print "Error reading bandpass file: number of lines should be 1 plus a multiple of 8."
    sys.exit(0)

# close the file
fid.close()

# initialise the body list
PX_lsq = []
PY_lsq = []
QX_lsq = []
QY_lsq = []
PX_fit = []
PY_fit = []
QX_fit = []
QY_fit = []

tmp1 = string.split( lines[0], ',' )
tmp2 = []
for val in tmp1:
    tmp2.append( float(val) )
N_ch = len(tmp2)

freq = numpy.zeros(N_ch)
for k in range(0,N_ch):
    freq[k] = tmp2[k]


ch = numpy.zeros(N_ch)
for k in range(0,N_ch):
    ch[k] = freq[k]/0.04

freq_idx = numpy.argsort(freq)



for lineIndex in range(1, len(lines), 8):  # get 0, 8, 16, ...

    tmp = string.split( lines[lineIndex+0], ',' ); PX_lsq.append([]); PX_lsq[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PX_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+1], ',' ); PX_fit.append([]); PX_fit[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PX_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+2], ',' ); PY_lsq.append([]); PY_lsq[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PY_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+3], ',' ); PY_fit.append([]); PY_fit[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): PY_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+4], ',' ); QX_lsq.append([]); QX_lsq[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QX_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+5], ',' ); QX_fit.append([]); QX_fit[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QX_fit[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+6], ',' ); QY_lsq.append([]); QY_lsq[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QY_lsq[-1][k] = float(tmp[k])
    tmp = string.split( lines[lineIndex+7], ',' ); QY_fit.append([]); QY_fit[-1]=numpy.zeros(N_ch*2+1)
    for k in range(0,len(tmp)): QY_fit[-1][k] = float(tmp[k])

N_ant = len(PX_lsq)

print "Stats for Jones matrices for %d frequency channels and %d antennas" % (N_ch, N_ant)

limit = 12

orderings = []
vals = []
PX_fit_top_bottom = []
for i in range(len(PX_fit)):
  mean_amp = do_line_stat(PX_fit[i])
  vals.append(mean_amp)
  orderings.append((PX_fit[i][0], mean_amp))
print "PX fit"
array_stats(vals)
data = sorted(orderings,key=itemgetter(1))
print "Outliers",
for i in range(len(data)): 
  if i < limit or i >= len(data)-limit: 
    print ( "%3d" % int(data[i][0]))+"("+( "%.2f" % data[i][1] )+")",
    PX_fit_top_bottom.append(data[i][0])
  elif i == limit+1: print "...",
print

orderings = []
vals = []
QY_fit_top_bottom = []
for i in range(len(QY_fit)):
  mean_amp = do_line_stat(QY_fit[i])
  vals.append(mean_amp)
  orderings.append((QY_fit[i][0], mean_amp))
print "QY fit"
array_stats(vals)
data = sorted(orderings,key=itemgetter(1))
print "Outliers",
for i in range(len(data)): 
  if i < limit or i >= len(data)-limit: 
    print ( "%3d" % int(data[i][0]))+"("+( "%.2f" % data[i][1] )+")",
    QY_fit_top_bottom.append(data[i][0])
  elif i == limit+1: print "...",
print

print "Common stands",
for val in PX_fit_top_bottom:
  if val in QY_fit_top_bottom:
    print int(val),
print

print "All stand numbers are 1-based"

