import numpy as np
import cmath, sys
import matplotlib.pyplot as plt
import scipy.interpolate

NUM_POLS = 4
GRID_SIZE = 512


import conversions


class Grid(object):
  def __init__(self, dim1, dim2, left, right, bottom, top, dtype=np.float32):		# left, right ... is in physical co-ords
    if dim1 <= 0 or dim2 <= 0:
      print "Invalid dimension for grid"
      exit(1)
    self.grid = np.zeros((dim1, dim2), dtype=dtype)

    if right <= left or top <= bottom:
      print "Invalid physical extent for grid"
      exit(1)  
    self.left = left
    self.right = right
    self.bottom = bottom
    self.top = top

  def inc_value(self, x, y, value):
    if not (self.left <= x and x < self.right and self.bottom <= y and y < self.top):
      print "Attempt to add value to grid outside its range"
      print "Physical co-ords", x, y, "extent", self.left, self.right, self.bottom, self.top
      exit(1)

    i, j = self.physical2grid(x, y); 

    self.grid[i, j] += value

  def value_at(self, x, y):		# physical co-ords
    i, j = self.physical2grid(x, y)
    return self.grid[i, j]

  def set_at(self, x, y, val):             # physical co-ords
    i, j = self.physical2grid(x, y)
    self.grid[i, j] = val

  def set_grid_at(self, i, j, val):		# grid co-ords
    self.grid[i, j] = val

  def physical2grid(self, x, y):
    dim1 = self.grid.shape[0]
    dim2 = self.grid.shape[1]
    i = np.int(np.round(dim1*(float(x)-float(self.left))/(float(self.right)-float(self.left))))
    j = np.int(np.round(dim2*(float(y)-float(self.bottom))/(float(self.top)-float(self.bottom))))
    return i, j

  def grid2physical(self, i, j):
    dim1 = self.grid.shape[0]
    dim2 = self.grid.shape[1]
    
    x = (self.right-self.left)*(float(i)/float(dim1))+self.left
    y = (self.top-self.bottom)*(float(j)/float(dim2))+self.bottom
    return x, y

  def set_new_extent(self, left, right, bottom, top):
    self.left = left
    self.right = right
    self.bottom = bottom
    self.top = top

  def all_zero(self):
    return np.max(abs(self.grid)) == 0
 
  def weight(self, weights):

    if not (weights.grid.shape == self.grid.shape):
      print "Attempt to weight a grid with a grid of different size"
      exit(1)

    for i in range(self.grid.shape[0]):
      for j in range(self.grid.shape[1]):
        if weights.grid[i, j] != 0: self.grid[i, j] /= weights.grid[i, j]

  def clip_grid(self, left, right, bottom, top):			# left, right ... is in grid co-ords
    if right <= left or top <= bottom:
      print "Invalid extent for clipping"
      exit(1)   

    # First get new phys extent
    new_left, new_bottom = self.grid2physical(left, bottom)
    new_right, new_top = self.grid2physical(right, top)
    self.grid = self.grid[left:right, bottom:top]
    self.left = new_left
    self.right = new_right
    self.bottom = new_bottom
    self.top = new_top

  def bbox(self):
    rows = np.any(self.grid, axis=1)
    cols = np.any(self.grid, axis=0)
    
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]

    return rmin, rmax, cmin, cmax

  def auto_clip(self):		# Clip out edges where there is no data
    left, right, bottom, top = self.bbox() 
    self.clip_grid(left, right+1, bottom, top+1)

class BaselineVis(object):
  def __init__(self, vis_info, freq):       # Unpack
    self.st1 = int(vis_info[0])//256
    self.st2 = int(vis_info[0])%256
    self.uv_length = vis_info[1]
    self.pols = [ 0, 0, 0, 0 ]
    self.pols[0] = complex(vis_info[2], vis_info[3])
    self.pols[1] = complex(vis_info[4], vis_info[5])
    self.pols[2] = complex(vis_info[6], vis_info[7])
    self.pols[3] = complex(vis_info[8], vis_info[9])

    z21cm = conversions.f2z(freq/1e9)
    self.k_perp = self.uv_length*conversions.dk_du(z21cm)

    self.horizon_delay = self.uv_length/freq*1e9

  def printb(self):
    print self.st1, self.st2, self.uv_length, self.pols, self.k_perp, self.horizon_delay



class Delays(object):
  def __init__(self):
    self.fft_size = 8192
    self.grid_size = 512
    self.chan_width = 24e3


  def compute_dft_complex_fast(self, data):
    n = len(data)

    return [ np.sum(np.multiply(data, np.exp(-2j*cmath.pi *np.arange(n)*k/n))) for k in range(n) ]

  def dumb_interpolate(self, signal):
    sig = np.abs(signal)
    x = np.nonzero(sig)[0]
    if len(x) == len(signal): return
            
    y = np.real(signal)[x]
    fr = scipy.interpolate.interp1d(x, y)
    y = np.imag(signal)[x]
    fi = scipy.interpolate.interp1d(x, y)

    for i in range(x[0], x[-1]+1):
      if i not in x: signal[i] = complex(fr(i), fi(i))

    # Problem if there are zeros at beginning and end
    sig = np.abs(signal)
    x = np.nonzero(sig)[0]
    if len(x) == len(signal): return

    for i in range(x[0]-1, -1, -1):
      signal[i] = signal[i+1]-(signal[i+2]-signal[i+1])
    for i in range(x[-1]+1, len(signal)):
      signal[i] = signal[i-1]+(signal[i-1]-signal[i-2])

  def fft_index_to_delay(self, index):	# index is relative to centre of FFT (DC component) index 0 means fft_size/2 index in FFT
    return float(index)/self.fft_size/self.chan_width*1e9        # nano-sec

  def delay_to_fft_index(self, delay):	# index is relative to centre of FFT (DC component) index 0 means fft_size/2 index in FFT
					# delay in nanosec
    return int(np.round((delay*self.fft_size*self.chan_width)/1e9))

  def get_pols(self, info):
    pols = [ 0, 0, 0, 0 ]
    pols[0] = complex(info[2], info[3])
    pols[1] = complex(info[4], info[5])
    pols[2] = complex(info[6], info[7])
    pols[3] = complex(info[8], info[9])
    return pols

  def get_vis(self, fname):
    f = open(fname,"rb")
    self.num_baselines = np.fromfile(f, dtype=np.int32, count=1)
    self.num_channels = np.fromfile(f, dtype=np.int32, count=1)
    self.frequencies = np.fromfile(f, dtype=np.int32, count=self.num_channels)

    contiguous = True
    for i in range(1, len(self.frequencies)):
      if self.frequencies[i]-self.frequencies[i-1] != int(self.chan_width): contiguous = False
    if not contiguous: print "Frequencies are not contigiuous"

    print "Num baselines", self.num_baselines[0], "Num channels", self.num_channels[0]

    self.vis = np.fromfile(f, dtype=np.float32, count=self.num_channels*self.num_baselines*(2+NUM_POLS*2))     # The 2 is for uv length and stand info
    print "Shapes", self.vis.shape, (self.num_channels*self.num_baselines*(2+NUM_POLS*2))
    f.close()

    self.vis = self.vis.reshape((self.num_channels, self.num_baselines, 2+NUM_POLS*2))
    self.vis = np.transpose(self.vis, [1, 0, 2])         # by baseline first

    print "Done loading"


    
  def get_delays(self):
    max_uv_length = 0.0
    for i in range(self.num_baselines):
      for j in range(self.num_channels):
        uv_length = self.vis[i, j, 1]
        if uv_length > max_uv_length: max_uv_length = uv_length
        
    print "max_uv_length", max_uv_length

    self.delays = Grid(self.grid_size, self.fft_size/2, 0, max_uv_length*1.05, 0, self.fft_size/2, dtype=np.complex64)
    weights = Grid(self.grid_size, self.fft_size/2, 0, max_uv_length*1.05, 0, self.fft_size/2)
   
    for bl in range(self.num_baselines):
      if bl % 1000 == 0: print bl
      all_zero = True

      for ch in range(self.num_channels):
    
        pols = self.get_pols(self.vis[bl, ch])     
        for k in range(NUM_POLS):
          if abs(pols[k]) != 0: all_zero = False

      if not all_zero:        
        pol = 0
        band_vis = np.zeros(self.num_channels, dtype=np.complex64)

        for ch in range(self.num_channels):
          pols = self.get_pols(self.vis[bl, ch])
          band_vis[ch] = pols[pol]

        self.dumb_interpolate(band_vis)		# Fix flagged channels

        fft = np.fft.fftshift(np.fft.fft(np.append(band_vis, np.zeros(self.fft_size, dtype=np.complex64))[:self.fft_size]))    # Pad out
        #fft = np.fft.fftshift(compute_dft_complex_fast(band_vis))

        # Now fold in negative frequencies
        for ch in range(self.fft_size/2+1, self.fft_size): fft[ch] += fft[self.fft_size-ch]

        # Add power into grid
        for ch in range(self.fft_size/2+1, self.fft_size):   	# Ignores DC  
          uv_length = self.vis[bl, 0, 1]		# wrong
 
	  mode = ch-self.fft_size/2
          
          self.delays.inc_value(uv_length, mode, fft[ch]);

          weights.inc_value(uv_length, mode, 1)

    self.delays.weight(weights)
    self.delays.grid = np.abs(self.delays.grid)**2		# power


 
class PowerSpectrum(Delays):
  def __init__(self, fname):
    Delays.__init__(self, fname)

  def get_grid(self, fname): 

    f = open(fname,"rb")
    num_baselines = np.fromfile(f, dtype=np.int32, count=1)
    num_channels = np.fromfile(f, dtype=np.int32, count=1)
    frequencies = np.fromfile(f, dtype=np.int32, count=num_channels)

    contiguous = True
    for i in range(1, len(frequencies)):
      if frequencies[i]-frequencies[i-1] != int(self.chan_width): contiguous = False
    if not contiguous: print "Frequencies are not contigiuous"

    print "Num baselines", num_baselines[0], "Num channels", num_channels[0]

    vis = np.fromfile(f, dtype=np.float32, count=num_channels*num_baselines*(2+NUM_POLS*2))     # The 2 is for uv length and stand info
    print "Shapes", vis.shape, (num_channels*num_baselines*(2+NUM_POLS*2))
    f.close()

    vis = vis.reshape((num_channels, num_baselines, 2+NUM_POLS*2))
    vis = np.transpose(vis, [1, 0, 2])         # by baseline first
    
    
    max_k_perp = 0.0
    for i in range(num_baselines):
      for j in range(num_channels):
        vis_info = BaselineVis(vis[i, j], frequencies[j])
        if vis_info.k_perp > max_k_perp: max_k_perp = vis_info.k_perp
        
    print "Done loading"
    print "max_k_perp", max_k_perp

    
    num_dead = 0
    kk_space = Grid(self.grid_size, self.fft_size/2, 0, max_k_perp*1.05, 0, self.fft_size/2)     # initially kpar is just the modes indexes, i.e. fft indexes 0 ..
    weights = Grid(self.grid_size, self.fft_size/2, 0, max_k_perp*1.05, 0, self.fft_size/2)

    for i in range(num_baselines):
    
      all_zero = True

      vis_info = [ None for k in range(num_channels) ]
      for j in range(num_channels):
    
        vis_info[j] = BaselineVis(vis[i, j], frequencies[j])
     
        for k in range(NUM_POLS):
          if abs(vis_info[j].pols[k]) != 0: all_zero = False

      if not all_zero:    
        
        pol = 0
        band_vis = np.zeros(num_channels, dtype=np.complex64)

        for ch in range(num_channels):
          band_vis[ch] = vis_info[ch].pols[pol]

        self.dumb_interpolate(band_vis)		# Fix flagged channels

        fft = np.fft.fftshift(np.fft.fft(np.append(band_vis, np.zeros(self.fft_size, dtype=np.complex64))[:self.fft_size]))    # Pad out
        #fft = np.fft.fftshift(compute_dft_complex_fast(band_vis))

        # Now fold in negative frequencies
        for ch in range(self.fft_size/2+1, self.fft_size): fft[ch] += fft[self.fft_size-ch]
 
        # Add power into grid
        for ch in range(self.fft_size/2+1, self.fft_size):   	# Ignores DC  
          k_perp = vis_info[0].k_perp	# wrong
 
	  mode = ch-self.fft_size/2
          
          kk_space.inc_value(k_perp, mode, abs(fft[ch]*np.conj(fft[ch])));

          weights.inc_value(k_perp, mode, 1)

      else: num_dead += 1

    kk_space.weight(weights)

    # Now some clipping operations

    # Find out which delay index has delay of 2000 and clip
    self.max_delay = 2000
    max_delay_index = self.delay_to_fft_index(2000)     # what if this is outside the range
    kk_space.clip_grid(0, kk_space.grid.shape[0], 0, max_delay_index)

    print "Grid", 0, kk_space.grid.shape[0], 0, kk_space.grid.shape[1]
    print "Extent", kk_space.left, kk_space.right, kk_space.bottom, kk_space.top
    
    kk_space.auto_clip()
    print "Grid", 0, kk_space.grid.shape[0], 0, kk_space.grid.shape[1]
    print "Extent", kk_space.left, kk_space.right, kk_space.bottom, kk_space.top

    # Plot horizon line.  t = d/v = (uvl lambda)/v = uvl/f
    max_value = np.max(kk_space.grid)
    for i in range(kk_space.grid.shape[0]):
      k_perp, junk = kk_space.grid2physical(i, 0)
      z21cm = conversions.f2z(frequencies[0]/1e9)
      uv_length = k_perp/z21cm
      delay = uv_length/frequencies[0]*1e9		# nanosec
      delay_index = self.delay_to_fft_index(delay)
      kk_space.set_grid_at(i, delay_index, max_value)
   
    k_par_1 = conversions.k_par(frequencies[0]/1e9, self.fft_size, self.chan_width/1e9) # 2 pi/ longest wavelength, mode 1
    max_k_par = k_par_1*self.fft_size/2

    k_par_bottom = kk_space.bottom*k_par_1			# kk_space.bottom is the smallest fft_index
    k_par_top = kk_space.top*k_par_1
    kk_space.set_new_extent(kk_space.left, kk_space.right, k_par_bottom, k_par_top)

    print "Grid", 0, kk_space.grid.shape[0], 0, kk_space.grid.shape[1]
    print "Extent", kk_space.left, kk_space.right, kk_space.bottom, kk_space.top

    

    self.kk_space = kk_space
    self.num_dead = num_dead


def log_transform(im):
    '''returns log(image) scaled to the interval [0,1]'''
    try:
        (min, max) = (im[im > 0].min(), im.max())
        if (max > min) and (max > 0):
            return (np.log10(im.clip(min, max)) - np.log10(min)) / (np.log10(max) - np.log10(min))*np.log10(max)
    except:
        pass
    return im


delays = Delays()
delays.get_vis(sys.argv[1])
delays.get_delays()
grid = delays.delays.grid
grid = grid.transpose()	
grid = grid[::-1]
plt.imshow(log_transform(grid), aspect="auto")
plt.show()
exit()


ps = PowerSpectrum()
ps.get_vis(sys.argv[1])
ps.get_ps()
grid = ps.kk_space.grid

	
grid = grid.transpose()	
grid = grid[::-1]	# flip, put zero delay at the bottom visually
print grid.shape

"""
plt.xlabel("UV Length (wavelengths)")
plt.ylabel(grid,   extent=[ ps.kk_space.left,  ps.kk_space.right, ps.kk_space.bottom, ps.kk_space.top ], aspect="auto")
plt.colorbar()
plt.savefig("kk_space.png")
plt.clf()
"""
plt.xlabel("$k_{\perp} [ h\mathrm{ Mpc}^{-1} ]$", fontsize=15)
plt.ylabel("$k_{\parallel} [ h\mathrm{ Mpc}^{-1} ]$", fontsize=15)
plt.imshow(log_transform(grid), extent=[ ps.kk_space.left,  ps.kk_space.right, ps.kk_space.bottom, ps.kk_space.top], aspect="auto")
plt.colorbar()
plt.savefig("kk_space_log.png")

