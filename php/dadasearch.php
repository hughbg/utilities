<!-- Date 0-0-0 should be unknown -->

<html>
<head><title>Dada File Searcher</title></head>
<body>
<!-- <?php
echo $_POST["location"];
echo $_POST["date"];
echo $_POST["type"];
echo $_POST["freq"];
echo $_POST["size"];
?>  -->
<?php
if ( empty($_POST["root"]) ) {
  echo "No ROOT!</body></html>";
  exit(0);
}
?>
  
<h2>Search for DADA files in <?php echo $_POST["root"]?></h2>
List files with these properties:
<form method="post">
<table cellpadding=5>
<tr><th>Location</th><th>Date</th><th>Type</th><th>Frequency</th><th>Size</th><th># Time Scans</th></tr>
<?php
function select_option($name, $value) {
  $s = "<option value=\"".$value."\"";
  if ( $_POST[$name] == $value ) { $s = $s." selected"; }
  $s = $s.">".$value."</option>";
  return $s;
}

function get_location($dir) {
  $splitted = explode("/",$dir);
  return $splitted[3]."/".$splitted[4];
}

function snumber($num) {
  if ( intval($num)-$num == 0 ) return $num;
  else return sprintf("%.6f",$num);
}

$database = fopen("catalog.txt","r");
while ( $line = fgets($database,512) ) {
  $fields = explode(",",$line);
  for ($i=0; $i<7;++$i) { 
    $fields[$i] = trim($fields[$i]);
    if ( $fields[$i] == "" ) $fields[$i] = "UNKNOWN"; 
  }

  if ( $fields[6] == "UNKNOWN" ) $n_scans = $fields[6];
  else {
    if ( intval($fields[6])-$fields[6] == 0 ) $n_scans = (string)(intval($fields[6]));
    else $n_scans = "INVALID";
  } 

  $select = True;
  #if ( $fields[4] == 2396160 ) {
  #  echo $_POST["location"].":".$fields[0].":<br>";
  #  echo $_POST["date"].":".$fields[1].":<br>";
  #  echo $_POST["type"].":".$fields[2].":<br>";
  #  echo $_POST["freq"].":".$fields[3].":<br>";
  #  echo $_POST["size"].":".$fields[4].":<br>";
  #
  #}
  if ( substr($fields[0],0,strlen($_POST["root"])) != $_POST["root"] ) $select = False;
  if ( !( empty($_POST["location"]) or $_POST["location"] == "ALL" or get_location($$fields[0]) == $_POST["location"]) ) $select = False;
  if ( !( empty($_POST["date"]) or $_POST["date"] == "ALL" or $fields[1] == $_POST["date"]) ) $select = False;
  if ( !( empty($_POST["type"]) or $_POST["type"] == "ALL" or $fields[3] == $_POST["type"]) ) $select = False;
  if ( !( empty($_POST["freq"]) or $_POST["freq"] == "ALL" or $fields[4] == $_POST["freq"]) ) $select = False;
  if ( !( empty($_POST["size"]) or $_POST["size"] == "ALL" or $fields[5] == $_POST["size"]) ) $select = False;
  if ( !( empty($_POST["scans"]) or $_POST["scans"] == "ALL" or $n_scans == $_POST["scans"]) ) $select = False;

  if ( $select ) {
    $locations[] = get_location($fields[0]); $dates[] = $fields[1];
    $types[] = $fields[3]; $freqs[] = $fields[4]; $sizes[] = $fields[5];
    $scans[] = $n_scans;
  }
}
fclose($database);
echo "<tr><td>\n";
echo "<select name=\"location\">";
echo select_option("location","ALL");
$locations = array_unique($locations);
sort($locations);
foreach ( $locations as $location ) {
  echo select_option("location",$location);
}
echo "</select>";
echo "</td><td>\n";
echo "<select name=\"date\">";
echo select_option("date","ALL");
$dates = array_unique($dates);
sort($dates);
foreach ( $dates as $date )     # Do some re-ordering
  if ( $date == "UNKNOWN" ) $new_dates[] = "UNKNOWN";
foreach ( $dates as $date )     # Do some re-ordering
  if ( $date != "UNKNOWN" ) $new_dates[] = $date;
foreach ( $new_dates as $date ) {
  echo select_option("date",$date);
}
echo "</select>";
echo "</td><td>\n";
echo "<select name=\"type\">";
echo select_option("type","ALL");
$types = array_unique($types);
foreach ( $types as $type )     # Do some re-ordering
  if ( $type == "UNKNOWN" ) $new_types[] = "UNKNOWN";
foreach ( $types as $type )     # Do some re-ordering
  if ( $type != "UNKNOWN" ) $new_types[] = $type;
foreach ( $new_types as $type ) {
  echo select_option("type",$type);
}
echo "</select>";
echo "</td><td>\n";
echo "<select name=\"freq\">";
echo select_option("freq","ALL");
$freqs = array_unique($freqs);
sort($freqs,SORT_NUMERIC);
foreach ( $freqs as $freq )     # Do some re-ordering
  if ( $freq == "UNKNOWN" ) $new_freqs[] = "UNKNOWN";
foreach ( $freqs as $freq )     # Do some re-ordering
  if ( $freq != "UNKNOWN" ) $new_freqs[] = $freq;
foreach ( $freqs as $freq ) {
  echo select_option("freq",$freq);
}
echo "</select>";
echo "</td><td>\n";
echo "<select name=\"size\">";
echo select_option("size","ALL");
$sizes = array_unique($sizes);
sort($sizes,SORT_NUMERIC);
foreach ( $sizes as $size )     # Do some re-ordering
  if ( $size == "UNKNOWN" ) $new_sizes[] = "UNKNOWN";
foreach ( $sizes as $size)     # Do some re-ordering
  if ( $size != "UNKNOWN" ) $new_sizes[] = $size;
foreach ( $new_sizes as $size ) {
  echo select_option("size",$size);
}
echo "</select>";
echo "</td><td>\n";
echo "<select name=\"scans\">";
echo select_option("scans","ALL");
$scans = array_unique($scans);
sort($scans,SORT_NUMERIC);
foreach ( $scans as $scan )     # Do some re-ordering
  if ( $scan == "UNKNOWN" ) $new_scans[] = "UNKNOWN";
foreach ( $scans as $scan )     # Do some re-ordering
  if ( $scan == "INVALID" ) $new_scans[] = "INVALID";
foreach ( $scans as $scan )     # Do some re-ordering
  if ( $scan != "INVALID" && $scan != "UNKNOWN" ) $new_scans[] = $scan;
foreach ( $new_scans as $scan) {
  echo select_option("scans",$scan);
}
echo "</select>";
echo "<input type=hidden name=root value=\"".$_POST["root"]."\">";
echo "</td></tr>";
?>
</table>
<p>
<input type="submit" value="Submit Search"> 
<?php if ( !empty($_POST["type"]) ) echo "Will search within the current search results. To go back to the top level click Start Over:";?>
</form>
<form method="get"><input type="submit" value="Start Over"></form>

<!-- End submission part -->

<?php

if ( !empty($_POST["type"]) ) {
  # incoming values for selection
  $printed_table_header = False;
  $database = fopen("catalog.txt","r");
  echo "<hr><h3>Selected files</h3>";
  echo "Selections: Location: ".$_POST["location"].", Date: ".$_POST["date"].", Type: ".$_POST["type"].", Freq: ".$_POST["freq"].", Size: ".$_POST["size"].", # Time Scans: ".$_POST["scans"]."<p>";
  $num_results = 0;

  # Count first
  while ( $line = fgets($database,512) ) {
    $fields = explode(",",$line);
    for ($i=0; $i<7;++$i) { 
      $fields[$i] = trim($fields[$i]);
      if ( $fields[$i] == "" ) $fields[$i] = "UNKNOWN"; 
    }

    if ( $fields[6] == "UNKNOWN" ) $n_scans = $fields[6];
    else {
      if ( intval($fields[6])-$fields[6] == 0 ) $n_scans = (string)intval($fields[6]);
      else $n_scans = "INVALID";
    } 

    $select = True;
    if ( substr($fields[0],0,strlen($_POST["root"])) != $_POST["root"] ) $select = False;
    if ( !( empty($_POST["location"]) or $_POST["location"] == "ALL" or get_location($fields[0]) == $_POST["location"]) ) $select = False;
    if ( !( empty($_POST["date"]) or $_POST["date"] == "ALL" or $fields[1] == $_POST["date"]) ) $select = False;
    if ( !( empty($_POST["type"]) or $_POST["type"] == "ALL" or $fields[3] == $_POST["type"]) ) $select = False;
    if ( !( empty($_POST["freq"]) or $_POST["freq"] == "ALL" or $fields[4] == $_POST["freq"]) ) $select = False;
    if ( !( empty($_POST["size"]) or $_POST["size"] == "ALL" or $fields[5] == $_POST["size"]) ) $select = False;
    if ( !( empty($_POST["scans"]) or $_POST["scans"] == "ALL" or $n_scans == $_POST["scans"]) ) $select = False;

    
    if ( $select ) {
      ++$num_results;
    }
  }
  fclose($database);

  if ( $num_results == 0 ) { echo "No Results"; }
  else { echo $num_results; echo " Results<p>"; }

  # Now get them
  $database = fopen("catalog.txt","r");
  while ( $line = fgets($database,512) ) {
    $fields = explode(",",$line);
    for ($i=0; $i<7;++$i) {
      $fields[$i] = trim($fields[$i]);
      if ( $fields[$i] == "" ) $fields[$i] = "UNKNOWN";
    }
    
    if ( $fields[6] == "UNKNOWN" ) $n_scans = $fields[6];
    else {
      if ( intval($fields[6])-$fields[6] == 0 ) $n_scans = (string)intval($fields[6]);
      else $n_scans = "INVALID";
    } 

    $select = True;
    if ( substr($fields[0],0,strlen($_POST["root"])) != $_POST["root"] ) $select = False;
    if ( !( empty($_POST["location"]) or $_POST["location"] == "ALL" or get_location($fields[0]) == $_POST["location"]) ) $select = False;
    if ( !( empty($_POST["date"]) or $_POST["date"] == "ALL" or $fields[1] == $_POST["date"]) ) $select = False;
    if ( !( empty($_POST["type"]) or $_POST["type"] == "ALL" or $fields[3] == $_POST["type"]) ) $select = False;
    if ( !( empty($_POST["freq"]) or $_POST["freq"] == "ALL" or $fields[4] == $_POST["freq"]) ) $select = False;
    if ( !( empty($_POST["size"]) or $_POST["size"] == "ALL" or $fields[5] == $_POST["size"]) ) $select = False;
    if ( !( empty($_POST["scans"]) or $_POST["scans"] == "ALL" or $n_scans == $_POST["scans"]) ) $select = False;

    if ( $select ) {
      if ( !$printed_table_header ) {
        $printed_table_header = True;
        echo "<p><table cellpadding=5><tr><th>File</th><th>Date</th><th>Time</th><th>Type</th><th>Freq</th><th>Size</th><th># Scans</th></tr>";
      }

      echo "<tr><td>".$fields[0]."<td>".$fields[1]."</td><td>".$fields[2]."</td><td>".$fields[3]."</td><td>".$fields[4]."</td><td>".$fields[5]."</td><td align=center>".snumber($fields[6])."</td></tr>";
    }
  }
  if ( $printed_table_header ) {
    echo "</table>";
  }
  fclose($database);
}
?>
</body>
</html>

