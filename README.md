# README #



### What is this repository for? ###

* Utilities I use to do useful things at work
* Version 1.0


### How do I get set up? ###

* There are C/C++ code files, Python scripts, and shell scripts. Each does or will have instructions
on how to use it. Some are specific to a task I need to do.
* Other libraries will be needed for Python scripts and some C programs